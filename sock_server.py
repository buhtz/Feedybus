#!/usr/bin/env python3
import socket

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.bind(('127.0.0.1', 7777))
    sock.listen()
    print('Listen...')

    while True:
        conn, addr = sock.accept()

        with conn:
            print('Connected with {}'.format(addr))
            while True:
                data = conn.recv(1024)
                if not data:
                    break
                print(data.decode('utf-8'))
                print('')
                #conn.sendall(data)

    print('end')
