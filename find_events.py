#!/usr/bin/env python3
"""
    Find and show the structure of events (derived by feedybus.Event) where
    the event object is located, who connects to it and where it is fired.
    :This tiny script check all given py-files for "event_*" objects and calls
    to it.
"""
import sys
import glob
import os.path
import json
from collections import defaultdict

""" Main content indexed by event name. """
events = defaultdict(lambda: defaultdict(list))  # dict(dict(list))

def get_py_files():
    # target directory
    if len(sys.argv) > 1:
        target_dir = sys.argv[1]
    else:
        target_dir = '.'

    print(f'Target directory is {target_dir}')

    # get names of all py files (recursive)
    file_names = glob.glob(os.path.join(target_dir, '**', '*.py'), recursive = True)

    # ignore test_ files
    for f in file_names[:]:
        if 'test_' in f or '/tests/' in f:
            file_names.remove(f)

    return file_names

def _prepare_lines(filename):
    """
    Read all lines from the file and give them back.

    Blank ines, comment lines and trailing spaces are removed.

    Args:
        filename (str): Name of the file to read from.

    Returns:
        (list(str)): List of strings.
    """

    # read lines from file
    with open(filename, 'r', encoding='utf-8') as f:
        lines_in_file = f.read().split('\n')

    lines = []
    prev_line = None
    in_comment = False

    # remove comments and spaces
    for l in lines_in_file:
        l = l.strip()

        if prev_line:
            l = prev_line + l
            prev_line = None

        if in_comment:
            if l.endswith('"""'):
                in_comment = False
            continue

        if l.startswith('"""'):
            in_comment = True
            continue

        if l.startswith('#') \
            or l == '':
            continue  # jump over it

        if l.endswith('\\'):
            prev_line = l[:-2]
            continue

        if l.startswith('def _sub_'):
            continue

        if l.startswith('class '):
            lines.append(l)
        if l.startswith('def '):
            lines.append(l)
        if 'event_' in l:
            lines.append(l)

    return lines

def extract_event_details(line):
    """
    init
    register
    notify
    """
    idxStart = line.find('event_')
    idxFunc = line.find('(', idxStart)
    if idxFunc == -1:
        idxFunc = sys.maxsize
    idxCall = line.find('.', idxStart)
    if idxCall == -1:
        idxCall = sys.maxsize
    idxDef = line.find('=', idxStart)
    if idxDef == -1:
        idxDef = sys.maxsize

    handler = None

    # not my Event()
    # e.g. loop = asyncio.new_event_loop()
    if idxFunc > 0 and idxFunc < idxCall and idxFunc < idxDef:
        #print('EXTRACT_EVENT_DETAILS\n\t{}\n\t'
        #      'idxStart={} idxFunc={} idxDef={} idxCall={}'
        #      .format(line, idxStart, idxFunc, idxDef, idxCall))
        #raise Exception('not an event')
        return line, 'ERRROR', handler


    # INIT: event definition
    # e.g. event_xyz = Event()
    if idxDef < idxCall:
        name = line[idxStart:idxDef].strip()
        action = 'init'
        return name, action, handler

    # REGISTER & NOTIFY
    if idxCall < idxDef:
        action = line[idxCall:idxFunc]
        if action[0] == '.':
            action = action[1:]
        if action[-1] == '.':
            action = action[:-1]
        name = line[idxStart:idxCall].strip()
        # register
        if action == 'register':
            handler = line[idxFunc+1:-1]
        return name, action, handler

    # NOTIFY

    return line, 'ERRROR', handler

def find_events(filename):
    lines = _prepare_lines(filename)
    curr_class = ''
    curr_func = ''
    context = ''
    old_context = context

    for l in lines:
        if l.startswith('def'):
            idx = l.find('(')
            curr_func = l[4:idx]
        elif l.startswith('class'):
            idx = l.find(':')
            curr_class = l[6:idx]
            curr_func = ''
        else:
            if 'event_' not in l:
                continue  # next line
            elif 'if ' in l:
                continue
            elif 'asyncio' in l:
                continue
            elif '.disconnect' in l:
                continue
            elif 'event_id' in l:
                continue
            elif 'DEPRECATED' in l:
                continue

            #if context != old_context:
            #    print(f'>->-> {context}')
            name, action, handler = extract_event_details(l)

            context = f'{context}()'
            if action == 'register':
                context = f'{context} HANDLER: {handler}()'
            events[name][action].append(context)

        old_context = context
        context = '{}.{}.{}'.format(filename, curr_class, curr_func)

def pretty_print_events():
    default_action_keys = ['init', 'register', 'deregister', 'notify']

    # event basic informations
    event_keys = events.keys()
    print(f'Event count: {len(event_keys)}')
    for e_key in event_keys:
        print(f'  {e_key}')

    # each event
    for e_key in events:
        e = events[e_key]
        e_str = f'\n--- {e_key} '
        e_str = e_str + ('-'*(75-len(e_str)))
        print(e_str)

        # correct sorting of the action-keys taking unknown keys into account
        actions = set(e.keys())
        missing_default_actions = set(default_action_keys) - actions
        unknown_actions = actions - set(default_action_keys)
        actions = default_action_keys
        for missing in missing_default_actions:
            actions.remove(missing)
        actions = actions + list(unknown_actions)

        # each action
        for a_key in actions:
            print(f'  {a_key}')
            
            # each context
            for c in e[a_key]:
                print(f'    {c}')
            
if __name__ == '__main__':
    py_files = get_py_files()

    for f in py_files:
        find_events(f)

    #print(json.dumps(events, indent=4))
    pretty_print_events()
