#!/usr/bin/env python3
import socket
import sys

manifesto = ('Regierungen der Industriellen Welt, ihr müden Riesen aus '
             'Fleisch und Stahl, ich komme aus dem Cyberspace, dem neuen '
             'Zuhause des Geistes. Als Vertreter der Zukunft bitte ich euch '
             'aus der Vergangenheit, uns in Ruhe zu lassen. Ihr seid nicht '
             'willkommen unter uns. Ihr habt keine Souveränität, wo wir uns '
             'versammeln.')

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.connect(('127.0.0.1', 7777))
    sock.sendall(sys.argv[1].encode('utf-8'))
    print('Send.')

print('end')
