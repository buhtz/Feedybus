#!/usr/bin/env python3

import glob
import base64
import os.path
import subprocess

# each PNG file
for png_filename in glob.glob('*.png'):
    gif_filename = os.path.splitext(png_filename)[0] + '.gif'

    # PNG in GIF convertieren
    subprocess.run(['convert',
                   png_filename,
                   '-channel',
                   'A',
                   '-threshold',
                   '15%',
                   '-resize',
                   '32x32',
                   gif_filename])


values = {} 
# each GIF file
for gif_filename in glob.glob('*.gif'):
    print('Computing {}...'.format(gif_filename))

    # read and delete GIF
    with open(gif_filename, 'br') as gif_file:
        content = gif_file.read()
    #os.remove(gif_filename)

    # re-code
    content = base64.b64encode(content)

    # remembe result
    values[os.path.splitext(gif_filename)[0]] = content


# save results to file
with open('icons.py', 'w') as py_file:
    py_file.write('# Auto-generated\n')
    for key in values:
        py_file.write("\n{} = {}".format(key, values[key]))

