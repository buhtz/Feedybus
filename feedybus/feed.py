# -*- coding: utf-8 -*-
import logging
import requests.models
import requests.exceptions
from xml.etree import ElementTree
import feedybus.basics as basics
import feedybus.group as group
import feedybus.data as data

"""
    short

    long
"""


_log = logging.getLogger(basics.GetLogId(__file__))


class Feed:
    """A feed in the application layer.

    The class can be used for existing feeds or a new feed can be created. See
    self.__init__() for details.

    Example:
        existing_feed = Feed(7)
    """

    ## Event (as class attribute): Feed created or modified.
    event_feed_new_or_modified = basics.Event()

    ## Event (as class attribute): Feed deleted.
    event_feed_deleted = basics.Event()

    ## Event (as class attribute): Feeds entry "read later" state modified.
    event_entry_read_later_modified = basics.Event()

    ## Event (as class attribute): Feed entry's "read" state was modified
    event_entry_read_modified = basics.Event()

    ## Event (as class attribute). All entry's of the feed marked read.
    event_feed_marked_read = basics.Event()

    @staticmethod
    def as_ids(feeds):
        """Convert a list of feeds to a list of their feeds_ids.

        Args:
            feeds (list(Feed)): List of feed objects.

        Returns:
            list: List of integer values, the feed ids.
        """
        ids = []
        for f in feeds:
            ids.append(f.fid)
        return ids

    @staticmethod
    def delete(feeds):
        """Delete multiple feeds.

        Each feed is deleted the data structure, including its entries and
        related files. Also considered is the parent group and the smart
        groups 'Unread' and 'Read Later'. At the end 'Feed.event_feed_deleted'
        is fired.

        Args:
            feeds(list(Feed)): One Feed object or a list of them.
        """
        if not type(feeds) is list:
            feeds = [feeds]

        # Looking for parents is ressource intensiv. We can assume (but
        # not beiing sure!) that this list of feeds all have the same
        # parent.
        # Asking the parent group for its children is easier and less
        # ressource intensive then asking the child feed for its parent.
        parent_child_ids = feeds[0].group.feed_ids

        for feedobj in feeds:
            if feedobj.fid not in parent_child_ids:
                parent_child_ids = feedobj.group.feed_ids

            # remove feed from its parent group
            feedobj.group.remove(feedobj)

            # delete references for unread and later read
            try:
                del group.Group.smart_group_index_unread[feedobj.fid]
            except KeyError:
                pass
            try:
                del group.Group.smart_group_index_later[feedobj.fid]
            except KeyError:
                pass

            # delete related files
            data.FeedsData.me.delete_entry_and_icon_files(feedobj.fid)

            # final removeing
            del data.FeedsData.me._feeds[feedobj.fid]
            del data.FeedsData.me._entries[feedobj.fid]

        # notify
        Feed.event_feed_deleted.notify(Feed.as_ids(feeds))

    def __init__(self, *args, **kwargs):
        """Construct a Feed object for an existing feed or for a new to be
        created feed.

        Mode A: Use for existing group:
        Use feed-id as one and only positional argument and without any
        keyword arguments. The instance then is connected to its
        representation in the data layer.
        A ValueError is raised if no feed with the givin id exists.
        The feed-id is allowed as single only - TypeError is raised if not.

        Mode B: Create a new feed:
        A new feed is created in the data layer. Use '**kwargs' for all feed
        fields. Minimum the fields 'title' and 'xmlUrl' should be given.
        Additational allowed fields: 'label', 'htmlUrl', 'version', 'author',
        'rights', 'description', 'encoding', 'group'.
        If 'group' as the feeds parent is not specified 'Group.root()' is used
        by default.
        If you have a 'feedparser.FeedParserDict' please see
        'Feed.convert_FeedParserDict()' as converter method.

        Args:
            args: See method description for allowed parameters.
            kwargs: See method description for allowed parameters.

        Raises:
            ValueError: If a feed-id does not exists or is out of the valid \
            range.
            TypeError: If parameters have wrong type, wrong number or name the
            wrong way.
        """
        fid = None

        # check possible errors because of wrong used arguments
        if len(args) == 0:
            if len(kwargs) == 0:
                raise TypeError('Instanceiation without arguments '
                                'not allowed.')
            elif len(kwargs) < 2:
                raise TypeError('To create a feed minimum "title" '
                                'and "xmlUrl" as keyword arguments need '
                                'to be givin.')
            elif len(kwargs) > 1:
                if not set(kwargs.keys()).issuperset({'title', 'xmlUrl'}):
                    raise TypeError('To create a feed "title" and "xmlUrl" '
                                    'need to be givin in the list of '
                                    'keyword arguments. kwargs={}'
                                    .format(kwargs))
        elif len(args) == 1:
            if len(kwargs) > 0:
                raise TypeError('First positional argument (the feed id) is '
                                'allowed as single-only. Keyword arguments '
                                'not allowed in that case.')
            elif type(args[0]) != int:
                raise TypeError('The feed id (as first positional argument) '
                                'should be of integer type.')
            else:
                # SUCCESS -> MODE A - Existing group
                fid = args[0]
        elif len(args) > 1:
            raise TypeError('To much positional (unnamed) arguments. Allowed '
                            'is only one: The feed id as an integer. args={} '
                            'kwargs={}'.format(args, kwargs))

        # MODE A - Existing feed
        if fid is not None:
            if not Feed.is_fid_valid(fid):
                raise ValueError(f'The feed id {fid} is not in the '
                                 'allowed range.')
            # xyy
            self._fid = fid
            # check if the feed exists
            if not data.FeedsData.me.feed_exists(self.fid):
                raise ValueError(f'A feed with id {fid} do not exist '
                                 'in the data.')
        # MODE B - Create a feed
        else:
            # get free id for a new Feed
            self._fid = Feed.next_free_id()

            # Get id of parent group (default: root)
            parent_group_id = kwargs.get('group', group.Group.root()).gid
            kwargs['group'] = parent_group_id

            # create feed in data layer
            fid = data.FeedsData.me.create_feed_v2(self.fid, kwargs)

            # notify (e.g. TreeView in GUI/presentation layer) about new item
            Feed.event_feed_new_or_modified.notify(self.fid)

    def __str__(self):
        """Printed representation.
        """
        return ('Feed #{} "{}"'
                .format(self.fid, self.label))

    def __eq__(self, other):
        """Equal operator check the fid."""
        return other.fid == self.fid

# ----------
# --- Misc
# ----------
    @classmethod
    def is_fid_valid(cls, fid):
        """Check if the givin identifier is valid for feeds.

        Technically it checks if the fid is in the allowed range.
        It does not check if the feed exists.

        Args:
            fid(int): Feed identifier

        Returns:
            (bool): Valid or not.
        """
        try:
            if fid > basics.GROUPID_FEEDS:
                return True
        except TypeError:
            # item_id is None
            pass

        return False

    @classmethod
    def next_free_id(cls):
        """Compute the next free id for a new feed.

        Returns:
            (int): An unused and free feed id.
        """
        feed_ids = sorted(data.FeedsData.me.get_feed_ids())

        # are there empty ids between existing feeds?
        try:
            if len(feed_ids) < feed_ids[-1]:
                new_id = basics.GROUPID_FEEDS + 1
                # find the free id between the existing ones
                while new_id in feed_ids:
                    new_id = new_id + 1
            else:
                new_id = feed_ids[-1] + 1
        except IndexError:
            return basics.GROUPID_FEEDS + 1

        return new_id

    @classmethod
    def convert_FeedParserDict(cls, fpdict):
        """Take a raw feed data as FeedParserDict and convert it to a
        dictionary that can be handled (e.g. in the ctor)."""

        # try to extract 'href' for 'htmlUrl'
        htmlUrl = None
        try:
            htmlUrl = fpdict['feed']['href']
        except KeyError:
            pass

        try:
            for link in fpdict['feed']['links']:
                if link['rel'] == 'alternate':
                    htmlUrl = link['href']
        except KeyError:
            pass

        # pack the result
        result = {
            'title': fpdict['feed'].get('title', _('<unnamed>')),
            'label': fpdict['feed'].get('text',
                                        fpdict['feed'].get('title',
                                                           _('<unnamed>'))),
            'htmlUrl': htmlUrl,
            'xmlUrl': fpdict['href'],
            'version': fpdict.get('version', 'unknown'),
            'author': fpdict['feed'].get('author', None),
            'rights': fpdict['feed'].get('rights', None),
            'description': fpdict['feed'].get('description', None),
            'encoding': fpdict.get('encoding', None)
        }

        return result

    @classmethod
    def is_url_valid(self, url):
        """Check if it is valid url.

        Returns:
            (bool): Result.
        """
        if url is None:
            return False

        # I observed situations where blocks of python-code (in the clipboard)
        # where accepted by request-test below.
        # But sometimes you mark the url for copy into the clipboard (e.g. on
        # a website) including the trailing newline. That is why one newline
        # is allowed here.
        if url.count('\n') > 1:
            return False

        try:
            requests.models.PreparedRequest().prepare_url(url, None)
        except (requests.exceptions.MissingSchema,
                requests.exceptions.InvalidURL):
            return False

        return True

    @staticmethod
    def count():
        """Count all feeds.

        Returns:
            (int): Number of feeds.
        """
        return len(data.FeedsData.me._feeds)

    def add_as_opml_element(self, parent_element):
        """
        """
        attrib = {'type': 'rss',
                  'text': self.label,
                  'title': self.title,
                  'xmlUrl': self.xmlUrl,
                  'htmlUrl': self.htmlUrl,
                  'description': self.description,
                  'author': self.author,
                  'rights': self.rights,
                  'encoding': self.encoding}
        # remove empty/None fields
        for key in attrib.copy():
            if not attrib[key]:
                del attrib[key]

        e = ElementTree.SubElement(parent_element, 'outline', attrib)

# ----------------
# -- Properties --
# ----------------

    @property
    def fid(self):
        """Identifier of the feed.

        Returns:
            (int): The identifier.
        """
        return self._fid

    @property
    def title(self):
        """Title of the feed.

        The title is set by the feeds author and can not be modified. Use
        'label' if you need to modify the feeds representtaion string in the
        GUI.

        Returns:
            (str): The title.
        """
        return data.FeedsData.me._feeds[self.fid]['title']

    @property
    def label(self):
        """Label of the feed.

        In OOPML specification it is the 'text' attribute.

        Returns:
            (str): The label of the feed.
        """
        try:
            return data.FeedsData.me._feeds[self.fid]['text']
        except KeyError:
            return self.title

    @label.setter
    def label(self, value):
        """Set label of the feed.

        Events:
            event_tree_item_new_or_modified

        Args:
            value (str): New label.
        """
        # remove leading/trailing whitespace chars
        value = value.strip()
        # set the value
        data.FeedsData.me._feeds[self.fid]['text'] = value
        # notify about modification
        Feed.event_feed_new_or_modified.notify(self.fid)

    @property
    def xmlUrl(self):
        """Get 'xmlUrl'.

        The URL to the remote feed file (RSS/Atom/etc).

        Returns:
            (str): The URL.
        """
        return data.FeedsData.me._feeds[self.fid]['xmlUrl']

    @xmlUrl.setter
    def xmlUrl(self, value):
        """Set 'xmlUrl', the URL to the remote feed file (RSS/Atom/etc).

        Args:
            value (str): URL
        """
        # set the value
        data.FeedsData.me._feeds[self.fid]['xmlUrl'] = value
        # notify about modification
        # QUESTION: WHY? xmlUrl is not shown in GUI. I would expect this event
        # notification is not necessary
        # data.FeedsData.me.event_tree_item_new_or_modified.notify(self.fid)

    @property
    def htmlUrl(self):
        """Get 'htmlUrl', the URL of the Website related to the feed.

        This is not the URL of the feed itself. Look at 'xmlUrl' for this.

        Returns:
            (str): URL of a website
        """
        try:
            return data.FeedsData.me._feeds[self.fid]['htmlUrl']
        except KeyError:
            return None

    @property
    def version(self):
        """Get 'version' of the feed.

        Returns:
            (str): Version of the feed (e.g. Atom, RSS1.0).
        """
        try:
            return data.FeedsData.me._feeds[self.fid]['version']
        except KeyError:
            return None

    @property
    def author(self):
        """Get the author information of the feed.

        In most cases this are names and email adresses.

        Returrns:
            (str): One or more informations about authors.
        """
        try:
            return data.FeedsData.me._feeds[self.fid]['author']
        except KeyError:
            return None

    @property
    def rights(self):
        """Get the information about rights (e.g. Copyright) and licence
        informations.

        Returrns:
            (str): Licence.
        """
        try:
            return data.FeedsData.me._feeds[self.fid]['rights']
        except KeyError:
            return None

    @property
    def description(self):
        """Get the description of the feed.

        Returrns:
            (str): Description.
        """
        try:
            return data.FeedsData.me._feeds[self.fid]['description']
        except KeyError:
            return None

    @property
    def encoding(self):
        """Get the encoding of the feed.

        Returrns:
            (str): Encoding.
        """
        try:
            return data.FeedsData.me._feeds[self.fid]['encoding']
        except KeyError:
            return None

    @property
    def group(self):
        """Parent group.

        Returns:
            (feedybus.group.Group): The parent group of that feed.
        """
        path = data.FeedsData.me.get_full_path(self.fid)
        parent_group_id = path[-2]
        return group.Group(parent_group_id)

    def set_update_mechanism(self, etag=None, modified_date=None,
                             entry_date=None, entry_links=None):
        """Set the update mechanism and its value.
        """
        # clear update mechanisms
        for key in ['etag', 'modified', 'max_entry_date']:
            try:
                del data.FeedsData.me._feeds[self.fid][key]
            except KeyError:
                pass

        if etag:
            data.FeedsData.me._feeds[self.fid]['update_mechanism'] = \
                data.UpdateMecha.etag
            data.FeedsData.me._feeds[self.fid]['etag'] = etag
        elif modified_date:
            # debug
            _log.info('Feed {} : set_update_mecha() modified_date={}'
                      .format(self, modified_date))
            data.FeedsData.me._feeds[self.fid]['update_mechanism'] = \
                data.UpdateMecha.modified
            data.FeedsData.me._feeds[self.fid]['modified'] = modified_date
        elif entry_date:
            data.FeedsData.me._feeds[self.fid]['update_mechanism'] = \
                data.UpdateMecha.entry_date
            data.FeedsData.me._feeds[self.fid]['max_entry_date'] = entry_date
        elif entry_links:
            data.FeedsData.me._feeds[self.fid]['update_mechanism'] = \
                data.UpdateMecha.entry_links

    @property
    def update_mechanism(self):
        """Return the update mechanism.
        """
        try:
            return data.FeedsData.me._feeds[self.fid]['update_mechanism']
        except KeyError:
            return None

    @property
    def update_mechanism_value(self):
        """Return the value related to the current update mechanism.
        """
        mecha = self.update_mechanism

        if mecha == data.UpdateMecha.etag:
            return data.FeedsData.me._feeds[self.fid]['etag']
        elif mecha == data.UpdateMecha.modified:
            return data.FeedsData.me._feeds[self.fid]['modified']
        elif mecha == data.UpdateMecha.entry_date:
            return data.FeedsData.me._feeds[self.fid]['max_entry_date']
        elif mecha == data.UpdateMecha.entry_links:
            return self._get_all_entry_links()

        return None

# ----------
# --- Entry handling
# ----------
    @property
    def entries(self):
        return data.FeedsData.me._entries[self.fid]

    def _get_all_entry_links(self):
        """A list of the URLs used by each entry.

        This is used by 'self.updated_mechanism_value()'.
        """
        return [e['link'] for e in data.FeedsData.me._entries[self.fid]]

    def add_new_entries(self, entries):
        """Add new entries to this feed.

        Args:
            entries (list()): Entrie list from a feedparser dict.
        """
        # add new/unread entries to the data structure
        for entry in entries:
            # unread by default
            entry['feedybus'] = {'read': False}

        # start-index of new entries
        idx_new_begin = len(data.FeedsData.me._entries[self.fid])
        # add new entry
        data.FeedsData.me._entries[self.fid].extend(entries)
        # end-index of new entries
        idx_new_end = len(data.FeedsData.me._entries[self.fid])

        # remember for unread
        idx_new_list = list(range(idx_new_begin, idx_new_end))
        try:
            group.Group.smart_group_index_unread[self.fid].extend(idx_new_list)
        except KeyError:
            group.Group.smart_group_index_unread[self.fid] = idx_new_list

    def get_entry_title(self, idx):
        """Return the title of an entry.

        Args:
            idx (int): Index of an existing entry.

        Returns:
            (str): The title.

        Raises:
            IndexError: If 'idx' not exist.
        """
        return self.entries[idx].get('title', '<{}>'.format(_('no title')))

    def count_total_and_unread_entries(self):
        """Number of all entries in that feed and number of unread entries.

        Returns:
            (int): total
            (int): unread
        """
        return (len(self.entries), len(self.get_unread_entries()))

    def get_entry_read(self, entry_idx):
        """Return the read state of a specific entry.

        Args:
            entry_idx(int): The index of the entry.

        Returns:
            bool: The read state.

        Raises:
            ValueError: When the entry does not exist.
        """
        try:
            return self.entries[entry_idx]['feedybus']['read']
        except KeyError:
            # unread by default
            return False
        except IndexError:
            raise ValueError('The entry does not exist.')

    def get_unread_entries(self):
        """Give a list of indexes of all entries marked as unread.
        The list is empty if all entries read.

        This also updates 'Group.smart_group_index_unread'.

        Returns:
            list: List of entry indicies.
        """
        result = []
        for idx in range(len(self.entries)):
            if not self.get_entry_read(idx):
                result.append(idx)

        # Remember this unread entries
        if len(result) > 0:
            group.Group.smart_group_index_unread[self.fid] = result
        elif self.fid in group.Group.smart_group_index_unread:
            del group.Group.smart_group_index_unread[self.fid]

        return result

    def set_entry_read(self, idx, read=True):
        """Mark an entry as read (or unread).

        Args:
            idx (int): Index of an existing entry.
            read (bool): Read mark 'True' (default) or 'False'.

        Raises:
            KeyError
            ValueError
        """
        # If the read state is the same do nothing
        if self.get_entry_read(idx) == read:
            return

        self.entries[idx]['feedybus']['read'] = read

        # smart group "Unread"
        if read:
            try:
                group.Group.smart_group_index_unread[self.fid].remove(idx)
            except ValueError:
                pass
            if len(group.Group.smart_group_index_unread[self.fid]) == 0:
                del group.Group.smart_group_index_unread[self.fid]
        else:
            if self.fid not in group.Group.smart_group_index_unread:
                group.Group.smart_group_index_unread[self.fid] = []
            group.Group.smart_group_index_unread[self.fid].append(idx)

        # notify
        Feed.event_entry_read_modified.notify(self.fid, idx)

    def set_read(self):
        """Set the whole feed (all entries) to read.
        """
        # do not fire an event for each entry
        with self.event_entry_read_modified.keep_silent():
            for idx in range(len(self.entries)):
                self.set_entry_read(idx)

        # notify
        Feed.event_feed_marked_read.notify(self.fid)

    def get_read_later_entries(self):
        """Give a list of indexes of all entries marked as read later.
        The list is empty if no entries are marked as read later.

        This also updates 'Group.smart_group_index_later'.

        Returns:
            list: List of entry indicies.
        """
        result = []
        for idx in range(len(self.entries)):
            try:
                if self.entries[idx]['feedybus']['read_later'] is True:
                    result.append(idx)
            except KeyError:
                pass

        # Remember this marked later entries
        if len(result) > 0:
            group.Group.smart_group_index_later[self.fid] = result
        elif self.fid in group.Group.smart_group_index_later:
            del group.Group.smart_group_index_later[self.fid]

        return result

    def set_entry_read_later(self, idx, later=True):
        """Mark an entry as read later (or not).

        Args:
            idx (int): Index of an existing entry.
            later (bool): Read later mark 'True (default) or 'False'.

        Raises:
            IndexError
        """
        try:
            self.entries[idx]['feedybus']['read_later'] = later
        except KeyError:
            self.entries[idx]['feedybus'] = {}
            self.entries[idx]['feedybus']['read_later'] = later

        # take care of global read_later reference list
        if later:
            if self.fid not in group.Group.smart_group_index_later:
                group.Group.smart_group_index_later[self.fid] = []
            group.Group.smart_group_index_later[self.fid].append(idx)
        else:
            group.Group.smart_group_index_later[self.fid].remove(idx)

        Feed.event_entry_read_later_modified.notify(self.fid, idx)

    def get_entry_date_str(self, idx):
        """Return the date of an entry as a string.

        When ['feedybus']['date_str'] is present it's values is returned. If
        not then the date is extracted from the entry data and stored for
        later access.

        Args:
            idx (int): Index of an existing entry.

        Returns:
            (str): The date as a string.

        Raises:
            IndexError: If 'idx' not exist.
        """
        # use a previouse computed result if it exist
        try:
            return self.entries[idx]['feedybus']['date_str']
        except KeyError:
            pass

        # Try to get the date from 'published_parsed' or 'updated_parsed' if
        # they are not empty.
        date_str = None
        for date_key in ['published_parsed',
                         'updated_parsed']:
            if date_key in self.entries[idx] and self.entries[idx][date_key]:
                date_str = '{}-{:02d}-{:02d}' \
                    .format(self.entries[idx][date_key][0],
                            self.entries[idx][date_key][1],
                            self.entries[idx][date_key][2])
                break

        # Try to get the date from 'published' if it is not empty.
        # Use '<n.a.>' as default
        if not date_str:
            # see https://github.com/kurtmckee/feedparser/issues/135
            na = '<n.a.>'
            date_str = self.entries[idx].get('published', na)
            # sometimes the field 'published' exists but is empty
            if not date_str:
                date_str = na

        # create 'feedybus' dict
        if 'feedybus' not in self.entries[idx]:
            self.entries[idx]['feedybus'] = {}

        # store the date string for later use
        self.entries[idx]['feedybus']['date_str'] = date_str

        # return the string
        return date_str
