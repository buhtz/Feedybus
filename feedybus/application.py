# -*- coding: utf-8 -*-
import logging
import platform
import webbrowser
import feedparser
import aiohttp
import configobj
import cairo
import gettext
import os
import queue
import gi
import email
from gi.repository import GLib  # just for a timer
from xml.etree import ElementTree
from xml.dom import minidom
from datetime import date
from feedybus import basics
from feedybus.data import FeedsData, UpdateMecha
from feedybus.fetchfeeds import FetchAsyncMultiprocessThread
from feedybus.feed import Feed
from feedybus.group import Group


"""
    Application layer of Feedybus.

    Application class of Feedybus representing the application layer. Also the
    class Feed and Group are part of that layer.
"""


_log = logging.getLogger(basics.GetLogId(__file__))


class FeedybusApplication ():
    """Main application class.
    """
    # The instance itself.
    me = None

    def __init__(self, config):
        """Acpplication ctor
        Args:
            config (configobj):
        """
        # Check for singleton
        if FeedybusApplication.me is not None:
            raise Exception('Only one instance of FeedybusApplication '
                            'can exist.')

        FeedybusApplication.me = self

        ## The configuration object.
        self.config = config

        # language setup
        self._config_language()

        ## The main data object holding feeds etc
        self._data = FeedsData()

        # is debug on?
        if _log.isEnabledFor(logging.DEBUG):
            self.debug = True
        else:
            self.debug = False
        self._data.debug = self.debug

        ## thread-safe data storing
        self._queue = queue.Queue()

        ## Event: Import from OPML begins.
        self.event_import_start = basics.Event()
        ## Event: Import from OPML finished.
        self.event_import_end = basics.Event()

        ## Event: Feed refreshed
        self.event_feed_refreshed = basics.Event()
        ## Event: Begin of refreshing all feeds
        self.event_started_refresh_all_feeds = basics.Event()
        ## Event: End of refreshing all feeds
        self.event_finished_refresh_all_feeds = basics.Event()

    def on_close_application(self):
        """Event handle for closing the application.

        Place to do cleaning and storing task while the application is closed.
        The methode is called by the graphic layer.
        """
        self._data.save_feedsdata()
        self.config.write_all_config()

# ---------
# -- Feed Update Machine
# ---------

    def update_feeds(self, feed_ids=None):
        """Main function to start _refresh_data_from_thread(). See there for
        more details.

        Args:
            feed_ids (list(int)): List of feed_ids to update. If 'None'
            (default) all feeds are updated.
            debug (bool): Debug mode on or off (default: True (default: True).
        """
        # count feeds
        try:
            feed_count = len(feed_ids)
        except TypeError:
            feed_count = Feed.count()
        _log.info('Fetching {} feeds...'.format(feed_count))

        # notify observers
        self.event_started_refresh_all_feeds.notify(feed_ids=feed_ids)

        # do fetching in separate thread
        self._thread \
            = FetchAsyncMultiprocessThread(feeds_data=FeedsData.me,
                                           feed_ids=feed_ids,
                                           result_queue=self._queue,
                                           debug=self.debug)
        self._thread.start()

        # wait for new data comming from the fetch-thread
        GLib.timeout_add(500, self._refresh_feeds_from_update_thread)

        return self._thread

    def _refresh_feeds_from_update_thread(self):
        """Refreshing the items with data coming in from another
            thread.

        Returns:
            bool:
        """
        # Stop here if the FeedsFetchThread is not running and there is no
        # more data inthe queue.
        if not self._thread.is_alive() and self._queue.empty():
            # notify observers
            self.event_finished_refresh_all_feeds.notify()
            _log.info('=== FINISHED refreshing all feeds. ===')
            return False  # end the timer

        # while there is new data available
        while not self._queue.empty():
            # get data from the Queue (stored by another thread)
            feed_id, feed_data = self._queue.get().popitem()

            # debug
            # print('Feed {}\n{}'
            #       .format(feed_id, json.dumps(feed_data, indent=4)))

            # the feed to update
            feed = Feed(feed_id)

            # DEBUG (???) raw downloaded data
            # try:
            #     self._feeds[feed_id]['raw_data'] = feed_data['raw_data']
            # except KeyError:
            #     pass

            # new url? (HTTP code 3**; some of them)
            old_url = feed.xmlUrl
            if old_url != feed_data['href']:
                # remember old url (e.g. for error messages or user-reset)
                # feed_data['old_url'] = old_url
                # set new url
                feed.xmlUrl = feed_data['href']
                # log
                _log.info('Because of HTTP error {} the URL '
                          'of {} was modified from {} to {}.'
                          .format(feed_data.response['status'], feed,
                                  old_url, feed.xmlUrl))

            if len(feed_data.entries) > 0:
                # store update mechanism
                key = 'update_mechanism'
                try:
                    if feed_data[key] is UpdateMecha.etag:
                        # etag
                        feed.set_update_mechanism(etag=feed_data['etag'])
                    elif feed_data[key] is UpdateMecha.modified:
                        # modified-date (alias for 'updated')
                        mod = feed_data['modified']
                        feed.set_update_mechanism(modified_date=mod)
                    elif feed_data[key] is UpdateMecha.entry_dates:
                        # entry dates
                        med = feed_data['max_entry_date']
                        feed.set_update_mechanism(entry_date=med)
                except Exception:
                    raise Exception('No update mechanism specified in '
                                    'received data for {}.'
                                    .format(feed))

                # add new entries
                feed.add_new_entries(feed_data.entries)

            # notify observers
            self.event_feed_refreshed.notify(feed_id=feed.fid,
                                             parsed_feed=feed_data)

        # refresh the timer
        # call this methode again
        return True  # timer repeated

# ---------
# -- Import & Export
# ---------
    def import_feeds(self, opml_filename):
        """Import Feeds and Groups from an OPML file.

        Args:
            opml_filename (str): Full name of an existing OPML file.
        """
        _log.info('Import feeds from file: {}'.format(opml_filename))
        self.event_import_start.notify()

        # read file content
        with open(opml_filename, 'r') as f:
            opml_content = f.read()

        # create import node in the data tree
        import_group = Group(_('Imported'))

        # import
        self._import_from_opml(opml_content, import_group)

        # notify
        self.event_import_end.notify(import_group.gid)

    def _import_from_opml(self, opml_content, parent_group):
        """Convert content of a OPML-file to Feedybus internal data structure
        and add it to the tree structure.

        The OPML file need to be read into memory and given as a string.
        Unknown fields or attributes in the OPML content is informed in debug
        messages.

        Args:
            opml_content (string): OPML-file content.
            parent_group (Group): The parent group.
        """
        # parse the OPML content
        tree = ElementTree.fromstring(opml_content)
        # the relevant part of the OPML (without meta data)
        body = tree.find('body')

        def _handle_outline(outline, current_parent_group):
            """Decide if it is a feed or a group and is recursive
            callable.

            Args:
                outline:

            Returns:
                dict()
            """
            # All fields that would be used for a Feed.
            # The keys are the attribute names in OPML. The values are the
            # attribute names in the class 'Feed'.
            # Keep this list consistent with 'Feed.__init__()'.
            opml_fields = {
                'title': 'title',
                'xmlUrl': 'xmlUrl',
                'htmlUrl': 'htmlUrl',
                'text': 'label',
                'version': 'version',
                'author': 'author',
                'rights': 'rights',
                'description': 'description',
                'encoding': 'encoding'
            }
            # Title
            if all(k not in outline.keys() for k in ['title', 'text']):
                _log.warning(_('Dropped OPML element because '
                               'of missing attribute "text" and '
                               'its alternative "title". {}'
                               .format(sorted(outline.attrib))))
                # drop that outline
                return

            # get sub-elements (children) of the current outline element
            sub_outlines = outline.findall('outline')

            # is it a FEED?
            if 'xmlUrl' in outline.keys():
                # Dict used by the Feed class to create a new feed.
                feed_data = {}

                # all possible attributes
                for k in opml_fields:
                    try:
                        # 'attrib' is the only way to provoke KeyError's
                        v = outline.attrib[k]

                        # ignore empty attributes
                        if not v:
                            raise KeyError

                        # convert OPML name/value to Feedybus structure
                        feed_data[opml_fields[k]] = v
                    except KeyError:
                        pass

                # In OPML "text" attribute is mandatory. In Feedybus
                # terminology it is the "label" of a Feed which is shown in
                # the GUI.
                # But for Feedybus the original "title" is mandatory and
                # different from the "label".
                # If the OPML does not contain a "title" the "text" attribute
                # (the "label" field in Feedybus terminology)
                # is used as "title".
                if 'title' not in feed_data:
                    feed_data['title'] = feed_data['label']

                # parent group of the feed
                feed_data['group'] = current_parent_group

                # report unknown attributes in the OPML element
                opml_fields = set(opml_fields).union({'type'})
                unknown_keys = list(set(outline.keys()) - opml_fields)
                unknown_keys = sorted(unknown_keys)
                if unknown_keys:
                    _log.debug('Unknown keys found in OPML outline: {}. '
                               'Feed: {} {}'
                               .format(unknown_keys,
                                       feed_data['title'],
                                       feed_data['xmlUrl']))

                # Creating the Feed object will add its data to the Feedybus
                # data structure.
                feed = Feed(**feed_data)
            else:
                # Use "title" or "text" as "Group label.
                # See in the previous block (where the Feed is handled) for
                # more detailes about it.
                try:
                    text = outline.attrib['text']
                except KeyError:
                    text = outline.attrib['title']

                # create a group key
                group = Group(text, current_parent_group)

                # report unknown attributes in the OPML element
                known_or_ignored = {'text', 'title', 'type'}
                unknown_keys = list(set(outline.keys()) - known_or_ignored)
                unknown_keys = sorted(unknown_keys)
                if unknown_keys:
                    _log.debug('Unknown keys found in OPML outline: {}. '
                               'Group: {}'
                               .format(unknown_keys,
                                       group))

                # go recursive
                for sub_outline in outline.findall('outline'):
                    _handle_outline(sub_outline,
                                    group)

        # start: each outline element on 1rst level of the body
        for outline in body.findall('outline'):
            _handle_outline(outline, parent_group)

    def export_feeds(self, opml_filename):
        """Export all Feeds and Groups to a OPML file.

        Args:
            opml_filename (str): Full name of a file.
        """
        # open file
        with open(opml_filename, 'w') as f:
            f.write(self._export_as_opml())

        msg = f'All feeds exported into {opml_filename}.'
        _log.info(msg)

        return msg

    def _export_as_opml(self):
        """All Feeds and Groups are converted to their OPML representation.

        Returns:
            (str): The OPML content as a string.
        """
        # opml intro
        opml = ElementTree.Element('opml')
        opml.set('version', '2.0')

        # head
        head = ElementTree.SubElement(opml, 'head')

        dateCreated = ElementTree.SubElement(head, 'dateCreated')
        dateCreated.text = email.utils.formatdate()  # RFC 2822 conform

        ownerName = ElementTree.SubElement(head, 'ownerName')
        ownerName.text = self.get_full_name()

        ownerId = ElementTree.SubElement(head, 'ownerId')
        ownerId.text = self.get_website()

        ownerEmail = ElementTree.SubElement(head, 'ownerEmail')
        ownerEmail.text = self.get_email()

        docs = ElementTree.SubElement(head, 'docs')
        docs.text = 'http://dev.opml.org/spec2.html'

        # body
        # The main group 'Feeds' us used...
        body = Group.root().add_as_opml_element(opml)
        # ...but we do not want to export it.
        # So we modfy the main outline ('Feeds') into the 'body' element.
        body.attrib.pop('text')
        body.tag = 'body'

        # create OPML string
        opml_str = ElementTree.tostring(opml, encoding='utf-8')
        # with easy to read indention with correct xml-tag
        opml_str = minidom.parseString(opml_str) \
            .toprettyxml(indent=' ' * 4, encoding='UTF-8') \
            .decode('utf-8')

        return opml_str

# ---------
# -- Misc
# ---------
    def _config_language(self):
        """Configure the applicatons language.

        With decreasing priority this influences the used language.
         - The applications configuration (file) itself.
         - The operating systems default lanugage.
         - The code intern untranslated strings (english) are used as last
           instance.
        """
        lang = self.config.language
        if lang:
            trans = gettext.translation('feedybus', 'locales',
                                        languages=[lang], fallback=True)
            trans.install()
            print(trans.info())
            fn = gettext.find('feedybus', 'locales', [lang])
        else:
            fn = gettext.find('feedybus', 'locales')

        if fn:
            _log.debug('Language file is {}'
                       .format(os.path.join(os.getcwd(), fn)))
        else:
            _log.debug('No language file used.')

    def open_in_browser(self, entries_or_url):
        """Open the URLs of the given 'entries' with the systems own browser.

        Args:
            entries_or_url (list(dict()) or dict() or str): List of feed
            entries or an URL as string.
        """
        def _browser_open(url):
            webbrowser.open(url,
                            new=0,
                            autoraise=False)  # not working on XFCE

        # an URL
        if type(entries_or_url) is str:
            _browser_open(entries_or_url)
            return

        # make a list if it is not
        if type(entries_or_url) != list:
            entries_or_url = [entries_or_url]

        # each entry
        for e in entries_or_url:
            _browser_open(e['link'])

    def do_handle_feed_update_mechanism_test(self):
        """Initiate a feed-update-meachanism-test if the past one was done
        last year.

        It is done indirect via removing the 'update-mechanism' entry from
        each feed. Because of that a new test will be done for each feed
        before the next update of a feed.
        """
        cur_year = date.today().year
        test_year = self.config.last_feed_update_test_year

        # initiate a new test if the past test was last year
        if cur_year > test_year:
            self._data.remove_feed_update_keys()

            # remember the current year as the year this test was done in
            _log.info('Remove update mechanism informations from all feeds.')
            self.config.set_last_feed_update_test()

# ---------
# -- Version & App informations
# ---------

    def get_website(self):
        """Return the applications own website URL.

        Returns:
            str: An URL.
        """
        return basics._APP_WEBSITE

    def get_email(self):
        """Return the applications (maintainer) own email address.

        Returns:
            str: An email address as string.
        """
        return basics._APP_EMAIL

    def print_full_version_informations(self):
        """
        """
        _log.debug('\n\t'.join(self.get_all_versions()))

    def get_full_name(self):
        """Complete name of the application including version.

        The complete and full name and version information of that
        application.

        Returns:
            str:
        """
        return basics._APPNAME_SHOW + ' ' + self.get_version()

    def get_version(self):
        """Return version of the application as a string.

        Returns:
            str:
        """
        rs = basics._APPVER_STR
        git = basics.git_revision()

        if len(git) > 0:
            rs = '{} git: {} (branch: {})' \
                 .format(rs, git, basics.git_branch())

        return rs

    def _get_feedparser_ver(self):
        """Return version of Feedparser.

        Returns:
            str:
        """
        return feedparser.__version__

    def _get_python_ver(self):
        """Return version and implementation of running Python interpreter.

        Returns:
            str:
        """
        return '{} {}'.format(platform.python_version(),
                              platform.python_implementation())

    def _get_gtk_ver(self):
        """Return version of Gtk.

        Returns:
            str:
        """
        return self._gtk_version

    def _get_gio_ver(self):
        """Return version of GIO.

        Returns:
            str:
        """
        return self._gio_version

    def _get_cairo_ver(self):
        """
        """
        return cairo.version

    def _get_platform(self):
        """Return information about the plattform, architecture, operating
        system.

        Returns:
            str:
        """
        return platform.platform()

    def _get_configobj_ver(self):
        """Return the version of third-party library configobj.

        Returns:
            (str): Version as a string.
        """
        return configobj.__version__

    def _get_aiohttp_ver(self):
        """Return the version of third-party library aiohttp.

        Returns:
            (str): Version as a string.
        """
        return aiohttp.__version__

    def get_all_versions(self):
        """Combine and return all version informations.

        Returns:
            (list): A list of strings.
        """
        vers = [self.get_full_name(),
                self._get_platform()]
        vers.extend(self._get_all_third_versions())
        return vers

    def _get_all_third_versions(self):
        """Combine and return all version information about third party
        components and packages.

        Returns:
            (list): A list of strings.
        """
        return ['Python {}'.format(self._get_python_ver()),
                'Feedparser {}'.format(self._get_feedparser_ver()),
                'aiohttp {}'.format(self._get_aiohttp_ver()),
                'Gtk {}'.format(self._get_gtk_ver()),
                'GIO {}'.format(self._get_gio_ver()),
                'Cairo {}'.format(self._get_cairo_ver()),
                'configobj {}'.format(self._get_configobj_ver())]
