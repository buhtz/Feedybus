# -*- coding: utf-8 -*-
import logging
import json  # debug
from feedybus import basics
import gi
# gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import GLib
from feedybus.entrieslistview import FeedEntriesListView
from feedybus.treeview import FeedTreeView
from feedybus.gtkhelper import create_menu_item, GeometryPersistentWindow
from feedybus.feeddialog import FeedDialog
import gettext


"""
    The main window class.

    A Gtk-based window class for the main window of feedybus.
"""


_log = logging.getLogger(basics.GetLogId(__file__))


class FeedybusWindow(Gtk.Window, GeometryPersistentWindow):
    """
        Main window class.
    """
    def __init__(self, app):
        """
        """
        Gtk.Window.__init__(self)
        _log.debug('Running {}'.format(__file__))

        ## application layer
        self._app = app

        # box layout
        box_main = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.add(box_main)

        # LEFT - TreeView
        self._tree = FeedTreeView(app, self)

        # RIGHT - ListView
        self._list = FeedEntriesListView(app, self)
        self._tree._feed_entries_listview = self._list

        # MenuBar & Toolbar
        toolbar_list_top = self._list.create_toolbar_top()
        toolbar_list_bottom = self._list.create_toolbar_bottom()
        toolbar_list_top.set_sensitive(False)
        self._toolbar_tree = self._tree.create_toolbar()
        self._create_menubar()
        box_main.pack_start(self._menubar, False, False, 0)

        # disable the entries list view (because there is no feed)
        self._list.do_disable()

        # PanedWindow
        self._paned = Gtk.Paned.new(orientation=Gtk.Orientation.HORIZONTAL)
        box_main.pack_start(self._paned, True, True, 0)
        # ...left - TreeView
        box_left = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        box_left.pack_start(self._toolbar_tree, False, False, 0)
        box_left.pack_start(self._tree.scroll, True, True, 0)
        self._paned.pack1(box_left)
        # ...right - ListView
        box_right = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        box_right.pack_start(toolbar_list_top, False, False, 0)
        box_right.pack_start(self._list.scroll, True, True, 0)
        box_right.pack_start(toolbar_list_bottom, False, False, 0)
        self._paned.pack2(box_right)
        # statusbar
        box_main.pack_start(self._tree.statusbar, False, False, 0)

        # restore gui geometry
        self.restore_gui_geometry()

        # EVENT: destroy
        self.connect('delete-event', self._on_delete_event)
        self.connect('destroy', self._on_destroy)
        # EVENT: resize
        self._connect_resize_done_event()

        # bind data-event
        self._app.event_started_refresh_all_feeds \
            .register(self.on_started_refresh_all_feeds)
        self._app.event_finished_refresh_all_feeds \
            .register(self.on_finished_refresh_all_feeds)

        # version informations
        self._app._gtk_version = Gtk._version
        self._app._gio_version = gi.__version__
        self._app.print_full_version_informations()

        # window title
        title = self._app.get_full_name()
        profile = basics.get_profile()
        if profile != '':
            title = '{} --- {}: {}'.format(title, _('profile'), profile)
        if self._app.debug:
            title = '{} --- DEBUG {}'.format(title, _('mode'))
            GLib.timeout_add(500, self._Debug_On_Start)
        self.set_title(title)

        # last-feed-update-mechanism check?
        self._app.do_handle_feed_update_mechanism_test()

    def _on_delete_event(self, window, event=None):
        """Window close event.
        """
        self.store_gui_geometry()
        # This difference is made because of "File -> Exit" menu entry
        if type(window) is not FeedybusWindow:
            self.destroy()

    def _on_destroy(self, caller):
        """Handle close window event.

        Store gui geometry to config file. Call the close handler of the
        application layer and close the Gtk application.

        Args:
            caller:
        """
        # close handler in application layer
        self._app.on_close_application()

        # quite Gtk application
        Gtk.main_quit()

    def _connect_resize_done_event(self):
        """
        See https://stackoverflow.com/a/54709415/4865723
        """
        # The id of a GLib.timeout_add() timer is stored to prevent the
        # situation installing more then one timer at the same time.
        # This timer will be used as a surrogate for the real 'size-allocate'
        # event. See self._on_resize_done_timer() for more detailes.
        self._resize_done_timer_id = None

        # for us only the change of the height is relevant
        height = self.get_allocated_size().allocation.height
        self._height_after_last_resize = height

        # connect Gtk "resize" event
        event_id = self.connect('size-allocate', self._on_size_allocated)
        ## remomber the id of the resize event for later disconnection
        self._event_id_size_allocate = event_id

    def _on_size_allocated(self, widget, alloc):
        """
        """
        # don't install more then one timer
        if self._resize_done_timer_id:
            return

        # remember the new size
        self._remembered_size = alloc

        # disconnect the 'size-allocate' event
        self.disconnect(self._event_id_size_allocate)

        # create a timer
        tid = GLib.timeout_add(250, self._on_resize_done_timer)
        self._resize_done_timer_id = tid

    def _on_resize_done_timer(self):
        """
        """
        # current window size
        curr = self.get_allocated_size().allocation

        # did the size change in?
        # NO
        if self._remembered_size.equal(curr):
            # fire event
            self._on_resize_done_event()
            # reconnect the 'size-allocate' event
            self._connect_resize_done_event()
            # stop the timer
            self._resize_done_timer_id = None
            return False

        # YES: resizing 's still going on...
        # remember the new size for the next check in 250ms
        self._remembered_size = curr
        # repeat timer
        return True

    def _on_resize_done_event(self):
        """
        """
        a = self.get_allocated_size().allocation
        if self._height_after_last_resize != a.height:
            self._height_after_last_resize = a.height
            self._list.on_resize_done()

    def _set_default_geometry(self):
        """
        """
        mg = GeometryPersistentWindow._set_default_geometry(self)
        # sash position
        self._paned.set_position(mg.width / 4)

    def restore_gui_geometry(self):
        """
        """
        geo = GeometryPersistentWindow.restore_gui_geometry(self)

        if geo:
            self._paned.set_position(geo['sash_pos'])

    def store_gui_geometry(self):
        """
        """
        screen_id = GeometryPersistentWindow.store_gui_geometry(self)

        self._app.config.set_geometry_sash(screen_id, str(self.__class__),
                                           self._paned.get_position())

    def _create_menubar(self):
        """Create entries visul and logical for the menubar with all its
        sub-menus.
        """
        self._menubar = Gtk.MenuBar.new()

        # /Feeds/*
        menu_file = Gtk.Menu.new()
        item_file = create_menu_item(
            label='Feeds',
            sub_menu=menu_file,
            parent=self._menubar)

        # /Feeds/New
        item_new_feed = create_menu_item(
            parent=menu_file,
            label=_('New'),
            stock_id=Gtk.STOCK_ADD,
            handler=self._on_new_feed)

        # /Feeds/Update
        item_update = create_menu_item(
            parent=menu_file,
            label=_('Update all'),
            stock_id=Gtk.STOCK_REFRESH,
            handler=self._tree._on_update_all_feeds)
        self.menu_item_update = item_update

        # /Feeds/Import
        item_import = create_menu_item(
            parent=menu_file,
            label='Import',
            stock_id=Gtk.STOCK_ADD,
            handler=self.on_import_feeds)

        # /Feeds/Export
        item_export = create_menu_item(
            parent=menu_file,
            label='Export',
            stock_id=Gtk.STOCK_SAVE,
            handler=self.on_export_feeds)

        # /Feeds/Exit
        item_exit = create_menu_item(
            parent=menu_file,
            label=_('Exit'),
            tooltip=_('Exit from Application'),
            stock_id=Gtk.STOCK_QUIT,  # 'application-exit',
            handler=self._on_delete_event)

        # /Extra/
        menu_extra = Gtk.Menu.new()
        item_extra = create_menu_item(
            label='Extra',
            sub_menu=menu_extra,
            parent=self._menubar)

        # /Extra/TestMecha
        item_mecha_test = create_menu_item(
            parent=menu_extra,
            label='Test feed update mechanism',
            stock_id=Gtk.STOCK_DELETE,
            handler=self._app.do_handle_feed_update_mechanism_test)

        if self._app.debug:
            # /Debug/
            menu_debug = Gtk.Menu.new()
            item_debug = create_menu_item(label='Debug',
                                          sub_menu=menu_debug,
                                          parent=self._menubar)

            # /Debug/CreateDummy
            menu_dummy = Gtk.Menu.new()
            item_dummy = create_menu_item(label='Create dummies',
                                          sub_menu=menu_dummy,
                                          parent=menu_debug)

            # /Debug/CreateDummy/*
            item_dummy_a = create_menu_item(
                label='1 feed 5 entries',
                parent=menu_dummy,
                handler=self._Debug_Dummy_Feed_1_5)
            item_dummy_b = create_menu_item(
                label='1 feed 100 entries',
                parent=menu_dummy,
                handler=self._Debug_Dummy_Feed_1_100)
            item_dummy_c = create_menu_item(
                label='10 feed 50 entries',
                parent=menu_dummy,
                handler=self._Debug_Dummy_Feed_10_50)

            # /Debug/Test
            item_debug_test = create_menu_item(
                label='Test',
                parent=menu_debug,
                handler=self._Debug_Test)

            # /Debug/WindowGemoetry
            item_debug_geometry = create_menu_item(
                label='Show misc config.',
                parent=menu_debug,
                handler=self._Debug_Misc_Config)

            # /Debug/IDs
            item_debug_ids = create_menu_item(
                label='Print IDs of groups and feeds',
                parent=menu_debug,
                handler=self._Debug_AllIds)

            # /Debug/TreeModel
            item_debug_treemodel = create_menu_item(
                label='Print Tree Model (presentation layer)',
                parent=menu_debug,
                handler=self._Debug_TreeModel)

            # /Debug/TreeData
            item_debug_treedata = create_menu_item(
                label='Print Tree data (data layer)',
                parent=menu_debug,
                handler=self._Debug_TreeData)

            # /Debug/FeedData
            item_debug_feeddata = create_menu_item(
                label='Print Feed data (data layer)',
                parent=menu_debug,
                handler=self._Debug_FeedData)

            # /Debug/ReadLaterUnread
            item_debug_readlater = create_menu_item(
                label='Print Read later & Unread(data layer)',
                parent=menu_debug,
                handler=self._Debug_ReadLaterUnread)

            # /Debug/Rebuild-TreeView
            item = create_menu_item(
                label='Rebuild TreeView',
                parent=menu_debug,
                handler=self._Debug_Rebuild_TreeView)

    def _toggle_update_items(self):
        """
        """
        def _toggle_item(item):
            item.set_sensitive(not item.get_sensitive())

        # entry in menübar
        for item in [self.menu_item_update]:
            _toggle_item(item)

        # treeview toolbar
        for idx in range(self._toolbar_tree.get_n_items()):
            item = self._toolbar_tree.get_nth_item(idx)
            _toggle_item(item)

    def _on_new_feed(self, caller):
        """
        """
        FeedDialog(self).run()

    def on_export_feeds(self, caller):
        """Export feeds handler.
        """
        # dialog to ask for filename
        dlg = Gtk.FileChooserNative(
            transient_for=self,
            title='Select import file (*.opml, *.xml) ...',
            action=Gtk.FileChooserAction.SAVE,
            do_overwrite_confirmation=True
        )
        fn = None
        response = dlg.run()

        # check users response
        if response in (Gtk.ResponseType.OK, Gtk.ResponseType.ACCEPT):
            fn = dlg.get_filename()
        dlg.destroy()

        # no file name was givin by the user
        if not fn:
            return

        # do the export
        msg = self._app.export_feeds(fn)

        dlg = Gtk.MessageDialog(parent=self.get_toplevel(),
                                title='Export',
                                text=msg,
                                type=Gtk.MessageType.INFO,
                                buttons=Gtk.ButtonsType.OK)
        dlg.run()
        dlg.destroy()

    def on_import_feeds(self, caller):
        """
        """
        # choose and open the file
        pattern = Gtk.FileFilter.new()
        pattern.add_pattern('*.opml')
        pattern.add_pattern('*.xml')
        dlg = Gtk.FileChooserNative(
            transient_for=self,
            title='Select import file (*.opml, *.xml) ...',
            action=Gtk.FileChooserAction.OPEN,
            filter=pattern
        )
        fn = None
        response = dlg.run()
        if response in (Gtk.ResponseType.OK, Gtk.ResponseType.ACCEPT):
            fn = dlg.get_filename()
        dlg.destroy()

        if not fn:
            return

        self._app.import_feeds(fn)

    def on_started_refresh_all_feeds(self, feed_ids):
        """Event handler for the start of feed update.
        """
        self._toggle_update_items()

    def on_finished_refresh_all_feeds(self):
        """Event handler for the end of feed update.
        """
        self._toggle_update_items()

    def _Debug_Misc_Config(self, event):
        """
        """
        self.store_gui_geometry()
        geo = json.dumps(self._app.config._misc, indent=4)
        _log.debug(geo)

    def _Debug_TreeModel(self, event):
        """
            Print the Gtk.TreeModel to stdout.
        """
        def print_rows(store, treeiter, indent):
            while treeiter is not None:
                print(indent + str(store[treeiter][:]))
                if store.iter_has_child(treeiter):
                    childiter = store.iter_children(treeiter)
                    print_rows(store, childiter, indent + "\t")
                treeiter = store.iter_next(treeiter)

        model = self._tree._model
        print_rows(model, model.get_iter_first(), '')

    def _Debug_TreeData(self, event):
        """
            Print the Feed-Tree structure from the data layer.
        """
        print(json.dumps(self._app._data._tree, indent=4))

    def _Debug_FeedData(self, event):
        """
        """
        print(json.dumps(self._app._data._feeds, indent=4))

    def _Debug_ReadLaterUnread(self, event):
        """
        """
        print('Read Later\n\t{}'
              .format(json.dumps(self._app._data._later, indent=4)))
        print('Unread\n\t{}'
              .format(json.dumps(self._app._data._unread, indent=4)))

    def _Debug_AllIds(self, event):
        """
            Print all IDs of groups and feeds.
        """
        g = self._app._data.get_group_ids()
        f = self._app._data.get_feed_ids()
        print('Group-IDs: {}\nFeed-IDs: {}'.format(g, f))

    def _Debug_Dummy_Feed_10_50(self, event):
        """
        """
        self._Debug_create_dummy_feeds(10, 50)

    def _Debug_Dummy_Feed_1_100(self, event):
        """
        """
        self._Debug_create_dummy_feeds(1, 100)

    def _Debug_Dummy_Feed_1_5(self, event):
        """
        """
        self._Debug_create_dummy_feeds(1, 5)

    def _Debug_create_dummy_feeds(self, feed_n, entry_n):
        """
        """
        for idx in range(feed_n):
            self._Debug_create_dummy_feed(entry_n)

    def _Debug_create_dummy_feed(self, entry_n):
        """
        """
        # create the feed
        fid = self._app._data.get_free_feed_id()
        self._app._data._feeds[fid] = {
            'title': 'Dummy Title {}'.format(fid),
            'text': 'Dummy Text {}'.format(fid),
            'htmlUrl': 'https://dummy-feed{}.space'.format(fid),
            'xmlUrl': 'https://dummy-feed{}.space'.format(fid),
            'type': 'dummy'
        }
        self._app._data._entries[fid] = []

        # create entries
        for idx in range(entry_n):
            entry = {
                'title': 'Dummy {} Entry # {}'.format(fid, idx + 1),
                'feedybus': {'read': False},
                'published_parsed': [1900, 1, 2],
                'link': 'http://localhost/{}'.format(idx)
            }
            self._app._data._entries[fid].append(entry)
        self._app._data._tree[1][3][0].append(fid)
        self._app._data.event_tree_item_new_or_modified.notify()

    def _Debug_Rebuild_TreeView(self, event):
        """
        """
        self._tree._model.build_tree()
        self._tree._model.update_folding()

    def _Debug_On_Start(self):
        """
        """
        pass

    def _Debug_Test(self, event):
        """
        """
        groups = self._tree.pulse_item(-1105)
        # raise NotImplementedError()
