# -*- coding: utf-8 -*-
import gi
# gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


"""
    Helper methods using Gtk.

    Some Gtk-related helper methods.
"""


def create_toolbar_item(icon, handler, parent, tooltip=None):
    """
    """
    item = Gtk.ToolButton.new_from_stock(stock_id=icon)

    if tooltip:
        item.set_tooltip_text(tooltip)

    item.connect('clicked', handler)
    parent.insert(item, -1)

    return item


def create_toolbar_spaceritem(parent, draw=False, expand=True):
    """
    """
    spacer = Gtk.SeparatorToolItem.new()
    spacer.set_draw(draw)
    spacer.set_expand(expand)
    parent.insert(spacer, -1)


def create_menu_separator(parent):
    """
    """
    item = Gtk.SeparatorMenuItem.new()
    parent.append(item)


def create_menu_item(label=None,
                     stock_id=None,
                     tooltip=None,
                     sub_menu=None,
                     parent=None,
                     handler=None):
    """
    """
    item = None

    if stock_id:
        item = Gtk.ImageMenuItem.new_from_stock(stock_id, None)

    if label:
        if item:
            item.set_label(label)
        else:
            item = Gtk.MenuItem.new_with_label(label)

    if handler:
        item.connect('activate', handler)

    if tooltip:
        item.set_tooltip_text(tooltip)

    if sub_menu:
        item.set_submenu(sub_menu)

    if parent:
        parent.append(item)

    return item


class GeometryPersistentWindow:
    """Abstract class implementing the persistence of window position and
    size.

    The size, position and maximized state of the inhereting window is stored
    in the config object. The inheriting class just need to call
    'restore_gui_geometry()' and 'store_gui_geometry()' at the right place in
    code.

    It is recommended to call 'store_gui_geometry()' in 'response' event
    handler of a 'Gtk.Dialog' and to call 'restore_gui_geometry()' after the
    'show_all()' in the ctor.
    """
    def __init__(self, app):
        """Constructor.

        Args:
            app (feedybus.FeedybusApplication): Application object to access
            the config object.
        """
        ##
        self._app = app

    def _set_default_geometry(self):
        """Size and position is set to default.

        Size is half of the screen and position is centered.

        Returns:
            (Gdk.Rectangle): Geometry of primary monitor.
        """
        # current monitor geometry
        mg = self._get_primary_monitor_geometry()
        # window half sized
        self.resize(mg.width / 2, mg.height / 2)
        # window centered
        self.set_position(Gtk.WindowPosition.CENTER)

        return mg

    def restore_gui_geometry(self):
        """Read geometry infos from config object and apply them to the
        window.

        Returns:
            (dict()): Geometry object.
        """
        geometry = self._app.config.get_geometry(self._get_screen_identifier(),
                                                 str(self.__class__))
        if geometry:
            try:
                self.resize(*geometry['win_size'])
                self.move(*geometry['win_pos'])
            except KeyError:
                _set_default_geometry()
            if geometry['maximized']:
                self.maximize()
        else:
            self._set_default_geometry()

        return geometry

    def store_gui_geometry(self, *args):
        """Get current window geometry and store it to the config object.

        Returns:
            (str): Current screens identifier.
        """
        screen_id = self._get_screen_identifier()

        self._app.config.set_geometry_win(screen_id, str(self.__class__),
                                          self.get_position(),
                                          self.get_size(),
                                          self.is_maximized())

        return screen_id

    def _get_primary_monitor_geometry(self):
        """Return geometry of current primary monitor.

        Returns:
            (Gdk.Rectangle): The primary monitors rectangle.
        """
        screen = self.get_screen()
        monitor_primary = screen.get_primary_monitor()
        monitor_geometry = screen.get_monitor_geometry(monitor_primary)
        return monitor_geometry

    def _get_screen_identifier(self):
        """Return the identifier of the current screen as a string.
        The identifier is based on the resolution of the primary monitor.

        Returns:
            (str): The screen identifier.
        """
        monitor_geometry = self._get_primary_monitor_geometry()
        screen_id = '{}x{}x{}x{}'.format(monitor_geometry.height,
                                         monitor_geometry.width,
                                         monitor_geometry.x,
                                         monitor_geometry.y)
        return screen_id
