# -*- coding: utf-8 -*-
import json
import os
import os.path
import logging
from functools import partial
import cairo
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GdkPixbuf
from gi.repository import GLib
from gi.repository import Gio
from gi.repository import Pango
from feedybus import basics
from feedybus import data
from feedybus.group import Group
from feedybus.feed import Feed
from feedybus import favicon
from feedybus.feeddialog import FeedDialog
from feedybus.gtkhelper import (create_toolbar_item,
                                create_toolbar_spaceritem,
                                create_menu_item,
                                create_menu_separator)
from feedybus.renamedialog import ReNameDialog
from feedybus.statusbar import Statusbar

"""
    The TreeView control in the main window displaying feeds and groups
    organized in a tree structure.

    The TreeView control depends on Gtk. It visualize the feeds in their
    groups in a tree structure. It also shows some virtual/smart groups and it
    is used to visualize feed specific errors.
"""


_log = logging.getLogger(basics.GetLogId(__file__))


## Icon update intervall in days.
_ICON_UPDATE_INTERVALL = 30


class FeedTreeStore (Gtk.TreeStore):
    """This is the 'model' of the TreeView.

    'Model' only in the meaning of Gtk but not in the case of MVC-pattern.
    """
    def __init__(self, app, view):
        """Ctor
        """
        Gtk.TreeStore.__init__(self,
                               str,  # group/feed name
                               int,  # weight: name (col 0)
                               int,  # "unread"
                               bool,  # vissible: "unread" (col 1)
                               int,  # "total"
                               bool,  # vissible: "total" (col 2)
                               int,  # item-id
                               str,  # label foreground color
                               int,  # style (e.g. italic)
                               GdkPixbuf.Pixbuf,  # icon: expander-close
                               GdkPixbuf.Pixbuf,  # icon: expander-open
                               GdkPixbuf.Pixbuf,  # icon: default feed icon
                               bool,  # background-set
                               Gdk.RGBA)  # background-color

        ## connection to application (and data) layer
        self._app = app

        ## connection to treeview
        self._view = view

        ## Dictionary with icons in Pixbuf format or their names as str
        self._icons = {}

        ## Icon size in pixel
        self._icon_size = 16

        # folder open/close icons
        icon_theme = Gtk.IconTheme.get_default()
        lookup_flag = Gtk.IconLookupFlags.GENERIC_FALLBACK
        for icon_name in [['folder'],
                          ['folder-open'],
                          ['important',
                           'embleme-important',
                           'folder-important']]:
            icon_info = icon_theme.choose_icon(icon_name,
                                               self._icon_size,
                                               lookup_flag)
            icon = icon_info.load_icon()
            self._icons[icon_name[0]] = icon

        # default feed icon
        for icon_name in ['application-rss+xml',
                          'application-atom+xml',
                          'application-rss+xml-symbolic',
                          'document']:
            if icon_theme.has_icon(icon_name):
                self._icons['feed'] = icon_theme.load_icon(icon_name,
                                                           self._icon_size, 0)
                break

        # Prepare data to check and load if needed specific feed icons in
        # separate process
        ## Thread object handling the loading of favicons
        self._favicon_worker = None
        self._favicon_check()

        # create a "model" for the tree
        self.build_tree(False)

        # bind data-event
        self._app.event_started_refresh_all_feeds \
            .register(self.on_refresh_started)
        self._app.event_feed_refreshed \
            .register(self._on_feed_refreshed)
        Feed.event_entry_read_modified \
            .register(self._on_entry_read_modified)
        Feed.event_entry_read_later_modified \
            .register(self.on_entry_toggled_read_later)
        Group.event_group_deleted \
            .register(self._on_items_has_been_deleted)
        Feed.event_feed_deleted \
            .register(self._on_items_has_been_deleted)

    def _favicon_check(self, only_this_feed_ids=None):
        """
        """
        if self._favicon_worker is not None:
            return
        self._favicon_worker = favicon.FaviconThread(self._app._data._feeds,
                                                     self._favicon_callback,
                                                     only_this_feed_ids)
        self._favicon_worker.start()

    def _favicon_callback(self):
        """Add loaded favicons to their feed items in the tree.

        This methode is called by 'self._favicon_worker' when it is finished.
        """
        # Update icon information
        update_feeds = self._favicon_worker.result_update_feeds
        for fid in update_feeds:
            # Maybe the feeds was deleted by while icon check
            if fid not in self._app._data._feeds:
                _log.warning('Feed {} does not exist anymore. Ignoring '
                             'its update of icon information.')
                continue

            # update icon information
            new_info = update_feeds[fid]['favicon']
            self._app._data._feeds[fid]['favicon'] = new_info

        # Add icons to the tree view
        icons = self._favicon_worker.result_icons
        for fid, icon_bytes in icons:
            # convert raw data to PixBuf icon
            icon_bytes = GLib.Bytes(icon_bytes)
            input_stream = Gio.MemoryInputStream.new_from_bytes(icon_bytes)
            pixbuf = GdkPixbuf.Pixbuf.new_from_stream(input_stream)
            # if pixbuf is None:
            #     print(icon_bytes)
            #     print(fid)
            # remember icon-pixbuf
            self._icons[fid] = pixbuf
            # store icon-pixbuf in tree-model
            self.icons(fid, (None, None, pixbuf))

        self._favicon_worker = None
        return False  # don't repeat this callback

    def _rebuild_iter_item_relation(self, item_id=None):
        """Rebuild iterator-item-id-relation-dictionary ('self._iters')
        for one specific 'item_id' or (by default) for all.
            This is usefull if you delete some items in the tree.

        Args:
            item_id (int): Default is 'None'.
        """
        def _walk_tree_iters(it):
            while it:
                # remember the path-id-relation
                id_found = self.item_id(it)
                if id_found == item_id or not item_id:
                    path_str = self.get_string_from_iter(it)
                    self._iters[id_found] = path_str
                    if item_id:
                        return False  # stop walk
                # check for children
                if self.iter_has_child(it):
                    child = self.iter_children(it)
                    if not _walk_tree_iters(child):
                        return False  # stop walk
                # next iterator
                it = self.iter_next(it)
            return True  # go on walkin

        root_it = self.get_iter_first()
        _walk_tree_iters(root_it)

    def _get_iter(self, item_id):
        """Returning the Gtk.TreeIter object related to 'item_id'.

        If nothing is found 'None' is returned.

        Args:
            item_id (int): Id of a tree item (e.g. feed or group)

        Returns:
            Gtk.TreeIter or None
        """
        # WHY do we need this?
        if item_id is None:
            raise Exception('Why does this happen?')

        try:
            path_string = self._iters[item_id]
        except KeyError:
            self._rebuild_iter_item_relation(item_id)
            if item_id in self._iters:
                path_string = self._iters[item_id]
            else:
                return None

        return self.get_iter_from_string(path_string)

    def _get_path(self, item_id):
        """Return a Gtk.TreePath object relating to the 'item_id'.

        Args:
            item_id (int): Id of a item in the tree.

        Returns:
            (Gtk.TreePath): A path representing the 'item_id'
        """
        return self.get_path(self._get_iter(item_id))

    def _get_set_column(self, idx, iter_or_id_or_path, value):
        """Set a value to or return a value from a cell.

        The cell is identified by column index and a TreeIter or a TreePath.

        Args:
            idx (int): Column index.
            iter_or_id_or_path (): Row.
            value (): A value.

        Returns:
            (): A value of the cell.
        """
        # feed id given
        if type(iter_or_id_or_path) is int:
            tree_iter = self._get_iter(iter_or_id_or_path)
            if not tree_iter:
                raise KeyError('There is no TreeIterator for Feed {}. '
                               'Maybe the feed exist in the data but not '
                               'in the tree.'.format(iter_or_id_or_path))
            iter_or_id_or_path = tree_iter
        # TreePath given
        elif type(iter_or_id_or_path) is Gtk.TreePath:
            iter_or_id_or_path = self.get_iter(iter_or_id_or_path)

        if value is None:
            # print('self[{}][{}]'.format(iter_or_id_or_path, idx))
            return self[iter_or_id_or_path][idx]

        self[iter_or_id_or_path][idx] = value

    def icons(self, iter_or_id_or_path, value=None):
        """Set or get the icons for that row.

        Args:
            value (tuple(GdkPixbuf.Pixbuf, GdkPixbuf.Pixbuf, str)): Icon infos

        The value is a three elements tuple. The first and second is a
        'GdkPixbuf.Pixbuf' to a closed and the open expander icon. The third
        one is a string with the icon-name of the default icon which is used
        for items without children.
        """
        return (self._get_set_column(9, iter_or_id_or_path, value[0]),
                self._get_set_column(10, iter_or_id_or_path, value[1]),
                self._get_set_column(11, iter_or_id_or_path, value[2]))

    def label(self, iter_or_id_or_path, value=None):
        """
        """
        return self._get_set_column(0, iter_or_id_or_path, value)

    def label_foreground_color(self, iter_or_id_or_path, value=None):
        """
        """
        return self._get_set_column(7, iter_or_id_or_path, value)

    def label_font_style(self, iter_or_id_or_path, value=None):
        """
        """
        return self._get_set_column(8, iter_or_id_or_path, value)

    def label_background_color(self, iter_or_id_or_path, color=None):
        """Set the background color of the text column.

        If 'color' is None the background is set back to default.

        Args:
            iter_or_id_or_path (): Item identifier.
            color (Gdk.RGBA): The color (default: None).
        """
        if color is None:
            self._get_set_column(12, iter_or_id_or_path, False)
            self._get_set_column(13, iter_or_id_or_path, None)
        else:
            self._get_set_column(12, iter_or_id_or_path, True)
            self._get_set_column(13, iter_or_id_or_path, color)

    def unread(self, iter_or_id_or_path, value=None):
        """Get the value of the unread column (if 'value' == None) or set its
        value.

        If 'value' is 0 the column will be left blank. The string '0' is not
        shown in that case. If 'value' is higher then 0 the column is shown.
        """
        rc = self._get_set_column(2, iter_or_id_or_path, value)

        # no string if nothing is unread
        if value is not None:
            visible = False if value == 0 else True
            self._get_set_column(3, iter_or_id_or_path, visible)

        return rc

    def inc_unread_and_total(self, iter_or_id_or_path, value):
        """Increase the value of the unread & total column.

        Args:
            iter_or_id_or_path (): Row identifier.
            value (int): Value to add to the current value.
        """
        # calculate new value
        new_unread = self.unread(iter_or_id_or_path) + value
        new_total = self.total(iter_or_id_or_path) + value

        # set new value
        self.unread(iter_or_id_or_path, new_unread)
        self.total(iter_or_id_or_path, new_total)

        # increment parents value
        item_id = self.item_id(iter_or_id_or_path)
        if Feed.is_fid_valid(item_id) or Group.is_gid_valid(item_id):
            try:
                parent_id = self._app._data.get_full_path(item_id)[-2]
                self.inc_unread_and_total(parent_id, value)
            except IndexError:
                # no parent anymore
                pass

    def total(self, iter_or_id_or_path, value=None):
        """
        """
        return self._get_set_column(4, iter_or_id_or_path, value)

    def item_id(self, iter_or_id_or_path):
        """Return the ID (data layer) of the item.

        Args:
            iter_or_id_or_path (): Identifier.

        Returns:
            (int): The ID related to an tree item.
        """
        return self._get_set_column(6, iter_or_id_or_path, None)

    def _on_items_has_been_deleted(self, item_ids):
        """Event handler for the situation after mutliple groups or feeds has
        been deleted in the data structure.

        Args:
            item_ids (list(int)): List of Id's of feeds or groups.
        """
        # To keep the iterator-path-item-id-dictionary consistent while delete
        # process we need to delete mutliple items in the correct order.
        # First we just collect but don't delete the path of each item.
        # _log.debug('item_ids: {}'.format(item_ids))
        # _log.debug('iters: {}'.format(self._iters))
        path_strings = []
        for item_id in item_ids:
            item_iter = self._get_iter(item_id)

            # a feed?
            if item_id > basics.GROUPID_FEEDS:
                # has unread entries?
                if self.unread(item_iter) > 0:
                    # update statusbar
                    self._view.statusbar.dec_unread()
                # has a problem?
                if item_id in self._err_paths:
                    del self._err_paths[item_id]
                    # update statusbar
                    self._view.statusbar.problems(len(self._err_paths))

            p = self.get_path(item_iter)
            path_strings.append(p)

        # Statusbar: feed count
        count = len(self._app._data._feeds)
        self._view.statusbar.total(count)

        # Order reverse
        path_strings = sorted(path_strings, reverse=True)

        # Delete each item beginning from bottom to top
        for ps in path_strings:
            self.delete(ps)

        # The iterator-path-id-dict is no inconsistent.
        self._iters = {}

    def exists(self, item_id_or_path):
        """Check if the id or path exists.

        Args:
            item_id_or_path: Id or path of a tree element.

        Returns:
            (bool): True if exsits. False if not.
        """
        # an item id is givin
        if type(item_id_or_path) is int:
            the_iter = self._get_iter(item_id_or_path)
            item_id = item_id_or_path
        # a item path is givin
        elif type(item_id_or_path) is str:
            the_iter = self.get_iter_from_string(item_id_or_path)
            item_id = self.item_id(the_iter)

        if item_id in self._iters:
            return True

        return False

    def delete(self, item_id_or_path):
        """Delete a tree item identified by its id.

        The item will be deleted from the model, from the tree view and from
        the model intern iterator-iid-relation-dictionary '_iters'. The
        original data (class 'FeedsData') won't be touched. Keep to things in
        mind: 1) A KeyError is raised if the iid doesn't
        exists. 2) The iterator-iid-relation-dict can become inconsistent.

        Args:
            item_id_or_path (int, str, Gtk.TreePath): Identifier or path of a
            group or feed.
        """
        # an item id is givin
        if type(item_id_or_path) is int:
            the_iter = self._get_iter(item_id_or_path)
            item_id = item_id_or_path
        else:
            # a item path string is givin
            if type(item_id_or_path) is str:
                the_iter = self.get_iter_from_string(item_id_or_path)
            # a item Gtk.TreePath is givin
            elif type(item_id_or_path) is Gtk.TreePath:
                the_iter = self.get_iter(item_id_or_path)
            else:
                raise TypeError()
            item_id = self.item_id(the_iter)

        # remove iterator-item-id relation
        del self._iters[item_id]

        # remove the item from the TreeModel will update the view also
        self.remove(the_iter)

    def _add(self, parent_group_id, item_id, label, bold,
             unread=None, total=None, do_relation=True):
        """Append new item (group or feed) to the tree model.

        The feedybus tree identifiers (iid) are converted to Gtk.TreeIter
        string values. The connection between them is stored in 'self._iters'
        dictionary.

        Args:
            parent_group_id (int): parent node
            item_id (int): Feedybus-IID of the new item.
            label (str):
            bold (bool):
            unread (int):
            total (int):
        """
        # group/feed name
        row = [label]

        # bold explicite?
        if bold:
            row.append(Pango.Weight.BOLD)
        else:
            row.append(Pango.Weight.BOOK)

        # unread
        row.append(unread)
        row.append(False if row[-1] in [None, 0] else True)
        # update statusbar
        if item_id > basics.GROUPID_FEEDS and unread > 0:
            self._view.statusbar.inc_unread(True)

        # total
        row.append(total)
        row.append(False if row[-1] is None else True)

        # item-id
        row.append(item_id)

        # label foreground color
        row.append('')

        # font style
        row.append(Pango.Style.NORMAL)

        # itertor of the parent
        if parent_group_id is None:
            parent_iter = None
        else:
            parent_iter = self._get_iter(parent_group_id)

        # icons
        if item_id == basics.GROUPID_INVALID:
            # error item
            row.append(None)
            row.append(None)
            row.append(self._icons['important'])
        elif item_id <= basics.GROUPID_FEEDS:
            # group item
            row.append(self._icons['folder'])  # folded
            row.append(self._icons['folder-open'])  # unfolded
            row.append(self._icons['folder'])  # empty
        else:
            # feed item
            row.append(None)
            row.append(None)
            try:
                row.append(self._icons[item_id])
            except KeyError:
                row.append(self._icons['feed'])

        # background color
        row.append(False)
        row.append(None)

        # append the new row
        cur_iter = self.append(parent=parent_iter, row=row)

        # relation between item-id and iter
        if do_relation:
            self._iters[item_id] = self.get_string_from_iter(cur_iter)

        return cur_iter

    def _on_entry_read_modified(self, feed_id, entry_idx):
        """Event handler if a feed entrie was marked state was modified.

        The methode does nothing if the state is modified to unread!
        This will update the "unread" column in the treeview for the specific
        feed and all its parents.

        Args:
            feed_id (int):
            entry_idx (int):
        """
        feedobj = Feed(feed_id)

        # act only if the states was modified from "unread" to "read"
        if not feedobj.get_entry_read(entry_idx):
            return

        # compute the tree items that should be updated
        # => the "unread" group, the feed itself and
        #    its category (parent) groups
        item_ids = self._app._data.get_full_path(feedobj.fid)
        item_ids.extend([basics.GROUPID_UNREAD])

        # do decrement unread-entry count for the feed, parent and logical
        # groups
        for item in item_ids:
            unread = self.unread(item) - 1
            self.unread(item, unread)

        # Any unread entry left in this feed?
        if self.unread(feedobj.fid) == 0:
            # update statusbar if not
            self._view.statusbar.dec_unread(True)

    def on_entry_toggled_read_later(self, feed_id, entry_idx):
        """Event handler if the an entrie toggled its read-later status.

        The event is fired by the data layer. The handler modify the 'total'
        count of the read-later group.

        Args:
            feed_id (int): Id of the feed.
            entry_idx (int): Index of the entry in the feed.
        """
        # the related entry
        entry = self._app._data._entries[feed_id][entry_idx]

        # check status
        if entry['feedybus']['read_later']:
            modification = 1
        else:
            modification = -1

        total = self.total(basics.GROUPID_LATER)
        total = total + modification
        self.total(basics.GROUPID_LATER, total)

    def on_refresh_started(self, feed_ids):
        """Event handler if the refreshing of all feeds begins.
        """
        # Reset error indicators (items, color, font) from the last refresh.
        # First we need to take care that the paths are reverse ordered.
        ep = sorted(self._err_paths.items(),
                    key=lambda x: x[1][0],
                    reverse=True)

        if feed_ids is None:
            feed_ids = self._app._data._feeds.keys()

        for item_id, tup in ep:
            if item_id in feed_ids:
                path = tup[0]
                # reset the color of the feed item
                self.label_foreground_color(item_id, '')
                # delete the error item
                self.remove(self.get_iter_from_string(path))

        # clear the error indicators list
        self._err_paths = {}

        # update statusbar
        self._view._statusbar.problems(len(self._err_paths))
        self._view._statusbar.updating(len(feed_ids))

    def _on_feed_refreshed(self, feed_id, parsed_feed):
        """Handler (observer) for data-event when one feed is refreshed with
        new entries.

        This is called while refreshing feeds when one feed finsihed
        refreshing AND new entries are received from it. When there are no new
        entries this handler is not invoked!

        Args:
            feed_id (int): The Treeview specific indentifier.
            parsed_feed (feedparser.FeedParserDict): parsed data
        """
        # get the iterator
        feed_it = self._get_iter(feed_id)

        # number of new entries
        num = len(parsed_feed.entries)

        # update "unread" & "total" (incl. parents)
        self.inc_unread_and_total(feed_it, num)

        # update "unread" for unread-group
        unread_it = self._get_iter(basics.GROUPID_UNREAD)
        self.inc_unread_and_total(unread_it, num)

        def _create_error_item(label, color):
            """
            """
            err_iter = self._add(parent_group_id=feed_id,
                                 item_id=basics.GROUPID_INVALID,
                                 label=label,
                                 bold=False,
                                 unread=None,
                                 total=None,
                                 do_relation=False)
            # italic font
            self.label_font_style(err_iter, Pango.Style.ITALIC)
            # highlight by color
            self.label_foreground_color(feed_it, color)
            # build error message for the user
            msg = self._create_error_text(parsed_feed)
            # remember the error relation for later reset
            path_str = self.get_string_from_iter(err_iter)
            self._err_paths[feed_id] = (path_str, msg)
            # unfold the feeds item and all its parents
            self._view.expand_to_path(self.get_path(err_iter))
            # update statusbar
            self._view._statusbar.problems(len(self._err_paths))

        status = parsed_feed.response['status']
        reason = parsed_feed.response['reason']
        label = None
        color = basics.COLOR_INFO
        # By default the first answer from server is displayed.
        # But if the last answer (in case of redirects) was an error,
        # the last answer is displayed.
        if status not in [200,  # OK,
                          304]:  # Not Modified
            # redirect with error?
            if 'history' in parsed_feed.response:
                if parsed_feed.response['history'][-1]['status'] >= 400:
                    status = parsed_feed.response['history'][-1]['status']
                    reason = parsed_feed.response['history'][-1]['reason']

            # report the response code
            label = '{} {}'.format(status, reason)
            if status >= 400:
                color = basics.COLOR_ERROR

        # bozo exception
        if 'bozo' in parsed_feed and parsed_feed.bozo is 1:
            if status < 400:
                # report parsing errors ("bozo exceptions") when no error in
                # response code
                label = parsed_feed.bozo_exception
                color = basics.COLOR_ERROR

        # add the error as child of feed-item in the treeview
        if label:
            _create_error_item(label, color)

        # Update EntryListView
        if feed_id is None or self._view._window._list.is_feed_shown(feed_id):
            self._view._window._list.populate_feed(reset_entry_references=True)

    def _create_error_text(self, feed):
        """Create a message for the user givin informations about the last
        error.

        This string is usualy used in the tooltip of the feed & error item.

        Args:
            feed (feedparser.dict): The feed data object.

        Returns:
            (str): The text.
        """
        msg = '<b>{} {}</b>'.format(feed.response['status'],
                                    feed.response['reason'])
        # HTTP codes 900+ are propritary and used for connect errors by me.
        # So there is no server in that case chat could "answer".
        if feed.response['status'] < 900:
            msg = 'First answer from server: {}.'.format(msg)

        # Parsing errors (Bozo excpetions)
        if 'bozo' in feed and feed.bozo is 1:
            msg = 'Parsing error <b>{}</b>.\n\n{}' \
                  .format(feed.bozo_exception, msg)

            if 'raw_data' in feed:
                raw = GLib.markup_escape_text(str(feed.raw_data), -1)
                msg += '\n<b>Raw data</b>:\n<tt>{}</tt>...' \
                       .format(raw)

        # following responses
        if 'history' in feed.response:
            msg += '\nNext answers'
            for r in feed.response['history']:
                msg += '\n\t<b>{} {}</b>'.format(r['status'], r['reason'])

        # URL modifications in case of redirects
        if feed.response['status'] in [301,  # moved permanently
                                       302,  # Found (Moved Temporarily)
                                       303,  # See other
                                       307,  # Temporary Redirect
                                       308]:  # Permanent redirect
            try:
                url = feed['old_url'].replace('&', '&amp;')
                msg += '\n\n<b>Old URL</b>: {}'.format(url)
            except KeyError:
                pass

        # URL & Timestamp
        url = feed.href.replace('&', '&amp;')
        msg += '\n<b>URL</b>: {}\n<b>Timestamp:</b>: {}' \
               .format(url, feed.headers.get('Date', 'unkown'))

        return msg

    def _add_group_from_data(self, groupobj):
        """Build the views tree based on the tree data.

        Args:
        """
        # the group itself
        total, unread = groupobj.count_total_and_unread_entries()
        # no parent if the current group is root
        try:
            parent_group_id = groupobj.parent.gid
        except AttributeError:
            parent_group_id = None
        # add
        self._add(parent_group_id=parent_group_id,
                  item_id=groupobj.gid,
                  label=groupobj.label,
                  bold=True,
                  unread=unread,
                  total=total)

        # child groups
        for child_group in groupobj.groups:
            self._add_group_from_data(child_group)

        # feeds
        for feedobj in groupobj.feeds:
            total, unread = feedobj.count_total_and_unread_entries()
            self._add(parent_group_id=groupobj.gid,
                      item_id=feedobj.fid,
                      label=feedobj.label,
                      bold=False,
                      unread=unread,
                      total=total)

    def update_folding(self, group_id=None):
        """ Fold or unfold all or one group node(s) based on the tree
        structure in the data layer.

        If 'group_id' is specified only (and without its children) this group
        is unfolded/expanded if its data-state is unfolded.

        Args:
            group_id (int): ID of a group or 'None' (default).
        """
        def _walk_groups_for_folding(sub_tree):
            for group in sub_tree[1:]:
                path = self._get_path(group[0])
                if group[2]:
                    self._view.expand_row(path, False)
                else:
                    self._view.collapse_row(path)
                _walk_groups_for_folding(group[3])

        if group_id is None:
            _walk_groups_for_folding(self._app._data._tree)
        else:
            groupobj = Group(group_id)
            if groupobj.unfolded:
                path = self._get_path(group_id)
                self._view.expand_row(path, False)

    def _handle_row_folding(self, it, unfold):
        """Store the folding-state (collapsed or expanded) of a group node in
        the data-layer.

        This methode is called by the handlers 'self._on_row_collapesed()' and
        'self._on_row_expanded()'.

        Args:
            it (Gtk.TreeViewIter): Iterator of the item.
            unfold (bool): Unfold state.
        """
        # is it a real group? (sometimes it is a feed item if it has an error
        # item as child)
        try:
            groupobj = Group(self.item_id(it))
        except ValueError:
            return

        # set value in the data layer
        groupobj.set_unfolded(unfold)

        # because of a wontfixed-Gtk-bug we need to take care of the childrens
        # folding-state by ourselfs
        # https://gitlab.gnome.org/GNOME/gtk/issues/1730
        if unfold:
            # IDs of child group (only of the first sub-level)
            child_ids = groupobj.group_ids
            for cid in child_ids:
                self.update_folding(cid)

    def build_tree(self, with_select_event_connection_managment=True):
        """Use the feeds data to build the feed tree in the window.
        """
        # deactivate item-select event
        if with_select_event_connection_managment:
            if self._view._event_id_on_select is not None:
                selection = self._view.get_selection()
                if selection:
                    selection.disconnect(self._view._event_id_on_select)
                    self._view._event_id_on_select = None

        # delete all elements
        self.clear()

        ## Keeps relation between data-layers item_ids and presentation-layers
        ## iterators/paths
        self._iters = dict()

        ## Tuple list for error items: (item_id, path_to_error_message_item)
        self._err_paths = dict()

        # Statusbar: reset & feed count
        self._view.statusbar.reset()
        self._view.statusbar.total(Feed.count())

        # logical node "Read later"
        self._add(None, basics.GROUPID_LATER, _('Read later'), True,
                  None, Group.count_read_later_entries())

        # logical node 'Unread'
        total, unread = Group.root().count_total_and_unread_entries()
        self._add(None, basics.GROUPID_UNREAD, _('Unread'), True,
                  unread, None)

        # add groups and feeds
        self._add_group_from_data(Group.root())

        # reconnect item-select event
        if with_select_event_connection_managment:
            self._view.connect_select_event()

    def do_row_draggable(self, path):
        """Decide if the dragged item can be dragged.
        """
        item_id = self.item_id(path)
        return (Feed.is_fid_valid(item_id) or Group.is_gid_valid(item_id))


class FeedTreeView (Gtk.TreeView):
    """
        The Treeview holding the feeds and groups of feeds.
    """
    def __init__(self, app, window):
        """Ctor.

        Args:
            app (FeedybusApplication): The application instance.
            window (FeedybusWindow): Main window instance.
        """
        ## connection to application layer
        self._app = app
        ## statusbar object
        self._statusbar = None
        ## Event id for item select event
        self._event_id_on_select = None
        ## model
        self._model = FeedTreeStore(app, self)
        # init treeview
        Gtk.TreeView.__init__(self, self._model)
        ## Reference to main window
        self._window = window

        self._model.update_folding()

        # column A - label/name
        ren_a_icon = Gtk.CellRendererPixbuf(is_expander=True)
        ren_a_text = Gtk.CellRendererText(ellipsize=Pango.EllipsizeMode.END,
                                          weight_set=True,
                                          style_set=True)
        col_a = Gtk.TreeViewColumn('')
        col_a.pack_start(ren_a_icon, False)
        col_a.add_attribute(ren_a_icon, 'pixbuf-expander-closed', 9)
        col_a.add_attribute(ren_a_icon, 'pixbuf-expander-open', 10)
        col_a.add_attribute(ren_a_icon, 'pixbuf', 11)
        col_a.pack_start(ren_a_text, True)
        col_a.add_attribute(ren_a_text, 'text', 0)
        col_a.add_attribute(ren_a_text, 'weight', 1)
        col_a.add_attribute(ren_a_text, 'style', 8)
        col_a.add_attribute(ren_a_text, 'background-set', 12)
        col_a.add_attribute(ren_a_text, 'background-rgba', 13)
        col_a.set_expand(True)
        col_a.set_cell_data_func(ren_a_text, self._label_cell_data)
        self.append_column(col_a)

        # column: B - unread
        col_b_renderer = Gtk.CellRendererText(alignment=Pango.Alignment.RIGHT,
                                              align_set=True)
        # row (y) padding (not sure: but it could be pixel)
        col_b_renderer.set_padding(0, 2)
        col_b = Gtk.TreeViewColumn(None,
                                   col_b_renderer,
                                   text=2,
                                   visible=3)
        self.renderer_b = col_b_renderer
        header_b = Gtk.Label(label='<span size="small">{}</span>'
                                   .format(_('Unread')),
                             use_markup=True)
        col_b.set_widget(header_b)
        col_b.get_widget().show()
        self.append_column(col_b)

        # column: C - total
        col_c_renderer = Gtk.CellRendererText(foreground_set=True)
        total_rgba = col_c_renderer.get_property('foreground-rgba')
        total_rgba.alpha = 0.5
        col_c_renderer.set_property('foreground-rgba', total_rgba)
        col_c = Gtk.TreeViewColumn(None,
                                   col_c_renderer,
                                   text=4,
                                   visible=5)
        header_c = Gtk.Label(label='<span size="small">{}</span>'
                                   .format(_('Total')),
                             use_markup=True)
        col_c.set_widget(header_c)
        col_c.get_widget().show()
        self.append_column(col_c)

        # scrollable
        self.scroll = Gtk.ScrolledWindow()
        self.scroll.add(self)
        self.scroll.set_policy(Gtk.PolicyType.AUTOMATIC,
                               Gtk.PolicyType.AUTOMATIC)

        # event: expand/collaps
        self.connect('row-collapsed', self._on_row_collapsed)
        self.connect('row-expanded', self._on_row_expanded)

        ## event: select
        self.connect_select_event()

        ## event: double-click
        self.connect('row-activated', self._on_double_click)

        self.set_has_tooltip(True)
        ## event: tooltip
        self.connect('query-tooltip', self._on_query_tooltip)

        ## event: drag & drop
        self.connect('drag-data-received', self.on_drag_data_received)
        ## event: drag & drop
        self.connect('drag-data-get', self.on_drag_data_get)
        target_entry = Gtk.TargetEntry.new('text/plain',
                                           Gtk.TargetFlags.SAME_WIDGET,
                                           0)
        drag_action = Gdk.DragAction.DEFAULT | Gdk.DragAction.MOVE
        self.enable_model_drag_source(Gdk.ModifierType.BUTTON1_MASK,
                                      [target_entry],
                                      drag_action)
        self.enable_model_drag_dest([target_entry],
                                    drag_action)
        ## event: drag & drop
        self.connect_after('drag-begin', self.on_drag_begin)

        ## event: context menu
        self.connect('button-press-event', self._on_button_press)
        ## Menu object for right-click
        self._context_menu = None

        # events: bind data
        self._app._data.event_finished_test_all_feeds \
            .register(self.on_update_test_finished)

        #
        Feed.event_feed_new_or_modified.register(self._on_rebuild_tree)
        #
        Feed.event_feed_marked_read.register(self._on_rebuild_tree)
        #
        Group.event_group_new_or_modified.register(self._on_rebuild_tree)

        self._app.event_import_start \
            .register(self._on_import_start)
        self._app.event_import_end \
            .register(self._on_import_end)

    def connect_select_event(self):
        """Connect the GUI event for selecting an item in the tree.

        The event id is stored for later temporary disconnection.
        """
        if self._event_id_on_select is not None:
            return

        selection = self.get_selection()
        if selection is None:
            return
        # connect event
        event_id = selection.connect('changed', self._on_select)
        ## Event id for tree-item selection event
        self._event_id_on_select = event_id

    @property
    def statusbar(self):
        """
        """
        if self._statusbar is None:
            self._statusbar = Statusbar()

        return self._statusbar

    def _label_cell_data(self, col, ren, model, it, date):
        """Cell data function for the label column.
        """
        color = model.label_foreground_color(it)
        rgba = None
        if color != '':
            rgba = Gdk.RGBA()
            rgba.parse(color)

        ren.set_property('foreground-rgba', rgba)

    def _on_button_press(self, widget, event):
        """Event handler for 'button-press-event' currently handling
        right-mouse-button clicks (for context menu).
        """
        # right mouse button
        if not (event.type == Gdk.EventType.BUTTON_PRESS
                and event.button == 3):
            return False

        # related id of feed or group
        path = self.get_path_at_pos(event.x, event.y)[0]
        feed_id = self._model.item_id(path)

        # if it is not a Feed or Group
        if not (Feed.is_fid_valid(feed_id) or Group.is_gid_valid(feed_id)):
            return False

        # select/highlight the item
        self.set_cursor(path, None, False)
        # create context menu
        self._create_context_menu(feed_id)
        # remember the item id for later use in the handler
        self._context_menu.context_id = feed_id
        # bring up the menu at the mouse pointer
        self._context_menu.popup_at_pointer()

        return True

    def _on_double_click(self, tree_view, path, column):
        """Double click an item activats the FeedDialog.
        """
        item_id = self._model.item_id(path)

        # group item
        try:
            feedobj = Feed(item_id)
            dlg = FeedDialog(self._window, feedobj)
            dlg.run()
        except ValueError:
            self._do_rename_feed_group(item_id)

    def _create_context_menu(self, feed_id):
        """Create the context menu object at 'Self._context_menu'.

        The menu object is stored in 'self._context_menu'.

        Args:
            feed_id (int): Id of related feed.
        """
        if Feed.is_fid_valid(feed_id):
            is_feed = True
        else:
            is_feed = False

        # reuse the last context menu object?
        if self._context_menu:
            self._context_menu.destroy()
            self._context_menu = None

        # create the menu obejct
        if not self._context_menu:
            self._context_menu = Gtk.Menu.new()
            self._context_menu.is_feed = is_feed

        # create tooltips
        if self._context_menu.is_feed:
            tip_update = _('Update this feed with new entries.')
            tip_rename = _('Rename the label of this feed.')
            tip_delete = _('Delete this feed.')
            tip_mark_read = _('Mark all entries as read.')
        else:
            tip_update = _('Update all feeds of that group.')
            tip_rename = _('Rename this group.')
            tip_delete = _('Delete this group and all its feeds '
                           'and sub-groups.')
            tip_mark_read = _('Mark entries of all feeds as read.')

        # create items
        cm_update = create_menu_item(label=_('Update'),
                                     stock_id=Gtk.STOCK_REFRESH,
                                     tooltip=tip_update,
                                     handler=self._on_menu_item_update,
                                     parent=self._context_menu)
        create_menu_separator(self._context_menu)
        cm_rename = create_menu_item(label=_('Rename'),
                                     stock_id=Gtk.STOCK_EDIT,
                                     tooltip=tip_rename,
                                     handler=self._on_menu_item_rename,
                                     parent=self._context_menu)
        cm_delete = create_menu_item(label=_('Delete'),
                                     stock_id=Gtk.STOCK_DELETE,
                                     tooltip=tip_delete,
                                     handler=self._on_menu_item_delete,
                                     parent=self._context_menu)

        cm_read = create_menu_item(label=_('Mark read'),
                                   stock_id=Gtk.STOCK_CLEAR,
                                   tooltip=tip_mark_read,
                                   handler=self._on_menu_item_mark_read,
                                   parent=self._context_menu)
        create_menu_separator(self._context_menu)

        if is_feed:  # FEED
            url = self._app._data._feeds[feed_id].get('htmlUrl', None)
            if url:
                item = create_menu_item(label=_('Open website'),
                                        stock_id=Gtk.STOCK_HOME,
                                        handler=self._on_menu_open_website,
                                        tooltip=url,
                                        parent=self._context_menu)
                self._context_menu.item_website = item

            url = self._app._data._feeds[feed_id].get('xmlUrl', None)
            if url:
                item = create_menu_item(label=_('Open feed URL'),
                                        stock_id=Gtk.STOCK_HOME,
                                        handler=self._on_menu_open_feed_url,
                                        tooltip=url,
                                        parent=self._context_menu)
                self._context_menu.item_feed_url = item
        else:  # GROUP
            # fold
            create_menu_item(label=_('Fold'),
                             handler=self._on_menu_item_fold,
                             parent=self._context_menu)
            # unfold
            create_menu_item(label=_('Unfold'),
                             handler=self._on_menu_item_unfold,
                             parent=self._context_menu)
            # new group
            create_menu_item(label=_('New group'),
                             handler=self._on_menu_item_new_group,
                             parent=self._context_menu)

        # DEBUG
        if self._app.debug:
            create_menu_separator(self._context_menu)
            if is_feed:
                create_menu_item(label='print raw feed data',
                                 handler=self._on_menu_dbg_feed_data,
                                 parent=self._context_menu)
            else:
                create_menu_item(label='split into sub-groups',
                                 handler=self._on_menu_dbg_split_group,
                                 parent=self._context_menu)

            create_menu_item(label='Check Favicons',
                             handler=self._on_menu_dbg_favicon,
                             parent=self._context_menu)
            create_menu_item(label='Full RESET',
                             handler=self._on_menu_dbg_reset_item,
                             parent=self._context_menu)

        self._context_menu.show_all()

    def _on_update_all_feeds(self, caller):
        """Event handler for toolbarbutton and menu-entries starting update of
        all feeds.
        """
        self._update_feeds()

    def _update_feeds(self, feed_ids=None):
        """Trigger the update of some or all feeds.

        It is usually called from GUI event handler for menus or buttons.
        """
        # create & start the thread for updating
        update_thread = self._app.update_feeds(feed_ids)

        # register data event handler to the running thread
        self.statusbar.register_update_events(update_thread)

# ------
# -- Menu Items
# ------
    def _on_menu_open_website(self, menu_item):
        """
        """
        fid = self._context_menu.context_id
        url = self._app._data._feeds[fid]['htmlUrl']
        self._app.open_in_browser(url)

    def _on_menu_open_feed_url(self, menu_item):
        """
        """
        fid = self._context_menu.context_id
        url = self._app._data._feeds[fid]['xmlUrl']
        self._app.open_in_browser(url)

    def _on_menu_item_update(self, menu_item):
        """Event handler for context menu entry to update all/some feeds.
        """
        item_id = self._context_menu.context_id

        # feed
        if Feed.is_fid_valid(item_id):
            ids = [item_id]
        else:
            ids = self._app._data.get_feed_ids(item_id)

        self._update_feeds(ids)

    def _on_menu_item_unfold(self, menu_item):
        """Event handler for groups context menu 'unfold'.

        The group and all its child groups are unfolded.
        """
        path = self._model._get_path(self._context_menu.context_id)
        self.expand_row(path, True)

    def _on_menu_item_fold(self, menu_item):
        """Event handler for groups context menu 'fold'.

        The group and all its child groups are folded.
        """
        group_id = self._context_menu.context_id

        # set fold state in data layer for this and all child groups
        self._app._data.set_group_unfolded(group_id=group_id,
                                           unfolded=False,
                                           children=True)
        # updated the TreeViews folding state based on the data layer
        self._model.update_folding()

    def _on_menu_item_new_group(self, menu_item):
        """Event handler for groups context menu 'New group'.

        It opens a dialog to name a new sub-group and create it.
        """
        dlg = ReNameDialog(self._window,
                           title=_('New group'),
                           placeholder_text=_('Name of new group'))
        dlg.run()

        if dlg.renamed_value:
            # create new group
            Group(dlg.renamed_value, Group(self._context_menu.context_id))

    def _on_menu_item_mark_read(self, menu_item):
        """Event handler for context menu entry 'mark all read'.
        """
        ids = self._context_menu.context_id

        try:
            # a feed
            feedobj = Feed(ids)
            feedobj.set_read()
            ids = [ids]
        except ValueError:
            try:
                # a group
                groupobj = Group(ids)
                groupobj.set_read()
            except ValueError:
                # a smart group?
                raise NotImplementedError()

    def _on_menu_item_rename(self, menu_item):
        """Event handler for context menu entry 'rename feed/group'.
        """
        self._do_rename_feed_group(self._context_menu.context_id)

    def _do_rename_feed_group(self, item_id):
        """Open dialog to rename feed/group.

        Args:
            item_id (int): Identifier of the tree item.
        """
        # is item a feed or a group
        try:
            groupobj = Group(item_id)
            dlg = ReNameDialog(self._window, groupobj.label)
            dlg.run()
            if dlg.renamed_value:
                groupobj.label = dlg.renamed_value
        except ValueError:
            feedobj = Feed(item_id)
            dlg = ReNameDialog(self._window, feedobj.label)
            dlg.run()
            if dlg.renamed_value:
                feedobj.label = dlg.renamed_value

    def _on_menu_item_delete(self, menu_item):
        """GUI Event handler for context menu entry 'Delete' when the user
        requests the deletion of a feed or group.

        Don't confuse this method with 'self._on_items_has_been_deleted()'
        which is called after an item has been deleted in the data layer.
        """
        item_id = self._context_menu.context_id

        # Group
        if Group.is_gid_valid(item_id):
            self._app._data.delete_group(item_id)
        elif Feed.is_fid_valid(item_id):
            Feed.delete(Feed(item_id))
        else:
            raise Exception('not regular group/feed {}'.format(item_id))

    def _on_menu_dbg_split_group(self, menu_item):
        """ DEBUG """
        root_gid = self._context_menu.context_id
        try:
            root_groupobj = Group(root_gid)
        except ValueError:
            return

        fids = self._app._data.get_feed_ids(root_groupobj.gid)

        g_idx = 0
        while fids:
            g_idx += 1
            splitted_groupobj = Group(f'splitted #{g_idx}', Group(root_gid))
            for idx in range(10 if len(fids) > 9 else len(fids)):
                feedobj = Feed(fids[idx])
                splitted_groupobj.insert_as_last(feedobj)
            fids = fids[10:]

    def _on_menu_dbg_reset_item(self, menu_item):
        """ DEBUG """
        fids = self._context_menu.context_id
        if Group.is_gid_valid(fids):
            fids = self._app._data.get_feed_ids(fids)
        else:
            fids = [fids]

        for feed_id in fids:
            _log.debug('Reset feed {}...'.format(feed_id))
            try:
                fn = self._app._data._feeds[feed_id]['feedentries_filename']
                fn = os.path.join(basics.feed_entries_directory(), fn)
                os.remove(fn)
            except KeyError:
                pass

            try:
                fn = self._app._data._feeds[feed_id]['favicon']['filename']
                fn = os.path.join(basics.feed_favicons_directory(), fn)
                os.remove(fn)
            except KeyError:
                pass
            for key in ['etag',
                        'modified',
                        'max_entry_date',
                        'favicon',
                        'update_mechanism',
                        'feedentries_filename']:
                try:
                    del self._app._data._feeds[feed_id][key]
                except KeyError:
                    pass
            self._app._data._entries[feed_id] = []

        self._model.build_tree()

    def _on_menu_dbg_favicon(self, menu_item):
        """ DEBUG """
        fids = self._context_menu.context_id

        if Group.is_gid_valid(fids):
            fids = self._app._data.get_feed_ids(fids)
        else:
            fids = [fids]

        self._model._favicon_check(fids)

    def _on_menu_dbg_feed_data(self, menu_item):
        """ DEBUG """
        feed_id = self._context_menu.context_id
        feed = self._app._data._feeds[feed_id]
        try:
            print('Feed #{}\n{}'.format(feed_id, json.dumps(feed, indent=4)))
        except Exception as err:
            print(err)
            print(feed)

    def create_toolbar(self):
        """Create a Gtk.Toolbar object and returns it.

        Returns:
            (Gtk.Toolbar): The toolbar object.
        """
        toolbar = Gtk.Toolbar.new()
        # toolbar.set_icon_size(Gtk.IconSize.SMALL_TOOLBAR)

        # update
        btn = create_toolbar_item(icon='gtk-refresh',
                                  tooltip=_('Update all feeds'),
                                  handler=self._on_update_all_feeds,
                                  parent=toolbar)

        # new feed
        btn = create_toolbar_item(icon='gtk-add',
                                  tooltip=_('New feed'),
                                  handler=self._window._on_new_feed,
                                  parent=toolbar)

        if self._app.debug:
            # Debug-Test
            create_toolbar_spaceritem(toolbar)
            icon_debug = 'gtk-execute'
            btn = create_toolbar_item(icon=icon_debug,
                                      tooltip='Debug-Test',
                                      handler=self._window._Debug_Test,
                                      parent=toolbar)

        ## Toolbar of the treeview
        self._toolbar = toolbar
        return toolbar

    def _on_row_collapsed(self, view, it, path):
        """Event handle if an item is folded/collapsed.

        The new unfolded state ('False' in this case) is stored in the
        data-layer. Please see 'self._model._handle_row_folding()' for more
        details.
        """
        self._model._handle_row_folding(it, False)

    def _on_row_expanded(self, view, it, path):
        """Event handle if an item is unfolded/expanded.

        The new unfolded state ('True' in this case) is stored in the
        data-layer. Please see 'self._model._handle_row_folding()' for more
        details.
        """
        self._model._handle_row_folding(it, True)

# ------
# -- Drag & Drop
# ------
    def on_drag_begin(self, widget, context):
        """The drag and drop begins.

        This methode create and set the drag icon.
        """
        model, path = self.get_selection().get_selected_rows()
        text = model[path][:][0]

        stylecontext = widget.get_style_context()

        # new pango layout
        pl = widget.create_pango_layout(text)
        ink_rect, log_rect = pl.get_pixel_extents()
        padding = int(ink_rect.height / 2)
        width = log_rect.width + (padding * 2)
        height = log_rect.height + (padding * 2)

        # There is a difference between cairo versions.
        if cairo.version_info >= (1, 13, 0):
            _surface_format = cairo.Format.RGB24
        else:
            _surface_format = cairo.FORMAT_RGB24

        # surface
        surface = cairo.ImageSurface(_surface_format,
                                     width, height)
        cr = cairo.Context(surface)
        Gtk.render_background(stylecontext, cr, 0, 0,
                              width, height)
        Gtk.render_layout(stylecontext, cr, padding, padding, pl)

        # draw border
        line_width = cr.get_line_width()
        cr.rectangle(-1 + line_width, -1 + line_width,
                     width - line_width,
                     height - line_width)
        cr.stroke()

        # set the surface as drag icon
        Gtk.drag_set_icon_surface(context, surface)

    def on_drag_data_received(self, widget, context, x, y, data, info, time):
        """The drag & drop is finished.

            Args:
                widget (): Widget.
                context (): Context.
                x (int): X-coordinate
                y (int): Y-coordinate
                data (): Data
                info (): Info
                time (): Time
        """
        # dragged id
        id_draged = int(data.get_text())

        # DRAGED object
        groupobj_draged = None
        feedobj_draged = None
        if Group.is_gid_valid(id_draged):
            groupobj_draged = Group(id_draged)
        elif Feed.is_fid_valid(id_draged):
            feedobj_draged = Feed(id_draged)
        else:
            # should never happend
            raise Exception('This should never happen! '
                            f'id_draged = {id_draged}.')

        # destination id
        path_dropped, drop_pos = self.get_dest_row_at_pos(x, y)
        id_dropped = self._model.item_id(path_dropped)

        # "DESTINATION" (drop position) object
        groupobj_dropped = None
        feedobj_dropped = None
        if Group.is_gid_valid(id_dropped):
            groupobj_dropped = Group(id_dropped)
            obj_dropped = groupobj_dropped
        elif Feed.is_fid_valid(id_dropped):
            feedobj_dropped = Feed(id_dropped)
            obj_dropped = feedobj_dropped
        else:
            # drop position is not allowed
            return

        def _sub_error_message_drag_to_child():
            txt = _('You can not drop this here!')
            dlg = Gtk.MessageDialog(parent=self.get_toplevel(),
                                    # title='Error',
                                    text=txt,
                                    type=Gtk.MessageType.ERROR,
                                    buttons=Gtk.ButtonsType.OK)
            txt = _('It makes no sense to drag '
                    'an item to its own child.')
            dlg.format_secondary_text(txt)
            dlg.run()
            dlg.destroy()

        if feedobj_draged:  # FEED was dragged
            # ENTRY to ENTRY
            if feedobj_dropped:
                # if Feed.is_fid_valid(id_dropped):
                _log.debug('ENTRY -> ENTRY')

                # parent group of the feed dropped to
                groupobj_destination = feedobj_dropped.group

                # (into or) before
                if drop_pos in [Gtk.TreeViewDropPosition.BEFORE,
                                Gtk.TreeViewDropPosition.INTO_OR_BEFORE]:
                    insert_mode = 'before'
                # (into or) after
                else:
                    insert_mode = 'after'

            # ENTRY to GROUP
            else:
                _log.debug('ENTRY -> GROUP')

                # into (before/after)
                if drop_pos in [Gtk.TreeViewDropPosition.INTO_OR_BEFORE,
                                Gtk.TreeViewDropPosition.INTO_OR_AFTER]:
                    insert_mode = 'last'
                    groupobj_destination = groupobj_dropped
                # after
                elif drop_pos == Gtk.TreeViewDropPosition.AFTER:
                    insert_mode = 'first'
                    if groupobj_dropped.unfolded:
                        groupobj_destination = groupobj_dropped
                    else:
                        groupobj_destination = groupobj_dropped.parent
                # before
                elif drop_pos == Gtk.TreeViewDropPosition.BEFORE:
                    prev_group = groupobj_dropped.get_previous_group()
                    groupobj_destination = groupobj_dropped.parent
                    insert_mode = 'first'
                    if prev_group is not None:
                        if prev_group.unfolded:
                            insert_mode = 'last'
                            groupobj_destination = Group(prev_group)

            # MOVE IT BABY!
            if insert_mode == 'before':
                _log.debug(f'insert_before(child={feedobj_draged}, '
                           f'afterwards={obj_dropped}')
                groupobj_destination.insert_before(child=feedobj_draged,
                                                   afterwards=obj_dropped)
            elif insert_mode == 'after':
                _log.debug(f'insert_after(child={feedobj_draged}, '
                           f'previous={obj_dropped}')
                groupobj_destination.insert_after(child=feedobj_draged,
                                                  previous=obj_dropped)
            elif insert_mode == 'first':
                _log.debug(f'insert_as_first(child={feedobj_draged})')
                groupobj_destination.insert_as_first(child=feedobj_draged)
            elif insert_mode == 'last':
                _log.debug(f'insert_as_last(child={feedobj_draged})')
                groupobj_destination.insert_as_last(child=feedobj_draged)
            else:
                Exception(f'Should never happen. insert_mode = {insert_mode}')
        else:  # GROUP was draged
            # GROUP to ENTRY
            if feedobj_dropped:  # Feed.is_fid_valid(id_dropped):
                # drop position doesn't matter!
                # it will become the last group of the feeds_entry
                # (id_dropped) parent
                _log.debug('GROUP -> ENTRY')

                # parent group of the feed it was dropped to
                groupobj_destination = feedobj_dropped.group
                insert_mode = 'last'

                # check for the situation that the dest_group is
                # child of the moving_group
                if groupobj_destination.has_ancestor(groupobj_draged):
                    _sub_error_message_drag_to_child()
                    return
            # GROUP to GROUP
            else:
                groupobj_destination = groupobj_dropped
                # if the destination a child of the drag item?
                if groupobj_destination.has_ancestor(groupobj_draged):
                    _sub_error_message_drag_to_child()
                    return

                # into (before/after)
                if drop_pos in [Gtk.TreeViewDropPosition.INTO_OR_AFTER,
                                Gtk.TreeViewDropPosition.INTO_OR_BEFORE]:
                    insert_mode = 'last'
                # after
                elif drop_pos == Gtk.TreeViewDropPosition.AFTER:
                    insert_mode = 'first'
                    # group is folder or empty
                    if (not groupobj_dropped.unfolded
                        or (not groupobj_destination.groups
                            and not groupobj_destination.feeds)):
                        insert_mode = 'after'
                        groupobj_destination = groupobj_destination.parent
                # before
                if drop_pos == Gtk.TreeViewDropPosition.BEFORE:
                    prev_group = groupobj_dropped.get_previous_group()
                    groupobj_destination = groupobj_destination.parent
                    if prev_group is None:
                        insert_mode = 'first'
                    elif prev_group.unfolded:
                        insert_mode = 'last'
                        groupobj_destination = Group(prev_group)
                    else:
                        insert_mode = 'before'

            # move it baby!
            if insert_mode == 'before':
                _log.debug(f'insert_before(child={groupobj_draged}, '
                           f'afterwards={obj_dropped}')
                groupobj_destination.insert_before(child=groupobj_draged,
                                                   afterwards=obj_dropped)
            elif insert_mode == 'after':
                _log.debug(f'insert_after(child={groupobj_draged}, '
                           f'previous={obj_dropped}')
                groupobj_destination.insert_after(child=groupobj_draged,
                                                  previous=obj_dropped)
            elif insert_mode == 'first':
                _log.debug(f'insert_as_first(child={groupobj_draged})')
                groupobj_destination.insert_as_first(child=groupobj_draged)
            elif insert_mode == 'last':
                _log.debug(f'insert_as_last(child={groupobj_draged})')
                groupobj_destination.insert_as_last(child=groupobj_draged)
            else:
                Exception(f'Should never happen. insert_mode = {insert_mode}')

        self._model.build_tree()
        self._model.update_folding()
        self.pulse_item(id_draged)

    def on_drag_data_get(self, widget, context, data, info, time):
        """
        """
        # related iid
        model, path = self.get_selection().get_selected_rows()
        item_id = model.item_id(path)
        # _log.debug('in dd-get: iid:{}'.format(iid))

        # remember the iid for the drag destination
        data.set_text(str(item_id), -1)

# ------
# -- Misc
# ------

    def _on_query_tooltip(self, widget, x, y, keyboard_mode, tooltip):
        """The handler is called when a tooltip could be displayed. The
        decision about it is done in the handler itself.

        Args:
            ...

        Returns:
            bool: True if the tooltip should be displayed and False if not.
        """
        # get treeview specific informations
        row, x, y, model, path, treeiter \
            = widget.get_tooltip_context(x, y, keyboard_mode)

        # is it a data holding row?
        if not row:
            return False

        item_id = model.item_id(treeiter)

        # is it an error-item (which is child of a feed)
        if item_id == -1:
            parent_iter = model.iter_parent(treeiter)
            item_id = model.item_id(parent_iter)

        # create the text
        txt = self._create_tooltip_text(item_id)

        # no text == no tooltip
        if txt == '':
            return False

        tooltip.set_markup(txt)
        return True

    def _create_tooltip_text(self, item_id):
        """Create the mouse over tooltip text for the given tree item.

        Args:
            item_id(int):

        Returns:
            (string): The tooltip text.

        Raises:
            KeyError: If iid is not a feed.
        """
        text = ''
        feedobj = None

        try:
            feedobj = Feed(item_id)
            # error message
            if feedobj.fid in self._model._err_paths:
                msg = self._model._err_paths[feedobj.fid][1]
                text = msg
            else:
                # feed URL
                url = feedobj.xmlUrl
                url = url.replace('&', '&amp;')
                text = '<b>URL</b>: {}'.format(url)
        except ValueError:
            try:
                groupobj = Group(item_id)
                nf = len(groupobj.feed_ids)
                ng = len(groupobj.group_ids)
                text = '{} {}'.format(nf, _('feeds'))
                if ng > 0:
                    text += ' in {} {}'.format(ng, _('groups'))
            except ValueError:
                pass

        # DEBUG infos
        if self._app.debug:
            if text is not '':
                text += '\n\n'
            text += '<tt>=== Debug informations ===</tt>\n'
            # iid
            text += '<b>ID</b>: {}'.format(item_id)

            # update mechanism
            if feedobj:
                um = feedobj.update_mechanism
                um = data.UpdateMecha.name(um)
                # um = '(n.a.)'
                text += '\n<b>Update-mechanism</b>: {}'.format(um)
                text += ' ({})'.format(feedobj.update_mechanism)

        return text

    def _on_select(self, selection):
        """Tree item is selected.

        One left mouse click highlights an item in the tree.
        """
        # get iterator of selected item
        model, treeiter = selection.get_selected()
        if not treeiter:
            return

        # get related item id
        item_id = model.item_id(treeiter)
        # identify the feed (or group) to the listview
        self._feed_entries_listview.feeds_id = item_id
        # enable the listview
        self._feed_entries_listview.do_disable(False)
        # show feed entries in the listbox
        self._feed_entries_listview.populate_feed()

    def on_update_test_finished(self):
        """
        """
        raise Exception('was passiert hier?')
        self._model.add_dev_info_update_mechanism()
        if self._app.debug:
            ## '_dev' gibts doch gar nicht mehr???
            self.expand_row(self._model._get_path('_dev'), True)

    def _on_import_start(self):
        """Event hanlder if a feed import from a file begins.

        It de-activates the 'FeedsData.event_new_tree_item' while import.
        See 'self.on_import_end' also.
        """
        callback = self._on_rebuild_tree
        self._app._data.event_tree_item_new_or_modified.deregister(callback)
        Feed.event_feed_new_or_modified.deregister(callback)

    def _on_import_end(self, imported_group):
        """Event handler if a feed import from a file finished.

        It re-activates 'FeedsData.event_new_tree_item' which causes visual
        updates of 'TreeView'.

        Args:
            imported_group (group.Group): Parent group.
        """
        # re-activate the new-item-event and fire it
        callback = self._on_rebuild_tree
        self._app._data.event_tree_item_new_or_modified \
            .register(callback)
        self._app._data.event_tree_item_new_or_modified \
            .notify(imported_group)
        # Warum hier nur Feed? Nicht auch Group?
        Feed.event_feed_new_or_modified.register(callback)
        Feed.event_feed_new_or_modified.notify(imported_group)

    def _on_rebuild_tree(self, tree_item_id=None):
        """Handler invoking a rebuilding of the tree.

        Args:
            tree_item (Group|Feed): The item is temporary highlighted.

        It can be used if a feed/group was created, moved or deleted.
        """
        self._model.build_tree()
        self._model.update_folding()
        if tree_item_id is not None:
            self.pulse_item(tree_item_id)

    def pulse_item(self, tree_item_id):
        """The item is highlighted for a short time via a pulse like
        modification of its background color.

        Args:
            tree_item (int): ID of the item to highlight.
        """
        tree_item_id

        def _do_mod_color(item_id, color):
            self._model.label_background_color(item_id, color)
            # stop this timer event
            return False

        # unfold till the item
        item_path = self._model._get_path(tree_item_id)
        self.expand_to_path(item_path)

        # the color
        color = Gdk.RGBA()
        color.parse(basics.COLOR_HIGHLIGHT)
        color.alpha = 1
        for intervall_offset in range(40):
            color.alpha = color.alpha - 0.025
            func = partial(_do_mod_color, tree_item_id, color.copy())
            GLib.timeout_add(15 * intervall_offset, func)
