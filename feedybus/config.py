# -*- coding: utf-8 -*-
import os
import logging
import json
from configobj import ConfigObj
from datetime import date
from feedybus import basics

"""
    Configuration data object for all parts of Feedybus.

    This configuration comes from two files. The ini-file 'feedybus.conf' is
    human readable and self explaining. The json-file 'feedybus.misc' is not
    intended to be human readable and stores application intern data. See the
    debug output, the log-file or basics.py to find out where the files are
    stored.
"""


_log = logging.getLogger(basics.GetLogId(__file__))


class Config (ConfigObj):
    """
    Hold config-data.

    Manage, hold and store configration data for MukiMuki.

    Members:
        _conf (configparser.ConfigParser):
            Access to the human-readable config file.
        _misc (dictionary):
            Store metric information about gui-elements depending
            on the size of the display.
            For windows and dialogs it store dimensions (size
            and position) in and instance of class 'WindowDimensions'
            indexed by the class-name of the gui-element.
            For list controls it store an object of
            class 'ListCtrlInfo' indexed by a tuple of the class-name
            and the number of columns of the listctrl.
            Format:
            {(x-y display resolution:
                {windowClass: class WindowDimensions}
                {tuple(listCtrlClass, column_count): class ListCtrlInfo}
            }
    """
    def __init__(self):
        """Constructor of the configuration object read the config file or
        create a default one."""
        try:
            # read config from file
            super(Config, self).__init__(infile=basics.config_filename(),
                                         encoding='utf-8',
                                         file_error=True,
                                         create_empty=False)
            print('Read configuration from {}.'.format(self.filename))
        except IOError as err:
            # create default config
            print('{} Will create a default config.'.format(err))
            content = basics.default_config_string().split('\n')
            super(Config, self).__init__(infile=content,
                                         encoding='utf-8',
                                         file_error=True)
            self.filename = basics.config_filename()
            self._write_config_obj()

        self._load_miscconfig()

    def _load_miscconfig(self):
        """Load the misc-configuration file to `self._displays`.
        """
        self._misc = dict()
        try:
            fn = basics.miscconfig_filename()
            with open(fn, 'r', encoding='utf-8') as f:
                print('Read MISC-configuration file from {}'.format(fn))
                content = f.read()
                if len(content) > 0:
                    self._misc = json.loads(content)
        except FileNotFoundError as err:
            print('{} Will create a default misc config.'.format(err))
            self._misc = basics.default_misc_config_data()
            self._write_misc_config()

    def _write_misc_config(self):
        """Store the misc-config data from `self._misc`.
        """
        fn = basics.miscconfig_filename()
        try:
            # MISC file
            _log.info('Save MISC-configuration to {}.'.format(fn))
            with open(fn, 'w', encoding='utf-8') as f:
                json.dump(self._misc, f, indent=4)

        except FileNotFoundError as err:
            _log.error(err)
            raise err

    def write_all_config(self):
        """Store all config data.
        """
        self._write_misc_config()
        self._write_config_obj()

    def _write_config_obj(self):
        """Store the ConfigObj itself.
        """
        try:
            # regular config file
            _log.info('Save config to {}.'.format(self.filename))
            self.write()

        except FileNotFoundError as err:
            _log.error(err)
            raise err

    def _handle_screen_key_error(self, screen_id, window_class):
        """If no section for the current screen and/or window exists in the
        config file that it is created.

        Args:
            screen_id (str): Identifier of the screen.
            window_class (class): Class of the window.
        """
        if screen_id not in self._misc:
            self._misc[screen_id] = {str(window_class): {}}
        else:
            self._misc[screen_id][str(window_class)] = {}

    def set_geometry_win(self, screen_id, window_class,
                         win_pos, win_size, maximized):
        """Store the position and size of main window related to the current
        screen which is identified by 'screen_id'.

        Args:
            screen_id (str): Screen identifier.
            window_class (str): Class name of window.
            win_pos ((int, int)): Tuple with x- and y-position of the window.
            win_size ((int, int)): Tuple with width and height of the window.
            maximized (bool): Window maximized. If True position and size of
                the window are not stored.
        """
        try:
            if not maximized:
                self._misc[screen_id][window_class]['win_pos'] = win_pos
                self._misc[screen_id][window_class]['win_size'] = win_size
            self._misc[screen_id][window_class]['maximized'] = maximized

        except KeyError:
            self._handle_screen_key_error(screen_id, window_class)
            self.set_geometry_win(screen_id, window_class, win_pos,
                                  win_size, maximized)

    def set_geometry_sash(self, screen_id, window_class, pos):
        """Store the sash position of the Gtk.Paned splitt-window related to
        the current screen which is identified by 'screen_id'.

        Args:
            screen_id (str): Screen identifier.
            window_class (str): Class name of window.
            pos (int): Position of sash on x-axis.
        """
        try:
            self._misc[screen_id][window_class]['sash_pos'] = pos
        except KeyError:
            self._handle_screen_key_error(screen_id, window_class)
            self.set_geometry_sash(screen_id, window_class, pos)

    def get_geometry(self, screen_id, window_class):
        """Return the geometry related to a specific screen (screen_id) and
        window (window_class).

        Args:
            screen_id (str): Screen identifier.
            window_class (str): Class name of the window/dialog.

        Returns:
            dict: {'win_pos': (x, y),
                   'win_size': (width, height),
                   'sash_pos': int,
                   'maximized': bool}
        """
        try:
            return self._misc[screen_id][window_class]
        except KeyError:
            return None

    @property
    def last_feed_update_test_year(self):
        return self._misc['last_feed_update_test_year']

    def set_last_feed_update_test(self, year=None):
        """Set the value of 'last_feed_update_test_year' in the misc-config
        file.

        Args:
            year (int): If 'None' (default) the current year is used.
        """
        if not year:
            year = date.today().year

        self._misc['last_feed_update_test_year'] = year

    def _get_value(self, sec_name, key, default):
        """Return the specified config value.

        In the case that the value or the section does not exist in the config
        file/object it is created.

        Args:
            sec_name (str): Name of config section.
            key (str): Key/name of the config option.
            default (): Default value of the config option.

        Returns:
            (): Value of the config option.
        """
        # get the section
        try:
            sec = self[sec_name]
        except KeyError:
            # create the section
            self[sec_name] = {}
            sec = self[sec_name]

        # the value
        try:
            return sec[key]
        except KeyError:
            # set default value
            sec[key] = default
            return sec[key]

    @property
    def language(self):
        """Return the language specifier used for the application.

        Returns:
            (str): Language specifier.
        """
        return self._get_value('GENERAL', 'language', '')

    @property
    def open_buttons(self):
        """Return the open buttons config.

        It is a commaseparated list where each element represents a button in
        the feed entries view. The number means how many feeds will be opened
        at once when clicking on that button.

        Returns:
            (list(int)): Values.
        """
        return self._get_value('GUI', 'open_buttons', ['3', '5', '10'])

    @property
    def log_max_size(self):
        """Maximum size of a log file in KiloByte.

        Returns:
            (int): Size in KB
        """
        return self._get_value('LOG', 'max_size_kb', 1000)

    @property
    def log_max_files(self):
        """ Maximum number log files (current + backups).

        Returns:
            (int): Number of files.
        """
        return self._get_value('LOG', 'max_files_n', 4)

    @property
    def debug(self):
        """Debug messages in log file and stdout/terminal.

        This represents only the setting in the config file and not the debug
        state of the application itself. The config can be overriden via shell
        switch.

        Returns:
            (bool): Debug value.
        """
        return self._get_value('GENERAL', 'debug', False)

    @property
    def gui_row_height_fx(self):
        """ Height of the row of feed entrie list depending on text height.

        Returns:
            (float): Factor of row height.
        """
        return self._get_value('GUI', 'row_height_factor', 1.4)
