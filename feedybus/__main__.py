#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import os.path
import logging
import logging.handlers
import colorlog
import docopt
import gettext
trans = gettext.translation('feedybus', 'locales', fallback=True)  # noqa
trans.install()  # noqa
import gi
gi.require_version('Gtk', '3.0')  # noqa
from gi.repository import Gtk
from feedybus.application import FeedybusApplication
from feedybus.window import FeedybusWindow
from feedybus import basics
from feedybus import config


docopt_string = """Feedybus.

Usage:
    feedybus [--profile=<name>] [-d | --debug] [-h | --help]

Options:
    --profile=<name>    Profile name.
    -d --debug          Switch debug output on.
    -h --help           Show this screen.
"""


_LOGID = basics.GetLogId()


def _SetupLogging(size, n, debug):
    """Setup the logging mechanism and return the logger object.

    Args:
        size (int): Maximum size of a log file in KiloBytes.
        n (int): Maximum number of log files keeped.
        debug (bool): Debug-Level on or off

    Returns:
        logging.Logger:
    """
    # logger
    log = logging.getLogger(_LOGID)

    # level of the logger
    if debug is True:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)

    # file handler
    fh = logging.handlers.RotatingFileHandler(basics.log_filename(),
                                              maxBytes=int(size) * 1024,
                                              backupCount=int(n) - 1)
    fstr = '%(asctime)s - %(levelname)s:%(name)s - %(message)s'
    dstr = '%Y-%m-%d %H:%M'
    formatter = logging.Formatter(fstr, dstr)
    fh.setFormatter(formatter)
    log.addHandler(fh)

    # console handler
    colorlog.default_log_colors['DEBUG'] = 'bold_white'
    sh = logging.StreamHandler()
    fstr = '%(log_color)s%(levelname)-8s%(reset)s%(message)s'
    formatter = colorlog.ColoredFormatter(fstr)
    sh.setFormatter(formatter)
    log.addHandler(sh)

    #
    log.info('Now logging to {} using log-level {}.'
             .format(basics.log_filename(),
                     ('DEBUG' if debug is True else 'INFO')))

    return log


if __name__ == '__main__':
    """Main routine.
    """
    args = docopt.docopt(docopt_string)
    basics.set_profile(args['--profile'])
    basics.check_directory_existance()

    # Config
    cfg = config.Config()

    # Logging
    if args['--debug']:
        debug_lvl = True
    else:
        debug_lvl = cfg.debug
    _log = _SetupLogging(size=cfg.log_max_size,
                         n=cfg.log_max_files,
                         debug=debug_lvl)

    # Starting...
    _log.info('Starting{}'.format('.' * 35))
    profile = basics.get_profile()
    if profile != '':
        _log.info('Using profile "{}"'.format(profile))
    application = FeedybusApplication(config=cfg)
    window = FeedybusWindow(application)
    window.show_all()
    Gtk.main()
