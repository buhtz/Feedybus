# -*- coding: utf-8 -*-
import os
import os.path
import subprocess
import logging
from contextlib import contextmanager
from slugify import slugify  # Package "python-slugify" NOT "slugify"

_APPNAME = 'feedybus'
_APPNAME_SHOW = 'Feedybus'
_APP_WEBSITE = 'https://codeberg.org/buhtz/Feedybus'
_APP_EMAIL = 'c.buhtz@posteo.jp'

_APPVER = '0.0.1'
_APPVER_MAJOR = 0
_APPVER_MINOR = 0
_APPVER_REVISION = 1
_APPVER_EXTRA = 'a'
_APPVER_STR = '{}.{}.{}{}' \
              .format(_APPVER_MAJOR,
                      _APPVER_MINOR,
                      _APPVER_REVISION,
                      _APPVER_EXTRA)

USER_AGENT = '{} {}'.format(_APPNAME_SHOW, _APPVER_STR)

_FEEDFILE = 'feedybus.feeds'
_CONFFILE = 'feedybus.conf'  # human readable (INI format)
_MISCCONFFILE = 'feedybus.misc'  # machine readable (JSON format)
_LOGFILE = 'feedybus.log'
_ENTRIES_FILE_EXT = 'entries'
_ENTRIES_FILE_DIR = _ENTRIES_FILE_EXT
_ICONS_FILE_DIR = 'favicons'

# IDs for groups in the feeds tree
GROUPID_FEEDS = 0
GROUPID_INVALID = -1
GROUPID_LATER = -10
GROUPID_UNREAD = -20
# GROUPID_DEV = -30
GROUPID_MIN = -1000

# Colors (e.g. for use with Gdk.RGBA.parse())
## Color indicates error messages (e.g. in TreeView)
COLOR_ERROR = '#b2182b'
## Color indicates info messages (e.g. in TreeView)
COLOR_INFO = '#2166ac'
## Color used for highlight a item in TreeView
COLOR_HIGHLIGHT = '#da950b'

## Name of the profile.
_PROFILE = None


def set_profile(profile):
    """Set the name of the profile.

    Args:
        profile (str): Name of profile.
    """
    global _PROFILE
    _PROFILE = profile


def get_profile():
    """Return the profile name.

    Returns:
        (str): Name of profile.
    """
    return _PROFILE


def GetLogId(the_file=None):
    """Create a id string for the logger using 'the_file'.

    Args:
        the_file (str):

    Returns:
        str: The id.
    """
    if the_file:
        return _APPNAME + '.' + os.path.basename(the_file)
    else:
        return _APPNAME


_log = logging.getLogger(GetLogId(__file__))


class Event:
    """
        See https://stackoverflow.com/a/48339861/4865723
    """
    def __init__(self):
        """Constructor.
        """
        self._callbacks = []

    def notify(self, *args, **kwargs):
        """Notify registered observers.

        The args, and kwargs are given to te observers.
        """
        for callback in self._callbacks:
            callback(*args, **kwargs)

    def register(self, callback):
        """Register an observer (callback function).
        """
        if callback not in self._callbacks:
            self._callbacks.append(callback)
        return callback

    def deregister(self, callback):
        """Deregister / remove an observer (callback function).
        """
        self._callbacks.remove(callback)

    @contextmanager
    def keep_silent(self):
        """A context manager function to suppress notification's of the
           observers."""
        try:
            self._silent_callbacks = self._callbacks
            self._callbacks = []
            yield
        finally:
            self._callbacks = self._silent_callbacks


def check_directory_existance():
    """Checks if the needed directories are there and create them if not.
    """
    for d in [_config_directory(),
              _log_directory(),
              _data_directory(),
              feed_entries_directory(),
              feed_favicons_directory()]:
        if not os.path.exists(d):
            # print('>>> Create "{}"'.format(d))
            os.makedirs(d)


def git_branch():
    """Return the current checkout git branch if there is a repository.

    If git is not present the return value is just empty.

    Returns:
        (str): Branch name.
    """
    branch = ''

    if not os.path.exists('.git'):
        return branch

    cmd = ['git', 'symbolic-ref', '--short', 'HEAD']

    try:
        branch = subprocess.check_output(cmd).decode().replace('\n', '')
    except FileNotFoundError:
        pass

    return branch


def git_revision():
    """Return git revision if there is a repository.

    If this is a git repository it return the current revison as a string. If
    not the string is empty.

    Returns:
        str:
    """
    rev = ''

    if not os.path.exists('.git'):
        return rev

    cmd = ['git']
    cmd += ['log']
    cmd += ['-n']
    cmd += ['1']
    cmd += ['--format=%h']

    try:
        rev = subprocess.check_output(cmd).decode().replace('\n', '')
    except FileNotFoundError:
        pass

    return rev


def _filename_with_profile(fn):
    """
    """
    # print('In filename_with_profile() -- {}'.format(_PROFILE))
    if not _PROFILE:
        return fn

    idx = fn.rindex('.')
    return fn[:idx] + '.' + _PROFILE + fn[idx:]


def _log_directory():
    """Return the full directory for log files as a string.
        e.g. ~/.local/share/feedybus
    """
    return _data_directory()


def log_filename():
    """
        Full path and nam of the log file.
        Returned as a string.
    """
    fn = _filename_with_profile(_LOGFILE)
    return os.path.join(_log_directory(), fn)


def _config_directory():
    """
        Return the full directory for config files as a string.
        e.g. ~/.config/feedybus
    """
    root = os.environ.get('XDG_CONFIG_HOME',
                          os.path.join(os.environ['HOME'], '.config'))
    return os.path.join(root, _APPNAME)


def config_filename():
    """Full path and filename of the config file.

    Returns:
        str:
    """
    fn = _filename_with_profile(_CONFFILE)
    return os.path.join(_config_directory(), fn)


def miscconfig_filename():
    """Full path and filename of the miscconfig file.

    Returns:
        str:
    """
    fn = _filename_with_profile(_MISCCONFFILE)
    return os.path.join(_data_directory(), fn)


def _data_directory():
    """
        Return the full directory for data/json/feed files as a string.
        e.g. ~/.feedybus
    """
    root = os.environ.get('XDG_DATA_HOME',
                          os.path.join(os.environ['HOME'], '.local', 'share'))

    return os.path.join(root, _APPNAME)


def feed_entries_directory():
    """Return the directory for saving feed entry files.

    Returns:
        (str): Full path.
    """
    fn = _ENTRIES_FILE_DIR
    if _PROFILE:
        fn = fn + '.' + _PROFILE
    return os.path.join(_data_directory(), fn)


def feed_favicons_directory():
    """Return the directory for saving favicons.

    Returns:
        (str): Full path.
    """
    fn = _ICONS_FILE_DIR
    if _PROFILE:
        fn = fn + '.' + _PROFILE
    return os.path.join(_data_directory(), fn)


def feeds_filename():
    """
        Full path and nam of the feed-data holding json-file.

        Returns:
            (str): Full path and filename.
    """
    fn = _filename_with_profile(_FEEDFILE)
    return os.path.join(_data_directory(), fn)


def create_feed_entries_filename(title):
    """Create a unique and not existing filename for the entries file.

    See '_create_feed_full_filename()' for more details.

    Args:
        title (str): Title of the feed.

    Returns:
        (str): The full path.
    """
    return _create_feed_full_filename(title,
                                      _ENTRIES_FILE_EXT,
                                      feed_entries_directory())


def create_feed_favicon_filename(title, file_extension):
    """Create a unique and not existing filename for the feeds favicon.

    See '_create_feed_full_filename()' for more details.

    Args:
        title (str): Title of the feed.
        file_extension (str): Icon format (e.g. png, ico).

    Returns:
        (str): The full path.
    """
    return _create_feed_full_filename(title,
                                      file_extension,
                                      feed_favicons_directory())


def _create_feed_full_filename(title, file_extension,
                               directory, extend_with=None):
    """Create a unique and not existing filename with full path out of the
    title of a feed.

    By default it will extend the string with 'extend' until the filename does
    not exist on the system.

    Args:
        title (str): The feed title.
        file_extension (str): File extension (suffix) to use.
        directory (str): The directory.
        extend_with (int): A number attached to the filename if it exists
                           (default: None).

    Returns:
        (str): The name and path of the file as a string.
    """
    filename = slugify(title, separator='_', max_length=110)
    if extend_with:
        filename = filename + str(extend_with)
    filename = filename + '.' + file_extension
    filename = os.path.join(directory, filename)

    if os.path.exists(filename):
        if extend_with is None:
            extend_with = 0
        filename = _create_feed_full_filename(title,
                                              file_extension,
                                              directory,
                                              extend_with + 1)

    return filename


def _DefaultFeeds():
    """Create feeds for default and return them as a dict.

    Returns:
        dict: Feeds & Groups data structure
    """
    # return json.loads(opml_import.opml_to_json('bamboo.opml'))

    # Tree structure (like folders or catagories) in which the feeds are
    # organized
    tree_groups = [
        [19, 18],
        (-1100, 'Science', False, [
            [1, 2]
        ]
        ),
        (-1101, 'Politics', False, [
            [3, 12]
        ]
        ),
        (-1102, 'Technology', False, [
            [4],
            (-1103, 'Software', True, [
                [5, 6, 7]
            ]
            ),
            (-1104, 'Misc', False, [[]])
        ]
        ),
        (-1105, 'Japan', False, [
            [8, 9]
        ]
        ),
        (-1106, 'misc', False, [
            [17, 16, 15, 14, 13, 10, 11]
        ]
        ),
        (-1107, 'empty group', False, [
            []
        ]
        )
    ]

    # The feeds themself. The keys (e. g.) specifying the position in the
    # groups tree structure. The part before the '/' is sepcify the key of the
    # group (see 'tree_groups' and after '/' is the position in the
    # group/node.
    feed_list = {
        1:
            {
                "title": "Cochrane Library - Highlighted Reviews",
                "text": "Cochrane Library - Highlighted Reviews",
                "htmlUrl": "http://www.cochranelibrary.com/",
                "type": "rss",
                "xmlUrl": "http://www.cochranelibrary.com/rss/"
                          "highlighted-reviews/"
            },
        3:
            {
                "title": "netzpolitik.org",
                "text": "netzpolitik.org",
                "htmlUrl": "",
                "type": "rss",
                "xmlUrl": "https://netzpolitik.org/feed/"
            },
        4:
            {
                "title": "Posteo.de - Aktuelles",
                "text": "Posteo.de - Aktuelles",
                "htmlUrl": "",
                "type": "rss",
                "xmlUrl": "https://posteo.de/blog/feed?format=atom"
            },
        5:
            {
                "title": "JabRef | Blog",
                "text": "JabRef | Blog",
                "htmlUrl": "https://blog.jabref.org/",
                "type": "rss",
                "xmlUrl": "http://blog.jabref.org/feed.xml"
            },
        8:
            {
                "title": "Japan Almanach: Tabibito's Japan-Blog",
                "text": "Japan Almanach: Tabibito's Japan-Blog",
                "htmlUrl": "",
                "type": "rss",
                "xmlUrl": "http://www.tabibito.de/japan/blog/feed/atom/"
            },
        9:
            {
                "title": "SALZ Tokyo",
                "text": "SALZ Tokyo",
                "htmlUrl": "http://www.salz-tokyo.com/",
                "type": "rss",
                "xmlUrl": "http://www.salz-tokyo.com/feed/"
            },
        6:
            {
                "title": "Free Software Foundation - News",
                "text": "Free Software Foundation - News",
                "type": "rss",
                "xmlUrl": "https://www.fsf.org/static/fsforg/rss/news.xml"
            },
        7:
            {
                "title": "Debian package news for feedparser",
                "text": "Debian package news for feedparser",
                "type": "rss",
                "xmlUrl": "https://tracker.debian.org/pkg/feedparser/rss"
            },
        10:
            {
                "title": "SRToolbox (Twitter)",
                "text": "SRToolbox (Twitter)",
                "htmlUrl": "http://twitrss.me/twitter_user_to_rss/"
                           "?user=SRToolbox",
                "type": "rss",
                "xmlUrl": "http://twitrss.me/twitter_user_to_rss/"
                          "?user=SRToolbox"
            },
        11:
            {
                'title': 'MarketWatch.com - MarketPulse',
                'text': 'MarketWatch.com - MarketPulse',
                'htmlUrl': 'https://www.marketwatch.com/search/'
                           '?query=&mode=Keyword&modeparam=91&siteid=rss',
                'type': 'rss',
                'xmlUrl': 'http://feeds.marketwatch.com/'
                          'marketwatch/marketpulse/'
            },
        2:
            {
                "title": "Cochrane Library - Special Collections",
                "text": "Cochrane Library - Special Collections",
                "htmlUrl": "http://www.cochranelibrary.com/",
                "type": "rss",
                "xmlUrl": "http://www.XXXanelibrary.com/rss/"
                          "special-collections/"
            },
        12:
            {
                "title": "Digitalcourage e.V.",
                "text": "Digitalcourage e.V.",
                "htmlUrl": "https://digitalcourage.de/rss.xml",
                "type": "rss",
                "xmlUrl": "https://digitalcourage.de/rss.xml"
            },
        13:
            {
                "text": "Canadian Journal Of Nursing Research",
                "type": "rss",
                "title": "Canadian Journal Of Nursing Research",
                "htmlUrl": "http://www.ingentaconnect.com/content/"
                           "mcgill/cjnr/latest",
                "xmlUrl": "http://api.ingentaconnect.com/content/"
                          "mcgill/cjnr/latest?format=rss"
            },
        14:
            {
                "text": "Resuscitation",
                "title": "Resuscitation",
                "xmlUrl": "http://www.resuscitationjournal.com/"
                          "current.rss",
                "htmlUrl": "http://www.resuscitationjournal.com/"
                           "issues?publicationCode=resus&rss=yes",
                "type": "rss"
            },
        15:
            {
                "xmlUrl": "http://www.internationalemergencynursing.com/"
                          "current.rss",
                "type": "rss",
                "htmlUrl": "http://www.internationalemergencynursing.com/"
                           "issues?publicationCode=ienj&rss=yes",
                "title": "International Emergency Nursing",
                "text": "International Emergency Nursing"
            },
        16:
            {
                "text": "Forschung & Lehre",
                "xmlUrl": "http://www.forschung-und-lehre.de/wordpress/"
                          "?feed=rss2",
                "title": "Forschung & Lehre",
                "type": "rss",
                "htmlUrl": "http://www.forschung-und-lehre.de/wordpress"
            },
        17:
            {
                "xmlUrl": "http://ejournals.library.ualberta.ca/index"
                          ".php/EBLIP/gateway/plugin/"
                          "WebFeedGatewayPlugin/rss2",
                "htmlUrl": "http://ejournals.library.ualberta.ca/index"
                           ".php/EBLIP",
                "type": "rss",
                "title": "Evidence Based Library And Information Practice",
                "text": "Evidence Based Library And Information Practice"
            },
        18:
            {
                "type": "rss",
                "text": "Leopoldina (Press)",
                "title": "Leopoldina (Press)",
                "htmlUrl": "http://www.leopoldina.org/",
                "xmlUrl": "http://www.leopoldina.org/de/presse/"
                          "rss-nachrichten/"
            },
        19:
            {
                "htmlUrl": "http://dgi-info.de",
                "text": "Deutsche Gesellschaft F\u00fcr Information &"
                        " Wissen e.V.",
                "title": "Deutsche Gesellschaft F\u00fcr Information &"
                         " Wissen e.V.",
                "type": "rss",
                "xmlUrl": "http://dgi-info.de/feed/"
            }
    }

    # entries of for each feed (currently empty)
    entries = {}
    for feed_id in feed_list:
        entries[feed_id] = []

    return {
        'tree': tree_groups,
        'feeds': feed_list,
        'entries': entries
    }


def default_misc_config_data():
    """
    """
    return {
        'last_feed_update_test_year': 0
    }


def default_config_string():
    """Create the default config string in INI format for ConfigParser.

    Returns:
        str:
    """
    return '''[GENERAL]
    # Per default (if empty) if available the systems own local is used.
    # If not available the applications native language (english) is used as
    # fallback.
    language = ""
    # debug messages in log file and stdout/terminal
    debug = False

    [LOG]
    # maximum size of a log file in KiloByte
    max_size_kb = 1000
    # maximum number log files (current + backups)
    max_files_n = 4

    [GUI]
    # Height of the row of feed entrie list depending on text height.
    row_height_factor = 1.4
    # Kommaseparated list where each element represents a button in the
    # feed entries view. The number means how many feeds will be opened at
    # once when clicking on that button
    open_buttons = 3, 5, 10'''
