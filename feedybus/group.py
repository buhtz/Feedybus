# -*- coding: utf-8 -*-
import logging
from xml.etree import ElementTree
import feedybus.basics as basics
import feedybus.feed as feed
import feedybus.data as data


"""
    The Group class is a logical representation of a group of feeds.

    The Group class is a representation of a feed group. This class belongs to
    the application layer and give you access to the data layer. Please see
    the constructor '__init()__' for details.

    TODO: Add details about valid group ids.
"""


_log = logging.getLogger(basics.GetLogId(__file__))


class Group:
    """A feed group in the application layer.
    """
# ------------
# --- Class events
# ------------
    ## Event (as class attriute): Group created or modified.
    event_group_new_or_modified = basics.Event()

    ## Event (as class attribute): Group deleted.
    event_group_deleted = basics.Event()

# ------------
# --- Class members
# ------------
    ## Unread entries index
    smart_group_index_unread = dict()

    ## Read later marked entries index
    smart_group_index_later = dict()

    @staticmethod
    def count_read_later_entries():
        """Return the number of entries marked as 'read later' in all Feeds.

        The index 'Group.smart_group_index_later' is updated while that.
        """
        later = 0
        for fid in data.FeedsData.me.get_feed_ids():
            fobj = feed.Feed(fid)
            later += len(fobj.get_read_later_entries())

        return later

    @staticmethod
    def delete(group):
        """Delete a group and all its children.

        Each element is deleted from the datastructure considering the smart
        groups 'Unread' and 'Read Later'. At the end
        'Group.event_group_deleted' is fired.

        Args:
            group(Group): A Group instance.
        """
        # delete child groups (and their feeds)
        for child_groupobj in group.groups:
            # keep in mind: This file fire events for each sub-group
            Group.delete(child_groupobj)

        # child feeds
        for child_feedobj in group.feeds:
            # keep in mind: This will fire events for each feed
            feed.Feed.delete(child_feedobj)

        # Do not delete if it is root. In that case an AttributeError
        # is raised.
        try:
            # finaly delete from data structure
            group.parent.remove(group)
        except AttributeError:
            return

        # notify
        Group.event_group_deleted.notify(group.gid)

# ------------
# --- Basic methods
# ------------

    def __init__(self, *args):
        """Create a group instance. See details for allowed parameters and
        different behaviours.

        Mode A - Use for existing group:
        If a group-id is used the instance is connected to its representation
        in the data layer. If no group with that id exists in the data layer a
        ValueError is raised. The group-id is allowed as single only -
        TypeError is raised if not.
        Example:
            existing_group = Group(-1103)

        Mode B - Create a new group:
        Give a name/label (as string) will create a new group in the data
        layer. As second argument another Group instance can be used to name
        it as the parent group. If not the root group is used by default as
        parent.
        Example:
            fresh_group = Group('New group with root as default parent')
            fresh_group = Group('New group', Group(-1103))

        Using no arguments will cause an TypeError.
        Using keyword arguments will cause an TypeError.

        Args:
            *args: See method description for allowed parameters.

        Raises:
            ValueError: If group-id does not exists or is out of the valid
            range.
            TypeError: If arguments of wrong type, wrong number or named with
            keywords.
        """
        # error: No or to much arguments.
        if len(args) < 1 or len(args) > 2:
            raise TypeError('Instantiation without or with more then '
                            'two arguments is not allowed.')

        gid = None
        label = None
        parent = None

        ## Prepare modes
        # Mode: Existing group
        if type(args[0]) == int:
            if len(args) != 1:
                raise TypeError('If group id is given no other argument '
                                'is allowed.')
            gid = args[0]
        # Mode: Create group
        elif type(args[0]) == str:
            label = args[0]
            try:
                # ... specific parent
                parent = args[1]
            except IndexError:
                # ... root as default parent
                parent = Group.root()
        else:
            raise TypeError('First argument has wrong type.')

        # MODE - Existing group
        if gid is not None:
            if not Group.is_gid_valid(gid):
                raise ValueError(f'The group id {gid} is not in the '
                                 'allowed range.')
            self._gid = gid
            if not data.FeedsData.me.group_exists(self.gid):
                raise ValueError(f'A group with id {gid} do not exist '
                                 'in the data.')
        # MODE - Create group
        else:
            # get a fresh and unused
            self._gid = Group.next_free_id()

            # create it in the data layer
            data.FeedsData.me.create_group(self.gid, label, parent.gid)

            # unfold all (grand)parents
            grandparent = self.parent
            while grandparent:
                grandparent.set_unfolded()
                grandparent = grandparent.parent

            # notify
            Group.event_group_new_or_modified.notify(self.gid)

    @classmethod
    def root(cls):
        """Give the root of the groups tree.

        The root has the identifier GROUPID_FEEDS.

        Returns:
            (Group): The root group object.
        """
        return Group(basics.GROUPID_FEEDS)

    def __str__(self):
        """Printed representation.
        """
        return('Group #{} "{}"'
               .format(self.gid, self.label))

    def __eq__(self, other):
        """Equal operater check the gid."""
        return other.gid == self.gid

# ------------
# --- Properties
# ------------

    @property
    def gid(self):
        """Identifier of the group.

        Returns:
            (int): The identifier.
        """
        return self._gid

    @property
    def parent(self):
        """Parent group of the group.

        Returns:
            (Group): The parent group or None.
        """
        try:
            parent_gid = data.FeedsData.me.get_full_path(self.gid)[-2]
            return Group(parent_gid)
        except IndexError:
            return None

    @property
    def label(self):
        """Label of the group.

        Returns:
            (str): Group label.

        Raises:
            TypeError if group do not exist.
        """
        return data.FeedsData.me._get_sub_tree_by_id(self.gid)[0][1]

    @label.setter
    def label(self, value):
        """Set label of the group.

        Args:
            value (str): New label.

        Raises:
            TypeError if not exist.
        """
        # remove leading/trailing whitespace chars
        value = value.strip()
        # set the value
        data.FeedsData.me._get_sub_tree_by_id(self.gid)[0][1] = value
        # notify about modification
        Group.event_group_new_or_modified.notify(self.gid)

    @property
    def unfolded(self):
        """Return the unfolded state as boolean.

        If the group is unfolded/expanded the return value is 'True'. If the
        group is folded/collapsed then it is 'False'.

        Returns:
            bool: Unfolded state.
        """
        return data.FeedsData.me._get_sub_tree_by_id(self.gid)[0][2]

    def set_unfolded(self, unfolded=True, include_child_groups=False):
        """Set the unfold/expanded state of the group.

        By default child groups are not affected.

        Args:
            unfolded (bool): Unfolded state (default: True)
            include_child_groups (bool): Set state recursive. (default: False)
        """
        data.FeedsData.me._get_sub_tree_by_id(self.gid)[0][2] = unfolded

        if include_child_groups:
            for child in self.groups:
                child.set_unfolded(unfolded, include_child_groups)

    @property
    def groups(self):
        """Return a list of all child groups from the next sub level.

        Only groups from the next sub level are handeld. This does not work
        recursive. The list of groups is orded as they appear in this group.
        The list is empty if no groups as child present.

        Returns:
            list(): A list of groups.
        """
        child_groups = []

        for gid in self.group_ids:
            child_groups.append(Group(gid))

        return child_groups

    @property
    def group_ids(self):
        """Return a list of all group_ids of that group.

        Only groups from the same level are handled - it is not
        recursive. Ordering is as they appear in the group.

        Returns:
            list(int): A list of group_ids.
        """
        group_ids = data.FeedsData.me.get_group_ids(self.gid, False)

        return group_ids

    @property
    def feeds(self):
        """Return a list of all feeds of that group.

        Only feeds from the same level are handled. This does not work
        recursive. The list of feeds is ordered as they appear in the group.

        Returns:
            list(Feed): A list of feeds
        """
        child_feeds = []

        for fid in self.feed_ids:
            child_feeds.append(feed.Feed(fid))

        return child_feeds

    @property
    def feed_ids(self):
        """Return a list of all feed_ids of that group.

        Only feeds from the same level are handled - it is not
        recursive. Ordering is as they appear in the group.

        Returns:
            list(int): A list of feed ids.
        """
        feed_ids = data.FeedsData.me.get_feed_ids(tree_or_group_id=self.gid,
                                                  recursive=False)
        return feed_ids

# ------------
# --- Misc
# ------------
    @classmethod
    def is_gid_valid(cls, gid):
        """Check if the givin identifier is valid for regular (non-smart)
        groups.

        Technically it checks if the gid is in the range reserved for groups.
        Smart groups like 'Unread', 'Read Later' are not checked.
        It does not check if the group really exist.

        Args:
            gid(int): Group identifier.

        Returns:
            (bool): Valid or not.
        """
        if gid <= basics.GROUPID_MIN or gid == basics.GROUPID_FEEDS:
            return True

        return False

    @classmethod
    def next_free_id(cls):
        """Compute the next free id for a new group.

        Returns:
            (int): An unused and free group id.
        """
        group_ids = sorted(data.FeedsData.me.get_group_ids())

        if len(group_ids) == 0:
            return basics.GROUPID_MIN

        # are there empty ids between existing groups?
        if len(group_ids) < abs(group_ids[0] - (basics.GROUPID_MIN + 1)):
            new_id = basics.GROUPID_MIN
            while new_id in group_ids:
                # keep in mind that group ids counted into negative direction
                new_id = new_id - 1
        else:
            new_id = group_ids[0] - 1

        return new_id

    def has_ancestor(self, ancestor):
        """Check if 'ancestor' is an ancestor of this group.

        Ancestor means parent, grand-parent, grand-grand... and so on.

        Args:
            ancestor (Group): Possible ancestor.

        Returns:
            (bool): True if 'ancestor' is.
        """
        curr_parent = self.parent
        while curr_parent:
            if curr_parent == ancestor:
                return True
            curr_parent = curr_parent.parent

        return False

    def add_as_opml_element(self, parent_element):
        """
        """
        attrib = {'text': self.label}
        element = ElementTree.SubElement(parent_element, 'outline', attrib)

        # sub-groups
        for g in self.groups:
            g.add_as_opml_element(element)

        # feeds
        for f in self.feeds:
            f.add_as_opml_element(element)

        return element

    def count_total_and_unread_entries(self):
        """The number of all and the unread entries in all feeds and
        child-groups feeds.

        Returns:
            (int): total
            (int): unread
        """
        total = 0
        unread = 0

        # feeds
        for f in self.feeds:
            t, u = f.count_total_and_unread_entries()
            total += t
            unread += u

        # groups
        for g in self.groups:
            t, u = g.count_total_and_unread_entries()
            total += t
            unread += u

        return total, unread

    def set_read(self):
        """Set all entries of child-feeds, child-groups and their childs to
        read.
        """
        # child-groups
        for groupobj in self.groups:
            groupobj.set_read()

        # child-feeds
        for feedobj in self.feeds:
            feedobj.set_read()

    def get_previous_group(self):
        """Return the previous group in the same level or the next.

        If there is no previous group in the current tree level because this
        group is the first then the search algoritm steps one
        level up. If nothing is found 'None' is returned.

        Returns:
            (Group): The previous group or 'None' if nothing was found.
        """
        # create list of group_ids
        ids = self.parent.group_ids

        # still first group?
        if ids[0] == self.gid:
            return self.parent

        # find index of previous group
        idx = ids.index(self.gid) - 1

        if idx < 0:
            return None

        return Group(ids[idx])

# ------------
# --- Move / Insert / Remove childs
# ------------

    def insert_as_first(self, child):
        """Insert 'child' at the beginning of the list of child groups or
        child feeds.

        Args:
            child (Group|Feed): A group or a feed.

        Raises:
            TypeError: Wrong parameters
        """
        # Group
        if type(child) is Group:
            # child still exists at that location
            if self.groups and self.groups[0] == child:
                raise ValueError('The group {} is already the first child in '
                                 'group {}.'.format(child, self))

            # check for parent-as-child-paradoxon
            if self.has_ancestor(child):
                raise ValueError('The group {} can not be a child of {} '
                                 'because it is still its ancestor.'
                                 .format(child, self))

            # insert
            data.FeedsData.me.move_group_to(group_id=child.gid,
                                            dest_group_id=self.gid,
                                            insert_mode='first')
            # notify
            Group.event_group_new_or_modified.notify(child.gid)
        # Feed
        elif type(child) is feed.Feed:
            # child still exists at that location
            if self.feeds and self.feeds[0] == child:
                raise ValueError('The feed {} is already the first child in '
                                 'group {}.'.format(child, self))

            # insert
            data.FeedsData.me.move_feed_to(feed_id=child.fid,
                                           dest_group_id=self.gid,
                                           insert_mode='first')
            # notify
            feed.Feed.event_feed_new_or_modified.notify(child.fid)

        # wrong type
        else:
            raise TypeError('The argument need to be of the type: '
                            'class Group or class Feed.')

    def insert_as_last(self, child):
        """Insert 'child' at the end of the list of child groups or child
        feeds.

        Args:
            child (Group|Feed): A group or a feed.

        Raises:
            TypeError: Wrong parameters.
        """
        # Feed type
        if type(child) is feed.Feed:
            # child still exists at the location
            if self.feeds and self.feeds[-1] == child:
                raise ValueError('The feed {} is already the last child in '
                                 'group {}.'.format(child, self))

            # insert
            data.FeedsData.me.move_feed_to(feed_id=child.fid,
                                           dest_group_id=self.gid,
                                           insert_mode='last')

            # notify
            feed.Feed.event_feed_new_or_modified.notify(child.fid)

        # Group type
        elif type(child) is Group:
            # child still exists at that location
            if self.groups and self.groups[-1] == child:
                raise ValueError('The group {} is already the last child in '
                                 'group {}.'.format(child, self))

            # check for parent-as-child-paradoxon
            if self.has_ancestor(child):
                raise ValueError('The group {} can not be a child of {} '
                                 'because it is still its ancestor.'
                                 .format(child, self))

            # insert
            data.FeedsData.me.move_group_to(group_id=child.gid,
                                            dest_group_id=self.gid,
                                            insert_mode='last')

            # notify
            Group.event_group_new_or_modified.notify(child.gid)

        # wrong argument type
        else:
            raise TypeError('The argument need to be of the type: '
                            'class Group or class Feed.')

    def insert_before(self, child, afterwards):
        """Insert 'child' before the child item 'afterwards'.

        Both paramters need to be of the same type: Group or Feed class.

        Args:
            child (Group|Feed): Group or Feed to insert.
            afterwards (Group|Feed): Group/Feed 'child' is inserted before.

        Raises:
            TypeError: Wrong paramters.
            ValueError: If 'afterwards' was not found.
        """
        # type match
        if type(child) != type(afterwards):
            raise TypeError('Both arguments need to be of the same type: '
                            'class Group or class Feed.')

        # Group type
        if type(child) is Group:
            try:
                # check: afterwards existence
                idx = self.groups.index(afterwards)
            except ValueError:
                raise ValueError('The afterwards group {} is not a child '
                                 'group of {}.'.format(afterwards, self))

            # check if child is allready there
            if child == self.groups[idx - 1]:
                raise ValueError('The group {} is already localized '
                                 'before {}.'.format(child, afterwards))

            # insert
            data.FeedsData.me.move_group_to(group_id=child.gid,
                                            dest_group_id=self.gid,
                                            insert_mode='before',
                                            rel_group_id=afterwards.gid)

            # notify
            Group.event_group_new_or_modified.notify(child.gid)

        # Feed type
        elif type(child) is feed.Feed:
            try:
                # check: afterwards existence
                idx = self.feeds.index(afterwards)
            except ValueError:
                raise ValueError('The afterwards feed {} is not a child '
                                 'feed of {}.'.format(afterwards, self))

            # check if child is allready there
            if child == self.feeds[idx - 1]:
                raise ValueError('The feed {} is already localized before '
                                 '{}.'.format(child, afterwards))

            # insert
            data.FeedsData.me.move_feed_to(feed_id=child.fid,
                                           dest_group_id=self.gid,
                                           insert_mode='before',
                                           rel_feed_id=afterwards.fid)

            # notify
            feed.Feed.event_feed_new_or_modified.notify(child.fid)

        # wrong child type
        else:
            raise TypeError('The arguments need to be one of the types: '
                            'class Feed or class Group.')

    def insert_after(self, child, previous):
        """Insert 'child' after the child item 'previous'.

        Both parameters need to be of the same type: Feed class.
        Hint: Currently it is unclear if we need Group class here, too.

        Args:
            child (Feed): Feed to insert.
            previous (Feed): Feed 'child' is inserted after.

        Raises:
            TypeError: Wrong parameters.
            ValueError: If 'previous' was not found.
        """
        # type match
        if type(child) != type(previous):
            raise TypeError('Both paramters need to be of the same type: '
                            'class Group or class Feed.')

        # Group type
        if type(child) is Group:
            # Does previous exists in that Group
            try:
                idx = self.groups.index(previous)
            except ValueError:
                raise ValueError('The previous group {} is not a child '
                                 'group of {}.'.format(previous, self))
            # Is child allready there?
            try:
                if child == self.groups[idx + 1]:
                    raise ValueError('The group {} is already localized '
                                     'after {}.'.format(child, previous))
            except IndexError:
                pass

            # insert
            data.FeedsData.me.move_group_to(group_id=child.gid,
                                            dest_group_id=self.gid,
                                            insert_mode='after',
                                            rel_group_id=previous.gid)

        elif type(child) is feed.Feed:
            # check: previous existance
            try:
                idx = self.feeds.index(previous)
            except ValueError:
                raise ValueError('The previous feed {} is not a child '
                                 'feed of {}.'.format(previous, self))
            # check: child is allready there
            try:
                if child == self.feeds[idx + 1]:
                    raise ValueError('The feed {} is already localized '
                                     'after {}.'.format(child, previous))
            except IndexError:
                pass

            # insert
            data.FeedsData.me.move_feed_to(feed_id=child.fid,
                                           dest_group_id=self.gid,
                                           insert_mode='after',
                                           rel_feed_id=previous.fid)

            # notify
            feed.Feed.event_feed_new_or_modified.notify(child.fid)

        # wrong child type
        else:
            raise TypeError('The arguments need to be of the type '
                            'class Feed.')

    def remove(self, child):
        """Remove a child feed or group from this group but don't delete it.

        This methode does nothing more then removing 'child'. It does not take
        care of any Groups or Feeds in lower levels of 'child'. It does not
        fire any events. It does not take care of data holding files
        (e.g. entries, favicon).
        The difference between deleting or removing s the handling of
        its child groups/feeds. Deleting a group will delete all its childs,
        removing won't.

        Args:
            child (Feed|Group): The feed or group to remove.

        Raises:
            ValueError: If the object is not a child of that group.
        """
        import json
        # Group
        if type(child) is Group:
            sub_tree = data.FeedsData.me._get_sub_tree_by_id(self.gid)[0][3]
            idx = None
            # find child in the subtree
            for i, g in enumerate(sub_tree):
                if g[0] == child.gid:
                    idx = i
                    break
            # delete from that tree
            if idx is None:
                raise ValueError(f'The group {child} is not a child of '
                                 f'{self}.')
            else:
                del sub_tree[idx]
        # Feed
        elif type(child) is feed.Feed:
            sub_tree = data.FeedsData.me._get_sub_tree_by_id(self.gid)[0]
            sub_tree[3][0].remove(child.fid)
        else:
            raise TypeError('Child must be a Group or Feed instance.')
