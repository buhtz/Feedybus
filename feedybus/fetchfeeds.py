# -*- coding: utf-8 -*-
import logging
import threading
import asyncio
import aiohttp
import concurrent.futures
import time
import json
import copy
from functools import partial
# gi.require_version('Gtk', '3.0')
from gi.repository import GLib
import feedparser
from feedybus import basics
from feedybus import data

"""
    Fetching the feed.

    Fetching the feed means download, parse and convert to Feedybus data. The
    process is realized with a combination of threads (on one cpu core),
    processes (multiple cores; real paralel) and asynchrone co-routines.
"""


_log = logging.getLogger(basics.GetLogId(__file__))


class FetchOneFeedThread(threading.Thread):
    """A thread fetching data from one feed-url using package 'feedparser'.

    It is used in FeedDialog for example.
    """
    def __init__(self, url):
        """Prepare the start of the thread.

        Args:
            data_queue (queue.Queue): Used to share results with the main
            thread.
            url (str): Feed URL as string.
        """
        ## The url to fetch from.
        self._url = url
        threading.Thread.__init__(self)

    def run(self):
        """The thread is alive."""
        thread_id = threading.get_ident()
        _log.info('{}: Starting to fetch feed {} using Agent-String "{}".'
                  .format(thread_id, self._url, basics.USER_AGENT))
        self.feed = feedparser.parse(self._url)
        _log.debug('Finisched {} |||'.format(self))


def _calculate_max_entry_date(entries):
    """Calculating the maxium (youngest) date on a list of feed entries.

    The 'published_parsed' attributes of all entries are computed and the
    youngest is return. Otherwise None is returned.
    The code is based on https://stackoverflow.com/a/13298129/4865723.

    Args:
        entries (list): The list of entries (parsed by feedparser).

    Returns:
        time.struct_time: The max entry date or None.
    """
    if len(entries) == 0:
        return None

    date_key = 'published_parsed'
    if date_key not in entries[0]:
        date_key = 'updated_parsed'

    entry_dates = []
    for e in entries:
        p = e.get(date_key)
        if p:
            entry_dates.append(time.struct_time(p))
        else:
            return None

    return max(entry_dates, default=None)


def _keep_entries_with_date_after(entries, date):
    """Remove entries older then or equal 'date' and returns the youngest
    date.

    Use the 'published_parsed' or 'update_parsed' attribute of each
    element/feedentry in 'entries' and remove entries they are equal
    or older then 'date'.
    Based on https://stackoverflow.com/a/13298129/4865723.

    Args:
        entries (list): List of feedentries (from feedparser).
        date (time.struct_time): The date from where newer/younger feeds
                                 are accepted.

    Returns:
        (time.struct_time): Youngest date.
    """
    # _before = len(entries)
    # _log.debug('entries with date after {}\n{}'
    #           .format(date, entries[0].link))

    # date field
    date_key = 'published_parsed'
    try:
        if date_key not in entries[0]:
            date_key = 'updated_parsed'
    except IndexError:
        return time.struct_time([0] * 9)

    # younges date
    youngest = date

    # remove entries older/equal then 'date'
    for e in entries[:]:
        e_date = e[date_key]  # could be 'None'
        if e_date:
            if e_date <= date:
                entries.remove(e)
            elif e_date > youngest:
                youngest = e_date

    # _log.debug('{} entries after {} in {} entries.'
    #           .format(len(entries), time.strftime('%c', date), _before))

    return youngest


def _keep_entries_without_this_links(entries, links):
    """Entries with a link/URL that is present in 'links' are removed from
    'entries'.

    Args:
        entries(list): List of entries.
        links(list): List of URls.
    """
    for e in entries[:]:
        if e['link'] in links:
            entries.remove(e)


def _feedparser_parse(data, max_entry_date=None, entry_links=None):
    """Parse raw/binary data as xml via 'feedparser' and evtl. cut irrelevant
    entries.

    This function use 'feedparser.parse()' and runs in its own process.

    Args:
        data (binary): Data read from an RSS/Atom feed file.
        max_entry_date (): Date
        entry_links (list(str)): List of URLs.

    Returns:
        (feedparser.FeedParserDict): The parsed result.
    """
    # _log.debug('in _feedparser_parse() - date {} links {}'
    #           .format(max_entry_date,
    #                   len(entry_links) if entry_links else None))
    # Let the beautiful Feedparser do the magic
    feed_dict = feedparser.parse(data)

    # The returning data need to be pickled (because of multiprocessing)
    # what is not possible because feedparser store unpickable objects in
    # 'bozo_exceptions'. So we translate it here into a simple strings.
    if feed_dict.bozo == 1:
        # replace the object by its message string
        feed_dict['bozo_exception'] = feed_dict.bozo_exception._msg
        # remember some details for debuging
        raw = data.decode('utf-8', errors='replace')
        if raw[:20].upper().startswith('<!DOCTYPE'):
            doctype = raw[2:raw.find('>')]
            feed_dict.bozo_exception += '; HINT: {}'.format(doctype)

        feed_dict['raw_data'] = raw[:1024]
    else:
        # remove entries by their dates
        if max_entry_date:
            new_max_date = _keep_entries_with_date_after(feed_dict.entries,
                                                         max_entry_date)
            feed_dict['max_entry_date'] = new_max_date

        # remove entries by their 'link' field
        if entry_links:
            _keep_entries_without_this_links(feed_dict.entries, entry_links)

    # Why do we need 'headers' fields?
    if 'headers' in feed_dict:
        raise Exception('Headers in result')
    else:
        feed_dict['headers'] = {}

    return feed_dict


class FetchAsyncMultiprocessThread(threading.Thread):
    """Get multiple RSS/Atom feeds from remote URLs and parse the data via
    Feedparser.

    This thread is started by the GUI to prevent the GUI from being blocked
    by the fetching process. The download of the files is done asynchrone via
    'aiohttp'. Because the parsing via 'feedparser' is a CPU-bound
    synchrone/blocking process it is done in a separate pool of processes (not
    threads).
    """
    def __init__(self, feeds_data, feed_ids, result_queue, debug):
        """Prepare the start of the thread.

        Args:
            feeds_data (data.FeedsData): The feeds data object itself.
            feed_ids (list(int)): List of feed ids should be updated.
            result_queue (queue.Queue): Used to share results with the main
            thread.
        """
        ## Queue for thread-safe data sharing with other threads
        self._result_queue = result_queue
        ## The complete FeedsData object itself.
        self._data = feeds_data
        ## List of feed ids to be fetched. If 'None' all feeds are fetched.
        self._ids = feed_ids
        ## Debug mode on/off?
        self._debug = debug
        ## session for all async jobs
        self._session = None

        # fetch all feeds if 'None'
        if self._ids is None:
            self._ids = self._data._feeds.keys()

        threading.Thread.__init__(self)

    def _get_executor(self):
        """Return the executor object to run code in a separate process.

        The executor is instanced only one time.

        Returns:
            (concurrent.futures.ProcessPoolExecutor): The executor.
        """
        if not getattr(self, '_executor', None):
            ## Multiprocessing executor for parsing feed data
            self._executor = concurrent.futures.ProcessPoolExecutor()
        return self._executor

    def _empty_feed_data(self):
        """Create a feedparser like dictionary for an empty feed.

        This is e.g. used for reporting errors.

        Returns:
            (feedparser.FeedParserDict): The feed dictionary.
        """
        result = feedparser.FeedParserDict()
        result['entries'] = []
        result['headers'] = {}
        return result

    async def _receive_via_aiohttp(self, url, headers):
        """
        """
        print('IN real RECEIVE_VIA')
        try:
            async with self._session.get(url, headers=headers) as response:
                content = await response.read()

                return response, content

        # Host Unknown
        except (aiohttp.ClientConnectionError,
                aiohttp.ClientConnectorError,
                aiohttp.ClientResponseError,
                aiohttp.ServerDisconnectedError) as err:
            return None, str(err)
        except ConnectionResetError as err:
            _log.critical(err)
            return None, str(err)

    async def _download_and_parse_feed(self,
                                       url,
                                       etag=None,
                                       modified=None,
                                       max_entry_date=None,
                                       entry_links=None,
                                       notify=True):
        """Download feed data and parse it.

        The download is done asynchrone via 'aiohttp'. The parsing use a pool
        of process (means other CPU cores) and the 'feedparser.parse()'
        methode.

        Args:
            url (str): The URL.
            etag (str): ETag (default: None).
            modified (str): Last modified date (default: None).
            max_entry_date (time.t_struct): .
            entry_links (list(str)): URL list.
            notify (bool): Notify observers (Default: True)

        Returns:
            (feedparser.FeedParserDict): The result.
        """
        # thread_id = threading.get_ident()
        _log.debug('Download & Parse {}\n\tetag: {}\n\tmodified: {}\n\t'
                   'max_entry_date: {}\n\tentry_links: {}\n...'
                   .format(url, etag, modified,
                           max_entry_date,
                           len(entry_links) if entry_links else None))

        # Header: User-Agent
        headers = {'User-Agent': basics.USER_AGENT}

        # Header: Etag
        if etag:
            headers['If-None-Match'] = etag

        # Header: Modified
        if modified:
            headers['If-Modified-Since'] = modified

        # Converty max_entry_date from list to time.struct_time
        if max_entry_date:
            max_entry_date = time.struct_time(max_entry_date)

        # use aiohttp to get feed/xml content and response object
        response, content = await self._receive_via_aiohttp(url,
                                                            headers)

        _log.debug('response: {} content: {}'.format(response, content))

        # update the status bar
        if notify:
            GLib.idle_add(self.callback_feed_fetched)

        # error happend?
        if not response:
            result = self._empty_feed_data()
            result['href'] = url
            result['response'] = {
                'status': 900,
                'reason': content  # the raised exception
            }
            if notify:
                GLib.idle_add(self.callback_feed_parsed)

            return result

        #
        feed_data = content

        if response.status == 304:  # Not Modified
            result = self._empty_feed_data()
        else:
            # get last response
            if len(response.history) > 0:
                last_status = response.history[-1].status
            else:
                last_status = response.status

            # >>> Parse...
            if last_status < 400:  # no error
                # ...in parallel process (not thread!)
                func_to_call = partial(_feedparser_parse,
                                       feed_data,
                                       max_entry_date,
                                       entry_links)
                result = await self._get_loop() \
                    .run_in_executor(self._get_executor(),
                                     func_to_call)

            else:  # an error happend
                result = self._empty_feed_data()
                result['raw_data'] = feed_data[:1024] \
                    .decode('utf-8', errors='replace')

        # Store relevant header information
        raw = ['{}: {}'
               .format(k, v) for k, v in response.raw_headers]
        result['headers']['raw'] = raw
        # ...Date
        result['headers']['Date'] = response.headers['Date']
        # ...ETag
        if 'ETag' in response.headers:
            result['etag'] = response.headers['ETag']
        # ...Last-Modified
        if 'Last-Modified' in response.headers:
            result['modified'] = response.headers['Last-Modified']
        # ...URL
        result['href'] = str(response.url)
        # ...HTTP return code
        hist = []
        if len(response.history) == 0:
            status = response.status
            reason = response.reason
            raw = self._DEBUG_dict(response)
        else:
            # In case of redirects, aiohttp store multiple
            # response objects.

            # the first/earliest response
            status = response.history[0].status
            reason = response.history[0].reason
            raw = self._DEBUG_dict(response.history[0])

            # all others
            if len(response.history) > 1:
                for idx in range(1, len(response.history)):
                    r = response.history[idx]
                    hist.append(
                        {
                            'status': r.status,
                            'reason': r.reason,
                            'raw': self._DEBUG_dict(r)
                        })

            # ...last response
            hist.append({'status': response.status,
                         'reason': response.reason,
                         'raw': ''})

            if status == 301:  # redirected: moved permanently
                result['old_href'] = str(response.history[0].url)

        # remember the response codes
        result['response'] = {
            'status': status,
            'reason': reason,
            'raw': raw
        }
        if len(hist) > 0:
            result['response']['history'] = hist

        # data-event
        if notify:
            GLib.idle_add(self.callback_feed_parsed)

        return result

    async def _DEPRECATED_BACKUP_download_and_parse_feed(self, url,
                                                         etag=None,
                                                         modified=None,
                                                         max_entry_date=None,
                                                         entry_links=None,
                                                         notify=True):
        """Download feed data and parse it.

        The download is done asynchrone via 'aiohttp'. The parsing use a pool
        of process (means other CPU cores) and the 'feedparser.parse()'
        methode.

        Args:
            url (str): The URL.
            etag (str): ETag (default: None).
            modified (str): Last modified date (default: None).
            max_entry_date (time.t_struct): .
            entry_links (list(str)): URL list.
            notify (bool): Notify observers (Default: True)

        Returns:
            (feedparser.FeedParserDict): The result.
        """
        # thread_id = threading.get_ident()
        _log.debug('Download & Parse {}\n\tetag: {}\n\tmodified: {}\n\t'
                   'max_entry_date: {}\n\tentry_links: {}\n...'
                   .format(url, etag, modified,
                           max_entry_date,
                           len(entry_links) if entry_links else None))

        # Header: User-Agent
        headers = {'User-Agent': basics.USER_AGENT}

        # Header: Etag
        if etag:
            headers['If-None-Match'] = etag

        # Header: Modified
        if modified:
            headers['If-Modified-Since'] = modified

        # Converty max_entry_date from list to time.struct_time
        if max_entry_date:
            max_entry_date = time.struct_time(max_entry_date)

        # Client Session
        async with aiohttp.ClientSession() as session:
            try:
                async with session.get(url, headers=headers) as response:
                    # >>> Download...
                    feed_data = await response.read()
                    if notify:
                        GLib.idle_add(self.callback_feed_fetched)

                    if response.status == 304:  # Not Modified
                        result = self._empty_feed_data()
                    else:
                        # get last response
                        if len(response.history) > 0:
                            last_status = response.history[-1].status
                        else:
                            last_status = response.status

                        # >>> Parse...
                        if last_status < 400:
                            # ...in parallel process (not thread!)
                            func_to_call = partial(_feedparser_parse,
                                                   feed_data,
                                                   max_entry_date,
                                                   entry_links)
                            result = await self._get_loop() \
                                .run_in_executor(self._get_executor(),
                                                 func_to_call)

                            # DEBUG
                            if 'headers' in result:
                                raise Exception('Headers in result')
                            else:
                                result['headers'] = {}
                        else:
                            result = self._empty_feed_data()
                            result['raw_data'] = feed_data[:1024] \
                                .decode('utf-8', errors='replace')

                    # Store relevant header information
                    raw = ['{}: {}'
                           .format(k, v) for k, v in response.raw_headers]
                    result['headers']['raw'] = raw
                    # ...Date
                    result['headers']['Date'] = response.headers['Date']
                    # ...ETag
                    if 'ETag' in response.headers:
                        result['etag'] = response.headers['ETag']
                    # ...Last-Modified
                    if 'Last-Modified' in response.headers:
                        result['modified'] = response.headers['Last-Modified']
                    # ...URL
                    result['href'] = str(response.url)
                    # ...HTTP return code
                    hist = []
                    if len(response.history) == 0:
                        status = response.status
                        reason = response.reason
                        raw = self._DEBUG_dict(response)
                    else:
                        # In case of redirects, aiohttp store multiple
                        # response objects.

                        # the first/earliest response
                        status = response.history[0].status
                        reason = response.history[0].reason
                        raw = self._DEBUG_dict(response.history[0])

                        # all others
                        if len(response.history) > 1:
                            for idx in range(1, len(response.history)):
                                r = response.history[idx]
                                hist.append(
                                    {
                                        'status': r.status,
                                        'reason': r.reason,
                                        'raw': self._DEBUG_dict(r)
                                    })

                        # ...last response
                        hist.append({'status': response.status,
                                     'reason': response.reason,
                                     'raw': ''})

                        if status == 301:  # redirected: moved permanently
                            result['old_href'] = str(response.history[0].url)

                    # remember the response codes
                    result['response'] = {
                        'status': status,
                        'reason': reason,
                        'raw': raw
                    }
                    if len(hist) > 0:
                        result['response']['history'] = hist

                    # data-event
                    if notify:
                        GLib.idle_add(self.callback_feed_parsed)

            # Host Unknown
            except (aiohttp.ClientConnectionError,
                    aiohttp.ClientConnectorError,
                    aiohttp.ClientResponseError,
                    aiohttp.ServerDisconnectedError) as err:
                result = self._empty_feed_data()
                result['href'] = url
                result['response'] = {
                    'status': 900,
                    'reason': str(err)
                }
                if notify:
                    GLib.idle_add(self.callback_feed_fetched)
                    GLib.idle_add(self.callback_feed_parsed)
            except ConnectionResetError as err:
                _log.critical(err)

        return result

    async def _set_update_mechanism(self, feed):
        """Do some tests to find out which update mechanism can be used with
        that feed.

        There are three ways to handle updates. Using Etag or 'Last-Modified'
        date are the two recommanded if the server offer them. The third way
        is a backfall/workaround: The date of the youngest entry from the
        last download before is used to delete older entries from the current
        download.

        Some servers offer an Etag but that does not work. Feedybus take also
        care of Cases like that.

        Args:
            feed (dict): Feedybus feed object.

        Returns:
            (feedparser.FeedParserDict): The parsed feed or 'None'.
        """
        _log.debug('Test update mechanism for {}...'.format(feed['xmlUrl']))

        def _debug_success():
            _log.debug('...mecha test SUCCESSFULL => {}\n   {}'
                       .format(data.UpdateMecha.name(feed['update_mechanism']),
                               feed['xmlUrl']))

        def _debug_abort():
            _log.debug('...mecha test ABORTED because of connection or '
                       'parsing problem.\n   {}'
                       .format(feed['xmlUrl']))

        # ...initial
        # _log.debug('...initial...')
        feed_zero = await self._download_and_parse_feed(feed['xmlUrl'],
                                                        notify=False)
        # _log.debug(self._DEBUG_dict(feed_zero))

        # stop the test if there is an error response
        if feed_zero.response['status'] != 200:  # not OK
            if feed_zero.response['status'] in [301, 308]:  # redirect
                if 'history' in feed_zero.response:
                    status_after_redirect = feed_zero \
                        .response['history'][-1]['status']
                    if status_after_redirect >= 400:
                        _debug_abort()
                        return feed_zero
                    elif status_after_redirect != 200:
                        _debug_abort()
                        raise Exception(feed_zero)
            elif feed_zero.response['status'] >= 400:
                _debug_abort()
                return feed_zero

        # stop if there is a parsing error
        if feed_zero.bozo == 1:
            _debug_abort()
            return feed_zero

        # ...test for Etag
        if 'etag' in feed_zero:
            # _log.debug('...try etag...')
            feed_etag = await self \
                ._download_and_parse_feed(feed['xmlUrl'],
                                          etag=feed_zero.etag,
                                          notify=False)
            if len(feed_etag.entries) == 0:
                feed['update_mechanism'] = data.UpdateMecha.etag
                _debug_success()
                return None

        # ...test for Modification Date (based on 'Last-Modified' header)
        if 'modified' in feed_zero:
            # _log.debug('...try modified...')
            feed_mod = await self \
                ._download_and_parse_feed(feed['xmlUrl'],
                                          modified=feed_zero.modified,
                                          notify=False)
            if len(feed_mod.entries) == 0:
                feed['update_mechanism'] = data.UpdateMecha.modified
                _debug_success()
                return None

        # ...calculcate based on entries date
        # _log.debug('...try entry dates...')
        max_entry_date = _calculate_max_entry_date(feed_zero.entries)
        if max_entry_date:
            feed_dates = await self \
                ._download_and_parse_feed(feed['xmlUrl'],
                                          max_entry_date=max_entry_date,
                                          notify=False)
            if len(feed_dates.entries) == 0:
                feed['update_mechanism'] = data.UpdateMecha.entry_dates
                _debug_success()
                return None

        # ...calculate based on entries links (last chance!!!)
        # _log.debug('...entry links...')
        entry_links = [e.link for e in feed_zero.entries]
        feed_links = await self \
            ._download_and_parse_feed(feed['xmlUrl'],
                                      entry_links=entry_links,
                                      notify=False)
        if len(feed_links.entries) == 0:
            feed['update_mechanism'] = data.UpdateMecha.entry_links
            _debug_success()
            _log.warning('Feed "{}" ({})\n'
                         '\tThis feed does not offer enough informations to '
                         'differ between old and new entries in that feed.\n'
                         '\tAs a dirty and ressource hungry workaround I '
                         'need to compare all entries each time.\n'
                         '\tPlease contact the provider of that feed and ask '
                         'for fixing that problem.'
                         .format(feed['title'], feed['xmlUrl']))
            return None

        _debug_abort()
        raise Exception('Not able to test an update mchanism for {}'
                        .format(feed['xmlUrl']))

    async def _fetch_feed(self, feed_id):
        """This methode is the starting point for each feed and stores the
        result to 'self._result_queue'.

        This methode is called from the asyncio event loop for each feed.
        It takes care of the update meachnism for the feed, calls another
        methode for fetching and parsing the feed and after that store the
        result in the queue.

        Args:
            feed_id (int): Id of a feed.
        """
        feed = self._data._feeds[feed_id]
        # thread_id = threading.get_ident()
        _log.debug('=> Fetching Feed #{}:{}'.format(feed_id, feed['xmlUrl']))

        # ToDo - refactor this
        # Workaround. This method is set from statusbar.py
        GLib.idle_add(self.callback_feed_update_started)

        # ask for update mechanism / or test it out
        try:
            mecha = feed['update_mechanism']
        except KeyError:
            feed_data_zero = await self._set_update_mechanism(feed)

        # stop here if there was problem (e.g. 404)
        try:
            mecha = feed['update_mechanism']
        except KeyError:
            self._result_queue.put({feed_id: feed_data_zero})
            # see statusbar.py
            GLib.idle_add(self.callback_feed_fetched)
            # see statusbar.py
            GLib.idle_add(self.callback_feed_parsed)
            return feed_data_zero

        # Some feeds offer Etag's they don't work. Because of that we consider
        # only our own test (see _set_update_mechanism() and don't take care
        # if the server offer an Etag or Modified date in its repsonse.
        # It only matters if the etag/modified date works or not!
        etag = None
        modified = None
        max_entry_date = None
        entry_links = None
        if mecha is data.UpdateMecha.etag:
            etag = feed.get('etag', None)
        elif mecha is data.UpdateMecha.modified:
            modified = feed.get('modified', None)
        elif mecha is data.UpdateMecha.entry_dates:
            max_entry_date = feed.get('max_entry_date',
                                      time.struct_time([0] * 9))
            if type(max_entry_date) is list:
                max_entry_date = time.struct_time(max_entry_date)
        elif mecha is data.UpdateMecha.entry_links:
            entry_links = [e['link'] for e in self._data._entries[feed_id]]
        # >>> DO IT !!!
        feed_data = await self \
            ._download_and_parse_feed(feed['xmlUrl'],
                                      etag=etag,
                                      modified=modified,
                                      max_entry_date=max_entry_date,
                                      entry_links=entry_links)
        # remember the update mechanism
        feed_data['update_mechanism'] = mecha

        # result to the Queue
        self._result_queue.put({feed_id: feed_data})

    def run(self):
        """The thread is alive while in this methode.

        The strucutre of the async-calls in this classes are based on the
        recommendations here:
        https://debianforum.de/forum/viewtopic.php?f=34&t=181931#p1281351
        """
        asyncio.run(self._run_async())

    async def _run_async(self):
        """Create a aiohttp.ClientSession() and prepare the async fetch-jobs
        for each feed.
        """
        # Debug flag
        asyncio.get_running_loop().set_debug(self._debug)

        # infos
        _log.info('Thread {}: Starting to fetch {} feeds '
                  'using Agent-String "{}".'
                  .format(threading.get_ident(),
                          len(self._ids),
                          basics.USER_AGENT))

        # one session for all jobs
        async with aiohttp.ClientSession() as session:
            self._session = session
            # create the "jobs" (futures)
            futures = [self._fetch_feed(feed_id)
                       for feed_id
                       in self._ids]

            # run them asynchrone
            await asyncio.gather(*futures)

            #
            self._session = None

    def _DEBUG_dict(self, d):
        """
        """
        if type(d) == feedparser.FeedParserDict and 'entries' in d:
            r = copy.deepcopy(d)
            del r['entries']
        else:
            r = d

        try:
            return json.dumps(r, indent=4)
        except TypeError:
            return str(r)
