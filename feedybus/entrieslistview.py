# -*- coding: utf-8 -*-
import logging
import math
from operator import itemgetter
from feedybus.gtkhelper import create_toolbar_item, create_toolbar_spaceritem
from feedybus import basics
from feedybus.feed import Feed
from feedybus.group import Group
# gi.require_version('Gtk', '3.0')
from gi.repository import Gdk
from gi.repository import Gtk
from gi.repository import Pango

"""
    Short

    Long
"""


_log = logging.getLogger(basics.GetLogId(__file__))


class FeedEntriesListView (Gtk.TreeView):
    """
        The list box widget holding the entries for the currently selected
        feed.
    """
    def __init__(self, app, window):
        """Ctor
        """
        ## The "model" (not in the meaning of MVC pattern!)
        self._model = Gtk.ListStore(bool,  # read later
                                    int,  # unread (as font weight)
                                    str,  # entry date
                                    str,  # title
                                    int,  # id of the related feed
                                    int)  # index in the list of the feed

        Gtk.TreeView.__init__(self, self._model)
        ## Reference to the application object.
        self._app = app
        ## Reference to the window object.
        self._window = window
        ## statusbar object
        self._statusbar = None
        ## See 'self.feeds_id' for details.
        self._current_feeds_id = None
        ## docu
        self.unread_only = False
        ## The id of the entry in the first row (no matter which page it is)
        self._first_row_entry_idx = 0
        ## Number of rows that can be displayed on one page.
        self._rows_per_page = None
        ## Current page
        self._current_page = 1

        # col: read later
        renderer_read_later = Gtk.CellRendererToggle()
        renderer_read_later.connect('toggled', self.on_read_later)
        col = Gtk.TreeViewColumn('L',
                                 renderer_read_later,
                                 active=0)
        col.set_sort_column_id(0)
        self.append_column(col)

        # col: date
        col = Gtk.TreeViewColumn('Datum',
                                 Gtk.CellRendererText(),
                                 text=2,
                                 weight=1,
                                 weight_set=True)
        col.set_sort_column_id(2)
        self.append_column(col)

        # col: title
        col = Gtk.TreeViewColumn('T',
                                 Gtk.CellRendererText(),
                                 text=3,
                                 weight=1,
                                 weight_set=True)
        col.set_sort_column_id(3)
        self.append_column(col)

        # CSS Provider Stuff (see self.create_toolbar_top())
        # based on https://stackoverflow.com/q/55111383/4865723
        css = b"""
            .open-button {
                min-height: 0px;
                padding-bottom: 0px;
                padding-top: 0px;
                padding-left: 5px;
                padding-right: 5px
            }
        """

        ## CSS provider
        self._css_provider = Gtk.CssProvider()
        self._css_provider.load_from_data(css)

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            self._css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

        # scrollable
        self.scroll = Gtk.ScrolledWindow()
        self.scroll.add(self)
        self.scroll.set_policy(Gtk.PolicyType.AUTOMATIC,
                               Gtk.PolicyType.AUTOMATIC)

        # event
        self.get_selection().connect('changed', self.on_select)
        self.connect('row-activated', self.on_double_click)

        # Feedybus events
        Feed.event_feed_marked_read.register(self._on_feed_marked_read)

    def _on_feed_marked_read(self, feed_id):
        """Event handler when a whole feed was marked as read."""
        if self.is_feed_shown(feed_id):
            self.populate_feed()

    def create_toolbar_top(self):
        """Create the toolbar on top of the entries list view.

        It is stored in 'self._toolbar_top'. The solution to make the buttons
        smaller then usual is based on
        https://stackoverflow.com/q/55111383/4865723

        Returns:
            (Gtk.Toolbar): The toolbar
        """
        toolbar = Gtk.Toolbar.new()

        # display filter "Unread"
        unread = Gtk.ToggleToolButton(label=_('Unread'))
        # style_ctx = unread.get_child().get_style_context()
        # style_ctx.add_class('open-button')
        unread.connect('toggled', self._on_btn_unread)
        toolbar.insert(unread, -1)
        self.unread_only = unread.get_active()

        create_toolbar_spaceritem(toolbar, draw=True, expand=False)

        # open buttons
        for ob in self._app.config.open_buttons:
            # create button
            btn = Gtk.ToolButton(label=ob, expand=False)
            # make it smaller then usual
            style_ctx = btn.get_child().get_style_context()
            style_ctx.add_class('open-button')
            # tooltip
            tip = _('Open the first {} entries in a browser.').format(ob)
            btn.set_tooltip_text(tip)
            # clicked event
            btn.connect('clicked', self._on_btn_open)
            # remember the "number"
            btn.n = int(ob)
            # add to layout
            toolbar.insert(btn, -1)

        ## Top toolbar
        self._toolbar_top = toolbar
        return toolbar

    def create_toolbar_bottom(self):
        """Create the toolbar on bottom of the entries list view.

        It is stored in 'self._toolbar_bottom'.

        Returns:
            (Gtk.Toolbar): The toolbar
        """
        toolbar = Gtk.Toolbar.new()

        # "Back"
        self._btn_back = create_toolbar_item(icon='gtk-go-back',
                                             handler=self._on_btn_back,
                                             parent=toolbar)

        # page counter
        self.label_page = Gtk.Label(_('3 of 7'),
                                    justify=Gtk.Justification.CENTER)
        toolitem = Gtk.ToolItem.new()
        toolitem.add(self.label_page)
        toolbar.insert(toolitem, -1)

        # "Forward"
        self._btn_fwd = create_toolbar_item(icon='gtk-go-forward',
                                            handler=self._on_btn_forward,
                                            parent=toolbar)

        # spacer
        create_toolbar_spaceritem(toolbar)

        # CheckBox
        item = Gtk.ToolItem.new()
        item.set_tooltip_text(_('Mark entries on the current page read '
                                'when going to next page.'))
        ## "Mark as read" checkbox
        self._btn_mark_read = Gtk.CheckButton.new_with_label(_('Mark as read'))
        item.add(self._btn_mark_read)
        toolbar.insert(item, -1)
        self._btn_mark_read.connect('clicked', self._on_btn_mark_as_read)

        ## Bottom toolbar
        self._toolbar_bottom = toolbar

        return self._toolbar_bottom

    def _on_btn_mark_as_read(self, button):
        """Event handler if the mark-as-read checkbox was clicked.
        """
        self._btn_fwd.set_sensitive(self._btn_mark_read.get_active())

    def _add(self, entry, feed_id, entry_idx, date_str=None):
        """
        Create and fill the list of feed entries in the GUI.

        Args:
            entry (dict):
            fedd_id (int):
            entry_idx (int):
            date_str (str):
        """
        feedobj = Feed(feed_id)

        # Date
        if date_str is None:
            date_str = feedobj.get_entry_date_string(entry_idx)

        # read later
        if entry_idx in feedobj.get_read_later_entries():
            read_later = True
        else:
            read_later = False

        # read/unread
        read_weight = self._get_weight_by_readstatus(entry)

        # append row
        self._model.append([read_later,
                            read_weight,
                            date_str,
                            feedobj.get_entry_title(entry_idx),
                            feed_id,
                            entry_idx])

    def _get_weight_by_readstatus(self, entry):
        """Return the correct pango font weight related to the read status of
        the entry.

        Unread entries are bold and read ones are normal.

        Args:
            entry (bool or feedparser.FeedParserDict): Entry or bool.

        Returns:
            (int): Pango weight
        """
        if type(entry) is bool:
            unread = entry
        else:
            unread = not (entry['feedybus'].get('read', False))

        if unread is True:
            return Pango.Weight.BOLD

        return Pango.Weight.BOOK

    def _calc_pages(self):
        """Calculates the current page that is shown in the view. It also sets
        the forward/back buttons end the page label.
        """
        idx = self._first_row_entry_idx

        # number of entries
        entries_n = len(self._entry_refs)

        # current page
        page_current = 0
        if idx > 0:
            page_current = math.ceil(idx / self._rows_per_page) + 1
        elif entries_n > 0:
            page_current = 1

        # max pages count
        page_max = entries_n / self._rows_per_page
        page_max = math.ceil(page_max)

        # _log.debug('\tpage {} of {}\tentries_n {}'
        #            .format(page_current, page_max, entries_n))

        # ??? maybe in resize event: calcute the new first
        # entry of current page

        # display the information to the user
        self.label_page.set_text(_('{} of {}')
                                 .format(page_current, page_max))

        # enable/disable Forward button
        sensitive = (page_current != page_max)
        if not sensitive:
            sensitive = (self._btn_mark_read.get_active())
        self._btn_fwd.set_sensitive(sensitive)
        # enable/disable Back button
        self._btn_back.set_sensitive(page_current > 1)
        # enable/disable "mark as read" checkbox
        # self._btn_mark_read.set_sensitive(page_max != 1)

    def _on_btn_open(self, button):
        """Event handler for open in browser buttons.

        The count of entries to open is specified by 'button.n'.

        Args:
            button (Gtk.Button): The button
        """
        idx_a = self._first_row_entry_idx
        idx_b = idx_a + button.n
        entries = []
        mark_read = {}
        ef = self._entry_refs[idx_a:idx_b]
        for feed_id, entry_idx, date_str, title in ef:
            feedobj = Feed(feed_id)
            # remember the entry object (for open in browser)
            entries.append(feedobj.entries[entry_idx])
            # remember the feed_id and entry_idx (for read mark)
            if feedobj.fid not in mark_read:
                mark_read[feedobj.fid] = []
            mark_read[feedobj.fid].append(entry_idx)

        # find related rows
        for row in self._model:
            feedobj = Feed(row[4])
            entry_idx = row[5]
            try:
                if entry_idx in mark_read[feedobj.fid]:
                    # mark it as read
                    feedobj.set_entry_read(entry_idx)
                    # normal font (indicating read)
                    row[1] = self._get_weight_by_readstatus(False)
            except KeyError:
                pass

        # open in browser
        self._app.open_in_browser(entries)

    def _on_btn_unread(self, button):
        """
        """
        self.unread_only = button.get_active()
        self._entry_refs = []
        self._build_entry_references()
        self._calc_pages()
        self.populate_feed()

    def _on_btn_back(self, button):
        """
        """
        new_idx = self._first_row_entry_idx - self._rows_per_page
        self._first_row_entry_idx = new_idx
        self.populate_feed()

    def _on_btn_forward(self, button):
        """Event handler if the forward button in the bottom toolbar of the
        entrieslistview was clicked.

        Args:
            button (Gtk.Button): The button.
        """
        # if "mark as read" checked mark entries on current page read
        if self._btn_mark_read.get_active():
            self._mark_current_page_read()

        # calc new rows
        new_idx = self._first_row_entry_idx + self._rows_per_page
        if new_idx <= len(self._entry_refs):
            self._first_row_entry_idx = new_idx

        self.populate_feed()

    def _mark_current_page_read(self):
        """
        """
        # specify the section for the current page
        idx_a = self._first_row_entry_idx
        idx_b = idx_a + self._rows_per_page

        # add entries to listview
        for feed_id, entry_idx, date_str in self._entry_refs[idx_a:idx_b]:
            feedobj = Feed(feed_id)
            feedobj.set_entry_read(entry_idx)

    def _calc_rows_per_page(self):
        """Compute the number of rows per page taking the current height into
        account.
        """
        # _log.debug('in _calc_rows_per_page() -- ')
        old_rpp = self._rows_per_page

        # cell/row height
        column = self.get_column(0)
        height = column.cell_get_size().height

        # padding
        renderer = column.get_cells()[0]
        ypad = renderer.get_padding()[1]

        # real row height
        row_h = height + ypad

        # height of list
        list_h = self.get_visible_rect().height

        #
        self._rows_per_page = int(list_h / row_h)

        # If the number of rows per page changes we have to take care that the
        # last first_row_entry_idx entry still appears on the new page.
        # So the current_page and its first_row_entry_idx needs to be
        # recalculated.
        if self._rows_per_page != old_rpp:
            old_idx = self._first_row_entry_idx
            if old_idx > 0:
                new_page = int(old_idx / self._rows_per_page)
                new_idx = new_page * self._rows_per_page
                _log.debug('re-calculated first_row_entry_idx from {} to {}'
                           .format(old_idx, new_idx))
                self._first_row_entry_idx = new_idx

    def on_resize_done(self):
        """
        """
        # _log.debug('on_resize_done() {}'.format(self.feeds_id))
        self._calc_rows_per_page()
        if self.feeds_id is not None:
            self._calc_pages()
            self.populate_feed()

    def do_disable(self, do=True):
        """
        """
        if do:
            self.label_page.set_text('')
            self.feeds_id = None
            self._model.clear()

        self.set_sensitive(not do)
        self._toolbar_top.set_sensitive(not do)
        self._toolbar_bottom.set_sensitive(not do)

    def is_feed_shown(self, feed_ids):
        """Check if ListView include this feed or the child feeds of the
        group.

        It means the complete ListView not only the current page.

        Args:
            feed_or_group_id (int): ID of a feed or group.

        Returns:
            (bool): True if the feed is included.
        """
        if self._current_feeds_id is None:
            return False

        if type(feed_ids) != list:
            feed_ids = [feed_ids]

        # list of ListViews feeds
        try:
            groupobj = Group(self._current_feeds_id)
            my_ids = groupobj.feed_ids
        except ValueError:
            my_ids = [self._current_feeds_id]

        # check
        for fid in feed_ids:
            if fid in my_ids:
                return True

        return False

    @property
    def feeds_id(self):
        """The id of one feed or a group of feeds (e.g.
        'basics.GROUPID_UNREAD' or a sub-group).

        It gets or returns 'self._current_feeds_id'. The setter cleares the
        list of feed-entry references ('self._entry_refs') also.
        """
        return self._current_feeds_id

    @feeds_id.setter
    def feeds_id(self, value):
        self._current_feeds_id = value
        self._first_row_entry_idx = 0
        ## References to feed entries
        self._entry_refs = []

    def _build_entry_references(self):
        """Build a list ('self._entry_refs') with references to feed-entries
        depending on 'self._current_feeds_id' which is used by that view.

        This ListView can show entries of one specific or multiple feeds at
        one time. It also can show the unread entries only
        ('self.unread_only'). The items in the reference list contains three
        fields: The feed_id, the index of the entry in that feed and the
        entries date as a string. The date can be None for performance
        reasons. It should be used for sorting.
        """
        self._entry_refs = []
        theID = self.feeds_id

        # If the item is just a feeds child error message item
        if theID == basics.GROUPID_INVALID:
            return

        _log.debug('Build entry references for {}'.format(theID))

        # no need to clear the current reference list because it should have
        # been cleared when the setter ('self.feeds_id()') for
        # 'self._current_feeds_id' has been used.

        def _sub_add_to_reference_list(current_feed):
            """
            """
            # refs = []
            # all or unread entries?
            if self.unread_only:
                # get index of all unread entries
                entries_idx = current_feed.get_unread_entries()
            else:
                # index for all entries
                entries_idx = list(range(len(current_feed.entries)))

            # count entries
            n = len(entries_idx)

            # create the reference list with three fields per entry
            # (feed_id, entry_idx, date_string)
            # the date is not calculated here (see self._add())
            refs = list(zip([current_feed.fid] * n, range(n)))

            self._entry_refs.extend(refs)

        # one feed only?
        if Feed.is_fid_valid(theID):
            _sub_add_to_reference_list(Feed(theID))
        else:
            # "smart" group "read later"
            if theID == basics.GROUPID_LATER:
                for feed_id in Group.smart_group_index_later:
                    for entry_idx in Group.smart_group_index_later[feed_id]:
                        # add to ref list
                        self._entry_refs.append((feed_id, entry_idx))
            # "smart" group "unread"
            elif theID == basics.GROUPID_UNREAD:
                for feed_id in Group.smart_group_index_unread:
                    for entry_idx in Group.smart_group_index_unread[feed_id]:
                        # add to ref list
                        self._entry_refs.append((feed_id, entry_idx))
            # regular group (with feeds as childs)
            else:
                for child_feed in Group(theID).feeds:
                    _sub_add_to_reference_list(child_feed)

        # add sorting informations
        for ref_idx in range(len(self._entry_refs)):
            # feed & entry index
            fid, eidx = self._entry_refs[ref_idx]
            # Feed object
            ref_feed = Feed(fid)
            # write date & title back to reference list
            self._entry_refs[ref_idx] = (fid,
                                         eidx,
                                         ref_feed.get_entry_date_str(eidx),
                                         ref_feed.get_entry_title(eidx))

    def populate_feed(self, reset_entry_references=False):
        """Build the list of entries and show them in the ListView.

        Args:
            reset_entry_references (bool): Reference list for entries is
            rebuild (default: False).

        The result depends on 'self._entry_refs', 'self._first_row_entry_ids'
        and 'self._rows_per_page'.
        """
        if not self._entry_refs or reset_entry_references:
            # build a references list of currently relevant entries.
            self._build_entry_references()

        # sorting
        # ...by date
        sort_item_idx = 2
        # ...by title
        # sort_item_idx = 3
        self._entry_refs = sorted(self._entry_refs,
                                  key=itemgetter(sort_item_idx),
                                  reverse=True)

        # calc current and max number of pages
        self._calc_pages()

        # clear the "view"
        self.set_model(None)
        self._model.clear()
        self.set_model(self._model)

        # specify the section for the current page
        idx_a = self._first_row_entry_idx
        idx_b = idx_a + self._rows_per_page

        # add entries to listview
        for feed_id, entry_idx, date_str, _ in self._entry_refs[idx_a:idx_b]:
            feedobj = Feed(feed_id)
            e = feedobj.entries[entry_idx]
            self._add(e, feed_id, entry_idx, date_str)

    def on_double_click(self, tree_view, path, column):
        """Extract the URL related to the double-clicked feed-entry and open
        it with an external browser.
        """
        # extract feed_id and entry_idx from the selected item
        item = self._model[self._model.get_iter(path)]
        feedobj = Feed(item[4])
        entry_idx = item[5]

        # get the related data object
        entry = feedobj.entries[entry_idx]

        # open the website
        self._app.open_in_browser(entry)

    def on_select(self, selection):
        """
        """
        model, treeiter = selection.get_selected()
        if not treeiter:
            return
        item = model[treeiter]

        # set read-status directly in data
        feedobj = Feed(item[4])
        entry_idx = item[5]
        feedobj.set_entry_read(entry_idx)

        # normal font (indicating read)
        entry = feedobj.entries[entry_idx]
        item[1] = self._get_weight_by_readstatus(entry)

    def on_read_later(self, renderer, path):
        """GUI event handle if the read-later checkbox was clicked.

        The handler will toogle the read-later status directly in the data
        layer which will cause another event which is usually catched by the
        treeview. The handler will also ask the data for the new status and
        set it directly to the checkbox.

        Args:
            renderer(): Unknown.
            path(Gtk.TreePath): Path to the related list item.
        """
        it = self._model.get_iter(path)
        item = self._model[it]

        feedobj = Feed(item[4])
        entry_idx = item[5]

        # current status
        status = entry_idx in feedobj.get_read_later_entries()
        # toogle read-later-status
        feedobj.set_entry_read_later(entry_idx, not status)

        # set the new status in the view (pseudo-model)
        item[0] = not status
