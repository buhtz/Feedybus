# -*- coding: utf-8 -*-
import logging
from feedybus import basics
import copy
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib
from feedybus.feed import Feed
from feedybus.group import Group
from feedybus.fetchfeeds import FetchOneFeedThread
from feedybus.gtkhelper import GeometryPersistentWindow
from feedybus.renamedialog import ReNameDialog

_log = logging.getLogger(basics.GetLogId(__file__))


class FeedDialog(Gtk.Dialog, GeometryPersistentWindow):
    """Dialog to add new Feeds.

    The dialog offers to enter an URL of a feed and fetch its data.

    """
    def __init__(self, window, feed=None):
        """Constructor.

        Args:
            window (FeedybusWindow): Main window.
        """
        Gtk.Dialog.__init__(self, 'Add new feed', window, 0,
                            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                             Gtk.STOCK_OK, Gtk.ResponseType.OK))
        GeometryPersistentWindow.__init__(self, window._app)

        ## Reference to main window
        self._window = window
        ## Dict (indexed by URL) of threads checking an url for a valid feed.
        self._threads = dict()
        ## URL of the current feed
        self._url = None
        ## A feed if it still exists
        self._feed = feed
        ## Identify the current timer. See self._do_fetch_feeds() for details.
        self._timerid = None

        main_box = self.get_content_area()

        label_size_group = Gtk.SizeGroup.new(Gtk.SizeGroupMode.HORIZONTAL)
        icon_size_group = Gtk.SizeGroup.new(Gtk.SizeGroupMode.HORIZONTAL)

        # URL
        label_box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 5)
        url_label = Gtk.Label('URL', xalign=0, expand=False)
        url_icon = Gtk.Image.new_from_icon_name('applications-internet', 32)
        label_box.pack_start(url_label, False, False, 0)
        label_box.pack_start(url_icon, False, False, 0)
        label_size_group.add_widget(url_label)
        icon_size_group.add_widget(url_icon)
        self._url_entry = Gtk.Entry(hexpand=True)
        if not self._feed:
            self.clipboard_url()
            self._url_entry.connect('activate', self._do_feed_fetch)
            self._url_entry.connect('focus-out-event', self._do_feed_fetch)
        else:
            self._url_entry.set_text(self._feed.xmlUrl)
        url_box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
        url_box.pack_start(label_box, False, False, 5)
        url_box.pack_start(self._url_entry, True, True, 0)
        main_box.pack_start(url_box, False, False, 5)

        # Combo-Box for feed group
        label_box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 5)
        group_label = Gtk.Label('{}'.format(_('to group')),
                                xalign=0, expand=False)
        group_icon = Gtk.Image.new_from_icon_name('folder', 32)
        label_box.pack_start(group_label, False, False, 0)
        label_box.pack_start(group_icon, False, False, 0)
        label_size_group.add_widget(group_label)
        icon_size_group.add_widget(group_icon)
        group_combo = self._create_group_combobox()
        group_box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
        group_box.pack_start(label_box, False, False, 5)
        group_box.pack_start(group_combo, True, True, 0)
        main_box.pack_start(group_box, False, False, 5)

        # Frame
        self._feed_label = Gtk.Label('',
                                     use_markup=True,
                                     selectable=True,
                                     xalign=0,
                                     yalign=0)
        self._feed_label.set_line_wrap(True)
        self._feed_label.set_markup('{}{}{}'
                                    .format(GLib.markup_escape_text('<'),
                                            _('no feed'),
                                            GLib.markup_escape_text('>')))
        self._feed_label.connect('activate-link', self._on_activate_link)
        boxv = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        boxv.pack_start(self._feed_label, True, True, 5)
        boxh = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 10)
        boxh.pack_start(boxv, True, True, 10)
        frame = Gtk.Frame.new('')
        frame.add(boxh)
        box_frame = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
        box_frame.pack_start(frame, True, True, 15)
        main_box.pack_start(box_frame, True, True, 0)

        # spacer
        main_box.pack_start(Gtk.Label(''), False, False, 0)

        # Geometry
        self.restore_gui_geometry()
        self.show_all()
        # close event
        self.connect('response', self._on_response)

        if not self._feed:
            # disable OK button
            self.set_response_sensitive(Gtk.ResponseType.OK, False)

            # try to get URL from the clipboard
            self._do_feed_fetch()
        else:
            self._set_feed_infos()

    def _on_response(self, dialog, response_id):
        """Event handler if OK or CANCEL was pressed.

        The methode handles the final data handling, closing and destroy fully
        autonomus.
        """
        self.store_gui_geometry()

        if response_id == Gtk.ResponseType.OK:
            selected_group = self._get_selected_group()
            # Mode: create new feed
            if not self._feed:
                feed_param = Feed.convert_FeedParserDict(self._raw_feed)
                # create the feed
                Feed(**feed_param, group=selected_group)
            # Mode: modify existing feed
            else:
                old_url = self._feed.xmlUrl
                # URL
                new_url = self._url_entry.get_text()
                if (Feed.is_url_valid(new_url)
                        and old_url != new_url):
                    self._feed.xmlUrl = new_url
                # Group
                if self._feed.group != selected_group:
                    selected_group.insert_as_last(self._feed)

        self.destroy()

    def _on_activate_link(self, label, uri):
        """Handler for clicking on links in the dialog."""
        if uri == 'renamelabel':
            try:
                self._window._tree._do_rename_feed_group(self._feed.fid)
            except AttributeError:
                curr_title = self._raw_feed.feed.get('title', '')
                dlg = ReNameDialog(self._window, curr_title)
                dlg.run()
                if dlg.renamed_value:
                    self._raw_feed.feed['title'] = dlg.renamed_value

            # This is a workaround
            # (from: # https://stackoverflow.com/a/66491174/4865723) because
            # of this bug: https://gitlab.gnome.org/GNOME/gtk/-/issues/1498
            # The bug is fixed in upstream but as not yet reached Debian 10.
            Gdk.threads_add_idle(GLib.PRIORITY_DEFAULT_IDLE,
                                 self._set_feed_infos)
            return True

        return False

    def _set_feed_infos(self):
        """Display infos about the current feed in the dialog.
        """
        if (not self._feed
                and self._raw_feed.get('bozo', None) == 1):
            bozo_msg = str(self._raw_feed['bozo_exception'])
            txt = '\n<span foreground="red" size="large"><b>{}</b></span>' \
                  .format(GLib.markup_escape_text(bozo_msg))

            validator = self._raw_feed.get('url',
                                           'https://validator.w3.org/feed/')
            validator = '<a href="https://validator.w3.org/feed/check.cgi?' \
                        f'url={validator}">W3C-Validator</a>'
            err_msg = _('The feed data seems invalid. See details with the '
                        f'{validator}.\nPlease report to the '
                        'author of the feed about that problem.')
            txt += f'\n\n<span size="large">{err_msg}</span>'
        else:
            def _get_label_infos():
                infos = {}
                if not self._feed:
                    # extract from a fresh fetched feedparser dict
                    infos['title'] = self._raw_feed.feed.get('title', None)
                    infos['text'] = self._raw_feed.feed.get('text',
                                                            infos['title'])
                    infos['htmlUrl'] = self._raw_feed.feed.get('link', None)
                    infos['author'] = self._raw_feed.feed.get('author', None)
                    infos['rights'] = self._raw_feed.feed.get('rights', None)
                    infos['description'] = self._raw_feed.feed \
                        .get('description', None)
                    infos['version'] = self._raw_feed.get('version', None)
                    infos['encoding'] = self._raw_feed.get('encoding', None)
                else:
                    # extract from a feedybus dict (still existing feed)
                    infos['title'] = self._feed.title
                    infos['text'] = self._feed.label
                    infos['htmlUrl'] = self._feed.htmlUrl
                    infos['author'] = self._feed.author
                    infos['rights'] = self._feed.rights
                    infos['description'] = self._feed.description
                    infos['version'] = self._feed.version
                    infos['encoding'] = self._feed.encoding

                return infos

            infos = _get_label_infos()

            # Based on the OOPML specification:
            # 'title' is the original fixed/unchangable title givin by the
            # feeds author
            # 'text' is the label as the feeds name/title is shown in the
            # feedreader
            if infos['title'] != infos['text']:
                txt_org_title = '\n{}: <b>{}</b>' \
                    .format(_('Original title'),
                            GLib.markup_escape_text(infos['title']))
            else:
                txt_org_title = ''

            txt = '<b><span size="xx-large">{}</span></b>' \
                  '{}<a href="renamelabel"><span face=' \
                  '"monospace">{}</span></a>' \
                  '{}\n\n' \
                  .format(GLib.markup_escape_text(infos['text']),
                          ' ' * 5,
                          _('Modify label'),
                          txt_org_title)

            url = infos['htmlUrl']
            txt += '<a href="{}">{}</a>'.format(url, url)

            if infos['author']:
                txt += '\n<b>{}</b>: {}'.format(_('Author'), infos['author'])

            if infos['rights']:
                txt += ' {}'.format(infos['rights'])

            if infos['description']:
                desc = infos['description'].replace('&', '&amp;')
                txt += '\n\n<b>{}</b>:\n{}\n'.format(_('Description'), desc)

            if infos['version']:
                txt += '\n<i>{}</i>: {}'.format(_('Version'), infos['version'])

            if infos['encoding']:
                txt += '\n<i>{}</i>: {}'.format(_('Encoding'),
                                                infos['encoding'])

        # replace chars
        # txt = txt.replace('&', '&amp;')
        self._feed_label.set_markup(txt)

    def _create_group_combobox(self):
        """Create a combobox object with all existing groups.

        Returns:
            (Gtk.ComboBox): The combo box.
        """
        # model
        store = Gtk.ListStore(int,  # ID
                              str)  # name
        """
        def _indent_group(group, level):
            txt = '{}{}'.format(' ' * 4 * level, group[1])
            store.append(row=[group[0], txt])

        FeedsData.me.walk_groups(_indent_group)
        """

        # group labels indented based on their level in the tree
        def _indented_group_labels(parent_group, level):
            txt = '{}{}'.format(' ' * 4 * level, parent_group.label)
            store.append(row=[parent_group.gid, txt])

            for child_group in parent_group.groups:
                _indented_group_labels(child_group, level + 1)

        _indented_group_labels(Group.root(), 0)

        # view
        combo = Gtk.ComboBox.new_with_model(store)
        rend_name = Gtk.CellRendererText()
        combo.pack_start(rend_name, False)
        combo.add_attribute(rend_name, 'text', 1)

        combo.set_active(0)

        # select a group
        if self._feed:
            for g in store:
                if g[0] == self._feed.group.gid:
                    combo.set_active_iter(g.iter)

        self._combo_group = combo
        return combo

    def _get_selected_group(self):
        """Returns the id of the group currently selected in the group combo
        box.

        Returns:
            (Group): Group identifier
        """
        idx = self._combo_group.get_active()
        group = self._combo_group.get_model()[idx]
        group_id = group[:][0]
        return Group(group_id)

    def clipboard_url(self):
        """Check the clipboard for a URL and fill it into the entry field.
        """
        # get string from clipboard
        display = Gdk.Display.get_default()
        clipboard = Gtk.Clipboard.get_default(display)
        text = clipboard.wait_for_text()

        # set the url in the entry field
        if Feed.is_url_valid(text):
            self._url_entry.set_text(text)

    def _check_feed_data_from_threads(self):
        """Check running or finished threads for feed data.

        This method is called by a timer activated in _do_feed_fetch().
        """
        if len(self._threads) == 0:
            ## Timer id
            self._timerid = None  # stop the timer
            return False

        url = self._url_entry.get_text()

        if url not in self._threads:
            # all existing threads outdated. kill them.
            ## Threads indicated by URL
            self._threads = {}
        else:
            # do nothing if the thread is still working
            if not self._threads[url].is_alive():
                # get the thread related to the current url
                thread = self._threads.pop(url)
                ## Feed data
                self._raw_feed = thread.feed
                # self._raw_feed.feed['text'] = self._raw_feed.feed['title']
                ## Feed url
                self._url = url
                # Base & details informations
                self._set_feed_infos()
                # enabke OK button
                if self.is_feed_valid():
                    self.set_response_sensitive(Gtk.ResponseType.OK, True)

        return True  # repeate timer

    def is_feed_valid(self):
        """Check if it is a valid feed in the meaning that it can be added as
        a regular feed.

        It checkes the existence of the 'entries' field and if there are any
        'bozo' exceptions.

        Returns:
            (bool): Valid or not.
        """
        if not hasattr(self._raw_feed, 'entries'):
            return False

        if self._raw_feed.bozo == 1:
            return False

        return True

    def _do_feed_fetch(self, *args):
        """Start (via a Thread) fetching feed data.
        """
        url = self._url_entry.get_text()
        if url == self._url:
            return

        self._url = None

        if not Feed.is_url_valid(url):
            return

        thread = FetchOneFeedThread(url)
        self._threads[url] = thread
        thread.start()

        # don't start multiple timers
        if not self._timerid:
            self._timerid = GLib \
                .timeout_add(1000, self._check_feed_data_from_threads)

    def _DEPRECATED_set_details_label(self):
        """Fill the list control with feed data.

        Returns:
            (str): A markup string.
        """
        f = copy.deepcopy(self._raw_feed)
        try:
            del f['entries']
        except KeyError:
            pass

        if f.bozo == 1:
            f['bozo_exception'] = str(f['bozo_exception'])
