# -*- coding: utf-8 -*-
import json
import warnings
import logging
import os
import os.path
import queue
import feedparser
from functools import partial
import feedybus.basics as basics

"""
    The data layer.

    Manager the feeds data including loading, storeing, conversition, import
    and export.
"""


_ENTRIES_FILE_KEY = 'feedentries_filename'


class UpdateMecha:
    """Class to represent the different types of update mechanisms.

    Means the different ways how a feed can update its content.
    """
    etag = 0
    modified = 1
    entry_dates = 2
    entry_date = 2
    entry_links = 3

    _names = ['etag', 'modified', 'entry_dates', 'entry_links']

    def name(mecha):
        try:
            return UpdateMecha._names[mecha]
        except TypeError:
            return '(n.a.)'


# Logger instance
_log = logging.getLogger(basics.GetLogId(__file__))


class FeedsData():
    """The data layer holding and managing the feeds data.

    Attributes:
        _tree (dict): Information about the tree structure in wich the feeds
                      are embeded.
        _feeds (dict): The feeds themselfs.
        _entries (dict): The entries for each feed.
        _unread (dict): List of indizes of unread entries for each feed.
        _later (dict): List of indizes of read-later-marked entries for each
                       feed.
        _queue (queue.Queue): For thread-safe data transmission between
                              threads.
    """
    # The instance itself.
    me = None

    def __init__(self):
        """Ctor.
        """
        # Check for singleton
        if FeedsData.me is not None:
            raise Exception('Only one instance of FeedsData can exist.')

        ## singleton instance
        FeedsData.me = self

        # thread-safe data storing to share data between this and other
        # threads
        self._DEPRECATED_queue = queue.Queue()

        # Unread entries.
        self._DEPRECATED_unread = dict()

        # Entries marked with "read later".
        # Initialy it is build in 'self.get_total()'.
        # It is modified in 'self.toggle_entry_read_later()'.
        self._DEPRECATED_later = dict()

        # feed data from file
        self._load_feedsfile()

        # Event: Feed refreshed
        self._DEPRECATED_event_feed_refreshed = basics.Event()
        # Event: Begin of refreshing all feeds
        self._DEPRECATED_event_started_refresh_all_feeds = basics.Event()
        # Event: End of refreshing all feeds
        self._DEPRECATED_event_finished_refresh_all_feeds = basics.Event()
        # Event: New or moved "tree item"
        self._DEPRECATED_event_tree_item_new_or_modified = basics.Event()
        #
        # self.event_started_test_all_feeds = basics.Event()
        # Event: End of testing all feeds for their update mechanism
        self.event_finished_test_all_feeds = basics.Event()
        # Event: Feed entry marked as read
        self._DEPRECATED_event_entry_marked_read = basics.Event()
        # Event: Feed entry read-later status toggled
        self._DEPRECATED_event_entry_toggled_read_later = basics.Event()
        # Event: Items deleted
        self._DEPRECATED_event_deleted_tree_items = basics.Event()

# ------------
# -- file IO operations
# ------------

    def _load_feedsfile(self):
        """Open and read the feedsfile.

        The default feedsfile (see `basics.FeedsFilename()`) is read as a JSON
        file. The data is hold in `self._data_tree`. Encoding ist UTF-8.

        If the file doesn't exists it is created with default values.
        """
        filename = basics.feeds_filename()

        # create default feed file if it not exists?
        if not os.path.exists(filename):
            _log.warning('Feeds file does not exist. Create a default file...')
            default_feeds = basics._DefaultFeeds()
            self._tree = default_feeds['tree']
            # add the root "Feeds" node to the tree structure
            self._tree = [[], [basics.GROUPID_FEEDS,
                               'Feeds', True, self._tree]]
            self._feeds = default_feeds['feeds']
            self._entries = default_feeds['entries']
            self.save_feedsdata()
            _log.info('Feeds file created: ' + filename)

        # read the feedsfile
        _log.info('Reading Feeds from {}.'.format(filename))
        with open(filename, 'r', encoding='utf-8') as feedsfile:
            data = json.loads(feedsfile.read())
            self._tree = data['tree']
            self._feeds = data['feeds']
            # dict-keys (as int's) are stored as 'str' with json
            # here the str-keys are converted back to int
            self._feeds = {int(feed_id): f for feed_id, f
                           in self._feeds.items()}

        # feed entries
        self._load_feed_entries()

    def _load_feed_entries(self):
        """Loading the entries for each feed.

        The results are stored in 'self._entries'.
        """
        fe_dir = basics.feed_entries_directory()
        self._entries = {}
        _log.info('Reading feed entries from {}...'.format(fe_dir))

        # each feed
        for feed_id in self._feeds:
            filename = self._feeds[feed_id][_ENTRIES_FILE_KEY]
            full_filename = os.path.join(fe_dir, filename)

            try:
                with open(full_filename, 'r', encoding='utf-8') as entriesfile:
                    entries = json.loads(entriesfile.read() or '[]')
            except FileNotFoundError as e:
                _log.warning('{} Create default empty entry.'.format(e))
                entries = []

            self._entries[feed_id] = entries

    def _save_feed_entries(self):
        """Write data about feed entries in separate files for each feed.
        """
        fe_dir = basics.feed_entries_directory()
        _log.info('Saving feed entries files to {}...'.format(fe_dir))

        # each feed
        for fid in self._feeds:
            feed = self._feeds[fid]

            # no feedentries filename yet
            if _ENTRIES_FILE_KEY not in feed:
                full_path = basics.create_feed_entries_filename(feed['title'])
                filename = os.path.split(full_path)[1]
                feed[_ENTRIES_FILE_KEY] = filename
                _log.debug('New feed entries file created: {}'
                           .format(full_path))

            filename = feed[_ENTRIES_FILE_KEY]
            full_filename = os.path.join(fe_dir, filename)

            # save feeds
            with open(full_filename, 'w', encoding='utf-8') as entriesfile:
                data = json.dumps(self._entries[fid], indent=4)
                entriesfile.write(data)

    def save_feedsdata(self):
        """Write the feed data to files in JSON format.

        The data in `self._data_tree` is written to the default feedsfile (see
        `basics.FeedsFilename()`). As encoding UTF-8 is used. The entries for
        the feeds are stored in separate files.
        """
        # this saves 'self._entries'
        self._save_feed_entries()

        # get filename
        filename = basics.feeds_filename()
        _log.info('Save Feeds to ' + filename)

        # each feed
        with open(filename, 'w', encoding='utf-8') as feedsfile:
            data = dict()
            # tree structure of the feeds
            data['tree'] = self._tree
            # the feeds itselfs
            data['feeds'] = self._feeds
            # write to file
            feedsfile.write(json.dumps(data, indent=4, sort_keys=True))

# ------------
# -- Walker
# ------------

    def walk_groups(self, func, item_ids=[], start_group_id=None):
        """Walk through the tree and call 'func' for each group.

        By default the function 'func' is called for each group and its
        child-groups of all levels. By default it is the whole tree but with
        'start_group_id' a section of the tree can be specified. In the latter
        case 'item_ids' is ignored.
        If 'item_ids' has items only for them the 'func' is called and the
        walk stops after the last of the specified id's.

        Args:
            func (function): Callback.
            item_ids (list(int)): Default it is an empty list.
            start_group_id (int): Sub-group to start the walk. Default is None.

        Returns:
            (bool): Default is 'None'. 'True' if all 'func' was called for all
            'item_ids'.

        Hint: To add extra parameters to 'func' use 'functools.partial'.
        e.g. obj.walk_groups(partial(1, 2))
        """
        def _walk_tree_groups(tree, func, item_ids, level=0):
            """Walk over the 'tree' or a section of it and call 'func' for
            each group element of it or only for groups with ids specified by
            'item_ids'.

            The walk stops if 'func' returns False or if all 'item_ids' are
            found.

            Returns:
                (bool): True if -i do not know-.
            """
            # each group in current level
            for group in tree[1:]:
                # take care of item_ids if there are some
                if item_ids:
                    if group[0] in item_ids:
                        # call
                        func(group, level)
                        # remove id
                        item_ids.remove(group[0])
                        # stop if nor more ids present
                        if not item_ids:
                            return True
                else:
                    # call
                    if func(group, level):
                        return True

                # upstairs
                if _walk_tree_groups(group[3], func, item_ids, (level + 1)):
                    return True

        rc = None

        # start walking...
        if start_group_id is not None:
            # First we need to find the correct sub-group and
            # its corrosponding level.
            grp, lvl = self._get_sub_tree_by_id(start_group_id)
            if grp is not None:
                # ...sub tree
                rc = _walk_tree_groups([None, grp], func, [], lvl)

        else:
            # create a list also if there is only one id
            if type(item_ids) != list:
                item_ids = [item_ids]

            # ...the complete tree
            rc = _walk_tree_groups(self._tree,
                                   func,
                                   item_ids=item_ids)

        return rc

# ------------
# -- Feed related
# ------------

    def create_feed_v2(self, new_id, feed_data):
        """Create a new feed based on 'feed_data'.

        This methode replace create_feed() and is called by Feed class while
        creating a new feed. See 'Feed.__init__' documentation for description
        of all possible fields.
        The method doesn't check if 'new_id' does exist. Data of an existing
        feed with same fid would be overwritten.
        The feeds parent group is given as id in 'feed_data['group']'.

        Args:
            feed_data (dict): Parsed feed data.
            parent_id (int): Id of the parent group.

        Returns:
            (int): The id of the new feed.
        """
        # this type check is unpythonic and will be removed in the future.
        # Currently it is needed because of existence of create_feed().
        if type(feed_data) is not dict:
            raise TypeError('feed_data should be of type dict. Maybe you '
                            'need the method create_feed().')

        # feeds meta data to data structure
        feed_meta = {
            'title': feed_data['title'],
            'xmlUrl': feed_data['xmlUrl']
        }

        # ...optional fields
        try:
            feed_meta['text'] = feed_data['label']
        except KeyError:
            pass

        for key in ['htmlUrl', 'version', 'author',
                    'rights', 'description', 'encoding']:
            try:
                feed_meta[key] = feed_data[key]
            except KeyError:
                pass

        # add feeds meta data to data structure
        self._feeds[new_id] = feed_meta

        # prepare datastructure for feed entries
        self._entries[new_id] = []

        # helper to insert it in the tree
        def _create_feed(group, level):
            group[3][0].append(new_id)

        # parent group id (default: root)
        parent_id = feed_data.get('group', basics.GROUPID_FEEDS)

        # walk
        self.walk_groups(_create_feed, [parent_id])

        return new_id

    def move_feed_to(self, feed_id, dest_group_id, insert_mode='first',
                     rel_feed_id=None):
        """Move one feed in the tree structure to a new position.

        The feed can be moved to the same or new group. Beside of the
        destination group the new position is declared relative to an existing
        feed.

        Args:
            feed_id (int): ID of feed to move.
            dest_group_id (int): ID of destination group.
            insert_mode (str): Possible values are 'first' (default), 'last',
                               'before', 'after'
            rel_feed_id (int): Related to 'insert_mode' and ignored if it
                               is 'first'.
        """
        def _sub_remove_feed(feed_id, group, level):
            if feed_id in group[3][0]:
                group[3][0].remove(feed_id)
                # stop the walk
                return True

        def _sub_insert_feed(feed_id, rel_feed_id, rel_mode, group, level):
            if insert_mode == 'first':
                group[3][0].insert(0, feed_id)
            elif insert_mode == 'last':
                group[3][0].append(feed_id)
            else:
                feed_idx = group[3][0].index(rel_feed_id)
                len_group = len(group[3][0])

                if insert_mode == 'before':
                    pass  # using the feed_idx is OK
                elif insert_mode == 'after':
                    if feed_idx < len_group:
                        feed_idx = feed_idx + 1
                else:
                    _log.critical('Wrong rel_mode: {}'.format(insert_mode))
                    return True  # stop the walk

                group[3][0].insert(feed_idx, feed_id)

        _log.debug('Move feed {} to group {} via {} {}'
                   .format(feed_id, dest_group_id,
                           insert_mode, rel_feed_id))

        # remove id from current position
        self.walk_groups(partial(_sub_remove_feed, feed_id))

        # setup new position
        self.walk_groups(partial(_sub_insert_feed, feed_id,
                                 rel_feed_id, insert_mode),
                         dest_group_id)

    def feed_exists(self, feed_id):
        """If a feed with the 'feed_id' exists 'True' is returend, otherwhise
        'False.'

        Args:
            feed_id (int): A feed-id to check.

        Returns:
            (bool): 'True' if feed exists.
        """
        if feed_id in self._feeds:
            return True

        return False

# ------------
# -- Group related
# ------------
    def create_group(self, new_id, name, parent=basics.GROUPID_FEEDS):
        """Create a new group and return its group_id.

        It does not check if 'new_id' exists or not.

        Args:
            name (str): Label of the group.
            parent (int): Group id of the parent. Default is root
            (GROUPID_FEEDS).

        Returns:
            (int): The id of new created group.
        """
        # helper function to insert the new group in the tree data
        def _sub_create_group(group, level):
            group[3].append([new_id, name, True, [[]]])

        # execute helper function
        self.walk_groups(_sub_create_group, [parent])

        return new_id

    def group_exists(self, group_id):
        """If a group with the 'group_id' exists 'True' is returend, otherwhise
        'False.'

        Args:
            group_id (int): A group-id to check.

        Returns:
            (bool): 'True' if group exists.
        """
        group_data = self._get_sub_tree_by_id(group_id)
        group_data = group_data[0]

        if group_data is not None:
            return True

        return False

    def move_group_to(self, group_id, dest_group_id,
                      insert_mode, rel_group_id=None):
        """Move one group in the tree structure to another parent group.

        You have to check yourself before if 'group_id' is a (grand)parent
        of 'dest_group_id': in that case the move would case undefinied
        situations. It should not be allowed.
        """
        def _sub_insert_group_dest(dest_group, level):
            # the destination group is localized by the calling
            # walker routine
            # now find the group to move in a second walker run
            self.walk_groups(partial(_sub_insert_group_src, dest_group),
                             group_id)

        def _sub_insert_group_src(dest_group, moving_group, level):
            # the group to move was localized by the second walker run
            # started in _insert_group_dest()
            # remove the moving group and its child groups from its source
            # self.remove_group(moving_group[0])
            parent_group_id = self.get_full_path(moving_group[0])[-2:-1]
            self.walk_groups(partial(_sub_remove_group, moving_group[0]),
                             parent_group_id)

            # insert the moving group to its destination
            if insert_mode == 'last':
                dest_group[3].append(moving_group)
            elif insert_mode == 'first':
                dest_group[3].insert(1, moving_group)
            elif insert_mode in ['before', 'after']:
                for idx in range(1, len(dest_group[3])):
                    if dest_group[3][idx][0] == rel_group_id:
                        if insert_mode == 'after':
                            idx = idx + 1  # after
                        dest_group[3].insert(idx, moving_group)
                        break
            else:
                raise Exception('Unknown insert_mode: {}'.format(insert_mode))

        def _sub_remove_group(group_id, parent_group, level):
            # The [1:] is because the first element in the list is a list of
            # feeds not groups. The groups starting in the second element.
            for child_group in parent_group[3][1:]:
                if child_group[0] == group_id:
                    parent_group[3].remove(child_group)
                    break

            return True

        _log.debug('Move group {} to {} via {} {}'
                   .format(group_id, dest_group_id,
                           insert_mode, rel_group_id))
        # connect the group to move with the new destination group
        self.walk_groups(_sub_insert_group_dest, dest_group_id)

# ------------
# -- Informations
# ------------
    def get_full_path(self, the_id):
        """Returning the path to a specific feed or group as a list of its
        parent group ids.

        If a feed with 'the_id' exists a 'list()' of group ids is returned
        representing the path to that feed. The first element is the highest
        group (first level). Be aware that the last element is not a group id
        but the 'feed_id' itself.

        Args:
            the_id (int): Id of a feed or group.

        Returns:
            (list): Path.
        """
        def _get_full_path_of_feed(feed_id, tree, path):
            if feed_id in tree[0]:
                path.append(feed_id)
                return path

            for group in tree[1:]:
                # append group id
                path.append(group[0])
                # upstairs into that group
                sub_path = _get_full_path_of_feed(feed_id=feed_id,
                                                  tree=group[3],
                                                  path=path)
                #
                if sub_path[-1] != group[0]:
                    # the feed was found and the path is correct
                    return sub_path
                else:
                    # feed not found in that group
                    path = sub_path[:-1]

            return path

        def _get_full_path_of_group(group_id, tree, path):
            for group in tree[1:]:
                # append group id
                path.append(group[0])

                # match?
                if group[0] == group_id:
                    return path

                # upstairs into that group
                sub_path = _get_full_path_of_group(group_id,
                                                   tree=group[3],
                                                   path=path)
                #
                if sub_path[-1] != group[0]:
                    # group was found and the path is correct
                    return sub_path
                else:
                    # group not found in that group
                    path = sub_path[:-1]

            return path

        # it is a Feed
        if the_id > basics.GROUPID_FEEDS:
            return _get_full_path_of_feed(the_id, self._tree, [])
        # it is a Group (incl. root group)
        elif (the_id <= basics.GROUPID_MIN or the_id == basics.GROUPID_FEEDS):
            return _get_full_path_of_group(the_id, self._tree, [])
        else:
            raise Exception('Unhandled situation. the_id {}'
                            .format(the_id))

# ------------
# -- Misc & uncatagorized
# ------------
    def get_entries_filename(self, feed_id):
        """Return the full path filename of the entries file related to
        'feed_id'.

        Args:
            feed_id (int): ID of a feed.

        Returns:
            (string): The full path including filename.

        Raises:
            KeyError: If the feed_id is unknown.
        """
        dir_entries = basics.feed_entries_directory()
        fn_entries = self._feeds[feed_id][_ENTRIES_FILE_KEY]
        fn_entries = os.path.join(dir_entries, fn_entries)

        return fn_entries

    def get_favicon_filename(self, feed_id):
        """Return the full path filename of the favicon file related to
        'feed_id'.

        Args:
            feed_id (int): ID of a feed.

        Returns:
            (string): The full path including filename.

        Raises:
            KeyError: If the feed_id is unknown.
        """
        fn_favicon = self._feeds[feed_id]['favicon']['filename']
        dir_icons = basics.feed_favicons_directory()
        fn_favicon = os.path.join(dir_icons, fn_favicon)

        return fn_favicon

    def delete_entry_and_icon_files(self, feed_id):
        """Delete the files holding entries and favicons.

        If the feed_id is unknown the raised KeyErrors are silently catched.
        """
        # delete entries file
        try:
            os.remove(self.get_entries_filename(feed_id))
        except KeyError:
            pass

        # delete icon file
        try:
            os.remove(self.get_favicon_filename(feed_id))
        except KeyError:
            pass

    def _get_href_from_feed_data(self, feed_data):
        """A helper function to get a href information from a
        feedparser.FeedParserDict object because it is located at different
        places. The meaning of href here is not the URL of the feed itself but
        the main website where the feed is located.

        Args:
            feed_data (feedparser.FeedParserDict): The original feed data.

        Returns:
            (str): The href information.
        """
        try:
            return feed_data.feed.href
        except AttributeError:
            pass

        try:
            for link in feed_data.feed.links:
                if link['rel'] == 'alternate':
                    return link['href']
        except AttributeError:
            pass

        return None

    def _DEPRECATED_set_feeds_read(self, feed_ids):
        """
        """
        if type(feed_ids) != list:
            feed_ids = [feed_ids]

        # each feed
        for fid in feed_ids:
            entry_idx = 0
            # each entry
            while entry_idx < len(self._entries[fid]):
                self.set_entry_read(fid, entry_idx)
                entry_idx += 1

    def _DEPRECATED_set_entry_read(self, feed_id, entry_idx):
        """Mark a feed entry as read.

        The entry is also removed from the unread list and an event is fired.

        Args:
            feed_id (int): Id of the feed.
            entry_idx (int): Index of the entry in the feed.
        """
        # remember current read status
        old_read = self._entries[feed_id][entry_idx]['feedybus']['read']

        # act only if it was unread
        if old_read is False:
            # mark entry read
            self._entries[feed_id][entry_idx]['feedybus']['read'] = True
            # remove feed from unread-list
            self._unread[feed_id].remove(entry_idx)
            # fire data event
            self.event_entry_marked_read.notify(feed_id, entry_idx)

    def remove_feed_update_keys(self, feed_id=None):
        """Remove all _FEED_UPDATE_MECHANIMS fields/keys from a feed.

        Args:
            feed_id (int): Feed identifier.
        """
        # normalize the 'id' argument to a list of IDs
        if feed_id is not None:
            # one iid
            key_list = [feed_id]
        else:
            # all id's
            key_list = self.get_feed_ids()

        # each iid in the list
        for cur_iid in key_list:
            try:
                del self._feeds[cur_iid]['update_mechanism']
            except KeyError:
                pass

            """
            for mecha_key in _FEED_UPDATE_MECHANISM:
                # remove if exist
                try:
                    del self._feeds[cur_iid][mecha_key]
                except KeyError as err:
                    pass
            """

    def _get_sub_tree_by_id(self, group_id):
        """Return a part of the tree where 'group_id' is the parent from

        If nothing was found it returns [None, None].

        Args:
            group_id (int): Id of a group.

        Returns:
            (int, int): The group itself and its level in the tree.
        """
        def _the_sub_tree(result, group, level):
            result.append(group)
            result.append(level)

        result = []
        self.walk_groups(partial(_the_sub_tree, result), group_id)

        # if nothing was found
        if result == []:
            warnings.warn('FeedsData:_get_sub_tree_by_id() found '
                          'nothing and should raise a ValueError in '
                          'the future. Currently just (None, None) '
                          'is returned.', FutureWarning)
            result = [None, None]

        return (result[0], result[1])

    def get_feed_ids(self, tree_or_group_id=None, recursive=True):
        """Return a list with feed id's of 'tree_or_group_id' (recursive).

        By default the whole tree is used. Also a section can be specified by
        using 'tree_or_group_id' which can be an id of a group or a group
        object itself.

        Args:
            tree_or_group_id (list() or int): Default 'None' means the whole
                                              tree.

        Returns:
            (list(int)): An unsorted list of feed ids.
        """
        # DEBUG, remove later
        if type(tree_or_group_id) is tuple:
            raise Exception('tree is tupple')

        # check if 'tree_or_group_id' is an id or a group object
        if tree_or_group_id is None:
            # use whole tree by default
            start_group_id = None
        else:
            if type(tree_or_group_id) is int:
                # sub-group by id
                start_group_id = tree_or_group_id
            else:
                # sub-group
                start_group_id = tree_or_group_id[0]

        # for the resulting ids
        feed_ids = []

        if recursive:
            # helper/callback function for each group
            def _get_feed_ids(group, level):
                feed_ids.extend(group[3][0])

            self.walk_groups(func=_get_feed_ids,
                             start_group_id=start_group_id)
        else:
            group = self._get_sub_tree_by_id(start_group_id)[0]
            feed_ids = group[3][0]

        return feed_ids

    def get_group_ids(self, tree_or_group_id=None, recursive=True):
        """Return a list with child groups IDs of the group specified or of
        all groups.

        Group can be specified by ID or can givin itself as a subtree. Feed
        IDs not used. The list of IDs is orded as they appear in the tree. It
        means: Imagine the complete unfolded tree (e.g. in the TreeCtrl) and
        go from top to bottom over each line and ignore the folding level.

        Args:
            tree_or_group_id (): Default is 'None'.
            recursive (bool): Default is 'True'

        Returns:
            list(int): An unsorted list of IDs.
        """
        # use complete tree
        if tree_or_group_id in [None, basics.GROUPID_FEEDS]:
            tree = self._tree[1][3]
        # group_id specified?
        elif type(tree_or_group_id) == int:
            # get sub-tree related to the group_id
            tree = self._get_sub_tree_by_id(tree_or_group_id)[0]
            tree = tree[3]
        else:
            # assume a valid tree object
            tree = tree_or_group_id

        keys = []
        for group_item in tree[1:]:
            keys.append(group_item[0])
            if recursive:
                keys.extend(self.get_group_ids(group_item[3], True))

        return keys

    def groups_have_same_parent(self, group_a, group_b):
        """Check if this groups are in the same level (means having the same
        parent group).

        Returns:
            bool:
        """
        a = self.get_full_path(group_a)
        b = self.get_full_path(group_b)
        if a[:-1] == b[:-1]:
            return True

        return False

# ------------
# -- DEPRECATED
# ------------
    def DEPRECATED_get_parent(self, the_id):
        """Return the ID of the parent.

        Args:
            the_id (int): ID of a feed or group.

        Returns:
            (int): ID of the parent or None.
        """
        warning.warn(DeprecationWarning)

        try:
            return self.get_full_path(the_id)[-2]
        except IndexError:
            return None

    def DEPRECATED_create_feed(self, feed_data, parent_id):
        """Create a new feed based on 'feed_data'.

        Args:
            feed_data (feedparser.FeedParserDict): Parsed feed data.
            parent_id (int): Id of the parent group.

        Returns:
            (int): The id of the new feed.
        """
        if type(feed_data) is not feedparser.FeedParserDict:
            raise TypeError('feed_data should be FeedParserDict. Maybe you '
                            'need the method create_feed_v2.')

        warnings.warn('Replace by create_feed_v2() and rename later',
                      DeprecationWarning)

        # get next/free feed_id
        new_id = self.get_free_feed_id()

        # add to data structure
        self._feeds[new_id] = {
            'title': getattr(feed_data.feed, 'title', _('<unnamed>')),
            'text': getattr(feed_data.feed, 'text',
                            getattr(feed_data.feed,
                                    'title', _('<unnamed>'))
                            ),
            'htmlUrl': self._get_href_from_feed_data(feed_data),
            'xmlUrl': feed_data.href,
            'version': getattr(feed_data, 'version', 'unknown'),
            'author': feed_data.feed.get('author', None),
            'rights': feed_data.feed.get('rights', None),
            'description': feed_data.feed.get('description', None),
            'encoding': feed_data.get('encoding', None)
        }
        self._entries[new_id] = []

        # helper to insert it in the tree
        def _sub_create_feed(group, level):
            group[3][0].append(new_id)

        # walk
        self.walk_groups(_sub_create_feed, [parent_id])

        # notify (e.g. the TreeView) about a new item
        self.event_tree_item_new_or_modified.notify(new_id)

        return new_id

    def DEPRECATED_is_regular_group(self, item_id, include_root=True):
        """Check it the item is a regular group.

        It means that it is a group with feeds as childs. It does not mean
        in-build 'smart groups' like 'Read Later'.

        Args:
            item_id(int): The id of the item to check.

        Returns:
            bool
        """
        warnings.warn('Replace by Group.is_gid_valid()', DeprecationWarning)

        # is it a feed group
        if item_id <= basics.GROUPID_MIN:
            return True

        # or the root group "Feeds" itself
        if item_id == basics.GROUPID_FEEDS and include_root is True:
            return True

        return False

    def DEPRECATED_is_regular_feed(self, item_id):
        """Check if the item is a regular feed.

        Args:
            item_id (int): The id of the item to check.

        Returns:
            bool
        """
        warnings.warn('Replace by Feed.is_fid_valid()', DeprecationWarning)

        try:
            if item_id > basics.GROUPID_FEEDS:
                return True
        except TypeError:
            # item_id == None
            pass

        return False

    def DEPRECATED_get_free_group_id(self):
        """Compute the next free id for a group.

        Returns:
            int: Unused group_id.
        """
        warnings.warn('', DeprecationWarning)

        group_ids = sorted(self.get_group_ids())

        if len(group_ids) == 0:
            return basics.GROUPID_MIN

        # are there empty ids between existing groups?
        if len(group_ids) < abs(group_ids[0] - (basics.GROUPID_MIN + 1)):
            new_id = basics.GROUPID_MIN
            while new_id in group_ids:
                # keep in mind that group ids counted into negative direction
                new_id = new_id - 1
        else:
            new_id = group_ids[0] - 1

        return new_id

    def DEPRECATED_get_free_feed_id(self):
        """Compute a free (not used) feed_id.

        Returns:
            (int): A unused/new feed id.
        """
        warnings.warn('', DeprecationWarning)

        feed_ids = sorted(self.get_feed_ids())

        # are there empty ids between existing feeds?
        try:
            if len(feed_ids) < feed_ids[-1]:
                new_id = basics.GROUPID_FEEDS + 1
                # find the free id
                while new_id in feed_ids:
                    new_id = new_id + 1
            else:
                new_id = feed_ids[-1] + 1
        except IndexError:
            return basics.GROUPID_FEEDS + 1

        return new_id

    def DEPRECATED_get_label(self, item_id):
        """Return the 'label' of an item.
        REFACTOR DUPLICATE

        For a feed 'label' mean its 'text' attribute. For a group it means the
        second element.

        Args:
            item_id (int): Id of feed or group.

        Returns:
            (str): The label or None if nothing was found.
        """
        warning.warn(DeprecationWarning)

        if self.is_regular_feed(item_id):
            return self._feeds[item_id]['text']

        if self.is_regular_group(item_id):
            group = self._get_sub_tree_by_id(item_id)[0]
            return group[1]

        return None

    def DEPRECATED_is_regular_feed_or_group(self, item_id, include_root=True):
        """Check if the item is a regular Group or Feed.

        Regular in the meaning of a feed or a group holding feeds. Logical or
        'smart' groups (e.g. 'unread', 'read later') not included.

        Args:
            item_id (int): ID of the item to test.
            include_root (bool): If True (default) the root group is treated
            as regular, too.
        """
        if self.is_regular_group(item_id, include_root) or \
           self.is_regular_feed(item_id):
            return True

        return False

    def DEPRECATED_rename_item(self, item_id, new_name):
        """Change the name of a group or feed item.

        This method is restricted to regular groups or feeds only.

        Args:
            item_id (int): Id of a feed or group.
            new_name (str). New name/label of the item.

        Returns:
            (bool): True if successfull. False if not.
        """
        if not self.is_regular_feed_or_group(item_id):
            _log.warning('FeedsData.rename_item() -- It is not allowed to '
                         'rename item {}'.format(item_id))
            return False

        new_name = new_name.strip()

        if self.is_regular_group(item_id):
            group = self._get_sub_tree_by_id(item_id)[0]
            group[1] = new_name
        else:
            self._feeds[item_id]['text'] = new_name

        self.event_tree_item_new_or_modified.notify(item_id)

        return True

    def DEPREACTED_insert_new_group(self, group_data, parent):
        """Create a new group in the data layer.

        Args:
            group_data ([]): Data structure desribing the group.
            parent (int): Id of the parent group.
        """
        # helper function to insert the new group in the tree data
        def _sub_create_group(group, level):
            group[3].append(group_data)

        # execute helper function
        helper_called = self.walk_groups(_sub_create_group, [parent])

        # The parent does not exist in the tree.
        if helper_called is not True:
            raise Exception('Unknown parent {}'.format(parent))

        # unfold all (grand)parents
        for group_id in self.get_full_path(group_data[0])[:-1]:
            self.set_group_unfolded(group_id, True)

        # notify (e.g. the TreeView) about a new item
        self.event_tree_item_new_or_modified.notify(group_data[0])

    def _DEPRECATED_get_date_string(self, entry, feed_id=None):
        """Extract the date of 'entry' in format YYYY-MM-DD, return it as
        as a string and store it to the entry (in ['feedybus']['date_str']).

        Args:
            entry (dict()): The feed entry.
            feed_id (int): For debug reasons (Default: None).

        Returns:
            (str): The date as a string.
        """
        # use a previouse computed result if it exist
        try:
            return entry['feedybus']['date_str']
        except KeyError:
            pass

        # Date
        date_str = None
        for date_key in ['published_parsed',
                         'updated_parsed']:
            if date_key in entry and entry[date_key]:
                date_str = '{}-{:02d}-{:02d}' \
                    .format(entry[date_key][0],
                            entry[date_key][1],
                            entry[date_key][2])
                break
        if not date_str:
            # see https://github.com/kurtmckee/feedparser/issues/135
            na = '<n.a.>'
            date_str = entry.get('published', na)
            # sometimes the field 'published' exists but is empty
            if not date_str:
                date_str = na

        if 'feedybus' not in entry:
            entry['feedybus'] = {}

        entry['feedybus']['date_str'] = date_str

        return date_str

    def _DEPRECATED_get_total(self, item_id):
        """Calculate the total and unread count of entries in a specific feed
        or group.

        If 'item_id' specifing the read-later group (bascis.GROUPID_LATER) the
        calculation is also used to build the 'self._later' reference list.
        If 'item_id' specifing the unread group (bascis.GROUPID_UNREAD) the
        calculation is also used to build the 'self._unread' reference list.

        Args:
            item_id (int): ID of a group or feed

        Returns:
            (bool): Unread
            (bool): Total
        """
        # smart/special groups
        if (item_id > basics.GROUPID_MIN and item_id <= basics.GROUPID_FEEDS):
            # compute total and unread for entries of all feeds
            unread = 0
            total = 0
            for feed_id in self._entries:
                feed_unread, feed_total = self.get_total(feed_id)
                unread += feed_unread
                total += feed_total

            if basics.GROUPID_FEEDS == item_id:
                return unread, total
            elif basics.GROUPID_UNREAD == item_id:
                return unread, None
            elif basics.GROUPID_LATER == item_id:
                later = 0
                # each feed
                for feed_id in self._entries:
                    lst_later = []
                    # each entry
                    for idx in range(len(self._entries[feed_id])):
                        entry = self._entries[feed_id][idx]
                        try:
                            read_later = entry['feedybus'] \
                                .get('read_later', False)
                        except KeyError:
                            read_later = False

                        if read_later is True:
                            later += 1
                            lst_later.append(idx)
                    if len(lst_later) > 0:
                        self._later[feed_id] = lst_later
                return None, later
            elif basics.GROUPID_DEV == item_id:
                return None, None

            _log.critical('Unknown item_id "{}" in get_total()'
                          .format(item_id))
            return None, None
        elif item_id <= basics.GROUPID_MIN:  # regular/catagory groups
            # DEPRECATED
            unread = 0
            total = 0
            sub_tree = self._get_sub_tree_by_id(item_id)[0]
            for feed_id in self.get_feed_ids(sub_tree):
                feed_unread, feed_total = self.get_total(feed_id)
                unread += feed_unread
                total += feed_total
            return unread, total
        elif item_id > basics.GROUPID_FEEDS:  # specific feed
            # DEPRECATED
            # compute number of unread entries
            unread = 0
            lst_unread = []
            # number of all entries
            total = len(self._entries[item_id])
            # each entry
            for idx in range(total):
                entry = self._entries[item_id][idx]
                try:
                    read_status = entry['feedybus']['read']
                except KeyError:
                    read_status = False

                if read_status is False:
                    unread += 1
                    lst_unread.append(idx)

            # remember the unread entries for the "Unread" group
            if len(lst_unread) > 0:
                self._unread[item_id] = lst_unread

            # result
            return unread, total
        else:
            _log.critical('Should never be reached! item_id = {}'
                          .format(item_id))
            raise Exception()

    def _DEPRECATED_get_group_unfolded(self, group_id):
        """Return the 'True' if the group is unfolded/expanded and 'False' if
        it is folded/collapsed.

        Returns:
            (bool): Unfolded state.
        """
        group = self._get_sub_tree_by_id(group_id)[0]
        return group[2]

    def _DEPRECATED_set_group_unfolded(self, group_id, unfolded,
                                       children=False):
        """Set the unfolded/expaneded state of a group.

        Args:
            group_id (int): ID of group.
            unfolded (bool): Unfolded state.
        """
        def _set_fold_state(group, level):
            group[2] = unfolded

        if children:
            # this group and all its children
            self.walk_groups(func=_set_fold_state,
                             item_ids=[],
                             start_group_id=group_id)
        else:
            # only this one group
            self.walk_groups(func=_set_fold_state,
                             item_ids=group_id)

    def DEPRECATED_get_previous_group(self, group_id, same_level_only=False):
        """Return the id of the previous group in the same level.

        If 'same_level_only' is False (the default) and the group does not
        exists in the current tree level then the search algoritm steps one
        level up. If nothing is found 'None' is returned.

        Args:
            group_id (int): Id of the group to start from.
            same_level_only (bool): Search only in the current level.
            (default: False)

        Returns:
            (int): The id of the previous group or 'None' if nothing was
            found.
        """
        if same_level_only:
            parent = self.get_parent(group_id)
            # get IDs only of the current level
            groups = self.get_group_ids(parent, False)
        else:
            # get a well ordered list of all group IDs
            groups = self.get_group_ids()
            # add root ot the list
            groups.insert(0, basics.GROUPID_FEEDS)

        # get index of previous group
        idx = groups.index(group_id) - 1

        if idx < 0:
            return None

        return groups[idx]

    def _DEPRECATED_delete_group(self, group_id):
        """Delete a group with its child groups and the feeds.

        Don't confuse this methode with 'self.remove_group()' which only
        deattach a group from the tree structure without deleting it.

        Args:
            group_id (int): The groups id.
        """
        # all feed ids of that group and its child groups
        feed_ids = self.get_feed_ids(group_id)

        # delete all feeds
        self.delete_feeds(feed_ids, False)

        # Do not delete root group but all its childs.
        if group_id == 0:
            child_group_ids = self.get_group_ids(recursive=False)
            for gid in child_group_ids:
                self.remove_group(gid)
                # remember for notification
                feed_ids.append(gid)
        else:
            # delete the group
            self.remove_group(group_id)
            # remember for notification
            feed_ids.append(group_id)

        # notify the presentation layer (TreeView)
        self.event_deleted_tree_items.notify(feed_ids)

    def _DEPRECATED_remove_group(self, group_id):
        """
        pre DEPRECATED. Remove when self.delte_group() is deprecated by
        Group.delete()
        Remove the group specified by its id from the tree structure
        including its child groups but do not delete it.

        The feeds in that group are not touched. You need to check for and
        handle feeds in that groups by yourself! See 'self.delete_group()' for
        an alternative.
        Do not remove group 0 ('Feeds' the root of all groups and feeds)
        because this would cause undefined behaviour.

        Args:
            group_id (int): The groups id.
        """
        def _remove_group(group_id, parent_group, level):
            # The [1:] is because the first element in the list is a list of
            # feeds not groups. The groups starting in the second element.
            for child_group in parent_group[3][1:]:
                if child_group[0] == group_id:
                    parent_group[3].remove(child_group)
                    break

            return True

        parent_group_id = self.get_full_path(group_id)[-2:-1]
        self.walk_groups(partial(_remove_group, group_id), parent_group_id)

    def _DEPRECATED_delete_feed(self, feed_id):
        """Delete one feed from the data structure.

        Args:
            feed_id (int): Id of the feed.
        """
        self.delete_feeds([feed_id])

    def _DEPRECATED_delete_feeds(self, feed_ids, notify=True):
        """Delete multiple feeds from the data structure.

        If 'notify' is 'True' the 'self.event_delete_tree_items' will be
        fired.

        Args:
            feed_ids (list(int)): List of feed id's.
            notify (bool): If 'True' (default) an event is fired.
        """
        parent = None
        for feed_id in feed_ids:
            # Do not do the ressource intensive parent search process for each
            # feed if they have the same parents
            if (not parent) or (feed_id not in parent[3][0]):
                parent = self.get_full_path(feed_id)[-2:-1]
                parent = self._get_sub_tree_by_id(parent)[0]

            # remove feed from group
            parent[3][0].remove(feed_id)

            # delete references for unread and later read
            try:
                del self._unread[feed_id]
            except KeyError:
                pass
            try:
                del self._later[feed_id]
            except KeyError:
                pass

            # delete entries file
            try:
                dir_entries = basics.feed_entries_directory()
                fn_entries = self._feeds[feed_id][_ENTRIES_FILE_KEY]
                fn_entries = os.path.join(dir_entries, fn_entries)
                os.remove(fn_entries)
            except KeyError:
                pass

            # delete icon file
            try:
                fn_favicon = self._feeds[feed_id]['favicon']['filename']
                dir_icons = basics.feed_favicons_directory()
                fn_favicon = os.path.join(dir_icons, fn_favicon)
                os.remove(fn_favicon)
            except KeyError:
                pass

            # delete the feed and its entries
            del self._feeds[feed_id]
            del self._entries[feed_id]

        # notify the presentation layer (TreeView)
        if notify:
            self.event_deleted_tree_items.notify(feed_ids)

    def DEPRECATED_toggle_entry_read_later(self, feed_id, entry_idx):
        """Toogle the read-later status of an entry and return the new status.

        In the data layer the status is stored in the entries list specified
        by keys ['feedybus']['read_later']. This methode also modifies the
        'self._later' reference list.

        Args:
            feed_id (int): Id of the feed.
            entry_idx (int): Index of the entrie in the feed.

        Returns:
            (bool): New read-later-status
        """
        keya = 'feedybus'
        keyb = 'read_later'
        # current state
        old_later = self._entries[feed_id][entry_idx][keya].get(keyb, False)
        # toggle the state
        self._entries[feed_id][entry_idx][keya][keyb] = not old_later
        # new state
        new_later = self._entries[feed_id][entry_idx]['feedybus']['read_later']
        # take care of read-later reference list
        if new_later:  # read-later
            if feed_id not in self._later:
                self._later[feed_id] = []
            self._later[feed_id].append(entry_idx)
        else:  # not read-later
            self._later[feed_id].remove(entry_idx)

        # fire event
        self.event_entry_toggled_read_later.notify(feed_id, entry_idx)

        # return new status
        return new_later
