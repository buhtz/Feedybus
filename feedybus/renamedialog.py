# -*- coding: utf-8 -*-
import logging
from feedybus import basics
from gi.repository import Gtk


"""
    A simple dialog to rename entities.

    The dialog is used to ask for new names (renaming) feeds or groups.
"""


_log = logging.getLogger(basics.GetLogId(__file__))


class ReNameDialog(Gtk.Dialog):
    """Dialog to modify a given string.

    It is used to rename feeds, rename groups and name new groups. A callback
    method is called when OK is clicked.
    """
    def __init__(self, window,
                 value='',
                 title=_('Rename'),
                 placeholder_text=_('name')):
        """Construct the dialog object but you need to call 'run()' on it
        yourself.

        Args:
            window (Gtk.Window): Main application window.
            title (str): String shown in the titlebar of the dialog.
            placeholder_text (str): Placeholder string for entry field.
        """
        Gtk.Dialog.__init__(self, title, window, 0,
                            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                             Gtk.STOCK_OK, Gtk.ResponseType.OK))
        self.set_position(Gtk.WindowPosition.MOUSE)

        ## the value
        self.renamed_value = None

        # edit field
        self._entry = Gtk.Entry(hexpand=True)
        self._entry.set_placeholder_text(placeholder_text)
        self._entry.set_text(value)
        self._entry.select_region(0, -1)

        # layout dialog
        self.get_content_area().add(self._entry)
        self.show_all()

        # event
        self.connect('response', self._on_response)
        self._entry.connect('activate', self.on_activate)

    def _on_response(self, dialog, response_id):
        """Event handler if OK/CANCLE was clicked.

        The methode handles the final data handling, closing and destroy fully
        autonomus."""
        if response_id == Gtk.ResponseType.OK:
            self.renamed_value = self._entry.get_text()

        self.destroy()

    def on_activate(self, entry):
        """Event handler for pressed ENTER key.
        """
        # empty string is invalid
        if len(self._entry.get_text()) > 0:
            # ENTER is per default not OK
            # because of that we set it explicite here
            self.response(Gtk.ResponseType.OK)

        self.close()
