import os
import multiprocessing
import threading
import uuid
import copy
import logging
import asyncio
import aiohttp
from html.parser import HTMLParser
from urllib.parse import urlparse
from datetime import datetime
from datetime import timedelta
from gi.repository import GdkPixbuf
from gi.repository import GLib
from gi.repository import Gio
import feedparser
from feedybus import basics


_log = logging.getLogger(basics.GetLogId(__file__))


## Default icon size.
ICON_SIZE = 16
## File format of local stored icons.
ICON_FORMAT = 'ico'
## Next icon check in n days.
ICON_UPDATE_INTERVAL = 30


class FaviconThread(threading.Thread):
    """Wraps up the start of FaviconProcess and wait for its result.
        see https://stackoverflow.com/a/56324244/4865723 for explanation
    """
    def __init__(self, feeds, callback, only_this_feed_ids=None):
        """
        """
        threading.Thread.__init__(self)
        ##
        self._feeds = {}

        #
        if only_this_feed_ids is None:
            only_this_feed_ids = list(feeds.keys())
        # Create deep/real copies of the icon (and some more) infos of each
        # feed. We need this because a Process is not allowed to modify the
        # original feed data.
        for f in only_this_feed_ids:
            self._feeds[f] = {}
            # favicon infos
            try:
                self._feeds[f]['favicon'] = copy.deepcopy(feeds[f]['favicon'])
            except KeyError:
                pass
            # newsfeed url
            self._feeds[f]['xmlUrl'] = copy.deepcopy(feeds[f]['xmlUrl'])
            # website url
            try:
                self._feeds[f]['htmlUrl'] = copy.deepcopy(feeds[f]['htmlUrl'])
            except KeyError:
                pass
            # title
            try:
                self._feeds[f]['title'] = copy.deepcopy(feeds[f]['title'])
            except KeyError:
                self._feeds[f]['title'] = copy.deepcopy(feeds[f]['text'])

        ## Called (by the GUI event loop) when thread finished
        self._callback = callback

    def run(self):
        """
        """
        _log.info('Check for favicons...')
        queue = multiprocessing.Queue()
        stop_sentinel = str(uuid.uuid1())
        process = FaviconProcess(queue, self._feeds, stop_sentinel)
        process.start()
        self.result_icons = []
        while True:
            process_result = queue.get()
            # stop mark means the process is finished
            if process_result == stop_sentinel:
                break
            # merge dicts
            self.result_icons.append(process_result)

        #
        self.result_update_feeds = queue.get()

        process.join()
        # inform GUI main loop about finished operation
        GLib.idle_add(self._callback)


class FaviconProcess(multiprocessing.Process):
    """Load favicons for the feeds from local drive or remote feed.
    """
    class IconFinder(HTMLParser):
        """HTML Parser that looks for icon information in HTML content.

        It parses only until the head section of HTML content ends and raise
        'HeaderEnd' exception in that case. If an icon URL is found it raises
        a 'Found' exception.
        This parser only likes str!"""

        class Found(Exception):
            """Icon info was found.

            The path is stored in 'self.message'. Derived from 'Exception'.
            """
            pass

        class HeaderEnd(Exception):
            """Finished parsing the head of HTML content without finding icon
            informations.

            Derived from 'Excetion'."""
            pass

        def __init__(self):
            """
            """
            ## The base URI
            self._icon_base = ''
            ## The results
            self._icons = []

            HTMLParser.__init__(self)

        def handle_starttag(self, tag, attrs):
            """Handler looking specific for icon informations.
            """
            if tag == 'base':
                # convert list of tuples into dict
                attrs = dict((k.lower(), v) for k, v in attrs)
                try:
                    self._icon_base = attrs['href']
                except KeyError:
                    pass
                return

            if tag != 'link':
                return

            # convert list of tuples into dict
            attrs = dict((k.lower(), v) for k, v in attrs)

            # Don't make to much hack-mac about it!
            # Just use the first icon found and ignore others.
            try:
                if attrs['rel'].lower() in ['shortcut icon', 'icon']:
                    self._icons.append(attrs['href'])
            except KeyError:
                pass

        def handle_endtag(self, tag):
            """Handler looking specific for the end of HTML header.

            Raises:
                'Found': A icon URL was found and stored as
                exception message.
                'HeaderEnd': No icon found and end of header info in HTML
                content was reached.
            """
            if tag != 'head':
                return

            if self._icons:
                self._icons = [self._icon_base + ico for ico in self._icons]
                found = FaviconProcess.IconFinder.Found()
                found.icons = self._icons
                raise found
            else:
                raise FaviconProcess.IconFinder.HeaderEnd()

    def __init__(self, queue, feeds, stop_sentinel):
        """
        """
        multiprocessing.Process.__init__(self)
        ## Queue to transfer data to `FaviconThread`
        self._queue = queue
        ## Unique string mark the end of the queue (generated with UUID)
        self._stop_sentinel = stop_sentinel
        ##  Feeds
        self._feeds = feeds
        ## Default headers for HTTP requests
        self._headers = {'User-Agent': basics.USER_AGENT}
        ## Default ClientTimeout for HTTP requests (30 sec)
        self._timeout = aiohttp.ClientTimeout(connect=10)

    def _favicon_load_local(self, fid):
        """Main function to check for favicon information for a specific feed.

        Args:
            fid (int): Feed it.
            feed (dict()): Feed data.

        Returns:
            tuple(): Feed id and pixbuf
        """
        # local favicon
        try:
            icon_file = self._feeds[fid]['favicon'].get('filename', None)
        except KeyError:
            icon_file = None
        finally:
            icon_bytes = None
            if icon_file:
                try:
                    with open(icon_file, 'rb') as f:
                        icon_bytes = f.read()
                except GLib.GError as err:
                    _log.error(err)

        return icon_bytes

    async def _download_raw_content_coroutine(self, fid, url):
        """Coroutine for 'self._download_raw_feed_data()'.
        """
        async with aiohttp.ClientSession(headers=self._headers,
                                         timeout=self._timeout) as session:
            raw_content = None
            try:
                async with session.get(url) as response:
                    raw_content = await response.read()
            except aiohttp.ClientError as err:
                _log.warning('{} {} {}'.format(url, type(err), err))
            except Exception as err:
                _log.critical('{} {} {} {}'.format(fid, type(err), err, url))

        return (fid, raw_content)

    def _download_raw_content(self, urlKey, loop):
        """Download asynchrone the complete content feed file (XML) or webpage
        (HTML) for all feeds specified by 'self._fids_check'.

        Args:
            urlKey (string): Must be 'xmlUrl' or 'htmlUrl'.
            loop (asyncio.AbstractEventLoop): Asyncio event loop.

        Returns:
            (dict()): Contente indexed by Feed ID.
        """
        # Do nothing if empty
        raw_content = {}
        if not self._fids_check:
            return raw_content

        # construct tasks
        futures = []
        for fid in self._fids_check:
            try:
                url = self._feeds[fid][urlKey]
            except KeyError as err:
                continue
            futures.append(self._download_raw_content_coroutine(fid, url))

        # run tasks
        done, pending = loop.run_until_complete(asyncio.wait(futures))

        # prepare results
        for future in done:
            fid, val = future.result()
            # Ignore empty results (e.g. in case of errors).
            if val is None:
                continue
            # Remember content.
            raw_content[fid] = val

        return raw_content

    def _favicon_html_data(self, raw_html, loop):
        """Parse HTML data and download icons inf found.

        Args:
            raw_html (dict): Feed ID indexed HTML data.
            loop (asyncio.AbstractEventLoop): Asyncio event loop.

        Returns:
            (dict): FeedID indexed icon data.
        """
        # Do nothing if no html data is present.
        icos = {}
        if not raw_html:
            return icos

        # Construct asnyncio tasks for each feed.
        futures = []
        for fid in raw_html:
            futures.append(self._favicon_html_data_coroutine(fid,
                                                             raw_html[fid]))

        # Run tasks asynchrone
        done, pending = loop.run_until_complete(asyncio.wait(futures))
        for future in done:
            fid, ico = future.result()
            if ico is not None:
                icos[fid] = ico

        return icos

    async def _favicon_html_data_coroutine(self, feed_id, content):
        """Coroutine for 'self._favicon_html_data'.
        """
        # _log.info(feed_id)
        icon_urls = []
        result_ico = None

        # HTMLParser instance.
        finder = FaviconProcess.IconFinder()

        # The parser likes str() only but we have bytes.
        # For simplicity we asume UTF-8. Webmasters not using it fooling
        # theirselfs.
        content = content.decode('utf-8')

        try:
            # start parsing
            finder.feed(content)
        except FaviconProcess.IconFinder.Found as ico_info:
            icon_urls = ico_info.icons
        except FaviconProcess.IconFinder.HeaderEnd:
            finder.reset()  # throw away unprocessed HTML content
            finder.close()  # end/stop parsing

        # download icon
        for url in icon_urls:
            url = self._relative_url_to_full(url, feed_id)
            result_ico = await self._download_ico(url)
            if result_ico is not None:
                break

        return (feed_id, result_ico)

    def _favicon_feed_data(self, raw_feeds, loop):
        """Parse XML data with feedparsers and download icons if found.

        Args:
            raw_feeds (dict()): Feed ID indexed XML data.
            loop (asyncio.AbstractEventLoop): Asyncio event loop.

        Returns:
            (dict()): Feed ID indexed icon data.
        """
        icos = {}
        if not raw_feeds:
            return icos

        futures = []
        for fid in raw_feeds:
            futures.append(self._favicon_feed_data_coroutine(fid,
                                                             raw_feeds[fid]))
        done, pending = loop.run_until_complete(asyncio.wait(futures))
        for future in done:
            fid, ico = future.result()
            if ico is not None:
                icos[fid] = ico

        return icos

    async def _favicon_feed_data_coroutine(self, fid, feed_data):
        """Coroutine for `self._favicon_feed_data()`.
        """
        # _log.debug('{} - {}'.format(fid, feed_data[:25]))

        # parse raw xml data
        parsed = feedparser.parse(feed_data)

        # Stop if there is a Bozo exception
        if parsed.bozo == 1:
            parsed['bozo_exception'] = str(parsed['bozo_exception'])
            return (fid, None)

        # Check for image information
        url = ''
        try:
            # check 'links'
            url_base = parsed['feed']['base']['href']
            for link in parsed['feed']['links']:
                if link['rel'] == 'shortcut icon':
                    url = url_base + link['href']
        except KeyError as err:
            pass

        try:
            # check 'images'
            if url == '':
                url = parsed['feed']['image']['href']
        except KeyError as err:
            pass

        # Found no url?
        if url == '':
            return (fid, None)

        # download ico
        ico = await self._download_ico(url)
        return (fid, ico)

    async def _download_ico(self, url):
        """Download an icon from the url and return it.

        The methodes checks for HTTP response codes in case of redirects,
        'Page not found' pages and other HTML content.

        Returns:
            (bytes): Icon data.
        """
        # _log.debug('Download ico at {}...'.format(url))
        ico = None

        async with aiohttp.ClientSession(headers=self._headers,
                                         timeout=self._timeout) as session:
            try:
                async with session.get(url) as response:
                    ico = await response.read()
                    # The webserver could send a "Page not found" html
                    # message with last response code 200.
                    # So we can not trust in that 'ico' is an icon file
                    # and we have to check if there was an http error
                    # before.
                    response_list = [response.status]
                    for hist_response in response.history:
                        response_list.append(hist_response.status)
                    last_response = sorted(response_list)[-1]
                    if last_response >= 400:
                        _log.warning('{}: Status {}'
                                     .format(url, last_response))
                        ico = None
                    # Check media type
                    elif ico.decode('utf-8', 'replace').strip() \
                            .lower().startswith('<!doctype'):
                        _log.warning('{}: Looks like HTML -> {}'
                                     .format(url, ico[:20]))
                        ico = None
            except aiohttp.ClientError as err:
                _log.warning('{} {} {}'.format(url, type(err), err))
            except Exception as err:
                _log.critical('{} {} {}'.format(url, type(err), err))

        # Sometimes websites have empty ico files - reallly!
        if ico == b'':
            ico = None

        _log.debug('...{}: Icon download from {}.'
                   .format('success' if ico is not None else 'failure', url))

        return ico

    async def _favicon_root_path_coroutine(self, fid):
        """Coroutine for 'self._favicon_root_path()'.
        """
        # create possible url's
        urls = []
        for url_key in ['xmlUrl', 'htmlUrl']:
            try:
                up = urlparse(self._feeds[fid][url_key])
                netloc = up.netloc.split('.')
                for idx in range(0, len(netloc) - 1):
                    urls.append(up.scheme + '://'
                                + '.'.join(netloc[idx:])
                                + '/favicon.ico')
            except KeyError:
                pass

        # remove duplicates
        urls = list(set(urls))

        # _log.debug('URLs for {}: {}'.format(fid, '\n'.join(urls)))

        # try downloading
        for url in urls:
            ico = await self._download_ico(url)
            if ico is not None:
                break

        return (fid, ico)

    def _favicon_root_path(self, loop):
        """Prepare asynchrone download of favicon in the root path of the
        feeds domains.

        The methode returns only successfull downloaded favicons.

        Args:
            loop (asnycio.EventLoop): The async loop

        Returns:
            (dict{int: bytes}): Dictionary with raw ico-file content indexed
                                by feed IDs
        """
        icos = {}
        # Empty feed ID list?
        if not self._fids_check:
            return icos

        futures = [self._favicon_root_path_coroutine(fid)
                   for fid in self._fids_check]
        done, pending = loop.run_until_complete(asyncio.wait(futures))
        for future in done:
            fid, val = future.result()
            if val is not None:
                icos[fid] = val

        return icos

    def _scale_and_store(self, feed_icos):
        """Try to create correct scaled Pixbuf objects from icon data and
        store it as a local ico-file.

        The feed icon informations is updated. The indexes 'self._fids_check'
        and 'self._fids_load' are updated also.
        """
        for fid in feed_icos:
            icon_bytes = feed_icos[fid]
            # Store the icon as local file and update feed data.
            if self._scale_and_store_coroutine(fid, icon_bytes):
                # Remove from check schedule because successfull
                self._fids_check.remove(fid)
                # Remember to load local icon file
                self._fids_load.append(fid)

    def _scale_and_store_coroutine(self, feed_id, icon_bytes):
        """Coroutine for 'self._scale_and_store()'.
        """
        # _log.debug('\tScale & store icon for feed #{}...'.format(feed_id))
        # create Pixbuf object
        ico = GLib.Bytes(icon_bytes)
        ico = Gio.MemoryInputStream.new_from_bytes(ico)
        try:
            ico = GdkPixbuf.Pixbuf.new_from_stream(ico)
        except GLib.GError as err:
            _log.warning('{} {}\nicon_bytes={}'
                         .format(feed_id, err, icon_bytes[:200]))
            return False

        # scale the icon
        ico = ico.scale_simple(ICON_SIZE, ICON_SIZE,
                               GdkPixbuf.InterpType.BILINEAR)

        # create icon filename
        title = self._feeds[feed_id]['title']
        fn = basics.create_feed_favicon_filename(title, ICON_FORMAT)

        # save icon file
        ico.savev(fn, ICON_FORMAT, '', '')

        # feed information
        self._feeds[feed_id] = {
            'favicon': {
                'filename': fn
            }
        }

        return True

    def _set_next_update(self):
        """Calculate and set the date when a feed needs a new icon update.

        Icon update means the remote feed is checked again for a new icon
        file. All feeds in 'self._fids_update' are took into account. The
        update intervall comes from 'self.ICON_UPDATE_INTERVAL' and is stored
        as an POXIS timestamp as float.
        """
        if not self._fids_update:
            return

        for fid in self._fids_update:
            # compute next update time
            intervall = timedelta(days=ICON_UPDATE_INTERVAL)
            now = datetime.now()
            update = now.timestamp() + intervall.total_seconds()

            if 'favicon' not in self._feeds[fid]:
                self._feeds[fid]['favicon'] = {}

            self._feeds[fid]['favicon']['next_update'] = update

    def _relative_url_to_full(self, url, feed_id):
        """Converts a relative URL to the full one.

        Just returns 'url' if it is not relative. The feeds 'htmlUrl' is used
        to compute the root of the URL.

        Args:
            url (str): Relative URL.
            feed_id (int): ID of related feed.

        Returns:
            (str): Full URL.
        """
        # is relative?
        if url[0] != '/':
            return url

        # parse feeds url
        parsed_url = urlparse(self._feeds[feed_id]['htmlUrl'])

        # // for shorthand protocol
        # see https://stackoverflow.com/a/9646435/4865723
        if url[:2] == '//':
            url = parsed_url.scheme + ':' + url
            return url

        # create root url
        root_url = parsed_url.scheme + '://' + parsed_url.netloc

        # _log.critical('root_url = {}\n\trel_url = {}'.format(root_url, url))

        # full url
        return (root_url + url)

    def run(self):
        """
        """
        ##
        self._fids_update = list(self._feeds.keys())

        ##
        self._fids_check = []

        ##
        self._fids_load = []

        def _debug_favicon_indexes():
            _log.debug('All (n={}): {}\n'
                       '\tCheck for remote favicon (n={}): {}\n'
                       '\tLoad local favicon (n={}): {}\n'
                       '\tUpdate icon information (n={}): {}'
                       .format(len(self._feeds), self._feeds.keys(),
                               len(self._fids_check), self._fids_check,
                               len(self._fids_load), self._fids_load,
                               len(self._fids_update), self._fids_update))

        # |--- Sort and prepare feeds ---|
        _log.debug('Favicon: Sort and prepare feeds...')
        for fid in list(self._feeds):
            # If no icon informations exist, then the feed is new and where
            # never checked for an favicon before.
            if 'favicon' not in self._feeds[fid]:
                self._feeds[fid]['favicon'] = {}
                # remember for favicon-check later
                self._fids_check.append(fid)
                continue

            # Is icon information up-to-date?
            update = self._feeds[fid]['favicon']['next_update']
            update = datetime.utcfromtimestamp(update)
            if update < datetime.now():  # out-dated
                # delete old icon file
                try:
                    icon_file = self._feeds[fid]['favicon']['filename']
                    os.remove(icon_file)
                except (KeyError, FileNotFoundError):
                    pass
                # clear complete icon information
                self._feeds[fid]['favicon'] = {}

                # remember for favicon-check later
                self._fids_check.append(fid)
            else:  # up to date
                # No need to update the original feed data in main process
                self._fids_update.remove(fid)
                if 'filename' not in self._feeds[fid]['favicon']:
                    # Nothing else to do because no 'filename' but up-to-date
                    # 'next_update' means the Feedybus default favicon is used
                    # for that feed because the feeds doesn't have an own
                    # icon.
                    # _log.debug('{} is up2date (default icon).'.format(fid))
                    del self._feeds[fid]
                else:
                    # _log.debug('{} is up-to-date (local icon).'.format(fid))
                    # remember for loading the local favicon file later
                    self._fids_load.append(fid)

        _debug_favicon_indexes()

        # Asyncio loop
        try:
            loop = asyncio.get_event_loop()
        except RuntimeError as err:
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)

        # |--- Check root path ---|
        if len(self._fids_check) > 0:
            _log.debug('Favicon: Checking root paths...')
            root_icos = self._favicon_root_path(loop)
            self._scale_and_store(root_icos)

            _debug_favicon_indexes()

        # |--- Download raw feed data ---|
        if len(self._fids_check) > 0:
            _log.debug('Favicon: Download raw feed data...')
            raw_feeds = self._download_raw_content('xmlUrl', loop)

            # |--- Parse raw feed data & download icon ---|
            _log.debug('Favicon: Parse feed data, download icons...')
            feed_icos = self._favicon_feed_data(raw_feeds, loop)
            self._scale_and_store(feed_icos)

            _debug_favicon_indexes()

        # |--- Download raw webpage data ---|
        if len(self._fids_check) > 0:
            _log.debug('Favicon: Download raw html content...')
            raw_html = self._download_raw_content('htmlUrl', loop)

            # |--- Parse raw html content & download icon ---|
            _log.debug('Favicon: Parse html content, download icons...')
            feed_icos = self._favicon_html_data(raw_html, loop)
            self._scale_and_store(feed_icos)

            _debug_favicon_indexes()

        # |--- Load local & up-to-date favicons ---|
        _log.debug('Favicon: Load {} local favicons...'
                   .format(len(self._fids_load)))
        for fid in self._fids_load:
            icon_bytes = self._favicon_load_local(fid)
            self._queue.put((fid, icon_bytes))

        # |--- Finish. Stop the process ---|
        # Inform the other side (FaviconThread) that we are finished and there
        # won't come any new data into the queue.
        self._queue.put(self._stop_sentinel)

        # Next update in the future
        self._set_next_update()

        # I was not able to solve this with list comprehension. :(
        update_feeds = {}
        for fid in self._fids_update:
            update_feeds[fid] = self._feeds[fid]
        self._queue.put(update_feeds)
