# -*- coding: utf-8 -*-
import logging
import datetime
import gi
# gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import GLib
from feedybus import basics

"""
    A Gtk-based statusbar for Feedybus.

    The statusbar is sepcific for Feedybus and depends on Gtk.
"""


_log = logging.getLogger(basics.GetLogId(__file__))


class Statusbar(Gtk.Statusbar):
    """A Gtk-based statusbar for the Feedybus mainwindow.

    The statusbar is able to 'observe' the threads checking for new feeds.
    """
    def __init__(self):
        """
        """
        Gtk.Statusbar.__init__(self)
        self._set('<FeedStatusbarBase>')

        self.set_has_tooltip(True)
        self.connect('query-tooltip', self._on_query_tooltip)

    def register_update_events(self, thread):
        """Connect 'thread' with the callback event handlers.

        Args:
            thread (threading.Thread): The thread.
        """
        thread.callback_feed_update_started = \
            self._on_callback_feed_update_started
        thread.callback_feed_fetched = self._on_callback_feed_fetched
        thread.callback_feed_parsed = self._on_callback_feed_parsed

    def _on_callback_feed_update_started(self):
        """
        """
        self.inc_started()
        return False

    def _on_callback_feed_fetched(self):
        """
        """
        self.inc_fetched()
        return False

    def _on_callback_feed_parsed(self):
        """
        """
        self.inc_parsed()
        return False

    def _set(self, content):
        """Set and remember the content of the statusbar.

        Args:
            content(str): Content of the bar.
        """
        ## Content of the statusbar
        self._content = content

        self.push(0, self._content)

    def _on_query_tooltip(self, widget, x, y, keyboard_mode, tooltip):
        """The handler is called when a tooltip could be displayed. The
        decision about it is done in the handler itself.

        Args:
            ...

        Returns:
            bool: True if the tooltip should be displayed and False if not.
        """
        tooltip.set_text(self._content)
        return True

    def reset(self, update=False):
        """Set all counters back to 0.

        Args:
            update(bool): Update statusbar text (Default: False)
        """
        ## total feeds count
        self._total = 0

        ## unread feeds count
        self._unread = 0

        ## feeds with problems count
        self._problems = 0

        ## updatding feeds count (Update mode if != 0)
        self._updating = 0

        ## started feeds count
        self._started = 0

        ## fetched feeds count (Update mode)
        self._fetched = 0

        ## parsed feeds count (Update mode)
        self._parsed = 0

        if update:
            self._refresh()

    def inc_started(self):
        """Increment the started feeds counter in statusbar.
        """
        self._started += 1
        self._refresh()

    def inc_fetched(self):
        """Increment the fetched feeds counter in statusbar.
        """
        self._fetched += 1
        self._refresh()

    def dec_fetched(self):
        """Decrement the fetched feeds counter in statusbar.
        """
        self._fetched -= 1
        self._refresh()

    def inc_parsed(self):
        """Increment the parsed feeds counter in statusbar.
        """
        self._parsed += 1
        self._refresh()

        # End of update process.
        if self._parsed == self._updating:
            self._started = 0
            GLib.timeout_add(15000, self._update_finished)

    def _update_finished(self):
        """Update event finished.

        This methodes is called because of a GLib.timeout_add() in
        'self.inc_parsed()'.
        """
        self._started = 0
        return False

    def dec_parsed(self):
        """Decrement the parsed feeds counter in statusbar.
        """
        self._parsed -= 1
        self._refresh()

    def inc_unread(self, update=False):
        """Increment the unread feeds counter in statusbar.

        Args:
            update(bool): Update statusbar text (Default: False)
        """
        self._unread += 1
        if update:
            self._refresh()

    def dec_unread(self, update=False):
        """Decrement the unread feeds counter in statusbar.

        Args:
            update(bool): Update statusbar text (Default: False)
        """
        self._unread -= 1
        if update:
            self._refresh()

    def updating(self, updating):
        """
        """
        if updating > 0:
            self._start_time = datetime.datetime.now()

        self._updating = updating
        self._parsed = 0
        self._fetched = 0
        self._refresh()

    def total(self, total):
        """
        """
        self._total = total
        self._refresh()

    def problems(self, problems):
        """
        """
        self._problems = problems
        self._refresh()

    def _refresh(self):
        """
        """
        if self._updating > 0:
            a = _(' all') if self._updating == self._total else ''
            if self._updating == self._parsed:  # update finished
                # calculcate update duration
                seconds = (datetime.datetime.now() - self._start_time).seconds
                minutes = round(seconds / 60)
                seconds = seconds - (minutes * 60)
                duration = '{} {}'.format(seconds, _('seconds'))
                if minutes > 0:
                    duration = '{} {} {}'.format(minutes,
                                                 _('minutes and'),
                                                 duration)
                # report problems
                problems = ''
                if self._problems > 0:
                    problems = ' {} {} {}' \
                               .format(_('with'),
                                       self._problems,
                                       _('problems'))

                # complete content string
                content = '{}{} {} {} {} {}{}.' \
                          .format(_('Updating'),
                                  a,
                                  self._updating,
                                  _('feeds'),
                                  _('finished in'),
                                  duration,
                                  problems)
            else:  # while update
                fetched_perc = round(100 / self._updating * self._fetched, 1)
                parsed_perc = round(100 / self._updating * self._parsed, 1)
                started_perc = round(100 / self._updating * self._started, 1)
                content = '{}{} {} {}... {}: {} ({} %) -- ' \
                          '{}: {} ({} %) -- {}: {} ({} %)' \
                          .format(_('Updating'),
                                  a,
                                  self._updating,
                                  _('feeds'),
                                  _('Started'),
                                  self._started,
                                  started_perc,
                                  _('Fetched'),
                                  self._fetched,
                                  fetched_perc,
                                  _('Parsed'),
                                  self._parsed,
                                  parsed_perc)
        else:  # default view
            # total
            content = '{} {} {}'.format(_('Total'), self._total, _('feeds'))

            # problems
            if self._problems != 0:
                content += ', {} {}'.format(self._problems, _('problems'))

            # unread
            if self._unread != 0:
                content += ', {} {}'.format(self._unread, _('unread'))

        self._set(content)
