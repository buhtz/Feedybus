import unittest
from . import helper
from feedybus import basics
from feedybus.data import FeedsData


"""
    short

    long
"""


class TestBasics(unittest.TestCase):
    """
    """
    @classmethod
    def setUpClass(cls):
        """
        """
        # set unittest profile
        basics.set_profile('UNITTEST.{}'.format(helper.rand_string()))
        basics.check_directory_existance()

        # load the data
        FeedsData()

    @classmethod
    def tearDownClass(cls):
        """
        """
        helper.clear_profile()
        FeedsData.me = None

    def test_pep8_conformance(self):
        """
        """
        # this test file
        self.assertTrue(helper.pep8_conform(__file__), 'Test-file')
        # the class to test
        self.assertTrue(helper.pep8_conform(basics.Event))

    def test_event(self):
        """Test the event class.
        """
        event = basics.Event()
        e_tester = helper.TestEventHelper(event)

        # simple notify
        event.notify()
        self.assertEqual(e_tester.notified, 1)

        # notify with args, kwargs
        event.notify(9, a=7)
        self.assertEqual(e_tester.notified, 2)
        self.assertEqual(e_tester.last_args[0], 9)
        self.assertEqual(e_tester.last_kwargs['a'], 7)

        # silent notify
        with event.keep_silent():
            event.notify()
        self.assertEqual(e_tester.notified, 2)
