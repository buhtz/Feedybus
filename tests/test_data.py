import unittest
import unittest.mock
from . import helper
from feedybus import basics
from feedybus.data import FeedsData


"""
    short

    long
"""


class TestFeedsData(unittest.TestCase):
    """
    """
    @classmethod
    def setUpClass(cls):
        """
        """
        # set unittest profile
        basics.set_profile('UNITTEST.{}'.format(helper.rand_string()))
        basics.check_directory_existance()

        # moch the data
        with unittest.mock.patch('feedybus.basics._DefaultFeeds',
                                 return_value=helper.DATA_default):
            # load the data
            FeedsData()

    @classmethod
    def tearDownClass(cls):
        """
        """
        helper.clear_profile()
        FeedsData.me = None

    def test_pep8_conformance(self):
        """
        """
        # this test file
        self.assertTrue(helper.pep8_conform(__file__), 'Test-file')
        # the class to test
        self.assertTrue(helper.pep8_conform(FeedsData), 'File to test')

    def test_singleton(self):
        """
        """
        with self.assertRaises(Exception) as cm:
            FeedsData()

        msg = 'Only one instance of FeedsData can exist.'
        self.assertEqual(str(cm.exception), msg)

    def test_feed_exists(self):
        """Test feed existance check."""
        fdata = FeedsData.me

        self.assertTrue(fdata.feed_exists(10))
        self.assertFalse(fdata.feed_exists(0))
        self.assertFalse(fdata.feed_exists(-10))
        self.assertFalse(fdata.feed_exists(200))

    def test_group_exsits(self):
        """Test group existance check."""
        fdata = FeedsData.me

        self.assertTrue(fdata.group_exists(0))
        self.assertTrue(fdata.group_exists(-1107))
        self.assertFalse(fdata.group_exists(-3000))
        self.assertFalse(fdata.group_exists(3))

    def test_id_is_number_not_string(self):
        """Dict keys stored in JSON always as strings. Check if they are
        converted back to int when loading a file."""

        # are there Feeds?
        self.assertTrue(FeedsData.me._feeds)

        # each Feed
        for feed_id in FeedsData.me._feeds:
            self.assertIsInstance(feed_id, int)


if __name__ == '__main__':
    unittest.main()
