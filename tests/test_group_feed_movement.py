import random
import unittest
from unittest import mock

from . import helper
import feedybus
from feedybus import group, feed
from feedybus.group import Group
from feedybus.feed import Feed
from feedybus import basics
from feedybus import data


"""
    Special test cases for class Group and Feed checking their movement in the
    data structure.
"""


class TestGroupFeedMovement(unittest.TestCase):
    """Class with special test cases for Group and Feed class."""
    def setUp(self):
        """Setup the environemnt for each tests in that module.

        It creates a temporary feedybus profile with a random name.
        """
        # set unittest profile
        basics.set_profile('UNITTEST.{}'.format(helper.rand_string()))
        basics.check_directory_existance()

        # moch the data
        with mock.patch('feedybus.basics._DefaultFeeds',
                        return_value=helper.DATA_default):
            # load the data
            data.FeedsData()

    def tearDown(self):
        """Clear the environment after each test.

        It trigger the deletion of the temporary profile.
        """
        helper.clear_profile()
        data.FeedsData.me = None

    def test_pep8_conformance(self):
        self.assertTrue(helper.pep8_conform(__file__), 'Test-file')

    def test_insert_as_first_errors(self):
        """Test the error handling of wrong parameters."""
        group = Group.root()

        # wrong parameter type
        with self.assertRaises(TypeError):
            group.insert_as_first('X')
        with self.assertRaises(TypeError):
            group.insert_as_first(1)

        # wrong parameter count
        with self.assertRaises(TypeError):
            group.insert_as_first(1, 'X')

        # group still exists at this location
        with self.assertRaises(ValueError):
            group.insert_as_first(Group(-1100))

        # feed still exists at this location
        with self.assertRaises(ValueError):
            group.insert_as_first(Feed(19))

    def test_insert_as_last_errors(self):
        """Test the error handling of wrong parameters."""
        group = Group.root()

        with self.assertRaises(TypeError):
            group.insert_as_last('X')
        with self.assertRaises(TypeError):
            group.insert_as_last(1)
        with self.assertRaises(TypeError):
            group.insert_as_last(1, 'X')

        # group still exists at this location
        with self.assertRaises(ValueError):
            group.insert_as_last(Group(-1107))

    def test_insert_before_errors(self):
        """Test the error handling of Group.insert_before()."""
        group = Group.root()

        # wrong parameters
        with self.assertRaises(TypeError):
            group.insert_before('X', Feed(11))
        with self.assertRaises(TypeError):
            group.insert_before('X', Group(-1105))
        with self.assertRaises(TypeError):
            group.insert_before(Feed(11), 'X')
        with self.assertRaises(TypeError):
            group.insert_before(Group(-1105), 'X')
        with self.assertRaises(TypeError):
            group.insert_before(1, 7.8)
        with self.assertRaises(TypeError):
            group.insert_before(1, 'X')

        # Do not mix Feed and Group
        with self.assertRaises(TypeError):
            group.insert_before(Feed(7), Group(-1103))
        with self.assertRaises(TypeError):
            group.insert_before(Group(-1103), Feed(7))

        # before-group do exist but at different location
        with self.assertRaises(ValueError):
            group.insert_before(Group(-1104), Group(-1103))

        # before-feed do exist but at different location
        with self.assertRaises(ValueError):
            group.insert_before(Feed(8), Feed(15))

        # group still exists at this location
        with self.assertRaises(ValueError):
            group.insert_before(Group(-1105), Group(-1106))

        # feed still exists at this location
        with self.assertRaises(ValueError):
            group.insert_before(Feed(19), Feed(18))

    def test_insert_after_errors(self):
        """Test the error handling of Group.insert_after()."""
        group = Group.root()

        # wrong parameters
        with self.assertRaises(TypeError):
            group.insert_after('X', Feed(11))
        with self.assertRaises(TypeError):
            group.insert_after(Feed(11), 'X')
        with self.assertRaises(TypeError):
            group.insert_after('X', Group(-1105))
        with self.assertRaises(TypeError):
            group.insert_after(Group(-1105), 'X')
        with self.assertRaises(TypeError):
            group.insert_after(1, 7.8)
        with self.assertRaises(TypeError):
            group.insert_after(1, 'X')

        # Do not mix Feed and Group
        with self.assertRaises(TypeError):
            group.insert_after(Feed(7), Group(-1103))
        with self.assertRaises(TypeError):
            group.insert_after(Group(-1103), Feed(7))

        # after-group do exist but at different location
        with self.assertRaises(ValueError):
            group.insert_after(Group(-1106), Group(-1103))

        # after-feed do exist but at different location
        with self.assertRaises(ValueError):
            group.insert_after(Feed(8), Feed(15))

        # group still exists at this location
        with self.assertRaises(ValueError):
            group.insert_after(Group(-1106), Group(-1105))

        # feed still exists at this location
        with self.assertRaises(ValueError):
            group.insert_after(Feed(18), Feed(19))

    def test_insert_group_as_first(self):
        """Test the insertion of an existing group as first child of
        another."""
        # move to empty group
        group = Group(-1107)  # empty parent group
        child = Group(-1102)  # child group

        group.insert_as_first(child)
        self.assertTrue(len(group.groups) == 1)
        self.assertTrue(group.groups[0] == child)

        # move in same level
        group = Group.root()
        child = Group(-1106)
        group.insert_as_first(child)
        self.assertTrue(len(group.groups) == 5)
        self.assertTrue(group.groups[0] == child)

        # simple: move to another group
        group = Group(-1102)
        child = Group(-1100)
        group.insert_as_first(child)
        result = [Group(-1100), Group(-1103), Group(-1104)]
        self.assertTrue(group.groups == result)

    def test_insert_group_as_last(self):
        """Test insertion of an existing group as last child."""
        # move to empty group
        group = Group(-1107)  # empty parent group
        child = Group(-1102)  # child group
        group.insert_as_last(child)
        self.assertTrue(len(group.groups) == 1)
        self.assertTrue(group.groups[-1] == child)

        # move in same level
        group = Group.root()
        child = Group(-1106)
        group.insert_as_last(child)
        self.assertTrue(len(group.groups) == 5)
        self.assertTrue(group.groups[-1] == child)

        # simple: move to another group
        group = Group(-1102)
        result = [Group(-1103), Group(-1104)]
        self.assertTrue(group.groups == result)

        child = Group(-1100)
        group.insert_as_last(child)
        result = [Group(-1103), Group(-1104), Group(-1100)]
        self.assertTrue(group.groups == result)

    def test_insert_group_before(self):
        """Test insertion of an existing group as child before a givin
        child."""
        # move in same level
        group = Group.root()
        child = Group(-1106)
        before = Group(-1101)
        group.insert_before(child, before)
        self.assertTrue(len(group.groups) == 6)
        self.assertTrue(group.groups[1] == child)

        # move simple
        child = Group(-1103)
        before = Group(-1101)
        group.insert_before(child, before)
        self.assertTrue(len(group.groups) == 7)
        self.assertTrue(group.groups[2] == child)

        # move before the first child
        child = Group(-1104)
        before = Group(-1100)
        group.insert_before(child, before)
        self.assertTrue(group.groups[0].gid == -1104)
        self.assertTrue(group.groups[1].gid == -1100)

    def test_insert_group_after(self):
        """Test moving of a group before after another group on the same
        level.
        """
        root = Group.root()

        # initial check
        self.assertEqual(len(root.groups), 6)

        # move
        root.insert_after(Group(-1104), Group(-1100))

        # test new order
        self.assertEqual(len(root.groups), 7)
        self.assertEqual(root.groups[0].gid, -1100)
        self.assertEqual(root.groups[1].gid, -1104)
        self.assertEqual(root.groups[2].gid, -1101)

        # test removed from old position?
        subgroup = Group(-1102)
        self.assertEqual(len(subgroup.groups), 1)
        self.assertEqual(subgroup.groups[0].gid, -1103)

    def test_group_insert_paradoxon(self):
        """Test errors when a (grand)parent is inserted as a child."""
        # group.insert_as_first
        group = Group(-1103)
        with self.assertRaises(ValueError):
            group.insert_as_first(Group(-1102))

        # group.insert_as_last
        group = Group(-1103)
        with self.assertRaises(ValueError):
            group.insert_as_last(Group(-1102))

    def test_insert_feed_as_first(self):
        """Test inserting a feed as first child to a group."""
        # sub to root
        group = Group.root()
        self.assertTrue(len(group.feeds) == 2)
        group.insert_as_first(Feed(8))
        self.assertTrue(len(group.feeds) == 3)
        self.assertTrue(group.feeds[0].fid == 8)
        result = [Feed(8), Feed(19), Feed(18)]
        self.assertTrue(group.feeds == result)
        self.assertTrue(Group(-1105).feeds == [Feed(9)])

        # in same level
        group = Group(-1106)
        self.assertTrue(len(group.feeds) == 7)
        group.insert_as_first(Feed(13))
        self.assertTrue(len(group.feeds) == 7)
        result = [Feed(13), Feed(17), Feed(16),
                  Feed(15), Feed(14), Feed(10), Feed(11)]
        self.assertTrue(group.feeds == result)

        # to empty group
        group = Group(-1107)
        self.assertFalse(group.feeds)
        group.insert_as_first(Feed(14))
        self.assertTrue(group.feeds == [Feed(14)])
        self.assertTrue(len(group.feeds) == 1)
        self.assertTrue(len(Group(-1106).feeds) == 6)

    def test_insert_feed_as_last(self):
        """Test insert feed as last child of a group."""
        # move to empty group
        group = Group(-1107)
        group.insert_as_last(Feed(13))
        self.assertTrue(group.feeds[-1].fid == 13)
        self.assertTrue(len(Group(-1106).feeds) == 6)

        # move in same level
        group = Group(-1106)
        group.insert_as_last(Feed(16))
        result = [Feed(17), Feed(15), Feed(14), Feed(10), Feed(11), Feed(16)]
        self.assertTrue(group.feeds == result)

        # move to antoher group
        group = Group(-1105)
        group.insert_as_last(Feed(19))
        self.assertTrue(len(Group.root().feeds) == 1)
        result = [Feed(8), Feed(9), Feed(19)]
        self.assertTrue(group.feeds == result)

    def test_insert_feed_before(self):
        """Test insert a feed before another feed."""
        # move in same level
        group = Group(-1106)
        group.insert_before(Feed(10), Feed(15))
        result = [Feed(17), Feed(16), Feed(10), Feed(15),
                  Feed(14), Feed(13), Feed(11)]
        self.assertTrue(group.feeds == result)

        # move simple
        group = Group(-1103)
        self.assertTrue(len(group.feeds) == 3)
        group.insert_before(Feed(12), Feed(7))
        self.assertTrue(len(Group(-1101).feeds) == 1)
        result = [Feed(5), Feed(6), Feed(12), Feed(7)]
        self.assertTrue(group.feeds == result)

        # move before first child
        group = Group(-1101)
        self.assertTrue(len(group.feeds) == 1)
        group.insert_before(Feed(9), Feed(3))
        result = [Feed(9), Feed(3)]
        self.assertTrue(group.feeds == result)

    def test_insert_feed_after(self):
        """Test insert a feed after another feed."""
        # move in same level
        group = Group(-1106)
        group.insert_after(Feed(10), Feed(15))
        result = [Feed(17), Feed(16), Feed(15), Feed(10),
                  Feed(14), Feed(13), Feed(11)]
        self.assertTrue(group.feeds == result)

        # move simple
        group = Group(-1103)
        self.assertTrue(len(group.feeds) == 3)
        group.insert_after(Feed(12), Feed(7))
        self.assertTrue(len(Group(-1101).feeds) == 1)
        result = [Feed(5), Feed(6), Feed(7), Feed(12)]
        self.assertTrue(group.feeds == result)

        # move after last child
        group = Group(-1103)
        self.assertTrue(len(group.feeds) == 4)
        group.insert_after(Feed(16), Feed(12))
        result = [Feed(5), Feed(6), Feed(7), Feed(12), Feed(16)]
        self.assertTrue(group.feeds == result)


if __name__ == '__main__':
    unittest.main()
