import multiprocessing
import unittest
from unittest import mock

from . import helper
from feedybus import basics
from feedybus import data
from feedybus import fetchfeeds


"""
"""


class TestFetchFeeds(unittest.TestCase):
    """Class with special test cases for Group and Feed class."""
    def setUp(self):
        """Setup the environemnt for each tests in that module.

        It creates a temporary feedybus profile with a random name.
        """
        # set unittest profile
        basics.set_profile('UNITTEST.{}'.format(helper.rand_string()))
        basics.check_directory_existance()

        # moch the data
        with mock.patch('feedybus.basics._DefaultFeeds',
                        return_value=helper.DATA_default):
            # load the data
            data.FeedsData()

    def tearDown(self):
        """Clear the environment after each test.

        It trigger the deletion of the temporary profile.
        """
        helper.clear_profile()
        data.FeedsData.me = None

    def test_pep8_conformance(self):
        self.assertTrue(helper.pep8_conform(__file__), 'Test-file')

    def test_foobar(self):
        """
        """
        queue = multiprocessing.Queue()

        the_thread = fetchfeeds.FetchAsyncMultiprocessThread(
            feeds_data=data.FeedsData.me,
            feed_ids=[1],
            result_queue=queue,
            debug=True
        )

        def _foobar():
            pass
        the_thread.callback_feed_update_started = _foobar
        the_thread.callback_feed_fetched = _foobar
        the_thread.callback_feed_parsed = _foobar

        def _mocked(session, url, headers):
            print('I am mocking...!')

            return None, 'ERR'

        with mock.patch('feedybus.fetchfeeds.FetchAsyncMultiprocessThread'
                        '._receive_via_aiohttp',
                        return_value=(None, 'MOCKED')):
            the_thread.run()
