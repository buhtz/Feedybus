import unittest
import unittest.mock
import email
from . import helper
from feedybus import basics
from feedybus import application
from feedybus import config
from feedybus.application import FeedybusApplication
from feedybus.data import FeedsData
from feedybus.group import Group
from feedybus.feed import Feed


"""
    Tests for import/export OPML files.
"""


class TestApplication(unittest.TestCase):
    """
    """
    ## logger ID
    logger_id = basics.GetLogId(application.__file__)

    @classmethod
    def setUpClass(cls):
        """
        """
        # set the unittest profile
        basics.set_profile('UNITTEST.{}'.format(helper.rand_string()))
        basics.check_directory_existance()

        # create appliaction
        FeedybusApplication(config.Config())

    @classmethod
    def tearDownClass(cls):
        """
        """
        helper.clear_profile()
        FeedybusApplication.me = None
        FeedsData.me = None

    def test_pep8_conformance(self):
        """
        """
        # this test file
        self.assertTrue(helper.pep8_conform(__file__), 'Test-file')
        # the class to test
        self.assertTrue(helper.pep8_conform(FeedybusApplication),
                        'File to test')

    def test_singleton(self):
        """
        """
        with self.assertRaises(Exception) as cm:
            FeedybusApplication(None)

        # test exception message
        msg = 'Only one instance of FeedybusApplication can exist.'
        self.assertEqual(str(cm.exception), msg)


class TestOPML_Import(unittest.TestCase):
    """
    """
    ## Expected tree (see _OPML_VALID)
    tree_expected = [
        [],
        [
            0,
            "Feeds",
            True,
            [
                [
                    2
                ],
                [
                    -1000,
                    "A",
                    True,
                    [
                        [
                            1
                        ]
                    ]
                ],
                [
                    -1001,
                    "B (text)",
                    True,
                    [
                        []
                    ]
                ]
            ]
        ]
    ]

    ## Expected feeds (see _OPML_VALID)
    feeds_expected = {
        1: {
            "title": "Simpel Feed (title)",
            "xmlUrl": "http://www.tagesschau.de/newsticker.rdf",
            "text": "Simpel Feed (text)",
            "htmlUrl": "https://www.tagesschau.de",
            "version": "RSS",
            "description": "tagesschau.de"
        },
        2: {
            "title": "Codeberg News",
            "xmlUrl": "https://blog.codeberg.org/feeds/all.atom.xml",
            "text": "Codeberg News",
            "htmlUrl": "https://blog.codeberg.org/",
            "version": "RSS",
            "description": "Codeberg Description"
        }
    }

    ## Expected entries (see _OPML_VALID)
    entries_expected = {
        1: [],
        2: []
    }

    ## logger ID
    logger_id = basics.GetLogId(application.__file__)

    @classmethod
    def setUpClass(cls):
        """
        """
        # setup loggin
        import logging
        logging.basicConfig(level=logging.DEBUG)

        # set the unittest profile
        basics.set_profile('UNITTEST.{}'.format(helper.rand_string()))
        basics.check_directory_existance()

        # mock the default feeds
        with unittest.mock.patch('feedybus.basics._DefaultFeeds',
                                 return_value=helper.DATA_empty_feeds):
            # create application instance (incl. FeedsData instance
            FeedybusApplication(config.Config())

    @classmethod
    def tearDownClass(cls):
        """
        """
        helper.clear_profile()
        FeedybusApplication.me = None
        FeedsData.me = None

    def setUp(self):
        """Called before each test checking if there are no feeds.

        Feeds are not deleted here but in tearDown().
        """
        # no feeds and groups
        root = Group.root()
        self.assertFalse(root.groups)
        self.assertFalse(root.feeds)

    def tearDown(self):
        """Called after each test deleting all feeds.
        """
        # This will re-read the feed-data from harddisk.
        # The data is empty and was created in setUpClass().
        FeedsData.me._load_feedsfile()

    def test_import_valid(self):
        """Import OPML data without anomalies.
        """
        OPML_VALID = '''<?xml version="1.0" encoding="UTF-8"?>
        <opml version="1.0">
            <head>
                <text/>
                <title>Simple</title>
            </head>
            <body>
                <outline title="A">
                    <outline text="Simpel Feed (text)"
                     title="Simpel Feed (title)"
                     xmlUrl="http://www.tagesschau.de/newsticker.rdf"
                     description="tagesschau.de"
                     version="RSS" htmlUrl="https://www.tagesschau.de"/>
                </outline>
                <outline title="B (title)" text="B (text)"/>
                <outline text="Codeberg News"
        xmlUrl="https://blog.codeberg.org/feeds/all.atom.xml"
        description="Codeberg Description" version="RSS"
        htmlUrl="https://blog.codeberg.org/" title="Codeberg News"/>
            </body>
        </opml>'''

        # do the import
        FeedybusApplication.me._import_from_opml(OPML_VALID, Group.root())

        self.maxDiff = None

        # check the tree
        self.assertEqual(FeedsData.me._tree, self.tree_expected)
        # the feeds
        self.assertEqual(FeedsData.me._feeds, self.feeds_expected)
        # the entries
        self.assertEqual(FeedsData.me._entries, self.entries_expected)

    def test_import_valid_with_unused_attributes(self):
        """Import OPML data which is valid but has attributs irrelevant for
        Feedybus.
        """
        OPML_VALID_UNUSED = '''<?xml version="1.0" encoding="UTF-8"?>
        <opml version="1.0">
            <head>
                <text/>
                <title>Simple</title>
            </head>
            <body>
                <outline id="12345" title="A">
                    <outline id="7654" text="Simpel Feed (text)"
                     title="Simpel Feed (title)"
                     xmlUrl="http://www.tagesschau.de/newsticker.rdf"
                     description="tagesschau.de"
                     version="RSS" htmlUrl="https://www.tagesschau.de"/>
                </outline>
                <outline title="B (title)" text="B (text)"/>
                <outline text="Codeberg News"
        xmlUrl="https://blog.codeberg.org/feeds/all.atom.xml"
        description="Codeberg Description" version="RSS"
        unused="Foobar" id="8888" useCustomFetchInterval="false"
        htmlUrl="https://blog.codeberg.org/" title="Codeberg News"/>
            </body>
        </opml>'''

        # take debug output about into account
        with self.assertLogs(self.logger_id, level='DEBUG') as cm:
            # do import
            FeedybusApplication.me._import_from_opml(OPML_VALID_UNUSED,
                                                     Group.root())
            expected_debug_output = [
                "DEBUG:feedybus.application.py:Unknown keys found in OPML "
                "outline: ['id']. Group: Group #-1000 \"A\"",
                "DEBUG:feedybus.application.py:Unknown keys found in OPML "
                "outline: ['id']. Feed: Simpel Feed (title) "
                "http://www.tagesschau.de/newsticker.rdf",
                "DEBUG:feedybus.application.py:Unknown keys found in OPML "
                "outline: ['id', 'unused', 'useCustomFetchInterval']. "
                "Feed: Codeberg News "
                "https://blog.codeberg.org/feeds/all.atom.xml"
            ]
            # test for debug output
            self.maxDiff = None
            self.assertEqual(cm.output, expected_debug_output)

        # check the tree
        self.assertEqual(FeedsData.me._tree, self.tree_expected)
        # the feeds
        self.assertEqual(FeedsData.me._feeds, self.feeds_expected)
        # the entries
        self.assertEqual(FeedsData.me._entries, self.entries_expected)

    def test_import_missing_attributes(self):
        """OPML elements with missing (but mandatory) attributes are dropped.
        """
        # Feed with missing 'title' and 'text' will be dropped
        OPML_MISS_F_TEXT_TITLE = '''<?xml version="1.0"
        encoding="UTF-8"?>
        <opml version="1.0">
            <head>
                <text/>
                <title>Simple</title>
            </head>
            <body>
                <outline notitle="invalid"
        xmlUrl="https://valid.foo"
        htmlUrl="https://valid.foo" />
            </body>
        </opml>'''

        with self.assertLogs(self.logger_id, level='DEBUG') as cm:
            # try import
            FeedybusApplication.me._import_from_opml(OPML_MISS_F_TEXT_TITLE,
                                                     Group.root())
            expected_output = ['WARNING:feedybus.application.py:Dropped OPML '
                               'element because of missing attribute "text"'
                               ' and its alternative "title". '
                               '[\'htmlUrl\', \'notitle\', '
                               '\'xmlUrl\']']
            self.maxDiff = None
            self.assertEqual(cm.output, expected_output)
            self.assertEqual(FeedsData.me._tree,
                             [[], [0, 'Feeds', True, [[]]]])

        # Group with missing 'title' and 'text' will be dropped
        OPML_MISS_G_TEXT_TITLE = '''<?xml version="1.0"
        encoding="UTF-8"?>
        <opml version="1.0">
            <head>
                <text/>
                <title>Simple</title>
            </head>
            <body>
                <outline notitle="A" />
            </body>
        </opml>'''

        with self.assertLogs(self.logger_id, level='DEBUG') as cm:
            # try import
            FeedybusApplication.me._import_from_opml(OPML_MISS_G_TEXT_TITLE,
                                                     Group.root())
            expected_output = ['WARNING:feedybus.application.py:Dropped OPML '
                               'element because of missing attribute "text"'
                               ' and its alternative "title". '
                               '[\'notitle\']']
            self.maxDiff = None
            self.assertEqual(cm.output, expected_output)
            self.assertEqual(FeedsData.me._tree,
                             [[], [0, 'Feeds', True, [[]]]])

        # Feed with missing 'xmlUrl' is treated as a Group
        OPML_MISS_F_XMLURL = '''<?xml version="1.0" encoding="UTF-8"?>
        <opml version="1.0">
            <head>
                <text/>
                <title>Simple</title>
            </head>
            <body>
                <outline title="Title"
        noxmlUrl="https://invalid.foo"
        htmlUrl="https://valid.foo" />
            </body>
        </opml>'''

        with self.assertLogs(self.logger_id, level='DEBUG') as cm:
            # try import
            FeedybusApplication.me._import_from_opml(OPML_MISS_F_XMLURL,
                                                     Group.root())
            expected_output = ['DEBUG:feedybus.application.py:'
                               'Unknown keys found in OPML outline: '
                               '[\'htmlUrl\', \'noxmlUrl\']. '
                               'Group: Group #-1000 "Title"']
            self.maxDiff = None
            self.assertEqual(cm.output, expected_output)
            # Feed must become a group (because "title" is correct)
            self.assertEqual(FeedsData.me._tree,
                             [[], [0, 'Feeds', True,
                                   [[],
                                    [-1000, 'Title', True, [[]]]]]])

    def test_import_title_as_text(self):
        """In OPML each outline element needs a "text" attribute. But Feedybus
        will treat a "title" attribute as surrogate for a missing "text". See
        'FeedsData.import_from_opml()' for details about it. Keep in mind that
        OPML's "text" attribute is treated and named as "label" in Feedybus.
        """
        OPML_MISSING_TEXT = '''<?xml version="1.0" encoding="UTF-8"?>
        <opml version="1.0">
            <head>
                <text/>
                <title>Simple</title>
            </head>
            <body>
                <outline title="feed title" xmlUrl="https://valid.foo" />
                <outline title="group title" />
            </body>
        </opml>'''

        # do the import
        FeedybusApplication.me._import_from_opml(OPML_MISSING_TEXT,
                                                 Group.root())

        # feed label
        feed = Group.root().feeds[0]
        self.assertEqual(feed.label, 'feed title')
        self.assertEqual(feed.title, 'feed title')

        # group title
        group = Group.root().groups[0]
        self.assertEqual(group.label, 'group title')

    def test_import_unicode_escape(self):
        """Import OPML with unicode escape sequence.
        """
        OPML_UNICODE_ESCAPE = '''<?xml version="1.0"?>
            <opml version="1.0">
              <head>
              </head>
              <body>
                <outline title="Simpel Feed"
        description="Tagesschau &#x770B;&#x8B77;&#x5E2B;"
        xmlUrl="http://www.tagesschau.de/newsticker.rdf"
        htmlUrl="http://www.tagesschau.de"/>
              </body>
            </opml>'''

        # do the import
        FeedybusApplication.me._import_from_opml(OPML_UNICODE_ESCAPE,
                                                 Group.root())

        # check
        feed = Feed(1)
        self.assertTrue(feed.description == "Tagesschau 看護師")

    def test_import_empty_attributes(self):
        """Some attributes are present but empty. Should be ignored while
        import.
        """
        OPML_EMPTY_ATTRIB = '''<?xml version="1.0"?>
            <opml version="1.0">
              <head>
              </head>
              <body>
                <outline title="Simpel Feed"
        xmlUrl="http://www.tagesschau.de/newsticker.rdf"
        description="" htmlUrl="" />
              </body>
            </opml>'''

        # do the import
        FeedybusApplication.me._import_from_opml(OPML_EMPTY_ATTRIB,
                                                 Group.root())

        # get the imported feed
        feed = Group.root().feeds[0]

        # check description
        self.assertEqual(feed.description, None)

        # check htmlUrl
        self.assertEqual(feed.htmlUrl, None)

        # check the raw data also
        feed_keys = FeedsData.me._feeds[feed.fid].keys()
        self.assertTrue(all(k not in feed_keys
                            for k in ['description', 'htmlUrl']))


class TestOPML_Export(unittest.TestCase):
    """
    """
    def setUp(self):
        """
        """
        # set the unittest profile
        basics.set_profile('UNITTEST.{}'.format(helper.rand_string()))
        basics.check_directory_existance()

    def tearDown(self):
        """
        """
        helper.clear_profile()
        FeedybusApplication.me = None
        FeedsData.me = None

    def _load_test_data(self, data_return):
        """Helper function that load data at the beginning of each test.

        Must be called explicite. Playes togethr with tearDown() and setUp()
        in a row.
        """
        # mock the default feeds
        with unittest.mock.patch('feedybus.basics._DefaultFeeds',
                                 return_value=data_return):
            # create application instance (incl. FeedsData instance
            FeedybusApplication(config.Config())

    def test_export_no_outlines(self):
        """Test OPML head and empty body.
        """
        self._load_test_data(helper.DATA_empty_feeds)

        app_name_vers = FeedybusApplication.me.get_full_name()
        date_rfc = email.utils.formatdate()  # mock that!
        app_website = FeedybusApplication.me.get_website()
        app_email = FeedybusApplication.me.get_email()
        OPML_EMPTY = (f'<?xml version="1.0" encoding="UTF-8"?>\n'
                      f'<opml version="2.0">\n'
                      f'    <head>\n'
                      f'        <dateCreated>{date_rfc}</dateCreated>\n'
                      f'        <ownerName>{app_name_vers}</ownerName>\n'
                      f'        <ownerId>{app_website}</ownerId>\n'
                      f'        <ownerEmail>{app_email}</ownerEmail>\n'
                      f'        <docs>http://dev.opml.org/spec2.html</docs>\n'
                      f'    </head>\n'
                      f'    <body/>\n'
                      f'</opml>\n')

        # patch the date
        with unittest.mock.patch('email.utils.formatdate',
                                 return_value=date_rfc):
            # do export
            opml = FeedybusApplication.me._export_as_opml()

        # test
        self.maxDiff = None
        self.assertEqual(OPML_EMPTY, opml)

    def test_export_feeds_groups(self):
        """
        """
        self._load_test_data(helper.DATA_export)

        app_name_vers = FeedybusApplication.me.get_full_name()
        date_rfc = email.utils.formatdate()  # mock that!
        app_website = FeedybusApplication.me.get_website()
        app_email = FeedybusApplication.me.get_email()
        OPML_expect = [
            '<?xml version="1.0" encoding="UTF-8"?>',
            '<opml version="2.0">',
            '    <head>',
            '        <dateCreated>{}</dateCreated>'.format(date_rfc),
            '        <ownerName>{}</ownerName>'.format(app_name_vers),
            '        <ownerId>{}</ownerId>'.format(app_website),
            '        <ownerEmail>{}</ownerEmail>'.format(app_email),
            '        <docs>http://dev.opml.org/spec2.html</docs>',
            '    </head>',
            '    <body>',
            '        <outline text="Folder A">',
            ('            <outline type="rss" text="Text B" title="Title B" '
             'xmlUrl="http://www.b.url/atom/"/>'),
            '        </outline>',
            '        <outline text="Folder B">',
            '            <outline text="Folder B-a">',
            ('                <outline type="rss" text="Text D" '
             'title="Title D" xmlUrl="http://www.d.url/atom/"/>'),
            ('                <outline type="rss" text="Text E" '
             'title="Title E" xmlUrl="http://www.e.url/atom/"/>'),
            ('                <outline type="rss" text="Text F" '
             'title="Title F" xmlUrl="http://www.f.url/atom/"/>'),
            '            </outline>',
            '            <outline text="Folder B-b"/>',
            ('            <outline type="rss" text="Text C" '
             'title="Title C" xmlUrl="http://www.c.url/atom/"/>'),
            '        </outline>',
            '        <outline text="Folder C"/>',
            ('        <outline type="rss" text="Text A" '
             'title="Title A" xmlUrl="http://www.a.url/atom/" '
             'htmlUrl="http://www.a.url" description="Description A" '
             'author="Mrs. A" rights="CC" encoding="UTF-8"/>'),
            '    </body>',
            '</opml>',
            ''
        ]
        OPML_expect = '\n'.join(OPML_expect)

        # patch the date
        with unittest.mock.patch('email.utils.formatdate',
                                 return_value=date_rfc):
            # do export
            opml = FeedybusApplication.me._export_as_opml()

        # test
        self.maxDiff = None
        self.assertEqual(OPML_expect, opml)


if __name__ == '__main__':
    unittest.main()
