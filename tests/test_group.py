import random
import unittest
from unittest import mock
from . import helper
from feedybus.group import Group
from feedybus.feed import Feed
from feedybus import basics
from feedybus import data


"""
    Test cases for Group class.

    Allowed:
    Group(-1103)
    Group('label')
    Group('label', parent)

    Not allowed:
    Group()
"""


class TestGroup_DataPerTest(unittest.TestCase):
    """Test cases for Group class in context of removing and deleting elements
    of a Group.
    """
    def setUp(self):
        """Set the environment for each tests in that module."""
        # set unittest profile
        basics.set_profile('UNITTEST.{}'.format(helper.rand_string()))
        basics.check_directory_existance()

        # mock the data
        with mock.patch('feedybus.basics._DefaultFeeds',
                        return_value=helper.DATA_default):
            # load the data
            data.FeedsData()

    def tearDown(self):
        """Clear the environment after all tests are finished.

        It trigger the deletion of the temporary profile.
        """
        helper.clear_profile()
        data.FeedsData.me = None

    def test_remove_feed(self):
        """Test removing (not deleting!) a feed from a group.
        """
        # remove existing feed
        group = Group(-1106)
        self.assertEqual(group.feed_ids, [17, 16, 15, 14, 13, 10, 11])
        group.remove(Feed(14))
        self.assertEqual(group.feed_ids, [17, 16, 15, 13, 10, 11])

        # try removing none-child
        with self.assertRaises(ValueError):
            group.remove(Feed(2))

        # try fid only
        with self.assertRaises(TypeError):
            group.remove(2)

    def test_remove_group(self):
        """Test removing (not deleting!) a group from a group.
        """
        # remove existing group
        group = Group(-1102)
        group.remove(Group(-1103))
        self.assertEqual(group.group_ids, [-1104])

        # remove group that is not a child
        with self.assertRaises(ValueError):
            group.remove(Group(-1106))

        # wrong type
        with self.assertRaises(TypeError):
            group.remove(-1102)

    def test_delete(self):
        """Test the full deletion of a group."""
        event_group = helper.TestEventHelper(Group.event_group_deleted)
        event_feed = helper.TestEventHelper(Feed.event_feed_deleted)
        # print(f'{id(event_group)} {id(event_feed)}')

        # pre-check
        group = Group(-1104)
        self.assertEqual(group.feeds, [])
        self.assertEqual(group.groups, [])
        self.assertEqual(group.parent, Group(-1102))

        # simple: delete a group with no children
        Group.delete(Group(-1104))
        group = Group(-1102)
        self.assertEqual(group.feeds, [Feed(4)])
        self.assertEqual(group.groups, [Group(-1103)])
        self.assertEqual(group.parent, Group.root())
        self.assertEqual(event_group.notified, 1)
        self.assertEqual(event_group.last_args[0], -1104)
        self.assertEqual(event_feed.notified, 0)
        with self.assertRaises(ValueError):
            Group(-1104)  # does not exist anymore

        # complex: with child groups and feeds
        Group.delete(Group(-1102))
        group = Group.root()
        with self.assertRaises(ValueError):
            Group(-1102)
            Group(-1103)  # child group
            Feed(4)  # child feed
            Feed(5)
            Feed(6)
            Feed(7)
        self.assertEqual(group.group_ids, [-1100, -1101, -1105, -1106, -1107])
        self.assertEqual(event_group.notified, 3)
        self.assertEqual(event_group.last_args[0], -1102)
        self.assertEqual(event_feed.notified, 4)
        self.assertEqual(event_feed.last_args[0], [4])

        # do not delete root
        root = Group.root()
        Group.delete(root)
        self.assertEqual(root.groups, [])
        self.assertEqual(root.feeds, [])
        self.assertEqual(event_group.notified, 8)
        # Because root will never be deleted, the gid of the last deleted
        # group is not root but its last child group.
        self.assertEqual(event_group.last_args[0], -1107)
        self.assertEqual(event_feed.notified, 19)


class TestGroup(unittest.TestCase):
    """Class with test cases for Group class."""
    @classmethod
    def setUpClass(cls):
        """Setup the environemnt for all tests in that module.

        It creates a temporary feedybus profile with a random name.
        """
        # set unittest profile
        basics.set_profile('UNITTEST.{}'.format(helper.rand_string()))
        basics.check_directory_existance()

        # mock the data
        with mock.patch('feedybus.basics._DefaultFeeds',
                        return_value=helper.DATA_default):
            # load the data
            data.FeedsData()

    @classmethod
    def tearDownClass(cls):
        """Clear the environment after all tests are finished.

        It trigger the deletion of the temporary profile.
        """
        helper.clear_profile()
        data.FeedsData.me = None

    def test_pep8_conformance(self):
        self.assertTrue(helper.pep8_conform(__file__), 'Test-file')
        self.assertTrue(helper.pep8_conform(Group), 'File to test')

    def test_equal_operator(self):
        """Test the equal operator."""
        # equal
        self.assertTrue(Group.root() == Group.root())
        self.assertTrue(Group(basics.GROUPID_FEEDS)
                        == Group(basics.GROUPID_FEEDS))
        self.assertTrue(Group.root() == Group(basics.GROUPID_FEEDS))

        # not equal
        self.assertTrue(Group.root() != Group(-1103))
        self.assertTrue(Group(basics.GROUPID_FEEDS) != Group(-1103))

    def test_instantiate_group_with_gid(self):
        """Instantiation of Group objects using a groupid as parameter."""
        # given group id: existing group
        group = Group(-1103)
        self.assertTrue(group.gid == -1103)
        self.assertTrue(group.label == 'Software')

    def test_instantiate_group_with_gid_invalid(self):
        """Instantiate with an unexisting and an out-of-range gid."""
        # given group id: not existing group
        with self.assertRaises(ValueError):
            Group(-2000)

        # Id not in the valid range for group id's
        with self.assertRaises(ValueError):
            Group(7)

    def test_instatiate_group_with_gid_allowed_as_single_only(self):
        """Exceptions when instantiate Group objecs using a group id parameter
        but some other more. If group id is givin no other paramter is
        allowed."""
        # with label
        with self.assertRaises(TypeError):
            Group(-1103, 'Wrong name')
        # with parent
        with self.assertRaises(TypeError):
            Group(-1103, Group.root())
        # with parent and label
        with self.assertRaises(TypeError):
            Group(-1103, Group.root(), 'Wrong name')

    def test_instantiate_group_wrong_type_of_parameter(self):
        """Exceptions if the used parameter is of wrong type."""
        # wrong paramter type
        with self.assertRaises(TypeError):
            Group([1, 2, 3])
        with self.assertRaises(TypeError):
            Group()

    def test_instantiate_root(self):
        """Instantiate root."""
        # By root()
        group = Group.root()
        self.assertTrue(group.gid == basics.GROUPID_FEEDS)

        # by root id
        group = Group(basics.GROUPID_FEEDS)
        self.assertTrue(group.gid == Group.root().gid)

    def test_root_parent(self):
        """Test parent of groups."""
        # root does not have a parent
        group = Group(basics.GROUPID_FEEDS)
        self.assertTrue(group.parent is None, 'No parent')

        # root is parent
        group = Group(-1100)
        self.assertTrue(group.parent.gid == basics.GROUPID_FEEDS,
                        'Root as parent')

    def test_create_group_by_label(self):
        """Create a new (logical) group with a label. It means instantiate a
        Group object and create the group in the data layer."""
        # create random name
        label = helper.rand_string()

        # create new group
        group_a = Group(label)
        self.assertTrue(group_a.label == label)

        group_b = Group(group_a.gid)
        self.assertTrue(group_b.label == label)

        # root should be default parent
        self.assertTrue(group_a.parent == Group.root())

        # ...with parent
        parent_gid = -1103
        parent = Group(parent_gid)
        group_a = Group(label, parent)
        self.assertTrue(group_a.parent == parent)
        self.assertTrue(group_a.parent.gid == parent_gid)
        self.assertTrue(group_a in parent.groups)

    def test_create_group_by_label_with_wrong_parent_type(self):
        """Exception if the first parameter is a label string (correct) but
        the second one not a Group object."""
        with self.assertRaises(Exception):
            group = Group('label', Object())

    @mock.patch('feedybus.data.FeedsData.get_group_ids')
    def test_next_free_id(self, group_ids_function):
        """Test mechanism to get the next free id for a group."""
        group_ids_function.return_value = []
        gid = Group.next_free_id()
        self.assertTrue(gid == -1000, '-1000')

        rv = list(reversed(range(-1077, (basics.GROUPID_MIN + 1))))
        group_ids_function.return_value = rv
        gid = Group.next_free_id()
        self.assertTrue(gid == -1078, '-1078')

        rv = list(reversed(range(-1077, (basics.GROUPID_MIN + 1))))
        gaps = random.sample(rv, 10)
        free = max(gaps)
        for g in gaps:
            rv.remove(g)
        group_ids_function.return_value = rv
        gid = Group.next_free_id()
        self.assertTrue(gid == free)

    def test_gid_validation(self):
        """Test if ids are correct validated as group ids."""
        gid_min = basics.GROUPID_MIN

        self.assertTrue(Group.is_gid_valid(gid_min))
        self.assertTrue(Group.is_gid_valid(gid_min - 1))
        self.assertFalse(Group.is_gid_valid(gid_min + 1))

    def test_root_label(self):
        """Test handling of the name/label of root group."""
        # as displayed string
        group = Group.root()
        self.assertTrue(str(group) == 'Group #0 "Feeds"')

        # expected label
        self.assertTrue(group.label == 'Feeds')

        # modify the label
        rname = helper.rand_string()  # create random group name
        group.label = rname
        self.assertTrue(group.label == rname)
        self.assertFalse(group.label == 'Feeds')

    def test_groups_label(self):
        """Test handling name/label of groups other than root."""
        # as displayed string
        group = Group(-1103)
        self.assertTrue(str(group) == 'Group #-1103 "Software"')

        # expected label
        self.assertTrue(group.label == 'Software')

        # modify the label
        rlabel = helper.rand_string()  # create random group name
        label_org = group.label  # remember org label
        group.label = rlabel
        self.assertTrue(group.label == rlabel)
        self.assertFalse(group.label == 'Software')
        group.label = label_org  # restore the org label

    def test_child_groups(self):
        """Test (non)existence of child groups using the Group.groups
        parameter.
        """
        # root sub groups
        group = Group.root()
        child_group_ids = [child.gid for child in group.groups]
        root_childs = [-1100, -1101, -1102, -1105, -1106, -1107]
        self.assertTrue(child_group_ids == root_childs)

        # 'Technology' sub groups
        group = Group(-1102)
        child_group_ids = [child.gid for child in group.groups]
        self.assertTrue(child_group_ids == [-1103, -1104])
        # wrong order
        self.assertFalse(child_group_ids == [-1104, -1103])

        # No sub groups in 'Japan'
        group = Group(-1105)
        self.assertTrue(group.groups == [])
        self.assertTrue(not group.groups)

    def test_child_feeds(self):
        """Test (non)existence of child feeds using the Group.feeds
        paramter.
        """
        # feeds of root
        group = Group.root()
        child_feed_ids = [child.fid for child in group.feeds]
        self.assertEqual(group.feeds[0].fid, 19)
        self.assertEqual(group.feeds[1].fid, 18)
        #
        self.assertTrue(group.feed_ids == [19, 18])
        # wrong order
        self.assertFalse(group.feed_ids == [18, 19])

        # sub group 'Technology'
        group = Group(-1102)
        self.assertEqual(group.feeds[0].fid, 4)
        self.assertEqual(group.feed_ids, [4])

        # empty group
        group = Group(-1107)
        self.assertEqual(group.feeds, [])
        self.assertEqual(group.feed_ids, [])
        self.assertTrue(not group.feeds)
        self.assertTrue(not group.feed_ids)

    def test_has_feed(self):
        """Check existence of specific feed as a child."""
        group = Group.root()
        self.assertTrue(Feed(19) in group.feeds)
        self.assertTrue(19 in group.feed_ids)
        # should not work recursive
        self.assertFalse(Feed(1) in group.feeds)
        self.assertFalse(1 in group.feed_ids)

        group = Group(-1103)
        self.assertTrue(Feed(7) in group.feeds)
        self.assertTrue(7 in group.feed_ids)
        self.assertFalse(Feed(19) in group.feeds)
        self.assertFalse(19 in group.feed_ids)
        self.assertFalse(Feed(1) in group.feeds)
        self.assertFalse(1 in group.feed_ids)

        with self.assertRaises(ValueError):
            Feed(777) in group.feeds

    def test_has_child_group(self):
        """Check existence of specific child group."""
        # direct sub group
        group = Group.root()
        self.assertTrue(Group(-1102) in group.groups)

        # sub-sub-group / should not work recursive
        self.assertFalse(Group(-1103) in group.groups)

        # In data unexisting group is not possible to ask for
        # because you can not instanciate an object for it.
        with self.assertRaises(ValueError):
            Group(-2000) in group.groups

    def test_has_ancestor(self):
        """Test ancestor check."""
        group = Group(-1105)
        self.assertTrue(group.has_ancestor(Group.root()))
        self.assertFalse(group.has_ancestor(Group(-1104)))

        group = Group(-1103)
        self.assertTrue(group.has_ancestor(Group(-1102)))

        group = Group(-1102)
        self.assertTrue(group.has_ancestor(Group.root()))
        self.assertFalse(group.has_ancestor(Group(-1103)))

    def test_unfolded(self):
        """Get and set folding states of groups.
        """
        # re-read the default data (see setUpClass())
        data.FeedsData.me._load_feedsfile()

        group = Group(-1103)
        self.assertTrue(group.unfolded)

        group = Group(-1102)
        self.assertFalse(group.unfolded)

        group = Group(-1107)
        self.assertFalse(group.unfolded)

        # modify fold state
        group.set_unfolded(False)
        self.assertFalse(group.unfolded)
        group.set_unfolded(True)  # explicite
        self.assertTrue(group.unfolded)
        group.set_unfolded(False)
        self.assertFalse(group.unfolded)
        group.set_unfolded()  # default
        self.assertTrue(group.unfolded)

        # recursive
        root = Group.root()
        group = Group(-1103)
        self.assertTrue(group.unfolded)
        root.set_unfolded(False, include_child_groups=True)
        self.assertFalse(root.unfolded)
        self.assertFalse(group.unfolded)
        root.set_unfolded(True, include_child_groups=True)
        self.assertTrue(root.unfolded)
        self.assertTrue(group.unfolded)

        # unfold new groups by default
        group = Group('unfolded by default')
        self.assertTrue(group.unfolded)

        # unfold new grups grandparents
        root.set_unfolded(False, True)  # fold all
        group = Group('new', Group(-1103))
        self.assertTrue(group.unfolded)
        group = Group(-1103)
        self.assertTrue(group.unfolded)
        group = Group(-1102)
        self.assertTrue(group.unfolded)
        self.assertTrue(root.unfolded)

    def test_group_ids(self):
        """Test list of group ids.
        """
        # re-read the default data (see setUpClass())
        data.FeedsData.me._load_feedsfile()

        group = Group.root()
        group_ids = [-1100, -1101, -1102, -1105, -1106, -1107]
        self.assertEqual(group.group_ids, group_ids)

        group = Group(-1105)
        self.assertEqual(group.group_ids, [])

    def test_previous_group(self):
        """Test finding the previous group."""
        # re-read the default data (see setUpClass())
        data.FeedsData.me._load_feedsfile()

        # same level
        group = Group(-1106)
        self.assertEqual(group.get_previous_group(), Group(-1105))

        # grand level
        group = Group(-1103)
        self.assertEqual(group.get_previous_group(), Group(-1102))


if __name__ == '__main__':
    unittest.main()
