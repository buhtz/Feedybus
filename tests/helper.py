import random
import string
import os
import os.path
import logging
import inspect
import shutil
import pycodestyle
from feedybus import basics

"""
    Helper functions and data for unit tests.

    Some functions helping with repetitive task while running unittests.
"""

_log = logging.getLogger(basics.GetLogId(__file__))


DATA_empty_feeds = {
    'tree': [[]],
    'feeds': {},
    'entries': {}
}


DATA_default_tree = [
    [19, 18],
    (-1100, 'Science', False, [
        [1, 2]
    ]
    ),
    (-1101, 'Politics', False, [
        [3, 12]
    ]
    ),
    (-1102, 'Technology', False, [
        [4],
        (-1103, 'Software', True, [
            [5, 6, 7]
        ]
        ),
        (-1104, 'Misc', False, [[]])
    ]
    ),
    (-1105, 'Japan', False, [
        [8, 9]
    ]
    ),
    (-1106, 'misc', False, [
        [17, 16, 15, 14, 13, 10, 11]
    ]
    ),
    (-1107, 'empty group', False, [
        []
    ]
    )
]


DATA_default_feeds = {
    1:
        {
            "title": "Cochrane Library - Highlighted Reviews",
            "text": "Cochrane Library - Highlighted Reviews",
            "htmlUrl": "http://www.cochranelibrary.com/",
            "type": "rss",
            "xmlUrl": "http://www.cochranelibrary.com/rss/"
                      "highlighted-reviews/"
        },
    3:
        {
            "title": "netzpolitik.org",
            "text": "netzpolitik.org",
            "htmlUrl": "",
            "type": "rss",
            "xmlUrl": "https://netzpolitik.org/feed/"
        },
    4:
        {
            "title": "Posteo.de - Aktuelles",
            "text": "Posteo.de - Aktuelles",
            "htmlUrl": "",
            "type": "rss",
            "xmlUrl": "https://posteo.de/blog/feed?format=atom"
        },
    5:
        {
            "title": "JabRef | Blog",
            "text": "JabRef | Blog",
            "htmlUrl": "https://blog.jabref.org/",
            "type": "rss",
            "xmlUrl": "http://blog.jabref.org/feed.xml"
        },
    8:
        {
            "title": "Japan Almanach: Tabibito's Japan-Blog",
            "text": "Japan Almanach: Tabibito's Japan-Blog",
            "htmlUrl": "",
            "type": "rss",
            "xmlUrl": "http://www.tabibito.de/japan/blog/feed/atom/"
        },
    9:
        {
            "title": "SALZ Tokyo",
            "text": "SALZ Tokyo",
            "htmlUrl": "http://www.salz-tokyo.com/",
            "type": "rss",
            "xmlUrl": "http://www.salz-tokyo.com/feed/"
        },
    6:
        {
            "title": "Free Software Foundation - News",
            "text": "Free Software Foundation - News",
            "type": "rss",
            "xmlUrl": "https://www.fsf.org/static/fsforg/rss/news.xml"
        },
    7:
        {
            "title": "Debian package news for feedparser",
            "text": "Debian package news for feedparser",
            "type": "rss",
            "xmlUrl": "https://tracker.debian.org/pkg/feedparser/rss"
        },
    10:
        {
            "title": "SRToolbox (Twitter)",
            "text": "SRToolbox (Twitter)",
            "htmlUrl": "http://twitrss.me/twitter_user_to_rss/"
                       "?user=SRToolbox",
            "type": "rss",
            "xmlUrl": "http://twitrss.me/twitter_user_to_rss/"
                      "?user=SRToolbox"
        },
    11:
        {
            'title': 'MarketWatch.com - MarketPulse',
            'text': 'MarketWatch.com - MarketPulse',
            'htmlUrl': 'https://www.marketwatch.com/search/'
                       '?query=&mode=Keyword&modeparam=91&siteid=rss',
            'type': 'rss',
            'xmlUrl': 'http://feeds.marketwatch.com/'
                      'marketwatch/marketpulse/'
        },
    2:
        {
            "title": "Cochrane Library - Special Collections",
            "text": "Cochrane Library - Special Collections",
            "htmlUrl": "http://www.cochranelibrary.com/",
            "type": "rss",
            "xmlUrl": "http://www.XXXanelibrary.com/rss/"
                      "special-collections/"
        },
    12:
        {
            "title": "Digitalcourage e.V.",
            "text": "Digitalcourage e.V.",
            "htmlUrl": "https://digitalcourage.de/rss.xml",
            "type": "rss",
            "xmlUrl": "https://digitalcourage.de/rss.xml"
        },
    13:
        {
            "text": "Canadian Journal Of Nursing Research",
            "type": "rss",
            "title": "Canadian Journal Of Nursing Research",
            "htmlUrl": "http://www.ingentaconnect.com/content/"
                       "mcgill/cjnr/latest",
            "xmlUrl": "http://api.ingentaconnect.com/content/"
                      "mcgill/cjnr/latest?format=rss"
        },
    14:
        {
            "text": "Resuscitation",
            "title": "Resuscitation",
            "xmlUrl": "http://www.resuscitationjournal.com/"
                      "current.rss",
            "htmlUrl": "http://www.resuscitationjournal.com/"
                       "issues?publicationCode=resus&rss=yes",
            "type": "rss"
        },
    15:
        {
            "xmlUrl": "http://www.internationalemergencynursing.com/"
                      "current.rss",
            "type": "rss",
            "htmlUrl": "http://www.internationalemergencynursing.com/"
                       "issues?publicationCode=ienj&rss=yes",
            "title": "International Emergency Nursing",
            "text": "International Emergency Nursing"
        },
    16:
        {
            "text": "Forschung & Lehre",
            "xmlUrl": "http://www.forschung-und-lehre.de/wordpress/"
                      "?feed=rss2",
            "title": "Forschung & Lehre",
            "type": "rss",
            "htmlUrl": "http://www.forschung-und-lehre.de/wordpress"
        },
    17:
        {
            "xmlUrl": "http://ejournals.library.ualberta.ca/index"
                      ".php/EBLIP/gateway/plugin/"
                      "WebFeedGatewayPlugin/rss2",
            "htmlUrl": "http://ejournals.library.ualberta.ca/index"
                       ".php/EBLIP",
            "type": "rss",
            "title": "Evidence Based Library And Information Practice",
            "text": "Evidence Based Library And Information Practice"
        },
    18:
        {
            "type": "rss",
            "text": "Leopoldina (Press)",
            "title": "Leopoldina (Press)",
            "htmlUrl": "http://www.leopoldina.org/",
            "xmlUrl": "http://www.leopoldina.org/de/presse/"
                      "rss-nachrichten/"
        },
    19:
        {
            "htmlUrl": "http://dgi-info.de",
            "text": "Deutsche Gesellschaft F\u00fcr Information &"
                    " Wissen e.V.",
            "title": "Deutsche Gesellschaft F\u00fcr Information &"
                     " Wissen e.V.",
            "type": "rss",
            "xmlUrl": "http://dgi-info.de/feed/"
        }
}

# This are 10 entries with one duplicate (by link).
DATA_default_entries_heise = [
    {
        'link': 'https://www.heise.de/ratgeber/Wie-dunkle-Farben-und-der-'
                'Dark-Mode-die-Laufzeit-von-Smartphones-beeinflussen-44956'
                '99.html?wt_mc=rss.ho.beitrag.atom',
        'updated_parsed': [
            2019,
            8,
            22,
            18,
            15,
            0,
            3,
            234,
            0,
        ],
        'guidislink': True,
        'updated': '2019-08-22T20:15:00+02:00',
        'published': '2019-08-22T20:15:00+02:00',
        'title_detail': {
            'value': 'heise+ | Wie dunkle Farben und der Dark Mode die '
                     'Laufzeit von Smartphones beeinflussen',
            'language': None,
            'type': 'text/plain',
            'base': '',
        },
        'published_parsed': [
            2019,
            8,
            22,
            18,
            15,
            0,
            3,
            234,
            0,
        ],
        'content': [{
            'value': "<p><a href=\"https://www.heise.de/ratgeber/Wie-"
                     "dunkle-Farben-und-der-Dark-Mode-die-Laufzeit-von-"
                     "Smartphones-beeinflussen-4495699.html\"><img src=\""
                     "https://www.heise.de/scale/geometry/450/q80//imgs/"
                     "02/2/7/3/0/7/0/6/image-1565260676615218-"
                     "e6866721c15d96d8.jpeg\" class=\"webfeedsFeatured"
                     "Visual\" alt=\"\" /></a></p><p>Dunkle Farben und "
                     "der Dark Mode auf dem Smartphone-Display sollen "
                     "die Akkulaufzeit enorm verbessern. Wir haben "
                     "gemessen, wann das tats\u00e4chlich der Fall "
                     "ist.</p>",
            'language': None,
            'type': 'text/html',
            'base': '',
        }],
        'summary_detail': {
            'value': "Dunkle Farben und der Dark Mode auf dem Smartphone-"
                     "Display sollen die Akkulaufzeit enorm verbessern. "
                     "Wir haben gemessen, wann das tats\u00e4chlich der "
                     "Fall ist.",
            'language': None,
            'type': 'text/html',
            'base': '',
        },
        'links': [{'rel': 'alternate',
                   'href': 'https://www.heise.de/ratgeber/Wie-dunkle-'
                           'Farben-und-der-Dark-Mode-die-Laufzeit-von-'
                           'Smartphones-beeinflussen-4495699.html?wt_mc'
                           '=rss.ho.beitrag.atom', 'type': 'text/html'}],
        'summary': "Dunkle Farben und der Dark Mode auf dem Smartphone-"
                   "Display sollen die Akkulaufzeit enorm verbessern. "
                   "Wir haben gemessen, wann das tats\u00e4chlich der "
                   "Fall ist.",
        'id': 'http://heise.de/-4495699',
        'feedybus': {'read': True, 'date_str': '2019-08-22'},
        'title': 'heise+ | Wie dunkle Farben und der Dark Mode die '
                 'Laufzeit von Smartphones beeinflussen',
    },
    {
        'link': 'https://www.heise.de/ratgeber/Wie-dunkle-Farben-und-der-'
                'Dark-Mode-die-Laufzeit-von-Smartphones-beeinflussen-'
                '4495699.html?wt_mc=rss.ho.beitrag.atom',
        'updated_parsed': [
            2019,
            8,
            22,
            18,
            15,
            0,
            3,
            234,
            0,
        ],
        'guidislink': True,
        'updated': '2019-08-22T20:15:00+02:00',
        'published': '2019-08-22T20:15:00+02:00',
        'title_detail': {
            'value': 'heise+ | Wie dunkle Farben und der Dark Mode die '
                     'Laufzeit von Smartphones beeinflussen',
            'language': None,
            'type': 'text/plain',
            'base': '',
        },
        'published_parsed': [
            2019,
            8,
            22,
            18,
            15,
            0,
            3,
            234,
            0,
        ],
        'content': [{
            'value': "<p><a href=\"https://www.heise.de/ratgeber/Wie-"
                     "dunkle-Farben-und-der-Dark-Mode-die-Laufzeit-von-"
                     "Smartphones-beeinflussen-4495699.html\"><img src=\""
                     "https://www.heise.de/scale/geometry/450/q80//imgs"
                     "/02/2/7/3/0/7/0/6/image-1565260676615218-e6866721c"
                     "15d96d8.jpeg\" class=\"webfeedsFeaturedVisual\" "
                     "alt=\"\" /></a></p><p>Dunkle Farben und der Dark "
                     "Mode auf dem Smartphone-Display sollen die "
                     "Akkulaufzeit enorm verbessern. Wir haben gemessen, "
                     "wann das tats\u00e4chlich der Fall ist.</p>",
            'language': None,
            'type': 'text/html',
            'base': '',
        }],
        'summary_detail': {
            'value': "Dunkle Farben und der Dark Mode auf dem Smartphone-"
                     "Display sollen die Akkulaufzeit enorm verbessern. "
                     "Wir haben gemessen, wann das tats\u00e4chlich der "
                     "Fall ist.",
            'language': None,
            'type': 'text/html',
            'base': '',
        },
        'links': [{'rel': 'alternate',
                   'href': 'https://www.heise.de/ratgeber/Wie-dunkle-'
                           'Farben-und-der-Dark-Mode-die-Laufzeit-von-'
                           'Smartphones-beeinflussen-4495699.html?wt_mc='
                           'rss.ho.beitrag.atom', 'type': 'text/html'}],
        'summary': "Dunkle Farben und der Dark Mode auf dem Smartphone-"
                   "Display sollen die Akkulaufzeit enorm verbessern. "
                   "Wir haben gemessen, wann das tats\u00e4chlich der "
                   "Fall ist.",
        'id': 'http://heise.de/-4495699',
        'feedybus': {'read': True, 'date_str': '2019-08-22'},
        'title': 'heise+ | Wie dunkle Farben und der Dark Mode die '
                 'Laufzeit von Smartphones beeinflussen',
    },
    {
        'link': 'https://www.heise.de/newsticker/meldung/Elektroautos-'
                'Merkel-will-bis-2022-die-Millionengrenze-knacken-'
                '4503187.html?wt_mc=rss.ho.beitrag.atom',
        'updated_parsed': [
            2019,
            8,
            22,
            17,
            14,
            0,
            3,
            234,
            0,
        ],
        'guidislink': True,
        'updated': '2019-08-22T19:14:00+02:00',
        'published': '2019-08-22T19:14:00+02:00',
        'title_detail': {
            'value': 'Elektroautos: Merkel will bis 2022 die '
                     'Millionengrenze knacken',
            'language': None,
            'type': 'text/plain',
            'base': '',
        },
        'published_parsed': [
            2019,
            8,
            22,
            17,
            14,
            0,
            3,
            234,
            0,
        ],
        'content': [{
            'value': "<p><a href=\"https://www.heise.de/newsticker/meldung"
                     "/Elektroautos-Merkel-will-bis-2022-die-Millionen"
                     "grenze-knacken-4503187.html\"><img src=\"https://"
                     "www.heise.de/scale/geometry/450/q80//imgs/18/2/7/3/"
                     "5/8/7/9/08C941_30-f960d24427c4c591.jpeg\" class=\"w"
                     "ebfeedsFeaturedVisual\" alt=\"\" /></a></p><p>Die "
                     "Bundeskanzlerin geht davon aus, dass die deutsche "
                     "Autoindustrie in Sachen Elektromobilit\u00e4t "
                     "aufholen wird.</p>",
            'language': None,
            'type': 'text/html',
            'base': '',
        }],
        'summary_detail': {
            'value': "Die Bundeskanzlerin geht davon aus, dass die "
                     "deutsche Autoindustrie in Sachen Elektro"
                     "mobilit\u00e4t aufholen wird.",
            'language': None,
            'type': 'text/html',
            'base': '',
        },
        'links': [{'rel': 'alternate',
                   'href': 'https://www.heise.de/newsticker/meldung/'
                           'Elektroautos-Merkel-will-bis-2022-die-Millio'
                           'nengrenze-knacken-4503187.html?wt_mc=rss.ho.'
                           'beitrag.atom', 'type': 'text/html'}],
        'summary': "Die Bundeskanzlerin geht davon aus, dass die deutsche"
                   " Autoindustrie in Sachen Elektromobilit\u00e4t "
                   "aufholen wird.",
        'id': 'http://heise.de/-4503187',
        'feedybus': {'read': True, 'date_str': '2019-08-22'},
        'title': 'Elektroautos: Merkel will bis 2022 die Millionengrenze '
                 'knacken',
    },
    {
        'link': 'https://www.heise.de/security/meldung/Firefox-und-Chrome'
                '-boykottieren-Spionage-Zertifikat-der-kasachischen-'
                'Regierung-4503077.html?wt_mc=rss.ho.beitrag.atom',
        'updated_parsed': [
            2019,
            8,
            22,
            16,
            44,
            0,
            3,
            234,
            0,
        ],
        'guidislink': True,
        'updated': '2019-08-22T18:44:00+02:00',
        'published': '2019-08-22T18:44:00+02:00',
        'title_detail': {
            'value': 'Firefox und Chrome boykottieren Spionage-Zertifikat'
                     'der kasachischen Regierung',
            'language': None,
            'type': 'text/plain',
            'base': '',
        },
        'published_parsed': [
            2019,
            8,
            22,
            16,
            44,
            0,
            3,
            234,
            0,
        ],
        'content': [{
            'value': "<p><a href=\"https://www.heise.de/security/meldung/"
                     "Firefox-und-Chrome-boykottieren-Spionage-Zertifikat"
                     "-der-kasachischen-Regierung-4503077.html\"><img "
                     "src=\"https://www.heise.de/scale/geometry/450/q80//"
                     "imgs/18/2/7/3/5/7/9/1/shutterstock_464324279-"
                     "e4eec5d07f5ac5ad.jpeg\" class=\"webfeedsFeatured"
                     "Visual\" alt=\"\" /></a></p><p>Mozillas und Googles"
                     "Webbrowser blocken das TLS-Zertifikat, mit dem "
                     "Kasachstan seit kurzem seine B\u00fcrger \u00fcber"
                     "wachen will. Mozilla r\u00e4t zu Tor-Browser "
                     "und VPN.</p>",
            'language': None,
            'type': 'text/html',
            'base': '',
        }],
        'summary_detail': {
            'value': "Mozillas und Googles Webbrowser blocken das TLS-"
                     "Zertifikat, mit dem Kasachstan seit kurzem seine "
                     "B\u00fcrger \u00fcberwachen will. Mozilla r\u00e4t"
                     " zu Tor-Browser und VPN.",
            'language': None,
            'type': 'text/html',
            'base': '',
        },
        'links': [{'rel': 'alternate',
                   'href': 'https://www.heise.de/security/meldung/Firefox'
                           '-und-Chrome-boykottieren-Spionage-Zertifikat-'
                           'der-kasachischen-Regierung-4503077.html?wt_mc'
                           '=rss.ho.beitrag.atom', 'type': 'text/html'}],
        'summary': "Mozillas und Googles Webbrowser blocken das TLS-"
                   "Zertifikat, mit dem Kasachstan seit kurzem seine "
                   "B\u00fcrger \u00fcberwachen will. Mozilla "
                   "r\u00e4t zu Tor-Browser und VPN.",
        'id': 'http://heise.de/-4503077',
        'feedybus': {'read': True, 'date_str': '2019-08-22'},
        'title': 'Firefox und Chrome boykottieren Spionage-Zertifikat der'
                 ' kasachischen Regierung',
    },
    {
        'link': 'https://www.heise.de/newsticker/meldung/Intels-KI-Besch'
                'leuniger-NNP-T-mit-PCIe-4-0-und-TSMC-Technik-4503082.'
                'html?wt_mc=rss.ho.beitrag.atom',
        'updated_parsed': [
            2019,
            8,
            22,
            15,
            58,
            0,
            3,
            234,
            0,
        ],
        'guidislink': True,
        'updated': '2019-08-22T17:58:00+02:00',
        'published': '2019-08-22T17:58:00+02:00',
        'title_detail': {
            'value': 'Intels KI-Beschleuniger NNP-T mit PCIe 4.0 und '
                     'TSMC-Technik',
            'language': None,
            'type': 'text/plain',
            'base': '',
        },
        'published_parsed': [
            2019,
            8,
            22,
            15,
            58,
            0,
            3,
            234,
            0,
        ],
        'content': [{
            'value': "<p><a href=\"https://www.heise.de/newsticker/"
                     "meldung/Intels-KI-Beschleuniger-NNP-T-mit-PCIe-4-0-"
                     "und-TSMC-Technik-4503082.html\"><img src=\"https:"
                     "//www.heise.de/scale/geometry/450/q80//imgs/18/2/"
                     "7/3/5/7/9/6/Intel-NNP-T-NNP-i-8f325d4ccd4d2ffd."
                     "jpeg\" class=\"webfeedsFeaturedVisual\" "
                     "alt=\"\" /></a></p><p>Intels NNP-T \"Spring "
                     "Crest\" soll Nvidias Tesla V100 beim KI-Training "
                     "schlagen; f\u00fcrs Inferencing kommt der NNP-"
                     "I1000 \"Spring Hill\".</p>",
            'language': None,
            'type': 'text/html',
            'base': '',
        }],
        'summary_detail': {
            'value': "Intels NNP-T \"Spring Crest\" soll Nvidias Tesla "
                     "V100 beim KI-Training schlagen; f\u00fcrs "
                     "Inferencing kommt der NNP-I1000 \"Spring Hill\".",
            'language': None,
            'type': 'text/html',
            'base': '',
        },
        'links': [{'rel': 'alternate',
                   'href': 'https://www.heise.de/newsticker/meldung/'
                           'Intels-KI-Beschleuniger-NNP-T-mit-PCIe-4-0-'
                           'und-TSMC-Technik-4503082.html?wt_mc=rss.ho.'
                           'beitrag.atom', 'type': 'text/html'}],
        'summary': "Intels NNP-T \"Spring Crest\" soll Nvidias Tesla "
                   "V100 beim KI-Training schlagen; f\u00fcrs Inferencing"
                   " kommt der NNP-I1000 \"Spring Hill\".",
        'id': 'http://heise.de/-4503082',
        'feedybus': {'read': True, 'date_str': '2019-08-22'},
        'title': 'Intels KI-Beschleuniger NNP-T mit PCIe 4.0 und '
                 'TSMC-Technik',
    },
    {
        'link': 'https://www.heise.de/newsticker/meldung/Geplantes-'
                'Polizeigesetz-fuer-Mecklenburg-Vorpommern-bleibt-'
                'umstritten-4503130.html?wt_mc=rss.ho.beitrag.atom',
        'updated_parsed': [
            2019,
            8,
            22,
            15,
            53,
            0,
            3,
            234,
            0,
        ],
        'guidislink': True,
        'updated': '2019-08-22T17:53:00+02:00',
        'published': '2019-08-22T17:53:00+02:00',
        'title_detail': {
            'value': "Geplantes Polizeigesetz f\u00fcr Mecklenburg-"
                     "Vorpommern bleibt umstritten",
            'language': None,
            'type': 'text/plain',
            'base': '',
        },
        'published_parsed': [
            2019,
            8,
            22,
            15,
            53,
            0,
            3,
            234,
            0,
        ],
        'content': [{
            'value': "<p><a href=\"https://www.heise.de/newsticker/"
                     "meldung/Geplantes-Polizeigesetz-fuer-Mecklenburg-"
                     "Vorpommern-bleibt-umstritten-4503130.html\"><img "
                     "src=\"https://www.heise.de/scale/geometry/450/q80"
                     "//imgs/18/2/7/3/5/8/3/2/1163057376250990592-images-"
                     "0-050a3c85f622d189.jpeg\" class=\"webfeedsFeatured"
                     "Visual\" alt=\"\" /></a></p><p>Ermittlung"
                     "sm\u00f6glichkeiten der Sicherheitsbeh\u00f6rden "
                     "sollen erweitert werden. Doch Kritiker "
                     "bef\u00fcrchten massive Einschr\u00e4nkungen "
                     "grundlegender Pers\u00f6nlichkeitsrechte.</p>",
            'language': None,
            'type': 'text/html',
            'base': '',
        }],
        'summary_detail': {
            'value': "Ermittlungsm\u00f6glichkeiten der Sicherheits"
                     "beh\u00f6rden sollen erweitert werden. Doch "
                     "Kritiker bef\u00fcrchten massive Einschr\u00e4nk"
                     "ungen grundlegender Pers\u00f6nlichkeitsrechte.",
            'language': None,
            'type': 'text/html',
            'base': '',
        },
        'links': [{'rel': 'alternate',
                   'href': 'https://www.heise.de/newsticker/meldung/Ge'
                           'plantes-Polizeigesetz-fuer-Mecklenburg-'
                           'Vorpommern-bleibt-umstritten-4503130.html'
                           '?wt_mc=rss.ho.beitrag.atom',
                   'type': 'text/html'}],
        'summary': "Ermittlungsm\u00f6glichkeiten der Sicherheitsbe"
                   "h\u00f6rden sollen erweitert werden. Doch Kritiker "
                   "bef\u00fcrchten massive Einschr\u00e4nkungen grund"
                   "legender Pers\u00f6nlichkeitsrechte.",
        'id': 'http://heise.de/-4503130',
        'feedybus': {'read': True, 'date_str': '2019-08-22'},
        'title': "Geplantes Polizeigesetz f\u00fcr Mecklenburg-Vorpommern"
                 " bleibt umstritten",
    },
    {
        'link': 'https://www.heise.de/mac-and-i/meldung/KNOB-Attacke-Apple'
                '-liefert-Patch-gegen-Bluetooth-Schwachstelle-4503139.'
                'html?wt_mc=rss.ho.beitrag.atom',
        'updated_parsed': [
            2019,
            8,
            22,
            15,
            46,
            0,
            3,
            234,
            0,
        ],
        'guidislink': True,
        'updated': '2019-08-22T17:46:00+02:00',
        'published': '2019-08-22T17:46:00+02:00',
        'title_detail': {
            'value': 'KNOB-Attacke: Apple liefert Patch gegen '
                     'Bluetooth-Schwachstelle',
            'language': None,
            'type': 'text/plain',
            'base': '',
        },
        'published_parsed': [
            2019,
            8,
            22,
            15,
            46,
            0,
            3,
            234,
            0,
        ],
        'content': [{
            'value': "<p><a href=\"https://www.heise.de/mac-and-i/meldung"
                     "/KNOB-Attacke-Apple-liefert-Patch-gegen-Bluetooth-"
                     "Schwachstelle-4503139.html\"><img src=\"https://www."
                     "heise.de/scale/geometry/450/q80//imgs/18/2/7/3/5/8"
                     "/4/1/urn-newsml-dpa-com-20090101-161206-99-437880_"
                     "large_4_3-98cab95a04f4223d.jpeg\" class=\"webfeeds"
                     "FeaturedVisual\" alt=\"\" /></a></p><p>In der "
                     "j\u00fcngsten Version der Betriebssysteme hat Apple"
                     " eine grundlegende Schwachstelle ausger\u00e4umt, "
                     "die ein Knacken der Bluetooth-Verschl\u00fcsselung "
                     "erm\u00f6glicht.</p>",
            'language': None,
            'type': 'text/html',
            'base': '',
        }],
        'summary_detail': {
            'value': "In der j\u00fcngsten Version der Betriebssysteme hat"
                     " Apple eine grundlegende Schwachstelle ausge"
                     "r\u00e4umt, die ein Knacken der Bluetooth-Ver"
                     "schl\u00fcsselung erm\u00f6glicht.",
            'language': None,
            'type': 'text/html',
            'base': '',
        },
        'links': [{'rel': 'alternate',
                   'href': 'https://www.heise.de/mac-and-i/meldung/KNOB-'
                           'Attacke-Apple-liefert-Patch-gegen-Bluetooth-'
                           'Schwachstelle-4503139.html?wt_mc=rss.ho.'
                           'beitrag.atom', 'type': 'text/html'}],
        'summary': "In der j\u00fcngsten Version der Betriebssysteme "
                   "hat Apple eine grundlegende Schwachstelle aus"
                   "ger\u00e4umt, die ein Knacken der Bluetooth-"
                   "Verschl\u00fcsselung erm\u00f6glicht.",
        'id': 'http://heise.de/-4503139',
        'feedybus': {'read': True, 'date_str': '2019-08-22'},
        'title': 'KNOB-Attacke: Apple liefert Patch gegen Bluetooth-'
                 'Schwachstelle',
    },
    {
        'link': 'https://www.heise.de/newsticker/meldung/Android-Kein-'
                'Nachtisch-mehr-und-ein-neues-Logo-4503119.html?wt_mc'
                '=rss.ho.beitrag.atom',
        'updated_parsed': [
            2019,
            8,
            22,
            15,
            42,
            0,
            3,
            234,
            0,
        ],
        'guidislink': True,
        'updated': '2019-08-22T17:42:00+02:00',
        'published': '2019-08-22T17:42:00+02:00',
        'title_detail': {
            'value': 'Android: Kein Nachtisch mehr und ein neues Logo',
            'language': None,
            'type': 'text/plain',
            'base': '',
        },
        'published_parsed': [
            2019,
            8,
            22,
            15,
            42,
            0,
            3,
            234,
            0,
        ],
        'content': [{
            'value': "<p><a href=\"https://www.heise.de/newsticker/"
                     "meldung/Android-Kein-Nachtisch-mehr-und-ein-neues"
                     "-Logo-4503119.html\"><img src=\"https://www.heise.de"
                     "/scale/geometry/450/q80//imgs/18/2/7/3/5/8/2/4/"
                     "shutterstock_517582159-23f8669d11b557a5.jpeg\" "
                     "class=\"webfeedsFeaturedVisual\" alt=\"\" /></a>"
                     "</p><p>Google hat Neuerungen f\u00fcr Android "
                     "angek\u00fcndigt. Die betreffen vor allem den Namen "
                     "und das Logo. Der Roboter wird bleiben, aber nur "
                     "teilweise. </p>",
            'language': None,
            'type': 'text/html',
            'base': '',
        }],
        'summary_detail': {
            'value': "Google hat Neuerungen f\u00fcr Android "
                     "angek\u00fcndigt. Die betreffen vor allem den Namen"
                     " und das Logo. Der Roboter wird bleiben, aber nur "
                     "teilweise.",
            'language': None,
            'type': 'text/html',
            'base': '',
        },
        'links': [{'rel': 'alternate',
                   'href': 'https://www.heise.de/newsticker/meldung/'
                           'Android-Kein-Nachtisch-mehr-und-ein-neues-'
                           'Logo-4503119.html?wt_mc=rss.ho.beitrag.atom',
                   'type': 'text/html'}],
        'summary': "Google hat Neuerungen f\u00fcr Android ange"
                   "k\u00fcndigt. Die betreffen vor allem den Namen und "
                   "das Logo. Der Roboter wird bleiben, aber nur "
                   "teilweise.",
        'id': 'http://heise.de/-4503119',
        'feedybus': {'read': True, 'date_str': '2019-08-22'},
        'title': 'Android: Kein Nachtisch mehr und ein neues Logo',
    },
    {
        'link': 'https://www.techstage.de/news/Motorola-One-Action-Test-'
                'Gute-Hardware-schlechte-Kamera-4502663.html?wt_mc='
                'rss.ho.beitrag.atom',
        'updated_parsed': [
            2019,
            8,
            22,
            15,
            35,
            0,
            3,
            234,
            0,
        ],
        'guidislink': True,
        'updated': '2019-08-22T17:35:00+02:00',
        'published': '2019-08-22T17:35:00+02:00',
        'title_detail': {
            'value': 'TechStage | Motorola One Action Test: Gute '
                     'Hardware, schlechte Kamera',
            'language': None,
            'type': 'text/plain',
            'base': '',
        },
        'published_parsed': [
            2019,
            8,
            22,
            15,
            35,
            0,
            3,
            234,
            0,
        ],
        'content': [{
            'value': "<p><a href=\"https://www.techstage.de/news/Motorola"
                     "-One-Action-Test-Gute-Hardware-schlechte-Kamera-"
                     "4502663.html\"><img src=\"https://www.heise.de/"
                     "scale/geometry/450/q80//imgs/18/2/7/3/5/7/5/7/TS_"
                     "Dachbild-9c5049bc9929401e.jpeg\" class=\"webfeeds"
                     "FeaturedVisual\" alt=\"\" /></a></p><p>Das Motorola"
                     " One Action ist dem Motorola One Vision sehr"
                     " \u00e4hnlich, setzt aber auf Actioncam statt Foto"
                     "squalit\u00e4t. Ob das eine gute Idee ist, zeigt "
                     "der Test.</p>",
            'language': None,
            'type': 'text/html',
            'base': '',
        }],
        'summary_detail': {
            'value': "Das Motorola One Action ist dem Motorola One "
                     "Vision sehr \u00e4hnlich, setzt aber auf Actioncam"
                     " statt Fotosqualit\u00e4t. Ob das eine gute Idee "
                     "ist, zeigt der Test.",
            'language': None,
            'type': 'text/html',
            'base': '',
        },
        'links': [{'rel': 'alternate',
                   'href': 'https://www.techstage.de/news/Motorola-One-'
                           'Action-Test-Gute-Hardware-schlechte-Kamera-'
                           '4502663.html?wt_mc=rss.ho.beitrag.atom',
                   'type': 'text/html'}],
        'summary': "Das Motorola One Action ist dem Motorola One Vision "
                   "sehr \u00e4hnlich, setzt aber auf Actioncam statt "
                   "Fotosqualit\u00e4t. Ob das eine gute Idee ist, zeigt "
                   "der Test.",
        'id': 'http://heise.de/-4503021',
        'feedybus': {'read': True, 'date_str': '2019-08-22'},
        'title': 'TechStage | Motorola One Action Test: Gute Hardware, '
                 'schlechte Kamera',
    },
    {
        'link': 'https://www.heise.de/ratgeber/Alte-Schwarzweissfotos-mit'
                '-Affinity-Photo-nachtraeglich-kolorieren-4494467.html?'
                'wt_mc=rss.ho.beitrag.atom',
        'updated_parsed': [
            2019,
            8,
            22,
            15,
            15,
            0,
            3,
            234,
            0,
        ],
        'guidislink': True,
        'updated': '2019-08-22T17:15:00+02:00',
        'published': '2019-08-22T17:15:00+02:00',
        'title_detail': {
            'value': "heise+ | Alte Schwarzwei\u00dffotos mit Affinity "
                     "Photo nachtr\u00e4glich kolorieren",
            'language': None,
            'type': 'text/plain',
            'base': '',
        },
        'published_parsed': [
            2019,
            8,
            22,
            15,
            15,
            0,
            3,
            234,
            0,
        ],
        'content': [{
            'value': "<p><a href=\"https://www.heise.de/ratgeber/Alte-"
                     "Schwarzweissfotos-mit-Affinity-Photo-nachtraeglich-"
                     "kolorieren-4494467.html\"><img src=\"https://www."
                     "heise.de/scale/geometry/450/q80//imgs/02/2/7/2/9/8/"
                     "6/5/001_Kolorieren-Aufmacher-7339b5e699269502."
                     "jpeg\" class=\"webfeedsFeaturedVisual\" alt=\"\" "
                     "/></a></p><p>Ebenen, Pinsel, Farbauswahl: Mit "
                     "Affinity Photo erwecken Sie alte Fotos zu neuem "
                     "Leben. Eine Schritt-f\u00fcr-Schritt-Anleitung vom "
                     "Scan bis zum fertigen Bild. </p>",
            'language': None,
            'type': 'text/html',
            'base': '',
        }],
        'summary_detail': {
            'value': "Ebenen, Pinsel, Farbauswahl: Mit Affinity Photo "
                     "erwecken Sie alte Fotos zu neuem Leben. Eine Schritt"
                     "-f\u00fcr-Schritt-Anleitung vom Scan bis zum "
                     "fertigen Bild.",
            'language': None,
            'type': 'text/html',
            'base': '',
        },
        'links': [{'rel': 'alternate',
                   'href': 'https://www.heise.de/ratgeber/Alte-Schwarz'
                           'weissfotos-mit-Affinity-Photo-nachtraeglich-'
                           'kolorieren-4494467.html?wt_mc=rss.ho.beitrag'
                           '.atom', 'type': 'text/html'}],
        'summary': "Ebenen, Pinsel, Farbauswahl: Mit Affinity Photo "
                   "erwecken Sie alte Fotos zu neuem Leben. Eine Schritt"
                   "-f\u00fcr-Schritt-Anleitung vom Scan bis zum "
                   "fertigen Bild.",
        'id': 'http://heise.de/-4494467',
        'feedybus': {'read': True, 'date_str': '2019-08-22'},
        'title': "heise+ | Alte Schwarzwei\u00dffotos mit Affinity Photo "
                 "nachtr\u00e4glich kolorieren",
    },
]


DATA_default_entries = {
    1: DATA_default_entries_heise,
    2: [],
    3: [],
    4: [],
    5: [],
    6: [],
    7: [],
    8: [],
    9: [],
    10: [],
    11: [],
    12: [],
    13: [],
    14: [],
    15: [],
    16: [],
    17: [],
    18: [],
    19: []
}


DATA_default = {
    'tree': DATA_default_tree,
    'feeds': DATA_default_feeds,
    'entries': DATA_default_entries
}

DATA_export_tree = [
    [1],
    (-1000, 'Folder A', False, [
        [2]
    ]
    ),
    (-1001, 'Folder B', False, [
        [3],
        (-1002, 'Folder B-a', True, [
            [4, 5, 6]
        ]
        ),
        (-1003, 'Folder B-b', False, [[]])
    ]
    ),
    (-1004, 'Folder C', False, [[]]),
]

DATA_export_feeds = {
    1:
        {
            "title": "Title A",
            "text": "Text A",
            "version": "atom10",
            "htmlUrl": "http://www.a.url",
            "xmlUrl": "http://www.a.url/atom/",
            "author": "Mrs. A",
            "rights": "CC",
            "description": "Description A",
            "encoding": "UTF-8"
        },
    2:
        {
            "title": "Title B",
            "text": "Text B",
            "xmlUrl": "http://www.b.url/atom/",
        },
    3:
        {
            "title": "Title C",
            "text": "Text C",
            "xmlUrl": "http://www.c.url/atom/",
        },
    4:
        {
            "title": "Title D",
            "text": "Text D",
            "xmlUrl": "http://www.d.url/atom/",
        },
    5:
        {
            "title": "Title E",
            "text": "Text E",
            "xmlUrl": "http://www.e.url/atom/",
        },
    6:
        {
            "title": "Title F",
            "text": "Text F",
            "xmlUrl": "http://www.f.url/atom/",
        },
}

DATA_export = {
    'tree': DATA_export_tree,
    'feeds': DATA_export_feeds,
    'entries': {
        1: [],
        2: [],
        3: [],
        4: [],
        5: [],
        6: [],
        7: [],
        8: [],
        9: []
    }
}

DATA_export_feeds = {
    1:
        {
            "title": "Title A",
            "text": "Text A",
            "version": "atom10",
            "htmlUrl": "http://www.a.url",
            "xmlUrl": "http://www.a.url/atom/",
            "author": "Mrs. A",
            "rights": "CC",
            "description": "Description A",
            "encoding": "UTF-8"
        },
    2:
        {
            "title": "Title B",
            "text": "Text B",
            "xmlUrl": "http://www.b.url/atom/",
        },
    3:
        {
            "title": "Title C",
            "text": "Text C",
            "xmlUrl": "http://www.c.url/atom/",
        },
    4:
        {
            "title": "Title D",
            "text": "Text D",
            "xmlUrl": "http://www.d.url/atom/",
        },
    5:
        {
            "title": "Title E",
            "text": "Text E",
            "xmlUrl": "http://www.e.url/atom/",
        },
    6:
        {
            "title": "Title F",
            "text": "Text F",
            "xmlUrl": "http://www.f.url/atom/",
        },
}

DATA_export_tree = [
    [1],
    (-1000, 'Folder A', False, [
        [2]
    ]
    ),
    (-1001, 'Folder B', False, [
        [3],
        (-1002, 'Folder B-a', True, [
            [4, 5, 6]
        ]
        ),
        (-1003, 'Folder B-b', False, [[]])
    ]
    ),
    (-1004, 'Folder C', False, [[]]),
]


# 3 feeds
# Feed 1: 4 entries without feedybus attribute
# Feed 2: 3 entries (read, unread, without)
# Feed 3: empty
# Feed 4: 10 entries (unread)
# Feed 5: 3 read
# Feed 6: 2 unread
# Feed 7: 3 (2 unread)
# Feed 8: 3 (1 unread)
DATA_test_feed_entries = {
    'tree': [
        [1, 2, 3, 4],
        (-1000, 'Folder A', False, [
            [5, 7],
            (-1001, 'Folder B', False, [
                [6],
                (-1002, 'Folder C', False, [
                    [8]
                ]
                )
            ]
            )
        ]
        )
    ],
    'feeds': {
        1:
            {
                "title": "Title A",
                "text": "Text A",
                "xmlUrl": "http://www.a.url/atom/",
            },
        2:
            {
                "title": "Title B",
                "text": "Text B",
                "xmlUrl": "http://www.b.url/atom/",
            },
        3:
            {
                "title": "Title C",
                "text": "Text C",
                "xmlUrl": "http://www.c.url/atom/",
            },
        4:
            {
                "title": "Entries date_str Test",
                "text": "Text D",
                "xmlUrl": "http://www.d.url/atom/",
            },
        5:
            {
                "title": "Title 5",
                "text": "Text 5",
                "xmlUrl": "http://www.5.url/atom/",
            },
        6:
            {
                "title": "Title 6",
                "text": "Text 6",
                "xmlUrl": "http://www.6.url/atom/",
            },
        7:
            {
                "title": "Title 7",
                "text": "Text 7",
                "xmlUrl": "http://www.7.url/atom/",
            },
        8:
            {
                "title": "Title 8",
                "text": "Text 8",
                "xmlUrl": "http://www.8.url/atom/",
            }
    },
    'entries': {
        5:
            [
                {
                    'feedybus': {
                        'read': True
                    },
                    'link': 'https://www.url.world/wtf.htm',
                    'title': 'title'
                },
                {
                    'feedybus': {
                        'read': True
                    },
                    'link': 'https://www.url.world/wtf.htm',
                    'title': 'title'
                },
                {
                    'feedybus': {
                        'read': True
                    },
                    'link': 'https://www.url.world/wtf.htm',
                    'title': 'title'
                },
            ],
        6:
            [
                {
                    'feedybus': {
                        'read': False
                    },
                    'link': 'https://www.url.world/wtf.htm',
                    'title': 'title'
                },
                {
                    'feedybus': {
                        'read': False
                    },
                    'link': 'https://www.url.world/wtf.htm',
                    'title': 'title'
                },
            ],
        7:
            [
                {
                    'feedybus': {},
                    'link': 'https://www.url.world/wtf.htm',
                    'title': 'title'
                },
                {
                    'feedybus': {
                        'read_later': True
                    },
                    'link': 'https://www.url.world/wtf.htm',
                    'title': 'title'
                },
                {
                    'feedybus': {
                        'read': True
                    },
                    'link': 'https://www.url.world/wtf.htm',
                    'title': 'title'
                },
            ],
        8:
            [
                {
                    'feedybus': {
                        'read': True,
                        'read_later': False
                    },
                    'link': 'https://www.url.world/wtf.htm',
                    'title': 'title'
                },
                {
                    'feedybus': {
                        'read': True,
                        'read_later': True
                    },
                    'link': 'https://www.url.world/wtf.htm',
                    'title': 'title'
                },
                {
                    'feedybus': {
                        'read': False
                    },
                    'link': 'https://www.url.world/wtf.htm',
                    'title': 'title'
                },
            ],
        1:
            [
                {
                    "feedybus": {
                        "date_str": "2019-05-21",
                        "read": False
                    },
                    'link': 'https://www.heise.de/ratgeber/Wie-dunkle-Farben-'
                            'und-der-'
                            'Dark-Mode-die-Laufzeit-von-Smartphones-'
                            'beeinflussen-4495699.html?wt_mc=rss.'
                            'ho.beitrag.atom',
                    'updated_parsed': [
                        2019,
                        8,
                        22,
                        18,
                        15,
                        0,
                        3,
                        234,
                        0,
                    ],
                    'guidislink': True,
                    'updated': '2019-08-22T20:15:00+02:00',
                    'published': '2019-08-22T20:15:00+02:00',
                    'title_detail': {
                        'value': 'heise+ | Wie dunkle Farben und '
                        'der Dark Mode die '
                        'Laufzeit von Smartphones beeinflussen',
                        'language': None,
                        'type': 'text/plain',
                        'base': '',
                    },
                    'published_parsed': [
                        2019,
                        8,
                        22,
                        18,
                        15,
                        0,
                        3,
                        234,
                        0,
                    ],
                    'summary_detail': {
                        'value': "Dunkle Farben und der Dark Mode auf "
                                 "dem Smartphone-"
                                 "Display sollen die Akkulaufzeit enorm"
                                 " verbessern. "
                                 "Wir haben gemessen, wann das "
                                 "tats\u00e4chlich der Fall ist.",
                        'language': None,
                        'type': 'text/html',
                        'base': '',
                    },
                    'links': [{'rel': 'alternate',
                               'href': 'https://www.heise.de/ratgeber'
                                       'Wie-dunkle-'
                                       'Farben-und-der-Dark-Mode-die-Laufzeit-'
                                       'von-Smartphones-beeinflussen-4495699.'
                                       'html?wt_mc'
                                       '=rss.ho.beitrag.atom',
                               'type': 'text/html'}],
                    'summary': "Dunkle Farben und der Dark Mode auf "
                               "dem Smartphone-"
                               "Display sollen die Akkulaufzeit "
                               "enorm verbessern. "
                               "Wir haben gemessen, wann das "
                               "tats\u00e4chlich der "
                               "Fall ist.",
                    'id': 'http://heise.de/-4495699',
                    'title': 'heise+ | Wie dunkle Farben',
                },
                {
                    "feedybus": {
                        "date_str": "2019-05-21",
                        "read": False
                    },
                    'link': 'https://www.heise.de/ratgeber/'
                            'Wie-dunkle-Farben-und-der-'
                            'Dark-Mode-die-Laufzeit-von-Smartphones-'
                            'beeinflussen-'
                            '4495699.html?wt_mc=rss.ho.beitrag.atom',
                    'updated_parsed': [
                        2019,
                        8,
                        22,
                        18,
                        15,
                        0,
                        3,
                        234,
                        0,
                    ],
                    'guidislink': True,
                    'updated': '2019-08-22T20:15:00+02:00',
                    'published': '2019-08-22T20:15:00+02:00',
                    'title_detail': {
                        'value': 'heise+ | Wie dunkle Farben und der Dark '
                                 'Mode die '
                                 'Laufzeit von Smartphones beeinflussen',
                        'language': None,
                        'type': 'text/plain',
                        'base': '',
                    },
                    'published_parsed': [
                        2019,
                        8,
                        22,
                        18,
                        15,
                        0,
                        3,
                        234,
                        0,
                    ],
                    'summary_detail': {
                        'value': "Dunkle Farben und der Dark Mode "
                                 "auf dem Smartphone-"
                                 "Display sollen die Akkulaufzeit "
                                 "enorm verbessern. "
                                 "Wir haben gemessen, wann das "
                                 "tats\u00e4chlich der "
                                 "Fall ist.",
                        'language': None,
                        'type': 'text/html',
                        'base': '',
                    },
                    'links': [{'rel': 'alternate',
                               'href': 'https://www.heise.de/ratgeber/'
                                       'Wie-dunkle-'
                                       'Farben-und-der-Dark-Mode-die-Laufzeit'
                                       '-von-'
                                       'Smartphones-beeinflussen-4495699.'
                                       'html?wt_mc='
                                       'rss.ho.beitrag.atom',
                               'type': 'text/html'}],
                    'summary': "Dunkle Farben und der Dark Mode auf "
                               "dem Smartphone-"
                               "Display sollen die Akkulaufzeit "
                               "enorm verbessern. "
                               "Wir haben gemessen, wann das "
                               "tats\u00e4chlich der "
                               "Fall ist.",
                    'id': 'http://heise.de/-4495699',
                    'title': 'heise+ | Wie dunkle Farben und der Dark '
                             'Mode die '
                             'Laufzeit von Smartphones beeinflussen',
                },
                {
                    "feedybus": {
                        "date_str": "2019-05-21",
                        "read": False
                    },
                    'link': 'https://www.heise.de/newsticker/meldung'
                            '/Elektroautos-'
                            'Merkel-will-bis-2022-die-Millionengrenze-knacken-'
                            '4503187.html?wt_mc=rss.ho.beitrag.atom',
                    'updated_parsed': [
                        2019,
                        8,
                        22,
                        17,
                        14,
                        0,
                        3,
                        234,
                        0,
                    ],
                    'guidislink': True,
                    'updated': '2019-08-22T19:14:00+02:00',
                    'published': '2019-08-22T19:14:00+02:00',
                    'title_detail': {
                        'value': 'Elektroautos: Merkel will bis 2022 die '
                                 'Millionengrenze knacken',
                        'language': None,
                        'type': 'text/plain',
                        'base': '',
                    },
                    'published_parsed': [
                        2019,
                        8,
                        22,
                        17,
                        14,
                        0,
                        3,
                        234,
                        0,
                    ],
                    'summary_detail': {
                        'value': "Die Bundeskanzlerin geht davon aus, dass "
                                 "die "
                                 "deutsche Autoindustrie in Sachen Elektro"
                                 "mobilit\u00e4t aufholen wird.",
                        'language': None,
                        'type': 'text/html',
                        'base': '',
                    },
                    'links': [{'rel': 'alternate',
                               'href': 'https://www.heise.de/newsticker'
                                       '/meldung/'
                                       'Elektroautos-Merkel-will-bis-2022-'
                                       'die-Millio'
                                       'nengrenze-knacken-4503187.'
                                       'html?wt_mc=rss.ho.'
                                       'beitrag.atom',
                               'type': 'text/html'}],
                    'summary': "Die Bundeskanzlerin geht davon aus, "
                               "dass die deutsche"
                               " Autoindustrie in Sachen Elektromo"
                               "bilit\u00e4t "
                               "aufholen wird.",
                    'id': 'http://heise.de/-4503187',
                    'title': 'Elektroautos: Merkel will bis 2022 '
                             'die Millionengrenze '
                             'knacken',
                },
                {
                    "feedybus": {
                        "date_str": "2019-05-21",
                        "read": False
                    },
                    'link': 'https://www.heise.de/security/meldung'
                            '/Firefox-und-Chrome'
                            '-boykottieren-Spionage-Zertifikat-der-'
                            'kasachischen-'
                            'Regierung-4503077.html?wt_mc=rss.ho.beitrag.atom',
                    'updated_parsed': [
                        2019,
                        8,
                        22,
                        16,
                        44,
                        0,
                        3,
                        234,
                        0,
                    ],
                    'guidislink': True,
                    'updated': '2019-08-22T18:44:00+02:00',
                    'published': '2019-08-22T18:44:00+02:00',
                    'title_detail': {
                        'value': 'Firefox und Chrome '
                                 'boykottieren Spionage-Zertifikat'
                                 'der kasachischen Regierung',
                        'language': None,
                        'type': 'text/plain',
                        'base': '',
                    },
                    'published_parsed': [
                        2019,
                        8,
                        22,
                        16,
                        44,
                        0,
                        3,
                        234,
                        0,
                    ],
                    'summary_detail': {
                        'value': "Mozillas und Googles Webbrowser blocken "
                                 "das TLS-"
                                 "Zertifikat, mit dem Kasachstan seit "
                                 "kurzem seine "
                                 "B\u00fcrger \u00fcberwachen will. "
                                 "Mozilla r\u00e4t"
                                 " zu Tor-Browser und VPN.",
                        'language': None,
                        'type': 'text/html',
                        'base': '',
                    },
                    'links': [{'rel': 'alternate',
                               'href': 'https://www.heise.de/security/'
                                       'meldung/Firefox'
                                       '-und-Chrome-boykottieren-Spionage-'
                                       'Zertifikat-'
                                       'der-kasachischen-Regierung-'
                                       '4503077.html?wt_mc'
                                       '=rss.ho.beitrag.atom',
                               'type': 'text/html'}],
                    'summary': "Mozillas und Googles Webbrowser blocken "
                               "das TLS-"
                               "Zertifikat, mit dem Kasachstan seit "
                               "kurzem seine "
                               "B\u00fcrger \u00fcberwachen will. Mozilla "
                               "r\u00e4t zu Tor-Browser und VPN.",
                    'id': 'http://heise.de/-4503077',
                    'title': 'Firefox und Chrome '
                             'boykottieren Spionage-Zertifikat der'
                             ' kasachischen Regierung',
                }
            ],
        2:
            [
                {
                    "feedybus": {
                        "date_str": "2019-06-12",
                        "read": True,
                        "read_later": True
                    },
                    "published": "Wed, 12 Jun 2019 20:47:25 +0000",
                    "id": "http://blog.do-foss.de/?p=3421",
                    "link": "http://blog.do-foss.de/kolumn"
                            "e/smart-city-dortmund-handyparken-"
                            "ein-beispiel-fuer-infrastrukturelle-"
                            "abhaengigkeit/",
                    "links": [
                        {
                            "href": "http://blog.do-foss.de/kolumne"
                                    "/smart-city-dortmund-handyparken-"
                                    "ein-beispiel-fuer-infrastrukturelle-"
                                    "abhaengigkeit/",
                            "type": "text/html",
                            "rel": "alternate"
                        }
                    ],
                    "title_detail": {
                        "value": "Smart City Dortmund: Handyparken \u2013 "
                                 "Ein Beispiel f\u00fcr infrastruk"
                                 "turelle Abh\u00e4ngigkeit",
                        "type": "text/plain",
                        "base": ""
                    },
                    "author_detail": {
                        "name": "Till Sch\u00e4fer"
                    },
                    "published_parsed": [
                        2019,
                        6,
                        12,
                        20,
                        47,
                        25,
                        2,
                        163,
                        0
                    ],
                    "summary_detail": {
                        "value": "<p>Aktuelle</p>",
                        "type": "text/html",
                        "base": ""
                    },
                    "content": [
                        {
                            "value": "<h2>content</h2>",
                            "type": "text/html",
                            "base": ""
                        }
                    ],
                    "slash_comments": "1",
                    "authors": [
                        {
                            "name": "Till Sch\u00e4fer"
                        }
                    ],
                    "author": "Till Sch\u00e4fer",
                    "summary": "<p>Summary</p>",
                    "title": "Smart City Dortmund: Handyparken "
                             "\u2013 Ein Beispiel f\u00fcr "
                             "infrastrukturelle Abh\u00e4ngigkeit",
                    "guidislink": False
                },
                {
                    "feedybus": {
                        "date_str": "2019-05-21",
                        "read": False
                    },
                    "published": "Tue, 21 May 2019 20:30:32 +0000",
                    "id": "http://blog.do-foss.de/?p=3331",
                    "link": "http://blog.do-foss.de/beitrag"
                            "/freie-software-fuer-die-"
                            "entwicklungszusammenarbeit/",
                    "links": [
                        {
                            "href": "http://blog.do-foss.de/beitrag"
                                    "/freie-software-fuer-die-"
                                    "entwicklungszusammenarbeit/",
                            "type": "text/html",
                            "rel": "alternate"
                        }
                    ],
                    "title_detail": {
                        "value": "Freie Software f\u00fcr "
                                 "die Entwicklungszusammenarbeit",
                        "type": "text/plain",
                        "base": ""
                    },
                    "author_detail": {
                        "name": "Till Sch\u00e4fer"
                    },
                    "wfw_commentrss": "http://blog.do-foss.de/beitrag/"
                                      "freie-software-fuer-die-"
                                      "entwicklungszusammenarbeit/feed/",
                    "published_parsed": [
                        2019,
                        5,
                        21,
                        20,
                        30,
                        32,
                        1,
                        141,
                        0
                    ],
                    "summary_detail": {
                        "value": "<p>Do-FOSS</p>",
                        "type": "text/html",
                        "base": ""
                    },
                    "content": [
                        {
                            "value": "content",
                            "type": "text/html",
                            "base": ""
                        }
                    ],
                    "slash_comments": "0",
                    "comments": "http://blog.do-foss.de/beitrag"
                                "/freie-software-fuer-die-"
                                "entwicklungszusammenarbeit/#respond",
                    "authors": [
                        {
                            "name": "Till Sch\u00e4fer"
                        }
                    ],
                    "author": "Till Sch\u00e4fer",
                    "summary": "<p>Do-FOSS</p>",
                    "title": "Freie Software f\u00fcr "
                             "die Entwicklungszusammenarbeit",
                    "guidislink": False
                },
                {
                    "feedybus": {
                        "date_str": "2019-05-08",
                        "read": False
                    },
                    "published": "Wed, 08 May 2019 19:43:47 +0000",
                    "id": "http://blog.do-foss.de/?p=3289",
                    "link": "http://blog.do-foss.de/kolumne"
                            "/software-rebellen-die-macht-des-teilens/",
                    "links": [
                        {
                            "href": "http://blog.do-foss.de/kolumne"
                                    "/software-rebellen-die-macht-des-"
                                    "teilens/",
                            "type": "text/html",
                            "rel": "alternate"
                        }
                    ],
                    "title_detail": {
                        "value": "Software-Rebellen \u2013 Die "
                                 "Macht des Teilens",
                        "type": "text/plain",
                        "base": ""
                    },
                    "author_detail": {
                        "name": "Christian N\u00e4hle"
                    },
                    "wfw_commentrss": "http://blog.do-foss.de/kolumne"
                                      "/software-rebellen-die-macht-"
                                      "des-teilens/feed/",
                    "published_parsed": [
                        2019,
                        5,
                        8,
                        19,
                        43,
                        47,
                        2,
                        128,
                        0
                    ],
                    "summary_detail": {
                        "value": "<p>Filmtipp</p>",
                        "type": "text/html",
                        "base": ""
                    },
                    "slash_comments": "0",
                    "author": "Christian N\u00e4hle",
                    "title": "Software-Rebellen \u2013 Die Macht des Teilens",
                    "guidislink": False
                },
            ],
        3:
            [
            ],
        4:
            [
                {
                    "feedybus": {
                        "date_str": "1982-08-06",
                        "read": False
                    },
                    'link': 'https://www.heise.de/ratgeber/Wie-dunkle',
                    'updated_parsed': [
                        2019,
                        8,
                        22,
                        18,
                        15,
                        0,
                        3,
                        234,
                        0,
                    ],
                    'published': '2019-08-22T20:15:00+02:00',
                    'published_parsed': [
                        2019,
                        8,
                        22,
                        18,
                        15,
                        0,
                        3,
                        234,
                        0,
                    ],
                    'title': 'heise+ | Wie dunkle Farben und der Dark ',
                },
                {
                    'feedybus': {},
                    'link': 'https://www.heise.de/ratgeber/Wie-dunkle',
                    'published_parsed': [
                        1982,
                        8,
                        6,
                        18,
                        23,
                        0,
                        4,
                        218,
                        0,

                    ],
                    'title': 'heise+ | Wie dunkle Farben und der Dark ',
                },
                {
                    'feedybus': {},
                    'link': 'https://www.heise.de/ratgeber/Wie-dunkle',
                    'updated_parsed': [
                        1982,
                        8,
                        6,
                        18,
                        23,
                        0,
                        4,
                        218,
                        0,
                    ],
                    'title': 'heise+ | Wie dunkle Farben und der Dark ',
                },
                {
                    'feedybus': {},
                    'link': 'https://www.heise.de/ratgeber/Wie-dunkle',
                    'published': '1982-08-06T18:23:00+02:00',
                    'title': 'heise+ | Wie dunkle Farben und der Dark ',
                },
                {
                    'feedybus': {},
                    'link': 'https://www.heise.de/ratgeber/Wie-dunkle',
                    'published': '',
                    'title': 'heise+ | Wie dunkle Farben und der Dark ',
                },
                {
                    'feedybus': {},
                    'link': 'https://www.heise.de/ratgeber/Wie-dunkle',
                    'title': 'FooBar'
                },
                {
                    'feedybus': {},
                    'link': 'https://www.heise.de/ratgeber/Wie-dunkle',
                    'updated_parsed': [
                        2019,
                        8,
                        22,
                        18,
                        15,
                        0,
                        3,
                        234,
                        0,
                    ],
                    'published_parsed': [
                        1982,
                        8,
                        6,
                        18,
                        23,
                        0,
                        4,
                        218,
                        0,
                    ],
                    'title': 'heise+ | Wie dunkle Farben und der Dark ',
                },
                {
                    'feedybus': {},
                    'link': 'https://www.heise.de/ratgeber/Wie-dunkle',
                    'updated_parsed': [
                        2019,
                        8,
                        22,
                        18,
                        15,
                        0,
                        3,
                        234,
                        0,
                    ],
                    'published': '2019-08-22T20:15:00+02:00',
                    'published_parsed': [
                        1982,
                        8,
                        6,
                        18,
                        23,
                        0,
                        4,
                        218,
                        0,
                    ],
                    'title': 'heise+ | Wie dunkle Farben und der Dark ',
                },
                {
                    'feedybus': {},
                    'link': 'https://www.heise.de/ratgeber/Wie-dunkle',
                    'published': '2019-08-22T20:15:00+02:00',
                    'published_parsed': [
                        1982,
                        8,
                        6,
                        18,
                        23,
                        0,
                        4,
                        218,
                        0,
                    ],
                    'title': 'heise+ | Wie dunkle Farben und der Dark ',
                },
                {
                    'feedybus': {},
                    'link': 'https://www.heise.de/ratgeber/Wie-dunkle',
                    'updated_parsed': [
                        1982,
                        8,
                        6,
                        18,
                        23,
                        0,
                        4,
                        218,
                        0,
                    ],
                    'published': '2019-08-22T20:15:00+02:00',
                    'title': 'heise+ | Wie dunkle Farben und der Dark ',
                },
            ],
    }
}


def rand_string(max_length=25):
    """Create a string with random uppercase characters and digits. Default
    length is 25.

    Args:
        max_length (int): Max length of the string (default: 25).

    Returns:
        (string): The created random string.
    """
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _
                   in range(random.randint(1, max_length)))


def clear_profile():
    """Delete all the current profile and all its data.

    It is used for test cases where temporary profiles created used only for
    the specific test.
    """

    for d in [basics.feed_favicons_directory(),
              basics.feed_entries_directory()]:
        shutil.rmtree(d)
        # print('<<< Removed directory tree {}.'.format(d))

    for f in [basics.config_filename(),
              basics.miscconfig_filename(),
              basics.feeds_filename(),
              basics.log_filename()]:
        if os.path.isfile(f):
            os.remove(f)


def pep8_conform(filename_or_class):
    """
    """
    if type(filename_or_class) is str:
        # It is a filename.
        filename = filename_or_class
    else:
        # It is a class. Get its filename.
        filename = inspect.getfile(filename_or_class)

    # E266 to many leading '#' ignored because '##' is used in doxygen to
    # describe class/ojbect attributes.
    pep = pycodestyle.Checker(filename, ignore=['E266', 'W503'])
    return pep.check_all() == 0


class TestEventHelper:
    def __init__(self, event):
        self.notified = 0
        self.last_args = None
        self.last_kwargs = None
        event.register(self._event_handler)

    def _event_handler(self, *args, **kwargs):
        self.notified += 1
        # print(f'{id(self)} {self.notified} {args} {kwargs}')
        self.last_args = args
        self.last_kwargs = kwargs
