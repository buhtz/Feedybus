import random
import unittest
import time
from unittest import mock
from . import helper
from feedybus.feed import Feed
from feedybus.group import Group
import feedybus.basics as basics
import feedybus.data as data
import feedybus.favicon as favicon
import os.path
import pathlib


"""
    Test cases for Feed class.

    Allowed:
    Feed(11)
    Feed(title, text, xmlUrl, htmlUrl, type, authors, rights, description,
    encoding, group)
    Feed(title, xmlUrl)

    Not allowed:
    Feed()
"""


class TestFeed_DataPerTest(unittest.TestCase):
    """Class with test cases for Feed class in the special context of deleting
    feeds."""
    def setUp(self):
        """Setup the environment for all tests in that test class.

        It creates a temporary feedybus profile with a random name.
        """
        # set unittest profile
        basics.set_profile('UNITTEST.{}'.format(helper.rand_string()))
        basics.check_directory_existance()

        # mock the data
        with unittest.mock.patch('feedybus.basics._DefaultFeeds',
                                 return_value=helper.DATA_default):
            # load the data
            data.FeedsData()

    def tearDown(self):
        """Clear the environment (include deletion of profile files) after all
        tests are finished."""
        helper.clear_profile()
        data.FeedsData.me = None

    def test_delete(self):
        """Test for the classmethode Feed.delete()"""

        feedobj = Feed(6)
        parent_groupobj = feedobj.group

        # create dummy icon file
        dummy_ico = basics.create_feed_favicon_filename(feedobj.title,
                                                        favicon.ICON_FORMAT)
        pathlib.Path(dummy_ico).touch(exist_ok=False)
        data.FeedsData.me._feeds[feedobj.fid]['favicon'] = {
            'filename': dummy_ico
        }

        iconfile = data.FeedsData.me.get_favicon_filename(feedobj.fid)
        entriesfile = data.FeedsData.me.get_entries_filename(feedobj.fid)

        # pre check
        self.assertEqual(feedobj.group, Group(-1103))
        self.assertIn(6, data.FeedsData.me._feeds)
        self.assertIn(6, data.FeedsData.me._entries)
        self.assertTrue(os.path.isfile(iconfile))
        self.assertTrue(os.path.isfile(entriesfile))

        # event
        event_tester = helper.TestEventHelper(Feed.event_feed_deleted)

        # delete the feed!
        Feed.delete(feedobj)

        # test event
        self.assertEqual(event_tester.notified, 1)
        self.assertEqual(event_tester.last_args[0], [feedobj.fid])

        # feed deleted
        self.assertNotIn(feedobj.fid, data.FeedsData.me._feeds)

        # entries deleted
        self.assertNotIn(feedobj.fid, data.FeedsData.me._entries)

        # from parent group deleted
        self.assertNotIn(feedobj, parent_groupobj.feeds)
        self.assertEqual(parent_groupobj.feed_ids, [5, 7])

        # files: favicon and entries
        self.assertFalse(os.path.isfile(iconfile))
        self.assertFalse(os.path.isfile(entriesfile))

        # smart group read later
        self.assertNotIn(feedobj.fid, Group.smart_group_index_later)

        # smart grup unread
        self.assertNotIn(feedobj.fid, Group.smart_group_index_unread)

    def test_as_ids(self):
        """Convert list of feeds in list of their ids."""
        feed_ids = [12, 6, 15, 9, 2, 11]
        feed_objs = []
        for fid in feed_ids:
            feed_objs.append(Feed(fid))

        self.assertEqual(feed_ids, Feed.as_ids(feed_objs))


class TestFeed(unittest.TestCase):
    """Class with test cases for Feed class."""
    @classmethod
    def setUpClass(cls):
        """Setup the environment for all tests in that test class.

        It creates a temporary feedybus profile with a random name.
        """
        # set unittest profile
        basics.set_profile('UNITTEST.{}'.format(helper.rand_string()))
        basics.check_directory_existance()

        # mock the data
        with unittest.mock.patch('feedybus.basics._DefaultFeeds',
                                 return_value=helper.DATA_default):
            # load the data
            data.FeedsData()

    @classmethod
    def tearDownClass(cls):
        """Clear the environment (include deletion of profile files) after all
        tests are finished."""
        helper.clear_profile()
        data.FeedsData.me = None

    def test_pep8_conformance(self):
        self.assertTrue(helper.pep8_conform(__file__), 'Test-file')
        self.assertTrue(helper.pep8_conform(Feed), 'File to test')

    def test_equal_operator(self):
        """Test the equal operator."""
        # equal
        self.assertTrue(Feed(19) == Group.root().feeds[0])
        self.assertTrue(Feed(18) == Group.root().feeds[1])
        self.assertFalse(Feed(10) == Group.root().feeds[0])
        self.assertFalse(Feed(19) == Feed(10))

        # not equal
        self.assertTrue(Feed(10) != Group.root().feeds[0])
        self.assertTrue(Feed(19) != Feed(10))

    def test_count(self):
        """Test counting all feeds."""
        n = len(data.FeedsData.me._feeds)
        self.assertTrue(n == Feed.count())
        self.assertTrue(19 == Feed.count())
        self.assertFalse(0 == Feed.count())
        self.assertFalse(-1 == Feed.count())
        self.assertFalse(100 == Feed.count())

    def test_instantiate_feed_with_fid(self):
        """Instantiation of a feed object using a feedid as parameter."""
        feed = Feed(4)
        self.assertTrue(feed.fid == 4)
        self.assertTrue(feed.title == 'Posteo.de - Aktuelles')

    def test_instantiate_feed_with_fid_invalid(self):
        """Instantiate with an unexisting and out-of-range feedid."""
        # unexisting feed
        with self.assertRaises(ValueError):
            Feed(30)

        # id not in the valid range for feed-ids
        with self.assertRaises(ValueError):
            Feed(-1)

        # feed-ids starting with 1
        with self.assertRaises(ValueError):
            Feed(0)

    def test_instantiate_feed_with_fid_allowed_as_single_only(self):
        """Exceptions when instantiate a Feed object using a feed id parameter
        but some other more. No other parameters are allowed if a feed id is
        givin."""
        # any karg
        with self.assertRaises(TypeError):
            Feed(4, param='Vier')

        # with valid kargs
        with self.assertRaises(TypeError):
            Feed(4, title='Title', xmlUrl='https://somewhere.universe')

    def test_instantiate_feed_with_wrong_type_of_parameter(self):
        """Exceptions if the used parameters are of wrong type."""
        # feed id
        for fid in ['4', 4.4]:
            with self.subTest(fid=fid):
                with self.assertRaises(TypeError):
                    Feed(fid)
                with self.assertRaises(TypeError):
                    Feed(param=fid)
        with self.assertRaises(TypeError):
            Feed()

    @mock.patch('feedybus.data.FeedsData.me.get_feed_ids')
    def test_next_free_id(self, feed_ids_function):
        """Test the mechanism to get a fresh and unused feed id."""
        feed_ids_function.return_value = []
        fid = Feed.next_free_id()
        self.assertTrue(fid == 1)

        feed_ids_function.return_value = [2, 3, 4, 5, 6, 8, 9, 10, 12]
        fid = []
        for i in range(4):
            free_id = Feed.next_free_id()
            fid.append(free_id)
            feed_ids_function.return_value.append(free_id)
        self.assertTrue(fid == [1, 7, 11, 13])

    def test_feed_id_validation(self):
        """Test if ids are correct validated as feed ids."""
        self.assertTrue(Feed.is_fid_valid(1))
        self.assertTrue(Feed.is_fid_valid(100))
        self.assertFalse(Feed.is_fid_valid(0))
        self.assertFalse(Feed.is_fid_valid(-1))

    def test_create_feed_with_minimal_parameters(self):
        """Creation of a feed object with minimum mandatory parameters."""
        # title, xmlUrl
        title = 'Title'
        xmlUrl = 'https://foo.bar/feed.xml'

        event_tester = helper.TestEventHelper(Feed.event_feed_new_or_modified)

        # with default parent
        feed = Feed(title=title, xmlUrl=xmlUrl)
        self.assertEqual(feed.title, title)
        self.assertEqual(feed.xmlUrl, xmlUrl)
        self.assertEqual(feed.group, Group.root())
        self.assertEqual(event_tester.notified, 1)
        self.assertEqual(event_tester.last_args[0], feed.fid)

        # check default values of unused paramters
        self.assertEqual(feed.label, feed.title)
        self.assertTrue(not feed.version)
        self.assertTrue(not feed.htmlUrl)
        self.assertTrue(not feed.author)
        self.assertTrue(not feed.rights)
        self.assertTrue(not feed.description)
        self.assertTrue(not feed.encoding)

        # with specific parent
        feed = Feed(title=title, xmlUrl=xmlUrl, group=Group(-1103))
        self.assertEqual(feed.group, Group(-1103))

    def test_create_feed_missing_parameters(self):
        """Exception when create a feed with missing mandatory parameters."""
        # title, xmlUrl
        title = 'Title'
        xmlUrl = 'https://foo.bar/feed.xml'
        group = Group(-1103)

        # missing title
        with self.assertRaises(TypeError):
            Feed(title=title)
        with self.assertRaises(TypeError):
            Feed(title=title, group=group)

        # missing xmlUrl
        with self.assertRaises(TypeError):
            Feed(xmlUrl=xmlUrl)
        with self.assertRaises(TypeError):
            Feed(xmlUrl=xmlUrl, group=group)

    def test_create_feed_missing_kargs(self):
        """Exception if 'valid' parameters used without names (possitional
        parameters)."""
        # title, xmlUrl
        title = 'Title'
        xmlUrl = 'https://foo.bar/feed.xml'
        group = Group(-1103)

        with self.assertRaises(TypeError):
            Feed(title, xmlUrl, group)
        with self.assertRaises(TypeError):
            Feed(title, xmlUrl, group=group)
        with self.assertRaises(TypeError):
            Feed(title, xmlUrl=xmlUrl, group=group)

    def test_create_feed_with_full_parameters(self):
        """Test the creation of a feed object with all allowed parameters."""
        title = 'Title'
        label = 'Text'
        xmlUrl = 'https://foo.bar/feed.xml'
        htmlUrl = 'https://foo.bar'
        version = 'atom10'
        author = 'ed <ed@freedom.world>'
        rights = 'CC'
        description = 'Something very interesting'
        encoding = 'UTF-8'
        group = Group(-1103)

        event_tester = helper.TestEventHelper(Feed.event_feed_new_or_modified)

        # full + parent
        feed = Feed(title=title, label=label, xmlUrl=xmlUrl, htmlUrl=htmlUrl,
                    version=version, author=author, rights=rights,
                    description=description, encoding=encoding, group=group)
        feed = Feed(feed.fid)
        self.assertEqual(feed.title, title)
        self.assertEqual(feed.label, label)
        self.assertEqual(feed.xmlUrl, xmlUrl)
        self.assertEqual(feed.htmlUrl, htmlUrl)
        self.assertEqual(feed.version, version)
        self.assertEqual(feed.author, author)
        self.assertEqual(feed.rights, rights)
        self.assertEqual(feed.description, description)
        self.assertEqual(feed.encoding, encoding)
        self.assertEqual(feed.group, group)
        self.assertEqual(event_tester.notified, 1)
        self.assertEqual(event_tester.last_args[0], feed.fid)

        # full without parent group
        feed = Feed(title=title, label=label, xmlUrl=xmlUrl, htmlUrl=htmlUrl,
                    version=version, author=author, rights=rights,
                    description=description, encoding=encoding)
        feed = Feed(feed.fid)
        self.assertEqual(feed.title, title)
        self.assertEqual(feed.label, label)
        self.assertEqual(feed.xmlUrl, xmlUrl)
        self.assertEqual(feed.htmlUrl, htmlUrl)
        self.assertEqual(feed.version, version)
        self.assertEqual(feed.author, author)
        self.assertEqual(feed.rights, rights)
        self.assertEqual(feed.description, description)
        self.assertEqual(feed.encoding, encoding)
        self.assertEqual(feed.group, Group.root())
        self.assertEqual(event_tester.notified, 2)
        self.assertEqual(event_tester.last_args[0], feed.fid)

        # less
        feed = Feed(title=title, xmlUrl=xmlUrl, htmlUrl=htmlUrl,
                    version=version, description=description,
                    encoding=encoding)
        feed = Feed(feed.fid)
        self.assertTrue(not feed.author)
        self.assertTrue(not feed.rights)
        self.assertEqual(feed.title, title)
        self.assertEqual(feed.label, title)
        self.assertEqual(feed.xmlUrl, xmlUrl)
        self.assertEqual(feed.htmlUrl, htmlUrl)
        self.assertEqual(feed.version, version)
        self.assertEqual(feed.description, description)
        self.assertEqual(feed.encoding, encoding)
        self.assertEqual(feed.group, Group.root())
        self.assertEqual(event_tester.notified, 3)
        self.assertEqual(event_tester.last_args[0], feed.fid)

        # less
        feed = Feed(title=title, xmlUrl=xmlUrl,
                    description=description)
        feed = Feed(feed.fid)
        self.assertTrue(not feed.htmlUrl)
        self.assertTrue(not feed.author)
        self.assertTrue(not feed.rights)
        self.assertTrue(not feed.encoding)
        self.assertTrue(not feed.version)
        self.assertEqual(feed.title, title)
        self.assertEqual(feed.label, title)
        self.assertEqual(feed.xmlUrl, xmlUrl)
        self.assertEqual(feed.description, description)
        self.assertEqual(feed.group, Group.root())
        self.assertEqual(event_tester.notified, 4)
        self.assertEqual(event_tester.last_args[0], feed.fid)

    def test_update_mecha_properties(self):
        """Test the properties for the update mechanisms.
        """
        feed = Feed(4)
        etag = '123456'
        modified_date = time.localtime()
        entry_date = time.localtime()

        # no mechanism
        self.assertTrue(not feed.update_mechanism)

        # set etag
        feed.set_update_mechanism(etag=etag)
        self.assertTrue(feed.update_mechanism == data.UpdateMecha.etag)
        self.assertTrue(feed.update_mechanism_value == etag)
        self.assertTrue(feed.update_mechanism != data.UpdateMecha.modified)
        self.assertTrue(feed.update_mechanism != data.UpdateMecha.entry_date)
        self.assertTrue(feed.update_mechanism != data.UpdateMecha.entry_links)
        self.assertFalse(feed.update_mechanism is None)

        # set modified_date
        feed.set_update_mechanism(modified_date=modified_date)
        self.assertTrue(feed.update_mechanism == data.UpdateMecha.modified)
        self.assertTrue(feed.update_mechanism_value == modified_date)
        self.assertTrue(feed.update_mechanism != data.UpdateMecha.etag)
        self.assertTrue(feed.update_mechanism != data.UpdateMecha.entry_date)
        self.assertTrue(feed.update_mechanism != data.UpdateMecha.entry_links)
        self.assertFalse(feed.update_mechanism is None)

        # set entry dates
        feed.set_update_mechanism(entry_date=entry_date)
        self.assertTrue(feed.update_mechanism == data.UpdateMecha.entry_date)
        self.assertTrue(feed.update_mechanism_value == entry_date)
        self.assertTrue(feed.update_mechanism != data.UpdateMecha.etag)
        self.assertTrue(feed.update_mechanism != data.UpdateMecha.modified)
        self.assertTrue(feed.update_mechanism != data.UpdateMecha.entry_links)
        self.assertFalse(feed.update_mechanism is None)

        # set entry links (without entries)
        feed.set_update_mechanism(entry_links=True)
        self.assertTrue(feed.update_mechanism == data.UpdateMecha.entry_links)
        self.assertTrue(feed.update_mechanism_value == [])
        self.assertTrue(feed.update_mechanism != data.UpdateMecha.etag)
        self.assertTrue(feed.update_mechanism != data.UpdateMecha.modified)
        self.assertTrue(feed.update_mechanism != data.UpdateMecha.entry_dates)
        self.assertFalse(feed.update_mechanism is None)

        # set entry links (with entries)
        feed = Feed(1)
        feed.set_update_mechanism(entry_links=True)
        self.assertTrue(len(feed.update_mechanism_value) == 10)

    def test_rename(self):
        """Rename a feed. Keep in mind that the 'text' attribute can be
        renamed because it is treated like a lable. A feed is shon by this
        string in a GUI. The 'title' attribute is fixed and can not modified
        because it is the feeds authors creation and intention. 'text' is
        'title' in most cases."""
        title = 'Title'
        fid = 14
        feed = Feed(fid)

        # renaming causes events
        event_test = helper.TestEventHelper(Feed.event_feed_new_or_modified)

        # 'title' can not be modified
        with self.assertRaises(AttributeError):
            feed.title = title

        # 'text'
        new_label = 'New text'
        feed.label = new_label
        feed = Feed(feed.fid)
        self.assertEqual(feed.label, new_label)

        # event fired?
        self.assertEqual(event_test.notified, 1)
        self.assertEqual(event_test.last_args[0], fid)

    def test_missing_setters(self):
        """Test the non existence of setters.

        It is more a documenation of all existing attributes of class 'Feed'
        with missing setters.
        """
        feed = Feed(14)

        with self.assertRaises(AttributeError):
            feed.htmlUrl = 'X'

        with self.assertRaises(AttributeError):
            feed.title = 'X'

        with self.assertRaises(AttributeError):
            feed.htmlUrl = 'X'

        with self.assertRaises(AttributeError):
            feed.version = 'X'

        with self.assertRaises(AttributeError):
            feed.author = 'X'

        with self.assertRaises(AttributeError):
            feed.rights = 'X'

        with self.assertRaises(AttributeError):
            feed.description = 'X'

        with self.assertRaises(AttributeError):
            feed.encoding = 'X'

    def test_modify_xmlUrl(self):
        """Modify the xmlUrl fields."""
        new_xml = 'http://new.url/feed.xml'

        feed = Feed(14)
        feed.xmlUrl = new_xml
        feed = Feed(14)
        self.assertTrue(feed.xmlUrl == new_xml)

    def test_convert_FeedParserDict(self):
        """Test conversion of a feedparser.FeedParserDict based dictionary to
        a dict that can handled by feedybus.Feed."""

        fpdict = {
            'feed': {
                'links': [
                    {
                        'href': 'https://www.n-tv.de/rss',
                        'rel': 'self',
                        'type': 'application/rss+xml'
                    },
                    {
                        'href': 'https://www.n-tv.de',
                        'rel': 'alternate',
                        'type': 'text/html'
                    }
                ],
                'title': 'n-tv.de',
                'link': 'https://www.n-tv.de',
                'subtitle': 'Nachrichten seriös',
                'description': 'Nachrichten seriös',
                'rights': 'Fully Open',
            },
            'entries': None,
            'bozo': 0,
            'headers': {},
            'updated': None,
            'updated_parsed': None,
            'href': 'https://www.n-tv.de/rss',
            'status': 200,
            'encoding': 'UTF-8',
            'version': 'rss20',
            'namespaces': None,
        }

        result = {
            'title': 'n-tv.de',
            'label': 'n-tv.de',
            'htmlUrl': 'https://www.n-tv.de',
            'xmlUrl': 'https://www.n-tv.de/rss',
            'version': 'rss20',
            'rights': 'Fully Open',
            'author': None,
            'description': 'Nachrichten seriös',
            'encoding': 'UTF-8',
        }

        self.assertEqual(Feed.convert_FeedParserDict(fpdict), result)

    def test_create_entries(self):
        """Creating entries for the feed.

        It won't fire any events."""

        # dummy entries
        entries = []
        for i in range(5):
            e = {
                'title': 'title {}'.format(i),
                'link': 'https://title_{}.free'.format(i),
                'published': '2020-01-01T{:02}:00:00+00:00'.format(i),
                'published_parsed': (2020, 1, 1, i, 0, 0, 3, 1, 0)
            }
            entries.append(e)

        # empty feed
        feed = Feed(11)
        event_test = helper.TestEventHelper(feed.event_feed_new_or_modified)
        # test: feed has no entries
        self.assertFalse(feed.entries)
        # test: event was not fired
        self.assertTrue(event_test.notified == 0)
        # test: no unread entries for that feed
        self.assertNotIn(feed.fid, Group.smart_group_index_unread)

        # add entries
        feed.add_new_entries(entries)
        self.assertTrue(len(feed.entries) == 5)
        # creating entries won't fire an event
        self.assertTrue(event_test.notified == 0)
        # new entries are unread by default and should be remembered
        self.assertEqual(Group.smart_group_index_unread[feed.fid],
                         [0, 1, 2, 3, 4])


class TestFeedEntries(unittest.TestCase):
    """Class testing the entry functionality of the Feed class."""
    def setUp(self):
        """
        """
        # set unittest profile
        basics.set_profile('UNITTEST.{}'.format(helper.rand_string()))
        basics.check_directory_existance()

        # mock feed data
        with unittest.mock.patch('feedybus.basics._DefaultFeeds',
                                 return_value=helper.DATA_test_feed_entries):
            data.FeedsData()

    def tearDown(self):
        """
        """
        helper.clear_profile()
        data.FeedsData.me = None
        Group.smart_group_index_unread = dict()
        Group.smart_group_index_later = dict()

    def test_count(self):
        """
        """
        self.assertEqual(len(Feed(1).entries), 4)
        self.assertEqual(len(Feed(2).entries), 3)
        self.assertEqual(len(Feed(3).entries), 0)

    def test_get_read_status(self):
        """Get read status of one entry."""
        feed = Feed(1)
        self.assertFalse(feed.get_entry_read(0))
        self.assertFalse(feed.get_entry_read(1))
        self.assertFalse(feed.get_entry_read(2))
        self.assertFalse(feed.get_entry_read(3))

        feed = Feed(2)
        self.assertTrue(feed.get_entry_read(0))
        self.assertFalse(feed.get_entry_read(1))
        self.assertFalse(feed.get_entry_read(2))

        # unexisting entry
        feed = Feed(3)
        with self.assertRaises(ValueError):
            feed.get_entry_read(0)

        # entry without explicite "read" field (unread by default)
        feed = Feed(4)
        self.assertFalse(feed.get_entry_read(1))

    def test_unread_indexes(self):
        """Test concrete index of undread entries.
        """
        # get all (read & unread) entries
        feed = Feed(1)
        self.assertEqual(feed.get_unread_entries(), [0, 1, 2, 3])
        feed = Feed(2)
        self.assertEqual(feed.get_unread_entries(), [1, 2])
        feed = Feed(3)
        self.assertEqual(feed.get_unread_entries(), [])

    def test_feed_read(self):
        """Set a whole feed to read."""
        ev_feed_read = helper.TestEventHelper(Feed.event_feed_marked_read)
        ev_entry_read = helper.TestEventHelper(Feed.event_entry_read_modified)

        # get all (read & unread) entries
        feed = Feed(1)
        self.assertEqual(feed.get_unread_entries(), [0, 1, 2, 3])
        feed.set_read()
        self.assertEqual(feed.get_unread_entries(), [])
        self.assertNotIn(feed.fid, Group.smart_group_index_unread)
        self.assertEqual(ev_feed_read.notified, 1)
        self.assertEqual(ev_feed_read.last_args[0], feed.fid)

        feed = Feed(2)
        self.assertEqual(feed.get_unread_entries(), [1, 2])
        feed.set_read()
        self.assertEqual(feed.get_unread_entries(), [])
        self.assertNotIn(feed.fid, Group.smart_group_index_unread)
        self.assertEqual(ev_feed_read.notified, 2)
        self.assertEqual(ev_feed_read.last_args[0], feed.fid)

        feed = Feed(3)
        self.assertEqual(feed.get_unread_entries(), [])
        feed.set_read()
        self.assertEqual(feed.get_unread_entries(), [])
        self.assertNotIn(feed.fid, Group.smart_group_index_unread)
        self.assertEqual(ev_feed_read.notified, 3)
        self.assertEqual(ev_feed_read.last_args[0], feed.fid)

        self.assertEqual(ev_entry_read.notified, 0)

    def test_unread_remembered(self):
        """Feeds with unread entries are shown in a 'smart' group called
        'Unread'. The indexes for this group are created while
        'Feed.get_unread_entries()' is called.
        """
        self.assertEqual(Group.smart_group_index_unread, {})

        # feed without unread entries
        feed = Feed(3)
        self.assertEqual(feed.get_unread_entries(), [])
        self.assertEqual(Group.smart_group_index_unread, {})

        # feed with unread entries
        feed = Feed(2)
        feed.get_unread_entries()
        self.assertEqual(Group.smart_group_index_unread, {2: [1, 2]})

        # a second feed with unread entries
        feed = Feed(1)
        feed.get_unread_entries()
        self.assertEqual(Group.smart_group_index_unread, {2: [1, 2],
                                                          1: [0, 1, 2, 3]})

    def test_mark_un_read(self):
        """
        """
        feed = Feed(2)
        self.assertEqual(feed.get_unread_entries(), [1, 2])

        ev_entry_read = helper.TestEventHelper(Feed.event_entry_read_modified)

        # mark unread with explicite 'False'
        feed.set_entry_read(0, False)
        self.assertEqual(feed.get_unread_entries(), [0, 1, 2])
        self.assertEqual(ev_entry_read.notified, 1)
        self.assertEqual(ev_entry_read.last_args, (feed.fid, 0))

        # mark read with implicte/default 'True'
        feed.set_entry_read(0)
        self.assertEqual(feed.get_unread_entries(), [1, 2])
        self.assertEqual(ev_entry_read.notified, 2)
        self.assertEqual(ev_entry_read.last_args, (feed.fid, 0))

        # mark read with explicite 'True'
        feed.set_entry_read(1, True)
        self.assertEqual(feed.get_unread_entries(), [2])
        self.assertEqual(ev_entry_read.notified, 3)
        self.assertEqual(ev_entry_read.last_args, (feed.fid, 1))

        # key error
        with self.assertRaises(ValueError):
            feed.set_entry_read(3)

        # TODO
        # Check smart_group_index_unread

    def test_count_total_and_unread(self):
        """Correct count of total and unread entries for Feed, Group and
        SmartGroups.
        """
        feed = Feed(2)
        self.assertEqual(feed.count_total_and_unread_entries(), (3, 2))

        # mark unread with explicite 'False'
        feed.set_entry_read(0, False)
        self.assertEqual(feed.count_total_and_unread_entries(), (3, 3))

        # mark read with implicte/default 'True'
        feed.set_entry_read(0)
        self.assertEqual(feed.count_total_and_unread_entries(), (3, 2))

        # mark read with explicite 'True'
        feed.set_entry_read(1, True)
        self.assertEqual(feed.count_total_and_unread_entries(), (3, 1))

        # empty feed
        feed = Feed(3)
        self.assertEqual(feed.count_total_and_unread_entries(), (0, 0))

        # another feed
        feed = Feed(1)
        self.assertEqual(feed.count_total_and_unread_entries(), (4, 4))

        # NOW lets count in groups
        group = Group(-1002)
        self.assertEqual(group.count_total_and_unread_entries(), (3, 1))

        group = Group(-1001)
        self.assertEqual(group.count_total_and_unread_entries(), (5, 3))

        group = Group(-1000)
        self.assertEqual(group.count_total_and_unread_entries(), (11, 5))

        group = Group.root()
        self.assertEqual(group.count_total_and_unread_entries(), (28, 20))

    def test_group_set_read(self):
        """Set entries read by their group."""

        root = Group.root()
        self.assertEqual(root.count_total_and_unread_entries(), (28, 21))

        # group with feeds and groups as childs
        group = Group(-1001)
        self.assertEqual(group.count_total_and_unread_entries(), (5, 3))
        group.set_read()
        self.assertEqual(group.count_total_and_unread_entries(), (5, 0))

        # root
        root.set_read()
        self.assertEqual(root.count_total_and_unread_entries(), (28, 0))

    def test_read_later_full_creation(self):
        """Feeds with entries marked as 'read later' are shown in a 'smart'
        group called 'Read Later'. The indexes for this group are created all
        together when 'Group.count_read_later_entries' is called.
        """
        self.assertFalse(Group.smart_group_index_later)

        self.assertEqual(Group.count_read_later_entries(), 3)

    def test_read_later_remembered(self):
        """Feeds with entries marked as 'read later' are shown in a 'smart'
        group called 'Read Later'. The indexes for this group are created
        while 'Feed.get_read_later_entries()' is called.
        """
        self.assertEqual(Group.smart_group_index_later, {})

        # Feed without marked entries
        feed = Feed(6)
        self.assertEqual(feed.get_read_later_entries(), [])
        self.assertEqual(Group.smart_group_index_later, {})

        # Feed with marked entries
        feed = Feed(7)
        self.assertEqual(feed.get_read_later_entries(), [1])
        self.assertEqual(Group.smart_group_index_later, {7: [1]})

        # a second feed
        feed = Feed(8)
        self.assertEqual(feed.get_read_later_entries(), [1])
        self.assertEqual(Group.smart_group_index_later, {7: [1],
                                                         8: [1]})

    def test_mark_read_later(self):
        """Mark a entry read later.
        """
        feed = Feed(8)
        self.assertEqual(feed.get_read_later_entries(), [1])

        event_tester = helper \
            .TestEventHelper(Feed.event_entry_read_later_modified)

        feed.set_entry_read_later(0)
        self.assertEqual(event_tester.notified, 1)
        self.assertEqual(event_tester.last_args, (feed.fid, 0))
        self.assertEqual(Group.smart_group_index_later[feed.fid], [1, 0])
        self.assertEqual(feed.get_read_later_entries(), [0, 1])

        feed.set_entry_read_later(2)
        self.assertEqual(event_tester.notified, 2)
        self.assertEqual(event_tester.last_args, (feed.fid, 2))
        self.assertEqual(Group.smart_group_index_later[feed.fid], [0, 1, 2])
        self.assertEqual(feed.get_read_later_entries(), [0, 1, 2])

        with self.assertRaises(IndexError):
            feed.set_entry_read_later(3)

        feed.set_entry_read_later(0, False)
        self.assertEqual(event_tester.notified, 3)
        self.assertEqual(event_tester.last_args, (feed.fid, 0))
        self.assertEqual(feed.get_read_later_entries(), [1, 2])

        feed.set_entry_read_later(0, True)
        self.assertEqual(event_tester.notified, 4)
        self.assertEqual(event_tester.last_args, (feed.fid, 0))
        self.assertEqual(feed.get_read_later_entries(), [0, 1, 2])

        # TODO
        # Check smart_group_index_later

    def test_mark_read_later_index(self):
        """Correct index of read later indexes.
        """
        feed = Feed(2)
        self.assertEqual(feed.get_read_later_entries(), [0])

        feed = Feed(8)
        self.assertEqual(feed.get_read_later_entries(), [1])

        feed = Feed(7)
        self.assertEqual(feed.get_read_later_entries(), [1])

        feed = Feed(6)
        self.assertEqual(feed.get_read_later_entries(), [])

    def test_date_str(self):
        """Test correct date_str extraction.
        """
        feed = Feed(4)
        exp_date = '1982-08-06'

        # 10 entries
        self.assertEqual(10, len(feed.entries))

        # return 'date_str'
        self.assertEqual(feed.get_entry_date_str(0), exp_date)

        # return date_str from 'published_parsed' only
        self.assertEqual(feed.get_entry_date_str(1), exp_date)

        # return date_str from 'updated_parsed' only
        self.assertEqual(feed.get_entry_date_str(2), exp_date)

        # return date_str from 'published' only
        self.assertEqual(feed.get_entry_date_str(3),
                         '1982-08-06T18:23:00+02:00')

        # return <n.a> because of empty 'published'
        self.assertEqual(feed.get_entry_date_str(4), '<n.a.>')

        # return '<n.a.>' if all three fields not present
        self.assertEqual(feed.get_entry_date_str(5), '<n.a.>')

        # return date_str from 'published_parsed' but 'updated_parsed' is
        # present
        self.assertEqual(feed.get_entry_date_str(6), exp_date)

        # return date_str from 'published_parsed' but 'updated_parsed' and
        # 'published' is present
        self.assertEqual(feed.get_entry_date_str(7), exp_date)

        # return date_str from 'published_parsed' but 'published' is present
        self.assertEqual(feed.get_entry_date_str(8), exp_date)

        # return date_str from 'updated_parsed' but 'published' is present
        self.assertEqual(feed.get_entry_date_str(9), exp_date)

        # unexisting entry
        with self.assertRaises(IndexError):
            feed.get_entry_date_str(10)

    def test_title(self):
        """Entry titles."""
        self.assertEqual('heise+ | Wie dunkle Farben',
                         Feed(1).get_entry_title(0))

        self.assertEqual('Elektroautos: Merkel will bis 2022 die '
                         'Millionengrenze knacken',
                         Feed(1).get_entry_title(2))

        self.assertEqual('Freie Software für die Entwicklungszusammenarbeit',
                         Feed(2).get_entry_title(1))

        self.assertEqual('FooBar',
                         Feed(4).get_entry_title(5))

        with self.assertRaises(IndexError):
            Feed(3).get_entry_title(0)


if __name__ == '__main__':
    unittest.main()
