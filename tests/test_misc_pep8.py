import os.path
import unittest

from . import helper
import feedybus


"""
    Test PEP8 conformance for py-files that do not have their own test cases.
"""


class TestMiscPep8(unittest.TestCase):
    """Test PEP8 conformance for py-files that do not have their own test
    cases."""
    def test_pep8_conformance(self):
        # test test-file
        self.assertTrue(helper.pep8_conform(__file__), 'Test-file')

        # unittest helper files
        filenames = ['helper.py']

        # test tests-helper file
        for f in filenames:
            p = os.path.join('tests', f)
            self.assertTrue(helper.pep8_conform(p), f)

        # files without own test cases
        filenames = ['config.py',
                     'entrieslistview.py',
                     'favicon.py',
                     'feeddialog.py',
                     'fetchfeeds.py',
                     'gtkhelper.py',
                     '__init__.py',
                     '__main__.py',
                     'renamedialog.py',
                     'statusbar.py',
                     'treeview.py',
                     'window.py']

        # test all other files without their own test case
        for f in filenames:
            p = os.path.join('feedybus', f)
            self.assertTrue(helper.pep8_conform(p), f)


if __name__ == '__main__':
    unittest.main()
