#!/bin/bash

# Use 'green' if available
if hash green 2>/dev/null; then
    echo "Using green..."
    # --processes Only 1 to preserve conflicts.
    green --processes 1 --failfast -vv $1
else
    echo "Using python3 standard way..."
    # standard way
    python3 -m unittest discover -v
fi
